\documentclass[a4paper,12pt]{article}
\usepackage[francais]{babel}		% typographie franaise (french marche aussi)
\usepackage[utf8]{inputenc}		% lettres accentuées
\usepackage[T1]{fontenc}
%\usepackage[mac]{inputenc}		% lettres accentuées
\usepackage{graphicx, amsmath, amssymb, color}
\usepackage{listings}


\setlength{\textheight}{24cm}
\setlength{\topmargin}{-10mm}
\setlength{\textwidth}{17cm}
\setlength{\oddsidemargin}{-5mm} % marge gauche (im)paire = 1in. + x1mm
%\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\parskip}{1.2ex plus 0.9ex minus 0.4ex}

\newcommand{\dd} {\textrm d}   
\newcommand{\im} {\mathrm i}   
\newcommand{\e}  {\textrm e}   
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {\textrm{d}^{2} #1} {\textrm{d} #2 ^{2}}}
\newcommand{\be}[1] {\begin{equation} \label{#1}}   
\newcommand{\ee} {\end{equation}}   

%-----------------------------------------------------------------------
\begin{document}

\noindent
M1 de Mécanique-\'Energétique \hfill 22 juin 2023

\begin{center}
{\bf Examen de rattrappage d'Ondes dans les fluides} \\
{\it \small Durée 2 heures. 
%Barême indicatif : I : 6 points, II : 7 points, III : 7 points. \\ %Présentation : 2 points.
%Document autorisé : une feuille manuscrite recto-verso.
} \\ 
\medskip
{\it \small On veillera à R\'EDIGER la copie en expliquant les calculs.}
\end{center}

%\medskip
%=======================================================================
%\section{Questions de cours}


%\medskip
%\clearpage
%=======================================================================
{ \bf 1. Ondes sur une corde présentant une discontinuité}


\noindent

On considère une corde tendue, de tension $T$ constante, et dont la masse linéique présente une discontinuité :
$\mu = \mu_1$ pour $x<0$ et $\mu = \mu_2$ pour $x>0$.

On note $Y(x,t)$ le déplacement latéral de la corde.

\begin{enumerate}

\item
Rappelez la forme de l'équation des ondes gouvernant l'évolution de $Y(x,t)$ et précisez l'expression de la vitesse des ondes $c$ en fonction de de $T$ et $\mu$.

\item On suppose que du coté $x<0$ provient une onde monochromatique d'amplitude-déplacement $A_Y$, de nombre d'onde $k_1$ et de fréquence $\omega$. Celle-ci produit une onde réfléchie d'amplitude-déplacement $B_Y$ et une onde transmise d'amplitude $C_Y$. C'est-à dire :

\begin{equation}
Y(x,t) = \left\{
\begin{array}{l l}
A_Y e^{ i (k_1 x - \omega t)  } + B_Y e^{-i (k_1 x + \omega t) }  & \quad x<0 ;\\
C_Y e^{i(k_2 x - \omega t) }& \quad x>0.
\end{array}\right.
\label{eq:ondesABC}
\end{equation}

A partir de l'équation des ondes, donnez la relation entre $\omega$ et $k_1$ puis entre $\omega$ et $k_2$.

\item Si la portion de corde $x>0$ est plus légère que la portion de corde $x<0$ (c.a.d. $\mu_2 < \mu_1$), la longueur d'onde de l'onde transmise est elle plus longue ou plus courte que celle de l'onde incidente ? expliquez physiquement.


\item Ecrire deux équations traduisant la continuité du déplacement et de la force transverse transmise par la corde en $x=0$.

\item En déduire le coefficient de transmission $C_Y/A_Y$ et le coefficient de réflexion $B_Y/A_Y$ en function de $\mu_1$ et $\mu_2$.

\item Que valent $C_Y/A_Y$ et $B_Y/A_Y$ dans la limite $\mu_2 \gg \mu_1$ ? A quoi correspond physiquement cette limite ?

\end{enumerate}


{\bf 2. Résonateur de Helmholtz }

\begin{figure}[!htb]
\begin{center}
\includegraphics[keepaspectratio=true,width=12cm]{../TDs_Ondes/Figures/potdechappementerRH.png}
\end{center}
\caption{(a) Structure interne d'un pot d'échappement de voiture ; (b) schéma d'un résonateur de Helmholtz}
\label{figpot}
\end{figure}

\noindent Considérons le résonateur de Helmoltz de la figure $\ref{figpot}(b)$, constitué d'une cavité de volume $V$ qui communique avec l'extérieur à travers un col de longueur $\ell$ et de section $S$. 

A l'intérieur, la pression est $p_0 + p'_2$ et la masse volumique $\rho_0+\rho'$. A l'extérieur, la pression est $p_0 + p'_1$.  Le flux volumique entrant dans le résonateur est noté $q'$, relié à la vitesse longitudinale en entrée $u'$ par la relation $q = S u $.


\begin{enumerate}

\item Ecrire un bilan de masse pour la cavité.  Montrez que dans l'hypothèse d'une évolution adiabatique ce bilan conduit à :
$$\frac{V}{c^2} \frac{d p_2'}{dt}=\rho_0 q'$$


\item Montrer que dans le col, l'évolution de la vitesse longitudinale $u'$ est donnée par :
$$\frac{d u'}{dt}=\frac{p'_1-p'_2}{\rho_0 \ell}$$

\item En déduire que la pression $p'_2$ est gouvernée par une équation d'oscillateur harmonique forcé, de la forme 
$$
\frac{d^2 p_2'}{dt^2} + \omega_0^2 p_2' = \omega_0^2 p_1'
$$
Et donnez l'expression de la fréquence propre $\omega_0$ en fonction des dimensions de la cavité et de la vitesse du son $c$.

\item On suppose que la cavité est forcée à la fréquence $\omega$, c.a.d. que la pression extérieure est de la forme $p_1' = \hat{p}_1 e^{-i\omega t}$, et que la pression interne a également un comportement harmonique de la forme  
$p_2' = \hat{p}_2 e^{-i\omega t}$. Donnez l'expression de la fonction de transfert $G(\omega) = \frac{\hat{p}_2}{\hat{p}_1}$.

\item Représentez $|G(\omega)|^2$ en fonction de $\omega$ en coordonnés semi-logarithmiques. Précisez notamment la pente de la courbe dans les limites $\omega\rightarrow 0$ et $\omega \rightarrow \infty$. Quelle est la nature de ce filtre ? 




\end{enumerate}


{\bf 3. Ondes de surface}

\begin{enumerate}


%-----------------------------------------------------------------------
\item On donne la relation de dispersion $\omega^2 = gk + \gamma k^3/\rho$, avec les notations habituelles. \`A quelle situation physique correspond cette relation ? Donnez l'expression de la célérité $c$ des ondes.  


%-----------------------------------------------------------------------
\item Représenter graphiquement la célérité des ondes $c$ en fonction du nombre d'onde $k$, en précisant le(s) point(s) remarquable(s) ainsi que les comportements asymptotiques pour $k \rightarrow 0 $ et $k \rightarrow \infty$.


\item Donnez la définition mathématique générale de la vitesse de groupe $c_g$ , puis son expression dans les limites de grandes longueurs d'ondes ($k \rightarrow 0 $)  et de courtes longueurs d'ondes ($k \rightarrow \infty$).

\item Que vaut le rapport $c_g/c$ dans la limite des grandes longueurs d'ondes ?
Quelle en est la conséquence sur le comportement d'un paquet d'ondes initialement localisé ? Illustrez votre réponse avec quelques schémas.

\item Même question dans la limite des courtes longueurs d'ondes.


\end{enumerate} 

%=======================================================================

\end{document} 