function y = dAlembert_F(x1)
global L c


 if(x1>=0&&x1<=L)
     y = posinit(x1)/2-1/c*intvitinit(x1)/2;
 elseif(x1<=0&&x1>-L) % prolongation par symetrie
     y = -posinit(-x1)/2-1/c*intvitinit(-x1)/2;
 else % prolongation par periodicite
     x1p = x1-2*L*round(x1/(2*L));
     y = dAlembert_F(x1p);
 end

% % cas de la corde frapp?e
% a=.3*L; b=.4*L; W=1;
% 
% if(x1>=0&&x1<=a)
%     y =0;
% elseif(x1>a&&x1<=b)
%     y = -(x1-a)*W/(2*c);
% elseif(x1>b&&x1<=L)
%     y = -(b-a)*W/(2*c);  
% elseif(x1<=0&&x1>-L) % prolongation par symetrie
%     y = dAlembert_F(-x1); 
% else % prolongation par periodicite
%     x1p = x1-2*L*round(x1/(2*L));
%     y = dAlembert_F(x1p);
% end

% cas de la corde pinc?e
%d=.3*L; e=1;

%if(x1>=0&&x1<=d)
%    y =(e*x1/d)/2;
%elseif(x1>d&&x1<=L)
%    y = e*(L-x1)/(L-d)/2;  
%elseif(x1<=0&&x1>-L) % prolongation par symetrie
%    y = dAlembert_F(-x1); 
%else % prolongation par periodicite
%    x1p = x1-2*L*round(x1/(2*L));
%    y = dAlembert_F(x1p);
%end

