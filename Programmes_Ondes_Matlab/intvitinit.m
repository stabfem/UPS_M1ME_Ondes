function y= intvitinit(x) 
%global L c;

for i=1:length(x)
   y(i) = quad(@vitinit,0,x(i));
end

