

% 
c1 = 1; rho1=1;
rho2 = myinput('Density ratio rho_2/rho_1 ? ',.5);
c2 = myinput('Wave velocity ratio c2/c1 ? ',.5);
thetadeg = myinput('Incidence angle theta_I (deg) ? ',45);
lambdaI = myinput('Wavelength of incident wave ? ',1);
autoadvance =myinput(' Generate a movie (1) or advance manually (0) ? ',0);

thetaI = thetadeg*pi/180;
kI = 2*pi/lambdaI;
omega = kI*c1;

A = 1;
thetaR = thetaI;

kyT = kI*sin(thetaI)
kxT2 = omega^2/c2^2-kyT^2
if (kxT2>0)
    disp(['Regime transmis.']); 
    kT = kI*c1/c2;
    thetaT = asin(sin(thetaI)*c2/c1);
    xiT = 0;
    C = 2*rho2*c2*cos(thetaI)/(rho1*c1*cos(thetaT)+rho2*c2*cos(thetaI))*A;
    B = (rho1*c1*cos(thetaT)-rho2*c2*cos(thetaI))/(rho1*c1*cos(thetaT)+rho2*c2*cos(thetaI))*A;
    T = (C/A)^2*rho1*c1/rho2/c2;
    R = (B/A)^2;
   
     disp([' Properties of transmitted wave : theta_T (deg) = ',num2str(thetaT*180/pi) ,' ; k_t= ', num2str(kT),'  ; lambda_T = ',num2str(2*pi/kT)]);
    disp(['B/A = ',num2str(B/A),' ; C/A = ',num2str(C/A)]);
     disp(['B/A = ',num2str(B/A),' ; C/A = ',num2str(C/A)]);
     disp(['R = ',num2str(R),' ;  T = ',num2str(T) ]);
   
     %(1-R)*cos(thetaI)
     %T*cos(thetaT)
     
else
    disp(['Regime evanesvent.']);
    ky = kI*sin(thetaI);
    xiT = sqrt(-kxT2);
    disp(['attenuation factor xiX = ',num2str(xiT)]);
    C = 2*rho2*c2*cos(thetaI)/(1i*rho1*c1*xiT+rho2*c2*cos(thetaI))*A;
    B = (1i*rho1*c1*xiT-rho2*c2*cos(thetaI))/(1i*rho1*c1*xiT+rho2*c2*cos(thetaI))*A;
end



ScaleFig = 4;Npx = 80; Npy = 50;
X1line = linspace(-ScaleFig,0,Npx);
X2line = linspace(0,ScaleFig,Npx);
Yline = linspace(-ScaleFig,ScaleFig,Npy);
[X1,Y1] = meshgrid(X1line,Yline);
[X2,Y2] = meshgrid(X2line,Yline);
close all;
for T = -2:.25:4 
  XfrontI = T*c1;
  XfrontR = XfrontI;
  XfrontT = T*c2;
  XfrontS = T*c1/sin(thetaI);
  figure(1);axis([-ScaleFig ScaleFig -ScaleFig ScaleFig]);
  p1 = real( A*1i*exp(1i*kI*(X1*cos(thetaI)+Y1*sin(thetaI)-T)).*(X1*cos(thetaI)+Y1*sin(thetaI)<XfrontI)...
         + B*1i*exp(1i*kI*(-X1*cos(thetaI)+Y1*sin(thetaI)-T)).*(-X1*cos(thetaI)+Y1*sin(thetaI)<XfrontR));
  u1 = real( A*1i*exp(1i*kI*(X1*cos(thetaI)+Y1*sin(thetaI)-T)).*(X1*cos(thetaI)+Y1*sin(thetaI)<XfrontI)*(cos(thetaI))...
         + B*1i*exp(1i*kI*(-X1*cos(thetaI)+Y1*sin(thetaI)-T)).*(-X1*cos(thetaI)+Y1*sin(thetaI)<XfrontR)*(-cos(thetaI)));
  v1 = real( A*1i*exp(1i*kI*(X1*cos(thetaI)+Y1*sin(thetaI)-T)).*(X1*cos(thetaI)+Y1*sin(thetaI)<XfrontI)*(sin(thetaI))...
         + B*1i*exp(1i*kI*(-X1*cos(thetaI)+Y1*sin(thetaI)-T)).*(-X1*cos(thetaI)+Y1*sin(thetaI)<XfrontR)*(-cos(thetaI)));
   if (xiT==0) 
     % Regime with transmission
    p2 = real( C*1i*exp(1i*kT*(X2*cos(thetaT)+Y2*sin(thetaT)-c2*T)).*(X2*cos(thetaT)+Y2*sin(thetaT)<XfrontT));
    u2 = real( C*1i*exp(1i*kT*(X2*cos(thetaT)+Y2*sin(thetaT)-c2*T)).*(X2*cos(thetaT)+Y2*sin(thetaT)<XfrontT)*(cos(thetaT)));
    v2 = real( C*1i*exp(1i*kT*(X2*cos(thetaT)+Y2*sin(thetaT)-c2*T)).*(X2*cos(thetaT)+Y2*sin(thetaT)<XfrontT)*(sin(thetaT)));
   else
    p2 = real( -C*exp(1i*(ky*Y2-omega*T)).*exp(-xiT*X2).*(Y2<XfrontS));
    u2 = real( -C*xiT/1i*exp(1i*(ky*Y2-omega*T)).*exp(-xiT*X2).*(Y2<XfrontS));
    v2 = real( -C*ky*exp(1i*(ky*Y2-omega*T)).*exp(-xiT*X2).*(Y2<XfrontS));    
  end
  contourf(X1,Y1,p1,10); hold on; 
  quiver(X1,Y1,u1,v1,'k'); hold on;
  contourf(X2,Y2,p2,10); hold on; 
  quiver(X2,Y2,u2,v2,'k'); hold on;
  plot([0 0],[-ScaleFig ScaleFig],'r-','linewidth',3);
  if autoadvance==1
    pause(.2);
  else
    pause;
  end
end










function answer = myinput(question, default)
answer = input([question, ' [default : ', num2str(default), ' ] : ']);
if isempty(answer)
    answer = default;
end
end