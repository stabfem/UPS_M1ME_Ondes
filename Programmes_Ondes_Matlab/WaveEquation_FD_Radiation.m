%%
%% Program WaveEquation_FiniteDifference_string.m
%% Computes the solution of the wave equation on a string of lenght L
%% with given initial conditions (requires two functions posinit and vitinit)
%%

clear all;
global L c;

% length and duration of integration
L = 10;
T = 1; mu = 1; c=sqrt(T/mu); 

stopTime = 100 ;

% time and space steps
dx = .005*L ;
dt = .00125*L/c;
Nx = L/dx + 1;
x = 0:dx:L;
YNstack = [0 0 0 0 0]; % five previous time steps
PN_past=0;PN_pluperfect=0;

% Set  current  and  past  to  the  graph  of  a  plucked  string
    Y_current = 0*(-intvitinit(x));
    Y_current = Y_current-Y_current(end);
    V_current = vitinit(x);
    Y_past = Y_current-V_current*dt;
    r = c*dt/dx 

figure(1);
subplot(2,1,1);
plot(x,Y_current,'r')
title('Initial shape (red)');
subplot(2,1,2);
plot(x,V_current,'b')
title('Initial velocity (blue)');
pause

% Loop over time
for t =0:dt:stopTime

% inner points : solve the wave equation
for j=2:Nx-1
Y_future(j) = r^2*(Y_current(j-1)+Y_current(j+1))+2*(1-r^2)*Y_current(j)-Y_past(j);
end

% boundary condition x=0:
    Y_future(1)  = 0;

%% First idea
%Z1I = 0; % positive -> delay but drift ; negative -> advance, no drift 
%Z2R = -0.001;
%    Y_future(Nx) = 0; 
%    Bn = 1/(2*dt^3)*[5, -18, 24, -14, 3]*YNstack'; % bi-acceleration
%Y_future(Nx) = 2*Y_current(Nx)-Y_past(Nx) ...
%    + dt^2*Z1I*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx) ...
%    + dt^2*Z2R*Bn;
%YNstack = [Y_future(Nx), YNstack(1:4)];

%% second idea 
%K1 = -0*0.1;    % related to correction length ; positive -> delay but drift. Negative -> advance, energy seems conserved
%K2 = 0.1;      % related to radiation ; 
%PN_current = (3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx);
%dtPN = (3*PN_current-4*PN_past+PN_pluperfect)/(2*dt);
%Y_future(Nx) = (2*Y_current(Nx)-Y_past(Nx)) + dt^2*(K1*PN_current-K2*dtPN);
%PN_pluperfect = PN_past;
%PN_past = PN_current;


%% third idea
Z1I = 0; % positive -> delay but drift ; negative -> advance, no drift 
Z2R = 0.001;
    Y_future(Nx) = [10/3, -12/3, 6/3, -1/3]*YNstack(1:4)' ...
    + 0.1*(2*dt^3)/3*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx);
    Y_future(Nx)
YNstack = [Y_future(Nx), YNstack(1:4)];


% Plot  the  graph  after  every  20 th  frame
if mod( t/dt ,  20) == 0   
    figure(1);
    plot( x ,  Y_current,'r');
    V_current = (Y_future-Y_past)/(2*dt);
    subplot(2,1,1);
    plot( x ,  Y_current,'r');
    xlim( [ 0 L ]);% -.3/c .3/c ] );
    title('String position Y(x) (red) ');
    subplot(2,1,2);
    plot( x ,  V_current,'b');

    title('String velocity V(x) (blue) ');
    xlim( [ 0 L ]);% -.3/c .3/c ] );
    pause(.001)
end
% Set  things  up  for  the  next  time  step
Y_past = Y_current ;
Y_current = Y_future ;
end

disp('max(|Y-Y0|) after one period :');
max(abs(Y_current-posinit(x)))





