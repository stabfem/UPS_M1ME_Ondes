clear all ; close all;

global L c
c = 1;
L = 10;

Tmax = (2*L)/c ; %% duree totale = 1 periode
dt = .025*Tmax; %% delai entre chaque instant represente
dx = .01*L;
x = -L:dx:2*L;

for t = 0:dt:Tmax

for i = 1:length(x)
    x1 = x(i)-c*t;
    x2 = x(i)+c*t;
    termeF(i) = dAlembert_F(x1);
    termeG(i) = dAlembert_G(x2);
    Y(i) = termeF(i)+termeG(i);
end

figure(1);
plot(x,termeF,'b--',x,termeG,'g--');
hold on;
plot(x(L/dx+1:2*L/dx+1),termeF(L/dx+1:2*L/dx+1),'b',x(L/dx+1:2*L/dx+1),termeG(L/dx+1:2*L/dx+1),'g');
plot(x,Y,'r--','LineWidth',2);
plot(x(L/dx+1:2*L/dx+1),Y(L/dx+1:2*L/dx+1),'r','LineWidth',2);
ymax = 2*max([termeF(L/dx+1:2*L/dx+1),termeG(L/dx+1:2*L/dx+1)]);
plot([0 0],[-1e3 1e3],'k:', [L L],[-1e3 1e3],'k:');
xlim([-L 2*L]);%ylim([-ymax, ymax]);
xlabel('x');ylabel('Y');
title('Y(x,t) (red) = F(x-ct) (blue) + G(x+ct) (green)')

ylim([-1.5 1.5]);
hold off;
pause
end

%% Estimation de la force au chevalet U = dY/dx en x=0

Nt = 200;
xeps=1e-4;
for j = 1:Nt
    t(j) = Tmax*j/Nt;
    x1 = x(i)-c*t;
    x2 = x(i)+c*t;
    Yeps = dAlembert_F(xeps-c*t(j)) + dAlembert_G(xeps+c*t(j));
    U(j) = (Yeps-0)/xeps;
end
figure(2); plot(t,U,'r')
xlabel('t');y

label('U(t)');title('force exercee au chevalet U(t) (dAlembert) ')

