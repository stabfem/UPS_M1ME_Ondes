function y = dAlembert_G(x2);
global L c

 if(x2>=0&&x2<=L)
     y = posinit(x2)/2+1/c*intvitinit(x2)/2;
 elseif(x2<=0&&x2>-L) % prolongation par symetrie
     y = -posinit(-x2)/2+1/c*intvitinit(-x2)/2;
 else % prolongation par periodicite
     x2p = x2-2*L*round(x2/(2*L));
     y = dAlembert_G(x2p);
 end

%corde frappee
%y = -dAlembert_F(x2);
% corde pincee
%y = dAlembert_F(x2);
%end

    
    


