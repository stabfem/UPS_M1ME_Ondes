% résolution du probleme "bidioptre" en régime établi avec une méthode matricielle
close all;
rho = 1;
c = 1;
L = 10;
Z1 = 1;
Z2 = 1/16.; % corresponds to  R2 = 4 R1
Z3 = 1;     % corresponds to  R3 = R1
A = 1;

omega = linspace(0.001,5*pi/10,3000);
kL = omega*L;

for j = 1:length(kL)
  [B(j),C(j),D(j),E(j)] = ResBidioptre(1,Z1,Z2,Z3,kL(j));
end
figure(4);
plot(kL,(abs(E.^2)/Z3)/(abs(A)^2/Z1),'r');ylim([0 1])
xlabel('$kL$','interpreter','latex');ylabel('$\cal T$','interpreter','latex')
saveas(gcf,'Tranmission_Bidioptre.png')

function [B,C,D,E] = ResBidioptre(A,Z1,Z2,Z3,kL)
%
% Cette fonction met le problème sous forme M X = Y avec X = [B;C;D;E] et
% le résout.
%
Matrice = [[ -1,         1,                1,              0              ] ; ... 
           [  1/Z1,      1/Z2,            -1/Z2,           0              ] ; ...
           [  0,         exp(1i*kL),      exp(-1i*kL),     -exp(1i*kL)    ] ; ...
           [  0,         exp(1i*kL)/Z2,  -exp(-1i*kL)/Z2,  -exp(1i*kL)/Z3 ] ];
Secondmembre = A*[1 ; 1/Z1 ; 0 ; 0];

X = Matrice\Secondmembre;
B = X(1);C = X(2);D = X(3);E = X(4);
end
