clear all;
global kappa
%

disp('###');
disp('### Visualisation of waves in solid media and reflexion on a surface');
disp('###');disp('');
disp('Select one of the following choices : ');
disp('  1 : longitudinal wave');
disp('  2 : transverse wave');
disp('  3 : reflexion of a longitudinal wave');
disp('  4 : reflexion of a transverse wave');
disp('  5 : Rayleigh wave')
disp(' 11 : plot of reflexion coefficients (incident longitudinal)');
disp(' 12 : plot of reflexion coefficients (incident transverse)');


choice = 1;
while (choice~=0)
choice = myinput('==> Your choice ? ',4);

switch(choice)
    
    case(11)
        kappa = myinput('Ratio of velocities of longitudinal/transverse waves ? kappa = ct / cl = ',0.5);
        thetaI = linspace(0,pi/2,200);
        [Al,At,Rl,Rt] = Reflexion(thetaI,'l');
        figure(21); plot(thetaI*180/pi,abs(Al),'b',thetaI*180/pi,abs(At),'g');
        xlabel('\theta_I'); legend('|rll|','|rlt|');
        figure(22); plot(thetaI*180/pi,Rl,'b',thetaI*180/pi,Rt,'g');
        xlabel('\theta_I'); legend('Rll','Rlt');
    
    case(12)
        kappa = myinput('Ratio of velocities of longitudinal/transverse waves ? kappa = ct / cl = ',0.5);
        thetaI = linspace(0,pi/2,200);
        [Al,At,Rl,Rt] = Reflexion(thetaI,'t');
        figure(21); plot(thetaI*180/pi,abs(Al),'b',thetaI*180/pi,abs(At),'g');
        xlabel('\theta_I'); legend('|rtl|','|rtt|');
        figure(22); plot(thetaI*180/pi,Rl,'b',thetaI*180/pi,Rt,'g');
        xlabel('\theta_I'); legend('Rtl','Rtt');    
        
      
        
    case(1)    
        
    thetadeg = myinput('Incidence angle theta_I (deg) ? ',45);
    lambdaI = myinput('Wavelength of incident wave ? ',1);

    thetaI = thetadeg*pi/180; thetal = thetaI; thetat = 0;
    kl = 2*pi/lambdaI; kt = 0; kI = 0;
    cl = 1; ct=0;
    omega = kl*cl;
    AIl = 0.;AIt =0;Al =1;At = 0;
    
    case(2)    
        
    thetadeg = myinput('Incidence angle theta_I (deg) ? ',45);
    lambdaI = myinput('Wavelength of incident wave ? ',1);

    thetaI = thetadeg*pi/180; thetat = thetaI; thetal = 0;
    kt = 2*pi/lambdaI; kl = 0; kI = 0;
    ct = 1; cl = 0;
    omega = kt*ct;
    AIl = 0.;AIt =0.;Al =0;At = 1;
    
    case(3)
    kappa = myinput('Ratio of velocities of longitudinal/transverse waves ? kappa = ct / cl = ',0.5);
    thetadeg = myinput('Incidence angle theta_I (deg) ? ',45);
    lambdaI = myinput('Wavelength of incident wave ? ',1);
    
    thetaI = thetadeg*pi/180;
    kI = 2*pi/lambdaI;
    cl = 1; ct = kappa*cl;
    kl = kI; kt = kI/kappa;
    omega = kI*cl;
    [Al,At,Rl,Rt,thetal,thetat] = Reflexion(thetaI,'l');
    AIl = 1;AIt =0.;

    case(4)
    kappa = myinput('Ratio of velocities of longitudinal/transverse waves ? kappa = ct / cl = ',0.75);
    thetadeg = myinput('Incidence angle theta_I (deg) ? ',30);
    lambdaI = myinput('Wavelength of incident wave ? ',1);
    
    thetaI = thetadeg*pi/180;
    kI = 2*pi/lambdaI;
    cl = 1/kappa; ct = 1;
    kt = kI; kl = kI*kappa;
    omega = kI*cl;
    
    
    [Al,At,Rl,Rt,thetal,thetat] = Reflexion(thetaI,'t');
    AIl = 0;AIt =1;
    
     case(5)
         disp('Not yet implemented')

end


if( choice < 5)
    % next if to generate animation  
    Amp = myinput('Amplitude of incident wave ? ',.05);
    repmode =myinput(' Representation : 1 [ Color=shear, altitude=div ] or 2 [ Color=div, altitude=shear ] ',1) ;  
    autoadvance =myinput(' Generate a movie (1) or advance manually (0) ? ',0);  
    
% mesh 
ScaleFig = 4;Npx = 41; Npy = 41;
Xline = linspace(-ScaleFig,ScaleFig,Npx);
Yline = linspace(-1.5*ScaleFig,0,Npy);
[X1,Y1] = meshgrid(Xline,Yline);
az = 0; el = 90;


close all;
for T = -4:.25:8 
  Xfrontl = cl*T;
  Xfrontt = ct*T;
%  XfrontIL = -XfrontI;
 % XfrontT = c2*T;
 % XfrontS = c1/sin(thetaI)*T;

figure(1);axis([-ScaleFig ScaleFig -ScaleFig ScaleFig]);
 
u1  =     real(Al*exp(1i*kl*(X1*sin(thetal)-Y1*cos(thetal))-1i*omega*T)*sin(thetal)) ...
         .* (X1*sin(thetal)-Y1*cos(thetal)<Xfrontl) ...
    +     real(At*exp(1i*kt*(X1*sin(thetat)-Y1*cos(thetat))-1i*omega*T)*cos(thetat)) ...
         .* (X1*sin(thetat)-Y1*cos(thetat)<Xfrontt) ... 
    + AIl * real(exp(1i*kl*(X1*sin(thetal)+Y1*cos(thetal))-1i*omega*T)*sin(thetal)) ...
         .* (X1*sin(thetal)+Y1*cos(thetal)<Xfrontl) ... 
    + AIt * real(exp(1i*kt*(X1*sin(thetat)+Y1*cos(thetat))-1i*omega*T)*(-cos(thetat))) ...
         .* (X1*sin(thetat)+Y1*cos(thetat)<Xfrontt) ...          
    ;
v1  =     real(Al*exp(1i*kl*(X1*sin(thetal)-Y1*cos(thetal))-1i*omega*T)*(-cos(thetal))) ...
         .* (X1*sin(thetal)-Y1*cos(thetal)<Xfrontl) ...
    +     real(At*exp(1i*kt*(X1*sin(thetat)-Y1*cos(thetat))-1i*omega*T)*sin(thetat)) ...
         .* (X1*sin(thetat)-Y1*cos(thetat)<Xfrontt) ...   
    + AIl * real(exp(1i*kl*(X1*sin(thetal)+Y1*cos(thetal))-1i*omega*T)*(+cos(thetal))) ...
         .* (X1*sin(thetal)+Y1*cos(thetal)<Xfrontl) ...
    + AIt * real(exp(1i*kt*(X1*sin(thetat)+Y1*cos(thetat))-1i*omega*T)*sin(thetat)) ...
         .* (X1*sin(thetat)+Y1*cos(thetat)<Xfrontt) ...   
    ;
div =     real(Al*1i*kl*exp(1i*kl*(X1*sin(thetal)-Y1*cos(thetal))-1i*omega*T)) ... 
         .* (X1*sin(thetal)-Y1*cos(thetal)<Xfrontl) ...
         +real(AIl*1i*kl*exp(1i*kl*(X1*sin(thetal)+Y1*cos(thetal))-1i*omega*T)) ... 
         .* (X1*sin(thetal)+Y1*cos(thetal)<Xfrontl) ...
    ;
rot =     real(At*exp(1i*kt*(X1*sin(thetat)-Y1*cos(thetat))-1i*omega*T)) ...
         .* (X1*sin(thetat)-Y1*cos(thetat)<Xfrontt) ...
        + real(AIt*exp(1i*kt*(X1*sin(thetat)+Y1*cos(thetat))-1i*omega*T)) ...
         .* (X1*sin(thetat)+Y1*cos(thetat)<Xfrontt) ...
    ;

   
if repmode ==1  
   surf(X1+Amp*u1,Y1+Amp*v1,Amp*div,rot); axis equal; colorbar; view(az,el);
else
   surf(X1+Amp*u1,Y1+Amp*v1,5*Amp*rot,div); axis equal; colorbar; view(az,el); 
end

  if autoadvance==1
    pause(.2);
  else
    pause;
  end
  [az,el] = view;
end

end
    
end








function answer = myinput(question, default)
answer = input([question, ' [default : ', num2str(default), ' ] : ']);
if isempty(answer)
    answer = default;
end
end

function [Al,At,Rl,Rt,thetal,thetat] = Reflexion(thetaI,typeI)
global kappa
if strcmp(typeI,"l")
    thetal = thetaI;
    thetat = asin(kappa*sin(thetaI));
    N = kappa^2*sin(2*thetal).*sin(2*thetat)+cos(2*thetat).^2;
    Al = (kappa^2*sin(2*thetal).*sin(2*thetat)-cos(2*thetat).^2)./N;
    At = 2*kappa*sin(2*thetal).*cos(2*thetat)./N;
    Rl = Al.^2;
    Rt = kappa*cos(thetat)./cos(thetal).*At.^2;
else
    thetat = thetaI;
    thetal = asin(1/kappa*sin(thetaI));
    N = kappa^2*sin(2*thetal).*sin(2*thetat)+cos(2*thetat).^2;
    At = (-kappa^2*sin(2*thetal).*sin(2*thetat)+cos(2*thetat).^2)./N;
    Al = 2*kappa*sin(2*thetat).*cos(2*thetat)./N;
    Rt = abs(At).^2;
    Rl = 1/kappa*cos(thetal)./cos(thetat).*abs(Al).^2;
end
end

