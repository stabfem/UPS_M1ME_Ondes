% logiciel développé par Laurent LACAZE ; modifie par D. Fabre (03/2017)
% Resolution des équations "shallow water" = "St Venant", complètement non linéaire
 
%%%%%%% Euler method base on Kuganov model 2002, 2002 and 2007
%%%%%%% well-balanced method to reconstuct the source term numerical scheme
%%%%%%% dw/dt = -(Hj+1/2 - Hj-1/2)/dx + Sj (see Kuganov 2007 for H)
%%%%%%% Sj = -g/2dx * hmj+1/2[ (Bpj+1/2-Bpj-1/2) + (Zpj+1/2-Zpj-1/2) ]
%%%%%%%      -g/2dx * hpj-1/2[ (Bmj+1/2-Bmj-1/2) + (Zmj+1/2-Zmj-1/2) ]
%%%%%%% with B the topgraphy and Z the rheology such that Dz/Dx = Tau(z=0)/gh
%%%%%%% Euler in time!!



function SaintVenant()

close all;
format long;

% PARAMETRES PHYSIQUES ET NUMERIQUES DE LA SIMULATION
g=9.81;     % acceleration de la pesanteur (ms^-2)
f=0.00;       % parametre de coriolis (rad s-1)
epsilonf=0.0001; % terme d'amortissement
L = 1000;     % taille du domaine (m)
Nx = 1000;  % nombre de points dans le domaine
Nt = 10001; % Nombre de pas de calcul puor la simulation 

% creation d'un maillage regulier
x = linspace(-L,L,Nx);
dx = x(2)-x(1);

% Initialisation de differents parametres
ul=0; ur=0; cl=0; cr=0; dul=0; dur=0; ulnm1=0; urnm1=0;
epsilon = 1.e-15.*dx^4;
v = zeros(size(x));
hv = zeros(size(x));
Cu = zeros(3,length(x)-2);
B = zeros(1,length(x)-1);
theta = 1;

fprintf('Type de condition initiale ? \n');
fprintf(' [1] -> surélévation gaussienne \n');
fprintf(' [2] -> surélévation rectangulaire \n');
fprintf(' [3] -> palier descendant (ouverture de vanne) \n');
fprintf(' [4] -> onde simple (mascaret) ; profil erf \n');
fprintf(' [5] -> onde simple (mascaret) ; profil linéaire \n');
fprintf(' [6] -> survitesse gaussienne \n');
fprintf(' [7] -> discontinuite \n');

flag = input('Choix : ');


if(flag==1)
hl = myinput('Hauteur d''eau ? ',2.00);% hauteur d'eau ? gauche et ? droite de la marche (en m)
hr = myinput('Hauteur d''eau au centre de la gaussienne ? ',2.1);     % hauteur d'eau sur la marche (en m)
largeur=myinput('Largeur  de la gaussienne ? ',100);  % largeur de la marche d'eau (m)
h = exp(-(x/largeur).^2)*(hr-hl)+hl;
u = zeros(size(x));
hu = zeros(size(x));
hG = hl; hD = hl; uG = 0; uD=0;
end

if(flag==2)
% CONDITION INITIALE MARCHE D'EAU
%largeur=100;  % largeur de la marche d'eau (m)
%hl=2.00;     % hauteur d'eau ? gauche et ? droite de la marche (en m)
%hr=2.60;     % hauteur d'eau sur la marche (en m)
hl = myinput('Hauteur d''eau ? ',2.00);% hauteur d'eau ? gauche et ? droite de la marche (en m)
hr = myinput('Hauteur d''eau de la surelevation ? ',2.6);     % hauteur d'eau sur la marche (en m)
largeur=myinput('Largeur  de la surelevation ? ',100);  % largeur de la marche d'eau (m)

h = hl*((x-largeur/2.0)<0)+ hr*(((x-largeur/2.0)>=0)&((x+largeur/2.0)<=0)) + hl*((x+largeur/2.0)>0);
%h = hl*((x-largeur/2.0)<0)+ hr*((x-largeur/2.0)>=0)+hr*((x+largeur/2.0)<=0)+hl*((x+largeur/2.0)>0)
%h = hr*((x+largeur/2.0)>=0)+hr*((x-largeur/2.0)<=0)-hr + hl*((x+largeur/2.0)<0)+hl*((x-largeur/2.0)>0);
u = zeros(size(x));
hu = zeros(size(x));
hG = hl; hD = hl; uG = 0; uD=0;
end


if(flag==3)
% CONDITION INITIALE palier descendant
hG = myinput('Hauteur d''eau a  gauche du palier ? ',3.00);
hL = myinput('Hauteur d''eau a droite du palier ? ',2.00);    
largeur=myinput('Largeur du palier ? ',100);  
largeur=100;  % largeur de la marche d'eau (m)
hG=3.00;     % hauteur d'eau ? gauche  (en m)
hD=2;     % hauteur d'eau ? droite  (en m)
h = hG*ones(size(x)) + (hD-hG)/largeur*((x>=-largeur/2)&(x<=largeur/2)).*(x+largeur/2) + (hD-hG)*((x-largeur/2.0)>0);
%h = hl*((x-largeur/2.0)<0)+ hr*((x-largeur/2.0)>=0)+hr*((x+largeur/2.0)<=0)+hl*((x+largeur/2.0)>0)
%h = hr*((x+largeur/2.0)>=0)+hr*((x-largeur/2.0)<=0)-hr + hl*((x+largeur/2.0)<0)+hl*((x-largeur/2.0)>0);
u = zeros(size(x));
hu = zeros(size(x));
uG = 0; uD=0;
end

if(flag==4)
% CONDITION INITIALE onde simple
largeur=100;  % largeur de la marche d'eau (m)
hG=3;     % hauteur d'eau ? gauche  (en m)
hD=2;     % hauteur d'eau ? droite  (en m)
s = 1; % type de l'onde : s=1-> onde J+, s=-1 -> onde J-

cG = sqrt(hG*g);
cD = sqrt(hD*g);
cc = cG+(cD-cG)*(erf(x/largeur)+1)/2;
h  = cc.^2/g; 

u = s*2*(sqrt(g*h)-cD);
hu = h.*u;
uG = u(1);
uD = u(length(x));
end


if(flag==5)
% CONDITION INITIALE onde simple
largeur=100;  % largeur de la marche d'eau (m)
hG=1;     % hauteur d'eau ? gauche  (en m)
hD=2;     % hauteur d'eau ? droite  (en m)
s = 1; % type de l'onde : s=1-> onde C+, s=-1 -> onde C-
cG = sqrt(hG*g);
cD = sqrt(hD*g);
h = hG*ones(size(x)) + (hD-hG)/largeur*((x>=-largeur/2)&(x<=largeur/2)).*(x+largeur/2) + (hD-hG)*((x-largeur/2.0)>0);

u = s*2*(sqrt(g*h)-cD);
hu = h.*u;
uG = u(1);
uD = u(length(x));
end


if(flag==6)
% condition initiale impulsion de vitesse
Ua = 1;
hr = 2;
largeur=100; 
u = Ua*exp(-(x/largeur).^2);
h = hr*ones(size(x));
hu = h.*u;
hG = hr; hD = hr; uG = 0; uD=0;
end

if(flag==7)
% CONDITION INITIALE onde simple
largeur=0.1;  % largeur de la marche d'eau (m)
hG=1;     % hauteur d'eau ? gauche  (en m)
hD=2;     % hauteur d'eau ? droite  (en m)
s = 1; % type de l'onde : s=1-> onde C+, s=-1 -> onde C-
cG = sqrt(hG*g);
cD = sqrt(hD*g);
h = hG*ones(size(x)) + (hD-hG)/largeur*((x>=-largeur/2)&(x<=largeur/2)).*(x+largeur/2) + (hD-hG)*((x-largeur/2.0)>0);

u = s*2*(sqrt(g*h)-cD);
hu = h.*u;
uG = u(1);
uD = u(length(x));
end



   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %%% FIN DES INITIALISATIONS  
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   
     fprintf('Condition initiale : \n');      
    figure(1)
    subplot(2,1,1);
    plot(x,h,'.')
%   axis([-L,L,hl-0.5*(hr-hl),hr+0.5*(hr-hl)])
%    title(['Time: ', int2str(t), ' seconds']);
      ylabel('Height (m)');
      xlabel('Distance (m)');
    subplot(2,1,2)
    plot(x,u,'.')
%    axis([-L,L,-0.5,0.5]);
%    title(['Time: ', int2str(t), ' seconds']);
      ylabel('U Speed (m/s)');
      xlabel('Distance (m)');
   
        pause
        
 %%%
    [ hmint,hpint,humint,hupint,hvmint,hvpint,umint,upint,vmint,vpint ] = VariablesBordMaille( theta,dx,h,u,v,hu,hv,epsilon );
    [ apint, amint ] = AvancementCaracteristiques( g,hpint,hmint,upint,umint );
    
%% temporal loop
t=0;

for n=1:Nt
    
    dtmax = dx/2./max(max([apint;-amint],[],1));
    dt = dtmax/10;
    
    %%% time evolution;
    
    [ RHSF, RHSS ] = RightHandSideMomentum(dx, amint,apint,umint,hmint,humint,hvmint,upint,hpint,hupint,hvpint,u,v,hu,hv,h,g,f,B,epsilonf );
    
        % time stepping
    Cu = [h(2:end-1); hu(2:end-1); hv(2:end-1)] + dt*(RHSF + RHSS);
    
    
    %%%% Boundary Conditions ??
    h(2:end-1) = Cu(1,:);
    h(1) = hG;
    h(end) = hD;
    %h(1)=h(2);
    %h(end)=h(end-1);
    
     hu(2:end-1) = Cu(2,:);
    u = sqrt(2).*h.*(hu)./sqrt(h.^4+max([h.^4;epsilon*ones(size(h))],[],1));
    
     %u(1) = ul + dt./dx.*cl*(dul);
     %u(end) = ur - dt./dx.*cr*(dur);
     u(1)=uG;
     u(end)=uD;
     
     hu(1) = h(1)*u(1);

     hu(end) = h(end)*u(end);
    
    hv(2:end-1) = Cu(3,:);
    v = sqrt(2).*h.*(hv)./sqrt(h.^4+max([h.^4;epsilon*ones(size(h))],[],1));
    hv(1) = hv(2);
    hv(end) = hv(end-1);
    v(1)=hv(1)/h(1);
    
    %%% 
    
    
    
    [ hmint,hpint,humint,hupint,hvmint,hvpint,umint,upint,vmint,vpint ] = VariablesBordMaille( theta,dx,h,u,v,hu,hv,epsilon );
    
    
%     c1=dx/dt.*(u(1)-u1)./(u(2)-u(1))
%     cend =-dx/dt.*(u(end)-uend)./(u(end)-u(end-1))
%     
%     if (abs(u(2)-u(1))<1.e-15)
%         c1=0;
%     end
%     if (abs(u(end)-u(end-1))<1.e-15)
%         cend=0;
%     end
%     
%     u1=u(1);
%     u2=u(2);
%     uend=u(end);
%     uendm1=u(end-1);
    
    
    
    
    
    %%%
    
    [ apint, amint ] = AvancementCaracteristiques( g,hpint,hmint,upint,umint );
    
    
    
    ul=u(1);
    cl=sqrt(g*h(1));
    cl=dx/dt.*(u(1)-ulnm1)./(u(2)-u(1));
    if abs(u(1)-ulnm1)<1.e-15
       cl=0;
    end
    
    dul=u(2)-u(1);
    
    ur=u(end);
    cr=sqrt(g*h(end));
    cr=-dx/dt.*(u(end)-urnm1)./(u(end)-u(end-1));
    if abs(u(end)-urnm1)<1.e-15
       cr=0;
    end
    
    dur=u(end)-u(1);
    
    ulnm1=u(1);
    urnm1=u(end);
    
    %%%%% POST PRO
    
    
      if (floor((n-1)/200)==(n-1)/200)
      
    fprintf('affichage ? t=%f s\n',t);      
    figure(1)
    subplot(2,1,1);
    plot(x,h,'.')
%   axis([-L,L,hl-0.5*(hr-hl),hr+0.5*(hr-hl)])
    title(['Time: ', int2str(t), ' seconds']);
      ylabel('Height (m)');
      xlabel('Distance (m)');
    subplot(2,1,2)
    plot(x,u,'.')
%    axis([-L,L,-0.5,0.5]);
%    title(['Time: ', int2str(t), ' seconds']);
      ylabel('U Speed (m/s)');
      xlabel('Distance (m)');
%   hold on
%    plot(x(2:end-1),fint,'r')
%    plot(x(2:end-1),RHSF(2,:),'g')
%    hold off
%     figure(3)
%     plot(x,v,'.')
%     axis([-L,L,-0.5,0.5])
%     title(['Time: ', int2str(t), ' seconds']);
%       ylabel('V Speed (m/s)');
%       xlabel('Distance (m)');
%     figure(4)
%     plot(n,sum(h.^2),'.')
%     hold on
   % if (n==1)
   %     pause
   % end
   % n
    pause(0.01)
    end
    
t = t+dt;    
end
%
end % Fin programme principal


function [ apint, amint ] = AvancementCaracteristiques( g,hpint,hmint,upint,umint )
%  Ceci est un sous-programme de SaintVenant.m

%%% find value of a; Riemann panel size
    ai1=upint+sqrt(g*hpint);
    ai2=umint +sqrt(g*hmint);
    ai3=zeros(size(umint));
    apint = max([ai1;ai2;ai3],[],1);
    ai1=upint-sqrt(g*hpint);
    ai2=umint -sqrt(g*hmint);
    ai3=zeros(size(upint));
    amint = min([ai1; ai2; ai3],[],1);
    %%%
end

function [ hmint,hpint,humint,hupint,hvmint,hvpint,umint,upint,vmint,vpint ] = VariablesBordMaille( theta,dx,h,u,v,hu,hv,epsilon )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%%% find h intermediaire avec dh/dx = 0 en x=0
    zk = [theta*(h(2:end-1)-h(1:end-2))/dx;(h(3:end)-h(1:end-2))/2/dx;theta*(h(3:end)-h(2:end-1))/dx];
    A = [min(zk);max(zk)];
    hx = [(h(2)-h(1))/dx A(1,:).*(A(1,:)>0) + A(2,:).*(A(2,:)<0) (h(end)-h(end-1))/dx];
     
    hmint = h(1:end-1) + dx/2*hx(1:end-1);
    hpint = h(2:end) - dx/2*hx(2:end);
    
    % def h positif/home/local/UOB/math073v/Documents/MatlabRoutines_ShallowModel/Euler
    kfind = find(hmint<0);
    if isempty(kfind)~=1
        hmint(kfind) = 0;
    
        if kfind(1)==1
            display(['h zero proche du bord gauche'])
        else
            hpint(kfind-1) = 2*h(kfind);
            
        end
    end
    
    kfind = find(hpint<0);
    if isempty(kfind)~=1
        hpint(kfind) = 0;
        if kfind(end)==1
            display(['h zero proche du bord gauche'])
        else
            hmint(kfind+1) = 2*h(kfind+1);
           
        end
    end
    
    %%% find hu intermediaire
    zk = [theta*(hu(2:end-1)-hu(1:end-2))/dx;(hu(3:end)-hu(1:end-2))/2/dx;theta*(hu(3:end)-hu(2:end-1))/dx];
    A = [min(zk);max(zk)];
    hux = [(hu(2)-hu(1))/dx A(1,:).*(A(1,:)>0) + A(2,:).*(A(2,:)<0) (hu(end)-hu(end-1))/dx];
    
    
    humint = hu(1:end-1) + dx/2*hux(1:end-1);
    hupint = hu(2:end) - dx/2*hux(2:end);
    %%%
    
    %%% find u
    
    upint = sqrt(2).*hpint.*(hupint)./sqrt(hpint.^4+max([hpint.^4;epsilon*ones(size(hpint))],[],1));
    umint = sqrt(2).*hmint.*(humint)./sqrt(hmint.^4+max([hmint.^4;epsilon*ones(size(hmint))],[],1));
    %u(1) = 0;
    %u(end) = 0;
    
    %%% find hv intermediaire
    zk = [theta*(hv(2:end-1)-hv(1:end-2))/dx;(hv(3:end)-hv(1:end-2))/2/dx;theta*(hv(3:end)-hv(2:end-1))/dx];
    A = [min(zk);max(zk)];
    hvx = [(hv(2)-hv(1))/dx A(1,:).*(A(1,:)>0) + A(2,:).*(A(2,:)<0) (hv(end)-hv(end-1))/dx];
    
    
    hvmint = hv(1:end-1) + dx/2*hvx(1:end-1);
    hvpint = hv(2:end) - dx/2*hvx(2:end);
    %%%
    
    %%% find u
    
    vpint = sqrt(2).*hpint.*(hvpint)./sqrt(hpint.^4+max([hpint.^4;epsilon*ones(size(hpint))],[],1));
    vmint = sqrt(2).*hmint.*(hvmint)./sqrt(hmint.^4+max([hmint.^4;epsilon*ones(size(hmint))],[],1));
    
end

function [ RHSF, RHSS ] = RightHandSideMomentum(dx, amint,apint,umint,hmint,humint,hvmint,upint,hpint,hupint,hvpint,u,v,hu,hv,h,g,f,B,epsilonf )
% sous-programme de SaintVenant.m

    Hint = ([apint.*humint./(apint-amint);apint.*(humint.*umint + g/2*hmint.^2)./(apint-amint); apint.*(hvmint.*umint)./(apint-amint)]-...
            [amint.*hupint./(apint-amint);amint.*(hupint.*upint + g/2*hpint.^2)./(apint-amint);amint.*(hvpint.*upint)./(apint-amint)]) ...
           + ([apint.*amint./(apint-amint).*hpint;apint.*amint./(apint-amint).*(hupint);apint.*amint./(apint-amint).*(hvpint)]-...
              [apint.*amint./(apint-amint).*hmint;apint.*amint./(apint-amint).*(humint);apint.*amint./(apint-amint).*(hvmint)]);
          
          Hint(1,abs(apint)<1.e-15 & abs(amint)<1.e-15) = 0;
          Hint(2,abs(apint)<1.e-15 & abs(amint)<1.e-15) = 0;
       
    RHSF =-1/dx.*(Hint(:,2:end)-Hint(:,1:end-1));
      
    RHSS = [zeros(size(hmint(2:end))); ...
            -g/dx.*h(2:end-1).*(B(2:end)-B(1:end-1)) + f*hv(2:end-1) - epsilonf*hu(2:end-1).*u(2:end-1);...
            - f*hu(2:end-1)- epsilonf*hv(2:end-1).*v(2:end-1)];

end

function answer = myinput(question, default)
answer = input([question, ' [default : ', num2str(default), ' ] : ']);
if isempty(answer)
    answer = default;
end
end

