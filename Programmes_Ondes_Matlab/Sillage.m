

disp('Choisissez le rapport cg/c ?');
%disp('[ 1 pour cas non dispersif]');
disp('[ 0.5 pour cas profondeur infinie ]');
disp('[ 1.5 pour cas capillaire profondeur infinie ]');
disp('[ 2 pour cas capillaire profondeur faible ]');
delta = input('===> Choix : ');





% delta = cg/c.  Choose one of the following values :

if(delta ==.5)

theta = [-.49:.01:.49]*pi;
X = cos(theta).*(1-.5*cos(theta).^2);
Y = 1/2*sin(theta).*cos(theta).^2;
Phi = atan(delta*sin(theta).*cos(theta)./(1-delta*cos(theta).^2));

figure(1);
title('Forme des fronts d onde')
plot(X,Y,2*X,2*Y,3*X,3*Y);

figure(2);
plot(180/pi*theta,180/pi*Phi);
title('Relation Phi(beta)')
xlabel('\beta (^o)');
ylabel('\phi (^o)');

end

%beta = 1; % for gravity waves in shallow water


% beta = 3/2 ; % for capilary waves in deep water

if(delta ==1.5)
theta = [-.45:.01:.45]*pi;
X = 1./cos(theta).^3.*(sin(theta).^2-1/2);
Y = 3/2*sin(theta)./cos(theta).^2;
Phi = atan2(delta*sin(theta).*cos(theta),(1-delta*cos(theta).^2));

figure(1);
title('Forme des fronts d onde')
plot(X,Y,2*X,2*Y,3*X,3*Y);

figure(2);
plot(180/pi*theta,180/pi*Phi);
title('Relation phi(beta)')
xlabel('\beta (^o)');
ylabel('\phi (^o)');

end




% beta = 2; % for capilary waves in shallow water
X = 1./cos(theta).^2.*(1-2*cos(theta).^2);
Y = 2*tan(theta);




