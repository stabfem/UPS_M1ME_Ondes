
%% Impedance d'un tuyau acoustique conique tronqué 
%
% comparaison entre plusieurs modelisations de l'impedance de sortie

global rho c
rho = 1.225;  % masse volumique (kg/m^3)
c = 0.34;     % vitesse du son (m / ms)
r1 = .6;      % distance pointe du cone - section d'entree
r2 = 1.2;     % distance pointe du cone - section de sortie
L2 = r2-r1;   % longueur du tuyau
alpha = 3*pi/180; % angle d'expansion du cone : 3 degres
a1 = r1*tan(alpha); % rayon du tuyau en entrée
a2 = r2*tan(alpha); % rayon du tuyau en sortie
Z0out = rho*c/(pi*a2^2);
Z0in = rho*c/(pi*a1^2);

omega = linspace(0,10,1000); % gamme de omega en rad/ms
ktab = omega/c;
Delta = 0.61*r2*tan(alpha); % correction de longueur
Zout0 = 0*ktab; % defini comme un tableau de meme dimension que k
Zout1 = Z0out*( -1i*Delta.*ktab );
Zout2 = Z0out*( -1i*Delta.*ktab + (ktab*r2*tan(alpha)).^2/4 );

figure(1);hold off;
semilogy(omega/(2*pi)*1000,abs(Zin_cone(ktab,r1,r2,alpha,Zout0)/Z0in),'r');hold on;
semilogy(omega/(2*pi)*1000,abs(Zin_cone(ktab,r1,r2,alpha,Zout1)/Z0in),'b');hold on;
semilogy(omega/(2*pi)*1000,abs(Zin_cone(ktab,r1,r2,alpha,Zout2)/Z0in),'g');hold on;
semilogy(omega/(2*pi)*1000,abs(Zin_cone_theo(ktab,r1,r2,alpha,Zout2)/Z0in),'k:');hold on;
xlabel('f [Hz]','fontsize',18); ylabel('|Z_{in}| / Z_1','fontsize',18);ylim([1e-2 1e2])
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
legend('Ideal open','End correction','End correction + radiation','location','SE','fontsize',16)
saveas(gcf,'Impedance_Pipe60cm.png');





function Z = Zin_cone(ktab,r1,r2,alpha,Zout)
global rho c
% calcul de l'impédance en inversant un système 2x2 pour les amplitudes C,D.
% 
% Condition en entrée, débit Qf imposé : 
% Q(0,t) = Qf exp (- i omega t) soit 
%       ( C (1+1/ikr1) e^(ikr1) + D (-1-1/ikr1 e^(-ikr1) )/r1  / Z0in = Qf 
%
% Condition en sortie : 
% P(L,t)/Q(L,t) = Zout 
%   = Z0out [C exp(ikr2)+D exp(-ikr2)] / [C(1-1/ikr2)e^(ikr2) +D(-1-1/ikr2)e^(-ikr2)]
%
% Mise sous forme matricielle  M * [C;D] = Y
% 
% Pour utiliser cette fonction de manière vectorielle il faut faire ce
% calul pour chaque valeur de kL en faisant une boucle.
Z1 = rho*c/(pi*r1^2*tan(alpha)^2);
Z2 = rho*c/(pi*r2^2*tan(alpha)^2);
L = r2-r1;
Qf = 1;
  for j=1:length(ktab)
    k = ktab(j);
    M = [ ...
      [ (1-1/(1i*k*r1))*exp(1i*k*r1) , (-1-1/(1i*k*r1))*exp(-1i*k*r1) ]; ...
      [ (Z2-Zout(j)*(1-1/(1i*k*r2)))*exp(1i*k*r2) , (Z2+Zout(j)*(1+1/(1i*k*r2)))*exp(-1i*k*r2) ] ... 
        ];
    Y = [Qf*Z1*r1 ; 0];
    X = M\Y;
    C = X(1);D=X(2);
    Z(j) = (C*exp(1i*k*r1)+D*exp(-1i*k*r1))/(r1*Qf);
  end
end



function Z = Zin_cone_theo(ktab,r1,r2,alpha,Zout)
global rho c
% calcul de l'impédance d'entrée en utilisant la solution theorique
% Référence : Fletcher & Rossing, page 210 ; Olson, 1957.
% Fonction vectorielle : marche si k et Zout sont des tableaux de valeur
Z1 = rho*c/(pi*r1^2*tan(alpha)^2);
Z2 = rho*c/(pi*r2^2*tan(alpha)^2);
kL = ktab*(r2-r1);
theta1 = atan(ktab*r1);
theta2 = atan(ktab*r2);
Z = Z1*(- 1i*Zout.*sin(kL-theta2)./sin(theta2) + Z2*sin(kL) ) ...
  ./ ( Zout    .*( sin(kL+theta1-theta2)./(sin(theta1).*sin(theta2) ) ) ...
     +1i*Z2 * sin(kL+theta1)./sin(theta1) ); 

end

%%
% Pour memoire : fonctions pour un tuyau cylindrique (non utilisées ici)

function Z = Zin_cyl(ktab,L,a,Zout)
% calcul de l'impédance en inversant un système 2x2 pour les amplitudes A,B.
% 
% Condition en entrée, débit Qf imposé : 
% Q(0,t) = Qf exp (- i omega t) soit 
%       ( A - B ) / Z0 = Qf 
%
% Condition en sortie : 
% P(L,t)/Q(L,t) = Z0[A exp(ikL)+B exp(-ikL)]/[A exp(ikL) -B exp(-ikL)] = Zout 
%       A exp(ikL) [ Z0 -Zout ] + B exp(-ikl) [ Z0 + Zout] = 0
%
% Mise sous forme matricielle  M * [A;B] = Y
% 
% Pour utiliser cette fonction de manière vectorielle il faut faire ce
% calcul pour chaque valeur de ktab en faisant une boucle.
  global rho c
  Z0 = rho*c/(pi*a^2);
  for j=1:length(ktab)
    kL = ktab(j)*L;
    M = [ [1, -1] ; [exp(1i*kL)*(Z0-Zout(j)),exp(-1i*kL)*(Z0+Zout(j)) ]];
    Qf = 1;
    Y = [Qf*Z0 ; 0];
    X = M\Y;
    A = X(1);B=X(2);
    Z(j) = (A+B)/Qf;
  end
end

function Z = Zin_cyl_theo(ktab,L,a,Zout)
% calcul de l'impédance d'entrée avec la formule du cours (calcul analytique)
% Fonction vectorielle : marche si ktab et Zout sont des tableaux de valeur
  global rho c
  Z0 = rho*c/(pi*a^2);
  Z = Z0*(Zout - 1i*Z0*tan(ktab*L))./(-1i*Zout.*tan(ktab*L) + Z0);
end