
H = 0.2; % profondeur
gamma = 0.076;
rho = 1000;
g = 9.81;

lc = sqrt(gamma/(rho*g));  % 

k = logspace(-1,5,100);
omega = sqrt(tanh(k*H).*(k*g+gamma/rho*k.^3));
c = omega./k;

cg = ((tanh(k*H).*(g+3*gamma/rho*k.^2)+H*(1-tanh(k*H).^2).*(k*g+gamma/rho*k.^3)))./(2*omega);
cg1 = omega./(2*k).*((1+3*gamma*k.^2/(rho*g))./(1+gamma*k.^2/(rho*g)) + 2*k*H./sinh(2*k*H));

figure;semilogx(k,c,'r',k,cg,'b',k,cg1,'b*');
xlabel('k (m^-^1)');
ylabel('c,cg (m/s)');
title(['Wave and group velocity of surface waves in water (H = ',num2str(H),'m)'])
legend('c(k)','cg(k)');
