%%
%% Program WaveEquation_FiniteDifference_string.m
%% Computes the solution of the wave equation on a string of lenght L
%% with given initial conditions (requires two functions posinit and vitinit)
%%

clear all;
global L c;

% length and duration of integration
L = 10;
T = 1; mu = 1; c=sqrt(T/mu); 

stopTime = 2*L/c;

% time and space steps
dx = .005*L ;
dt = .0025*L/c;
Nx = L/dx + 1;
x = 0:dx:L;

% Set  current  and  past  to  the  graph  of  a  plucked  string
    Y_current = posinit(x);
    V_current = vitinit(x);
    Y_past = Y_current-V_current*dt;
    r = c*dt/dx 

figure(1);
subplot(2,1,1);
plot(x,Y_current,'r')
title('Initial shape (red)');
subplot(2,1,2);
plot(x,V_current,'b')
title('Initial velocity (blue)');
pause

% Loop over time
for t =0:dt:stopTime

% inner points : solve the wave equation
for j=2:Nx-1
Y_future(j) = r^2*(Y_current(j-1)+Y_current(j+1))+2*(1-r^2)*Y_current(j)-Y_past(j);
end


% boundary condition x=0:
    Y_future(1) = 0;
    Y_future(Nx) = 0; 

% Plot  the  graph  after  every  20 th  frame
if mod( t/dt ,  20) == 0   
    figure(1);
    plot( x ,  Y_current,'r');
    V_current = (Y_future-Y_past)/(2*dt);
    subplot(2,1,1);
    plot( x ,  Y_current,'r');
    xlim( [ 0 L ]);% -.3/c .3/c ] );
    title('String position Y(x) (red) ');
    subplot(2,1,2);
    plot( x ,  V_current,'b');
    title('String velocity V(x) (blue) ');
    xlim( [ 0 L ]);% -.3/c .3/c ] );
    pause(.001)
end
% Set  things  up  for  the  next  time  step
Y_past = Y_current ;
Y_current = Y_future ;
end

disp('max(|Y-Y0|) after one period :');
max(abs(Y_current-posinit(x)))





