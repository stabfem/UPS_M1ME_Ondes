% Programme WaveEquation_Fourier.m
% Ce programme fait partie du cours "Ondes" du M1ME, Paul Sabatier.
% Version 2023.

global L c
close all;

% physical parameters
L = 10; %length of string
T = 1; mu = 1; c=sqrt(T/mu); 

% computation of Fourier coefficients and frequencies
Nfourier = 100;

for m=1:Nfourier
coef_fourier_A(m) = 2/(m*pi*c) * integral( @(x)(vitinit(x).*sin(m*pi*x/L)),0,L);
coef_fourier_B(m) = (2/L) * integral( @(x)(posinit(x).*sin(m*pi*x/L)),0,L);    
omega(m) = m*pi*c/L;
end




% plots the Fourier coefficients in figure 20


max_vertical_range = 2*max(max(abs(coef_fourier_A)), max(abs(coef_fourier_B)));
min_vertical_range = 1e-4*max_vertical_range;

min_horizontal_range = 0;
max_horizontal_range = 20;
figure(20);
semilogy(omega/omega(1),abs(coef_fourier_A),'bo',omega/omega(1),abs(coef_fourier_B),'ro') 
axis( [ min_horizontal_range max_horizontal_range min_vertical_range max_vertical_range ] )
ylabel('|A_n|, |B_n|')
xlabel('f_n/f_1')
title('Fourier Coefficients |A_n| (blue) and |B_n| (red) as function of frequencies');

%hold off;
%subplot(2,1,2);
%plot(1:Nfourier,omega/(2*pi),'o');
%xlim([0,10]);
%title('Frequencies f_n = \omega_n/(2 \pi)')



% construction of solution at instant t;
Nx = 500;
dx =L/Nx;
x = 0:dx:L;
Y0 = posinit(x);
V0 = vitinit(x);

% plot in figure 10
figure(10);
subplot(2,1,1);
plot(x,Y0,'r')
title('String position  (red)'); 
subplot(2,1,2);
plot(x,V0,'b')
title('String velocity (blue)'); 
pause;

Tmax = 2*L/c;
dt = 0.01*Tmax;

for t = 0:dt:Tmax
    Yt = 0*x;
    Vt = 0*x;
    for m=1:Nfourier
        Yt = Yt + sin(m*pi*x/L).*  ...
        (coef_fourier_A(m).*sin(omega(m)*t)+coef_fourier_B(m).*cos(omega(m)*t));
        Vt = Vt + sin(m*pi*x/L).*omega(m).*  ...
        (coef_fourier_A(m).*cos(omega(m)*t)-coef_fourier_B(m).*sin(omega(m)*t));
    end
    figure(10);
    subplot(2,1,1);
    plot(x,Y0,'r:',x,Yt,'r');
    title('String position (red)'); 
    subplot(2,1,2);
    plot(x,V0,'b:',x,Vt,'b');
    title('String velocity (blue)'); 
    axis([0 L -1 1]);
    pause(0.01);
end 

%% Représentation du signal acoustique U(t) et de son spectre


for m=1:Nfourier
coef_fourier_U(m) = m*pi/(L)*(coef_fourier_A(m)+1i*coef_fourier_B(m));
end

max_vertical_range = 2*max(abs(coef_fourier_U));
min_vertical_range = 1e-4*max_vertical_range;
min_horizontal_range = 0;
max_horizontal_range = 20;
figure(41);
semilogy(omega/omega(1),abs(coef_fourier_U).^2,'ko') 
axis( [ min_horizontal_range max_horizontal_range min_vertical_range max_vertical_range ] )
ylabel('|U_n|^2')
xlabel('f_n/f_1')
title('Power spectrum of acoustic signal (|U_n|^2 as function of frequencies)');
%%
dt = Tmax/200;
ttab = 0:dt:Tmax;
Ut = 0*ttab;
figure(40);
for n=1:length(ttab)
  for m=1:Nfourier
        Ut(n) = Ut(n) + (m*pi/L)*  ...
        (coef_fourier_A(m).*sin(omega(m)*ttab(n))+coef_fourier_B(m).*cos(omega(m)*ttab(n)));
  end
end
plot(ttab,Ut);
ylabel('U(t)')
xlabel('t')
title('Reconstruction of acoustic signal');
