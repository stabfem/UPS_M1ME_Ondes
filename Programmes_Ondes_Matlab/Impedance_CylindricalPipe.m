
%% Impedance d'un tuyau acoustique cylindrique 
% comparaison entre plusieurs modelisations de l'impedance de sortie
close all;
global rho c
rho = 1.225;  % masse volumique (kg/m^3)
c = 0.34;     % vitesse du son (m / ms)
L = .6;       % longueur du tuyau (m)
a = .06;      % rayon du tuyau (m)
S = pi*a^2;   % section du tuyau (m^2)

%% calcul |Z(omega)| pour une gamme de omega (ou de frequence f)
omega = linspace(0,10,1000); % gamme de omega en rad/ms
ktab = omega/c;
Z0 = rho*c/S;
Delta = 0.81*a; % correction de longueur cas "flanged"
Zout0 = 0*ktab; % 
Zout1 = Z0*(-1i*Delta*ktab);
Zout2 = Z0*(-1i*Delta*ktab + ktab.^2*a^2/2);
% Figure
figure(1);hold off;
semilogy(omega/(2*pi)*1000,abs(Zin_cyl(ktab,L,a,Zout0)/Z0),'r');hold on;
semilogy(omega/(2*pi)*1000,abs(Zin_cyl(ktab,L,a,Zout1)/Z0),'b');hold on;
semilogy(omega/(2*pi)*1000,abs(Zin_cyl(ktab,L,a,Zout2)/Z0),'g');hold on;
semilogy(omega/(2*pi)*1000,abs(Zin_cyl_theo(ktab,L,a,Zout2)/Z0),'k:');hold on;
xlabel('f [Hz]','fontsize',18); ylabel('|Z_{in}|/Z_0','fontsize',18);ylim([1e-2 1e2])
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
legend({'Ideal open','End correction','End correction + radiation'},'location','SE','fontsize',16);
saveas(gcf,'Impedance_Pipe60cm.png');

%% calcul de la structure p'(x) et u'(x) (parties relles et imaginaire) 
%  pour la frequence correspondant a la 3e resonance : omega = 4.134 rad/ms
omega = 4.134;
k = omega/c;
Zout = Z0*(-1i*Delta*k + k^2*a^2/2);
[Zin,A,B] = Zin_cyl(k,L,a,Zout);
xT = linspace(0,L,500);
p = A*exp(1i*k*xT) + B*exp(-1i*k*xT);
u = 1/(rho*c)*(A*exp(1i*k*xT) - B*exp(-1i*k*xT));
% Figure
figure(2); hold off;
plot(xT,real(p),'r',xT,imag(p),'r--',xT,real(u),'b',xT,imag(u),'b--');
legend('Re(p'')','Im(p'')','Re(u'')','Im(u'')');
saveas(gcf,'StructureAxe_Theory_omega4134.png')


%% Comparaison avec les deux autres méthodes
load('WS_FinalTime.mat')
%       Ce fichier a été écrit par le programme WaveSimulator.m
%       il contient les tableaux suivants :
%           V_current   -> débit q'(x,FinalTime)
%           F_current   -> pression p'(x,FinalTime)
%           x           -> les abcisses correspondantes
                         %
M = importdata('Data_Axe.txt');
% Fichier généré par le programme FreeFem++.
Xaxis  = M(:,1) ;             % colonne 1 : x
Paxis = M(:,2)+1i*M(:,3) ;    % colonnes 2 et 3 : pression (parties réelle et imaginaire).
Uyaxis = M(:,4)+1i*M(:,5) ;   % colonnes 4 et 5 : vitesse  (parties réelle et imaginaire).
figure(3);
subplot(2,1,1);
plot(xT,imag(p),'r',Xaxis+L,imag(Paxis)/S,'g--',x,F_current(1:length(x)),'b:')
xlim([0 1]);xlabel('x');ylabel('p''');
legend('Theory','Finite elements','Temporal simulation','location','SE')
subplot(2,1,2);
plot(xT,imag(u),'r',Xaxis+L,imag(Uyaxis)/S,'g--',x,V_current(1:length(x))/S,'b:')
xlim([0 1]);xlabel('x');ylabel('u''');
saveas(gcf,'StructureAxe_Comparison.png')


%% Et voici la fonction Zin_cyl
function [Z,A,B] = Zin_cyl(ktab,L,a,Zout)
% calcul de l'impédance en inversant un système 2x2 pour les amplitudes A,B.
% 
% Condition en entrée, débit Qf imposé : 
% Q(0,t) = Qf exp (- i omega t) soit 
%       ( A - B ) / Z0 = Qf 
%
% Condition en sortie : 
% P(L,t)/Q(L,t) = Z0[A exp(ikL)+B exp(-ikL)]/[A exp(ikL) -B exp(-ikL)] = Zout 
%       A exp(ikL) [ Z0 -Zout ] + B exp(-ikl) [ Z0 + Zout] = 0
%
% Mise sous forme matricielle  M * [A;B] = Y
% 
% Remarque : cette fonction peut etre utilisee de deux manieres
%   1/ Zin = Zin_cyl(k,L,a,Zout)
%      où ktab et Zout sont des tableaux de valeur (vecteurs)
%   2/ [Zin,A,B]
%      ou k et Zout sont scalaires (une seule valeur). Cette seconde methode permet 
%      de recuperer aussi les amplitudes A et B pour tracer la structure spatiale

  global rho c
  Z0 = rho*c/(pi*a^2);
  for j=1:length(ktab)
    kL = ktab(j)*L;
    M = [ [1, -1] ; [exp(1i*kL)*(Z0-Zout(j)),exp(-1i*kL)*(Z0+Zout(j)) ]];
    Qf = 1;
    Y = [Qf*Z0 ; 0];
    X = M\Y;
    A = X(1);B=X(2);
    Z(j) = (A+B)/Qf;
  end
end

function Z = Zin_cyl_theo(ktab,L,a,Zout)
% calcul de l'impédance d'entrée avec la formule du cours (calcul analytique)
% Fonction vectorielle : marche si kL et Zout sont des tableaux de valeur
  global rho c
  Z0 = rho*c/(pi*a^2);
  Z = Z0*(Zout - 1i*Z0*tan(ktab*L))./(-1i*Zout.*tan(ktab*L) + Z0);
end