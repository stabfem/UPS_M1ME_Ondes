%%
%% Programme pour generer la figure de l'exercice 5.1.2

% tout d'abord lancer OndesEvolution.m, selectionner cas 4, L = 24, T = 4
xlim([-2 10]);ylim([-.1 .1]);
saveas(gcf,'OndeEvol_4s.png');
pause;
xlim([.5 2]);
saveas(gcf,'OndeEvol_4s_zoom.png');

%% puis T = 6
xlim([-2 10]);ylim([-.1 .1]);
saveas(gcf,'OndeEvol_6s.png');
pause;
xlim([.75 3]);
saveas(gcf,'OndeEvol_6s_zoom.png');

%% TF eta0

k = linspace(-200,200,1e4);Fh = sqrt(pi)*a*exp(-k.^2*a^2/4);
figure(400); plot(k,Fh)
xlabel('k'); ylabel('$\hat{\eta}(k)$','interpreter','latex')
saveas(gcf,'OndeEvol_0_TF.png');