%%
% This program traces the dispersion relation omega(k), the phase velocity c(k)
% and group velocity cg(k) for gravity/capillary waves in finite length

%% parameters
H =  0.2; % 20 metres
g = 9.81;
gamma = 0.076; % surface tension of air-water
rho = 1000; % for water

%% compute and plot omega(k)
k = logspace(-2,5,800); % vector containing an array of k values ranging from 10^-3 to 10^5 (with log scaling)
omega = sqrt((k*g+gamma/rho*k.^3).*tanh(k*H));
figure(11);
subplot(2,1,1);
loglog(k,omega);
xlabel('k [rad/m]');ylabel('\omega [rad/s]');
subplot(2,1,2);
loglog(2*pi./k,2*pi./omega);
xlabel('\lambda (m)');ylabel('T (s)');
saveas(gcf,['Reldisp_H',num2str(H),'.png']); % save figure as graphical file

%% compute and plot c and cg
k = logspace(-2,5,800);
c = omega./k;

% group velocity using finite-difference
cg = diff(omega)./diff(k); %  equivalent to cg(i) = (omega(i+1)-omega(i))/(k(i+1)-k(i))
cg = [cg(1), cg]; % add one point because array cg computed above has one point less than array k
% other method using analytical formula
cgtheo = ((g+3*gamma/rho*k.^2).*tanh(k*H)+((g*k+gamma/rho*k.^3)*H./cosh(k*H).^2))./(2*omega);

% plot
figure(12);hold off;
loglog(k,c,'b');
hold on;
loglog(k,cg,'r',k,cgtheo,'k:')
xlabel('k [rad/m]');ylabel('c,c_g [m/s]');
legend('c','c_g (finite diff.)','c_g (analytical formula)')
saveas(gcf,['WaveVelocity_H',num2str(H),'.png']); % save figure as graphical file


%% Same figure in linear plot
k = linspace(.01,1,1000);
omega = sqrt((k*g+gamma/rho*k.^3).*tanh(k*H));
c = omega./k;
cgtheo = ((g+3*gamma/rho*k.^2).*tanh(k*H)+((g*k+gamma/rho*k.^3)*H./cosh(k*H).^2))./(2*omega);

% plot
figure(22);hold off;
set(gcf, 'Position',  [100, 100, 500, 250])
plot(k,c,'b');
hold on;
plot(k,cgtheo,'r')
xlabel('k [rad/m]');ylabel('c,c_g [m/s]');
legend('c','c_g')
saveas(gcf,['WaveVelocity_lin_H',num2str(H)],'png'); % save figure as graphical file
