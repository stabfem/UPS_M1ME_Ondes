% Programme WaveEquation_Fourier.m
% Ce programme fait partie du cours "Ondes" du M1ME, Paul Sabatier.
% Version 2023.

global L c
close all;

% physical parameters
L = 10; %length of string
T = 1; mu = 1; c=sqrt(T/mu); 

% computation of Fourier coefficients and frequencies
Nfourier = 200;

for m=1:Nfourier
coef_fourier_A(m) = 2/(m*pi*c) * integral( @(x)(vitinit(x).*sin(m*pi*x/L)),0,L);
coef_fourier_B(m) = (2/L) * integral( @(x)(posinit(x).*sin(m*pi*x/L)),0,L);    
omega(m) = m*pi*c/L;
tau = 0.001; % VISCO : constante du modele viscoelastique
lambda(m) = -tau*(m*pi/L)^2*c^2/2; % VISCO : taux d'amortissement viscoélastique
end




% plots the Fourier coefficients in figure 20


max_vertical_range = 2*max(max(abs(coef_fourier_A)), max(abs(coef_fourier_B)));
min_vertical_range = 1e-4*max_vertical_range;

min_horizontal_range = 0;
max_horizontal_range = 20;
figure(20);
semilogy(omega/omega(1),abs(coef_fourier_A),'bo',omega/omega(1),abs(coef_fourier_B),'ro') 
axis( [ min_horizontal_range max_horizontal_range min_vertical_range max_vertical_range ] )
ylabel('|A_n|, |B_n|')
xlabel('f_n/f_1')
title('Fourier Coefficients |A_n| (blue) and |B_n| (red) as function of frequencies');

%hold off;
%subplot(2,1,2);
%plot(1:Nfourier,omega/(2*pi),'o');
%xlim([0,10]);
%title('Frequencies f_n = \omega_n/(2 \pi)')



% construction of solution at instant t;
Nx = 500;
dx =L/Nx;
x = 0:dx:L;
Y0 = posinit(x);
V0 = vitinit(x);

% plot in figure 10
h = figure(10); %VISCO : use variable h to generate video
subplot(2,1,1);
plot(x,Y0,'r')
title('String position  (red)'); 
subplot(2,1,2);
plot(x,V0,'b')
title('String velocity (blue)'); 
    % VISCO : ecriture d'un fichier video ; initialisation
    filename = 'String_Fourier_Viscoelasticity.gif';
    set(gca,'nextplot','replacechildren');
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
pause;

% VISCO : ON REDEFINIT LES INTERVALLES DE TEMPS POUR LES FIGURES
Tperiod = 2*L/c;
Tmin =  00*Tperiod;
Tmax = 2*Tperiod;
dt = 0.01*(Tmax-Tmin);

for t = Tmin:dt:Tmax
    Yt = 0*x;
    Vt = 0*x;
    for m=1:Nfourier
        Yt = Yt + sin(m*pi*x/L).*  ...
        (coef_fourier_A(m).*sin(omega(m)*t)+coef_fourier_B(m).*cos(omega(m)*t))*exp(lambda(m)*t);
        Vt = Vt + sin(m*pi*x/L).*omega(m).*  ...
        (coef_fourier_A(m).*cos(omega(m)*t)-coef_fourier_B(m).*sin(omega(m)*t))*exp(lambda(m)*t);
    end
    figure(10);
    subplot(2,1,1);
    plot(x,Y0,'r:',x,Yt,'r');
    title('String position (red)'); 
    subplot(2,1,2);
    plot(x,V0,'b:',x,Vt,'b');
    title('String velocity (blue)'); 
    axis([0 L -1 1]);
    pause(0.01);
      % VISCO : ecriture d'un fichier video
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      imwrite(imind,cm,filename,'gif','WriteMode','append'); 
end 

%% Représentation du signal acoustique U(t) et de son spectre


for m=1:Nfourier
coef_fourier_U(m) = m*pi/(L)*(coef_fourier_A(m)+1i*coef_fourier_B(m));
end

max_vertical_range = 2*max(abs(coef_fourier_U));
min_vertical_range = 1e-4*max_vertical_range;
min_horizontal_range = 0;
max_horizontal_range = 20;
figure(41);
semilogy(omega/omega(1),abs(coef_fourier_U).^2,'ko') 
axis( [ min_horizontal_range max_horizontal_range min_vertical_range max_vertical_range ] )
ylabel('|U_n|^2')
xlabel('f_n/f_1')
title('Power spectrum of acoustic signal (|U_n|^2 as function of frequencies)');
%%

% VISCO : ON REDEFINIT LES INTERVALLES DE TEMPS POUR LES FIGURES
Tmin =  100*Tperiod;
Tmax = 110*Tperiod;
dt = (Tmax-Tmin)/1000;
ttab = Tmin:dt:Tmax;
Ut = 0*ttab;
figure(40);
for n=1:length(ttab)
  for m=1:Nfourier
        Ut(n) = Ut(n) + (m*pi/L)*  ...
        (coef_fourier_A(m).*sin(omega(m)*ttab(n))+coef_fourier_B(m).*cos(omega(m)*ttab(n)))*exp(lambda(m)*ttab(n));
  end
end
plot(ttab,Ut);
ylabel('U(t)')
xlabel('t')
title('Reconstruction of acoustic signal');
