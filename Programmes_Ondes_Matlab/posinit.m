function y= posinit(x) 
global L

%% Pour la corde pincée :
d = .15*L;  % position du pincement
e = 1;      % amplitude du pincement
y = (x<d).*e.*x/d + (x>=d).*e/(L-d).*(L-x);

% Explanation : this is equivalent to
%if(x<h)
%    y =x/h;
%else
%    y=(L-x)/(L-h);
%end
% except that this simplest syntax is not vectorial ! 
% ( the if/else/end will work if x is a single value but not if x is an array of values)
% The main programs used here require the function to work in vectorial syntax.
 
%% Pour la  corde frappée :
%y = 0.*x; 


end
