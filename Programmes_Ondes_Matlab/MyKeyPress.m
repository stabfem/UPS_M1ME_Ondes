function MyKeyPressListener
hFig = figure;
hAxes = gca;
set(hFig,'WindowKeyPressFcn',@keyPressCallback);
    function keyPressCallback(source,eventdata)
        % determine the key that was pressed
        keyPressed = eventdata.Key;
        if strcmpi(keyPressed,'rightarrow')
            C = [0 2 4; 8 10 12; 16 18 20];
            imagesc(C, 'Parent', hAxes);
        elseif strcmpi(keyPressed,'leftarrow')
            C = [0 2 4 6; 8 10 12 14; 16 18 20 22];
            imagesc(C, 'Parent', hAxes);
        end
    end
end