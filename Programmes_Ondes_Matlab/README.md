
Ce répertoire contient un ensemble de programmes, illustrant 
le cours "Ondes dans les fluides" du M1 ME de l'UPS.
Certains des programmes sont utilisés en TP, d'autres 
seulement en cours et en TD.

Les programmes sont écrits en Matlab/Octave, et également en FreeFem++

Programmes concus par David Fabre, avec des contributions 
de Frédéric Moulin, Laurent Lacaze, Olivier Thual.

Contenu de ce répertoire :
 
 1a. Ondes sur les cordes
 - WaveEquation_dAlembert.m
 - WaveEquation_Fourier.m
 - WaveEquation_FiniteDifference_string.m
 Sous-programmes utilisés par ceux-ci :
 	- posinit.m
	- vitinit.m
 	- dAlembert_F.m
	- dAlembert_G.m
	- intvitinit.m

1b. Programme résolvant l'équation des ondes 1D dans un cadre général,
incluant les tuyaux acoustiques, les cordes et les canaux de faible profondeur.
- WaveSimulator.m 

2a. Ondes acoustiques
- Visualisation_Reflexion_Transmission_Oblique_Wave.m 

2b. Ondes acoustiques : programmes FreeFem++

3. Ondes de surface
- OndesEvolution.m
- OndesdeSurface_RelDisp.m
- SaintVenant.m
- Sillage.m
Sous-programmes utilisés par ceux-ci :
	- AvancementCaracteristiques.m
	- RightHandSideMomentum.m
	- SWexactLL.m
	- VariablesBordMaille.m

4. Ondes dans les solides
- Visualisation_SolidWaves.m

