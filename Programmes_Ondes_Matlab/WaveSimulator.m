function WaveSimulator()
%
% Program to solve the 1D wave equation considering various physical
% problems, and various type of boundary conditions.
% How to use :
%  - Select problemtype, boundary conditions, initial conditions
%    and numerical parameters in the preamble
%  - Change properties of the medium in function cZ defined at the bottom
%  - Launch the program, redispose the figures so that they do not overlap
%    and type any key to launch the simulation
%
%    Written by D. Fabre, 2014-2023, distributed freely under GNU licence.

global L x Z c xstag Zstag cstag dt dx Rout
%close all

%% Definition of the problem
problemtype = 'pipe';
% allowed values are 'string', 'string1disc', "string2disc',
%                    'pipe','pipe1disc','pipe2disc'
%                    'acousticuniform','acousticdisc'
%                    'canal1disc','canalprog'
%
% see in function cZ at the end of this program
% to change the parameters, and add your own cases !

%% Definition of the boundary conditions
BC_0  = 'P0_Pulse';
% This is the boundary condition at inlet (x=0).
% Allowed values include :
%   'Y0'                        : Imposed sinusoidal displacement
%   'V0'                        : Imposed sinusoidal velocity/flow rate
%   'F0' | 'P0'                 : Imposed sinusoidal force/pressure
%   'incident' | 'I0'           : Incident sinusoidal wave coming from the region x<0
%   'pulse' | 'Y0_Pulse'        : Gaussian pulse imposed as displacement at the inlet.
%   'F0_Pulse' | 'P0_Pulse'     : Gassian pulse imposed as pressure/force
%   at the inlet++
%   'Incident_Pulse'|'I0_Pulse' : Gaussian incident pulse coming from the region x<0
%   'transparent' | 'noreflect' : Non-reflexion condition


% parameters for sinusoidal forcing (Y0 or F0 or I0 or V0)
omega = 4.1;
Amp = 1;
% parameters for 'pulse'-type forcings
t1 = .5;
ta = .1;
AmpPulse = 1;

BC_L = 'radiation_flanged';
% This is the boundary condition at outlet (x=L).
% Allowed values include :
%   'Fixed'  | 'closed'         : fixed string (or closed pipe)
%   'Free'   | 'open'           : free string (or ideally open pipe)
%   'transparent' | 'noreflect' : Radiation condition
%   'impedance'                 : We specity an outlet impedance.
%   'incident_pulse'|'I0_Pulse' : Gaussian incident pulse coming from the region x>L
%   'radiation'                 : Radiation condition in a 3D domain (full)
%   'radiation_flanged'         : Radiation condition in a 3D domain (flanged)

% parameter for 'Impedance'
Zout = 10;

%% Definition of the initial condition
Initial_Condition = 'Zero';
% allowed values :
% 'Zero'
% 'Gaussian'
% 'GaussianY'
% 'custom' -> use posinit, vitinit

% parameters for a Gaussian condition
InitGausD= 0.1; InitGausLoc=0.5; InitGausAmp = 1;

%% Numerical parameters relevant to the time and space discretization

Nx = 401; % Number of spatial points
% spatial step is deduced subsequently by dx = L/(Nx-1);
dt = .003 ;

%% Numerical parameters defining how to plot the results
FinalTime = 10; % total duration of the simulation
NP.fig1 = 1; % to plot string shape on figure 1
NP.plotstep = 20; % number of time steps between two plots in figure 1
NP.wait = 0; % set to 1 to advance step-by-step ; to zero to advance continuously
NP.fig1generatemovie = 1; % to generate a movie from figure 1;
NP.movienamefile = 'WaveSimulator.gif';
NP.figY = 0; % to plot Y in figure 1
NP.figV = 1; % to plot V as well as Y in figure 1
NP.figT = 1; % to plot F as well as Y in figure 1
NP.fig2 = 1; % to plot various statistics in figure 2
NP.fig2inst = 1; % to plot instantaneous quantities in figure 2
NP.fig2mean = 0; % to plot instantaneous quantities in figure 2
NP.figVplusVmoins=0; % to decompose Vplus/vmoins in figure 2
NP.fig3 = 1; % to plot inlet/outlet quantities on figure 3
NP.Tscalefig3 = 10;
NP.fig4 = 0; % to plot C and Z in figure 4
NP.fig5 = 0; % to plot energy in figure 5
NP.problemclass = 'pipe'; % use 'string', 'pipe' or 'canal' to have the right legends on the figures
%global stopsignal;
%stopsignal = 0;

% Miscleanous parameters
NP.smoothingtime = 100*dt; % to soften the starting (useful with Y0)
NP.N_averaging = round(2*pi/omega/dt); % number of previous instants to compute time-averages
NP.tpause = 0.02; % pause after each plot to let time for figure refreshing
NP.FinalTime = FinalTime; % we also need to know this one in the post-processing routine

%%% END OF PARAMETERS ; THE REMAINDER SHOULD NOT BE MODIFIED

%% Build the mesh and the properties of the medium

% First build a full mesh whith twice the number of points
[xF,cF,ZF] = cZ(problemtype,2*Nx-1);
% We deduce the mesh at principal points (x_i, i=1 to Nx)
x = xF(1:2:end);
c = cF(1:2:end);
Z = ZF(1:2:end);
dx = x(2)-x(1);
% And the staggered mesh (intermediate points x_{i+1/2} plus extremities)
xstag = xF([ 1, 2:2:end-1, end]);
cstag = cF([ 1, 2:2:end-1, end]);
Zstag = ZF([ 1, 2:2:end-1, end]);



%% initial conditions
switch(lower(Initial_Condition))
    case('zero')
        Y_current = 0*x; % here you can also use posinit(x) as in TP1 !
        V_current = 0*x; % here you can also use vitinit(x) as in TP1 !
    case('gaussiany')
        Y_current = InitGausAmp*exp(-(x-InitGausLoc).^2/InitGausD^2);
        V_current = 0*x;
    case('gaussian')
        Y_current = -InitGausAmp*erf((x-InitGausLoc)/InitGausD);
        V_current = 0*x;
    case('custom')
        Y_current = posinit(x);
        V_current = vitinit(x);
    otherwise
        error('Initial condition type not recognized !')
end

F_current(2:Nx) = -cstag(2:Nx).*Zstag(2:Nx).*diff(Y_current)/dx;
F_current(1) = c(1)*Z(1)*(3*Y_current(1)-4*Y_current(2)+Y_current(3))/(2*dx);
F_current(Nx+1) = -c(Nx)*Z(Nx)*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx);
Y_past = Y_current-dt*V_current;
Y_future = Y_current+dt*V_current;

t= 0;
if (NP.fig4==1)
    plotCZ(problemtype,L)
end
PostProcessingAndPlot(t,Y_current,V_current,F_current,NP);



%% Defines a few object needed in the time loop
r = c*dt/dx;
rZ = c./Z*dt/dx;
rstag = cstag.*Zstag*dt/dx;
r0 = c(1)*dt/dx;
rL = c(Nx)*dt/dx;

%YNstack = [0 0 0 0 0]; % five previous time steps ; needed for radiation condition

disp(' ');disp('## Program Wavesimulator.m ready to run');disp(' ');
disp(['Selected case                : ',problemtype]);
disp(['Inlet boundary condition     : ',BC_0]);
if any(strcmp({'I0','P0','F0','Y0','V0'},BC_0))
    disp(['    -> Frequency omega = ',omega]);
end
disp(['Outlet boundary condition    : ',BC_L]);
disp(['Initial condition            : ',Initial_Condition]);disp(' ');
disp(['numerical stability criterion : max(r) = ' num2str(max(abs(r)))])
if(max(abs(r))>1)
    disp('WARNING : max(r)  > 1 ! reduce dt to avoid intability');
else
    if(max(abs(r))<0.5)
        disp('max(r)  < 1 ; stability criterion is largely satisfied ; you may increase the time step dt to speed up the code ');
    end
end
disp(' ');
disp('Position figures so that they do not overlap and type any key to start ! ')


pause % wait for key press

%% Main Loop over time
for t =0:dt:FinalTime

    % inner points : solve the wave equation
    for j=2:Nx-1
        Y_future(j) = rZ(j)*(rstag(j)*Y_current(j-1)+rstag(j+1)*Y_current(j+1)-(rstag(j)+rstag(j+1))*Y_current(j))...
            + 2*Y_current(j)-Y_past(j);
    end

    % boundary condition x=0:
    switch(lower(BC_0))
        case {'fixed','closed'}
            Y_future(1) = 0;
        case {'free','open'}
            Y_future(1) = 2*r0^2*Y_current(2)+2*(1-r0^2)*Y_current(1)-Y_past(1) ;
        case {'f0','p0'}
            Y_future(1) = 2*r0^2*Y_current(2)+2*(1-r0^2)*Y_current(1)-Y_past(1)+2*r0^2*dx*Amp*sin(omega*(t))/(Z(1)*c(1));
        case {'y0'}
            Y_future(1)=Amp*sin(omega*(t+dt))*tanh((t+dt)/NP.smoothingtime); % the smoothing period is to make the start less abrupt
        case {'v0'}
            Y_future(1)=Amp*(cos(omega*(t+dt))-1)*tanh((t+dt)/NP.smoothingtime)/omega; % the smoothing period is to make the start less abrupt
        case {'i0','incident'}
            Y_future(1) = (2*Y_current(1)+(r0-1)*Y_past(1)+2*r0^2*(Y_current(2)-Y_current(1)))/(1+r0)...
                -Amp/Z(1)*sin(omega*t)*dt*4*r0/(1+r0);
        case {'pulse'}
            Y_future(1) = AmpPulse*exp(-((t+dt)-t1)^2/ta^2);
        case {'v0_pulse'}
            Y_future(1) = AmpPulse*sqrt(pi)/2*(erf((t+dt-t1)/ta)+1)*ta;
        case {'f0_pulse','p0_pulse'}
            Y_future(1) = 2*r0^2*Y_current(2)+2*(1-r0^2)*Y_current(1)-Y_past(1)+2*r0^2*dx...
                *AmpPulse*exp(-(t-t1)^2/ta^2)/(Z(1)*c(1));
        case {'i0_pulse','incident_pulse'}
            Y_future(1) = (2*Y_current(1)+(r0-1)*Y_past(1)+2*r0^2*(Y_current(2)-Y_current(1)))/(1+r0)...
                +AmpPulse/(Z(1)*c(1))*exp(-(t-t1)^2/ta^2)*dt*4*c(1)*r0/(1+r0);
        case {'transparent','noreflect'}
            Y_future(1) = (2*Y_current(1)+(r0-1)*Y_past(1)+2*r0^2*(Y_current(2)-Y_current(1)))/(1+r0);
        otherwise
            disp('WARNING : left boundary condition not implemented !');
    end%switch

    % boundary condition x=L:
    switch(lower(BC_L))
        case {'fixed','closed'}
            Y_future(Nx) = 0;
        case {'free','open'}
            Y_future(Nx) = 2*rL^2*Y_current(Nx-1)+2*(1-rL^2)*Y_current(Nx)-Y_past(Nx) ;
        case {'transparent','noreflect'}
            Y_future(Nx) = (2*Y_current(Nx)+(rL-1)*Y_past(Nx)+2*rL^2*(Y_current(Nx-1)-Y_current(Nx)))/(1+rL);
        case {'impedance'}
            rL = c(Nx)*dt/dx;
            rZZ = rL/Zout*Z(Nx);
            Y_future(Nx) = (2*rZZ*Y_current(Nx)+(rL^2-rZZ)*Y_past(Nx)+2*rL^2*rZZ*(Y_current(Nx-1)-Y_current(Nx)))/(rZZ+rL^2);
        case {'i0_pulse','incident_pulse'}
            Y_future(Nx) = (2*Y_current(Nx)+(rL-1)*Y_past(Nx)+2*rL^2*(Y_current(Nx-1)-Y_current(Nx)))/(1+rL)...
                -AmpPulse/(Z(Nx)*c(Nx))*exp(-(t-t1)^2/ta^2)*dt*4*c(Nx)*rL/(1+rL);
        case {'i0'}
            Y_future(Nx) = (2*Y_current(Nx)+(rL-1)*Y_past(Nx)+2*rL^2*(Y_current(Nx-1)-Y_current(Nx)))/(1+rL)...
                -Amp/Z(Nx)*sin(omega*t)*dt*4*rL/(1+rL);
        case('radiation_flanged')
            rL = c(Nx)*dt/dx;
            Zout = Z(end)*(omega/c(end))^2*Rout^2/2;
            rZZ = rL/Zout*Z(Nx);
            Y_future(Nx) = (2*rZZ*Y_current(Nx)+(rL^2-rZZ)*Y_past(Nx)+2*rL^2*rZZ*(Y_current(Nx-1)-Y_current(Nx)))/(rZZ+rL^2);

        case('radiation')
            rL = c(Nx)*dt/dx;
            Zout = Z(end)*(omega/c(end))^2*Rout^2/4;
            rZZ = rL/Zout*Z(Nx);
            Y_future(Nx) = (2*rZZ*Y_current(Nx)+(rL^2-rZZ)*Y_past(Nx)+2*rL^2*rZZ*(Y_current(Nx-1)-Y_current(Nx)))/(rZZ+rL^2);
            % other idea which does not work yet...
            %     Z1I = 0; % positive -> delay but drift ; negative -> advance, no drift
            %     Z2R = 1;
            %     Bn = 1/(2*dt^3)*[5, -18, 24, -14, 3]*YNstack'; % bi-acceleration
            %     Y_future(Nx) = 2*Y_current(Nx)-Y_past(Nx) ...
            %       + dt^2*Z1I*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx) ...
            %       + dt^2*Z2R*Bn;
            %     Y_future(Nx)
            %    YNstack = [Y_future(Nx), YNstack(1:4)];
        otherwise
            disp('WARNING : right boundary condition not implemented !');
    end%switch

    % end of main integration scheme ; next is for figure displays

    % deduce the velocity and force
    V_current = (Y_future-Y_past)/(2*dt);
    F_current(2:Nx) = -cstag(2:Nx).*Zstag(2:Nx).*diff(Y_current)/dx;
    F_current(1) = c(1)*Z(1)*(3*Y_current(1)-4*Y_current(2)+Y_current(3))/(2*dx);
    F_current(Nx+1) = -c(Nx)*Z(Nx)*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx);

    % Call the postprocessing subroutine
    PostProcessingAndPlot(t,Y_current,V_current,F_current,NP);

    if(NP.wait==1)
        pause
    end
    %if stopsignal
    %    error('exit loop')
    %end



    %% Set  things  up  for  the  next  time  step
    Y_past = Y_current ;
    Y_current = Y_future ;

end

% Last call to
PostProcessingAndPlot(-t,Y_current,V_current,F_current,NP);


end
%%
function [] = PostProcessingAndPlot(t,Y_current,V_current,F_current,NP)
%% This function handles the postprocessing

global x Z c xstag Zstag cstag dt dx

persistent tab_t tab_Y0 tab_F0 tab_V0 tab_V0plus tab_V0moins...
    tab_YL tab_FL tab_VL tab_VLplus tab_VLmoins ...
    tab_Power_0 tab_Power_0_plus tab_Power_L tab_Power_L_plus ...
    tab_Power_0_Av tab_Power_0_plus_Av tab_Power_L_Av tab_Power_L_plus_Av ...
    tab_TotalEnergy tab_TotalEnergy_Av ...
    minfig1Y maxfig1Y minfig1V maxfig1V ...
    minfig1F maxfig1F h1; 
persistent minfig2 maxfig2 minfig2b maxfig2b minfig3 maxfig3;

switch(NP.problemclass) % legendes des figures
    case('string')
        legendY = 'Y';legendT = 'F_y';legendV = 'V';
    case('pipe')
        legendY = 'X';legendT = 'p''';legendV = 'q';
    case('chanel')
        legendY = 'X';legendT = '\eta';legendV = 'Q';
    otherwise
        legendY = 'Y';legendT = 'F_y';legendV = 'V';
end


%%

if(t==0)
    % Prepare the figures


    if (NP.fig2==1)
        hFig = figure(2);
%        set(hFig,'WindowKeyPressFcn',@keyPressCallback);
        %   subplot(2,1,1);
        hold off;
        plot(0,0);
        xlim( [ 0 NP.FinalTime ] );
        title('Energy flux : Incident (red), reflected (green), transmitted (blue)')% 'net at inlet (black), net at outlet (cyan)  ');
        %    subplot(2,1,2);
        %    hold off;
        %    plot(0,0);
        %    xlim( [ 0 NP.FinalTime ] );
        %    title('Total Energy in domain');
    end

    if (NP.fig3==1)
        figure(3);
        subplot(2,1,1);
        hold off;
        plot(0,0);
        %   xlim( [ 0 NP.FinalTime  ] );
        title('Conditions at x=0 : Y(0,t) (red) ; V(0,t) (blue) ; F(0,t) (green)   ');
        subplot(2,1,2);
        hold off;plot(0,0);
       % xlim( [ 0 NP.FinalTime  ] );
        title('Conditions at x=L : Y(L,t) (red) ; V(L,t) (blue),  F(L,t) (green) ');
    end


    if(NP.fig1)
        h1 = figure(1);
        %    set(hFig,'WindowKeyPressFcn',@keyPressCallback);
        Nsubfig1 = NP.figY+NP.figV+NP.figT;
        thesubfig = 1;
        if(NP.figY==1)
            subplot(Nsubfig1,1,thesubfig);hold off;
            plot(x,Y_current,'r');
            title([legendY,'(x,0)']);
            thesubfig = thesubfig+1;
        end
        if(NP.figT==1)
            subplot(Nsubfig1,1,thesubfig);hold off;
            plot(xstag,F_current,'g');
            %title('Transverse force (green)');
            title([legendT,'(x,t=0)']);
            thesubfig = thesubfig+1;
        end
        if(NP.figV==1)
            subplot(Nsubfig1,1,thesubfig);hold off;
            plot(x,V_current,'b');
            %title('Initial velocity (blue)');
            title([legendV,'(x,t=0)']);
        end
        if (NP.fig1generatemovie==1) % generate a movie from figure 1
            set(gca,'nextplot','replacechildren');
            frame = getframe(h1); 
            im = frame2im(frame); 
            [imind,cm] = rgb2ind(im,256); 
            imwrite(imind,cm,NP.movienamefile,'gif', 'Loopcount',inf); 
        end
    end


    % Initialisations for figures
    tab_t = []; tab_Y0 = []; tab_F0 = []; tab_V0 = [];
    tab_V0plus = []; tab_V0moins = [];
    tab_YL = []; tab_FL = []; tab_VL = [];
    tab_VLplus = []; tab_VLmoins = [];
    tab_Power_0 = []; tab_Power_0_plus = [];
    tab_Power_L = []; tab_Power_L_plus = [];
    tab_Power_0_Av = []; tab_Power_0_plus_Av = [];
    tab_Power_L_Av = []; tab_Power_L_plus_Av = [];
    tab_TotalEnergy = []; tab_TotalEnergy_Av = [];
    minfig1Y = 0; maxfig1Y = 0;minfig1V = 0; maxfig1V = 0;
    minfig1F = 0; maxfig1F = 0;
    minfig2 = -1; maxfig2 = 1;minfig2b=-0.1;maxfig2b=0.1;
    minfig3 = -1; maxfig3 = 1;


else

    %% Post-processing actions at each time step
    % First compute various statistics for figures 2 and 3

    tab_t = [tab_t abs(t)];
    tab_Y0 = [tab_Y0 Y_current(1)];
    tab_V0 = [tab_V0 V_current(1)];
    tab_F0 = [tab_F0 F_current(1)];

    V0plus = (Z(1)*V_current(1)+F_current(1))/(2*Z(1));
    V0moins = (Z(1)*V_current(1)-F_current(1))/(2*Z(1));
    tab_V0plus =  [tab_V0plus V0plus];
    tab_V0moins = [tab_V0moins V0moins];

    tab_YL = [tab_YL Y_current(end)];
    tab_VL = [tab_VL V_current(end)];
    tab_FL = [tab_FL F_current(end)];

    VLplus = (Z(end)*V_current(end)+F_current(end))/(2*Z(end));
    VLmoins = (Z(end)*V_current(end)-F_current(end))/(2*Z(end));

    tab_VLplus = [tab_VLplus VLplus];
    tab_VLmoins = [tab_VLmoins VLmoins];


    % Power at inlet and outlet (for figure 3)

    tab_Power_0 = [tab_Power_0 F_current(1)*V_current(1)];
    tab_Power_0_plus = [tab_Power_0_plus V0plus^2*Z(1)];
    tab_Power_L = [tab_Power_L V_current(end)*F_current(end)];
    tab_Power_L_plus = [tab_Power_L_plus VLplus^2*Z(end)];

    Nt = round(abs(t)/dt);

    Power_0_Av = mean(tab_Power_0(max([1,Nt-NP.N_averaging]):Nt));
    tab_Power_0_Av = [tab_Power_0_Av Power_0_Av];

    Power_0_plus_Av = mean(tab_Power_0_plus(max([1,Nt-NP.N_averaging]):Nt));
    tab_Power_0_plus_Av = [tab_Power_0_plus_Av Power_0_plus_Av];

    Power_L_Av = mean(tab_Power_L(max([1,Nt-NP.N_averaging]):Nt));
    tab_Power_L_Av = [tab_Power_L_Av Power_L_Av];
    Power_L_plus_Av = mean(tab_Power_L_plus(max([1,Nt-NP.N_averaging]):Nt));
    tab_Power_L_plus_Av = [tab_Power_L_plus_Av Power_L_plus_Av];

    TotalEnergy = dx/2*( .5*(V_current(1)^2*Z(1)/c(1)) ...
        + sum(V_current(2:end-1).^2.*Z(2:end-1)./c(2:end-1)) ...
        + .5*(V_current(end)^2*Z(end)/c(end)) ...
        + sum(F_current(2:end-1).^2./(Zstag(2:end-1).*cstag(2:end-1)))  );

    tab_TotalEnergy = [tab_TotalEnergy TotalEnergy];
    TotalEnergy_Av = mean(tab_TotalEnergy(max([1,Nt-NP.N_averaging]):Nt));
    tab_TotalEnergy_Av = [tab_TotalEnergy_Av TotalEnergy_Av];

    %% Figure 1 : Plot  the  graph  after  every  'NP.plotstep'  frame

    if (t<0||mod( t/dt , NP.plotstep) == 0)
        if(NP.fig1==1)
            Nsubfig1 = NP.figY+NP.figV+NP.figT;
            thesubfig = 1;
            figure(1);
            if(NP.figY==1)
                subplot(Nsubfig1,1,thesubfig);
                plot( x ,  Y_current,'r');
                plotgrid;
                title([legendY,'(x,t=' num2str(t), ')']);
                hold off;
                maxfig1Y = max([Y_current 1e-10 maxfig1Y]);
                minfig1Y = min([Y_current -1e-10 minfig1Y]);
                ylim([minfig1Y maxfig1Y]);
                thesubfig = thesubfig+1;
            end
            if(NP.figT==1)
                subplot(Nsubfig1,1,thesubfig);
                plot(xstag,F_current,'g' );
                plotgrid;
                %title(['F(x,t=' num2str(t), ')']);
                title([legendT,'(x,t=' num2str(t), ')']);
                hold off;
                maxfig1F = max([F_current 1e-10 maxfig1F]);
                minfig1F = min([F_current -1e-10 minfig1F]);
                ylim([minfig1F maxfig1F]);
                thesubfig = thesubfig+1;
            end
            if(NP.figV==1)
                subplot(Nsubfig1,1,thesubfig);
                plot(x,V_current,'b' );
                plotgrid;
                %title(['V(x,t=' num2str(t), ')']);
                title([legendV,'(x,t=' num2str(t), ')']);
                hold off;
                maxfig1V = max([V_current 1e-10 maxfig1V]);
                minfig1V = min([V_current -1e-10 minfig1V]);
                ylim([minfig1V maxfig1V]);
            end
            pause(NP.tpause)
            if (NP.fig1generatemovie==1) % generate a movie from figure 1
                frame = getframe(h1); 
                im = frame2im(frame); 
                [imind,cm] = rgb2ind(im,256); 
                imwrite(imind,cm,NP.movienamefile,'gif','WriteMode','append'); 
            end
        end

        %% Figure 2
        if (NP.fig2==1 )
            figure(2);
            %        subplot(2,1,1);
            if(NP.fig2mean==1)
                plot(tab_t,tab_Power_0_Av,'k','LineWidth',4);
            end
            hold on;
            if(NP.figVplusVmoins==1)
                plot(tab_t,tab_Power_0_plus_Av,'c','LineWidth',2);
                plot(tab_t,(tab_Power_0_Av-tab_Power_0_plus_Av),'m','LineWidth',2);
            end
            if(NP.fig2mean==1)
               plot(tab_t,tab_Power_L_Av,'r','LineWidth',4);
            end
            if(NP.figVplusVmoins==1)
                plot(tab_t,tab_Power_L_plus_Av,'b','LineWidth',2);
                plot(tab_t,(tab_Power_L_Av-tab_Power_L_plus_Av),'g','LineWidth',2);
            end
            if(NP.fig2inst==1)
                plot(tab_t,tab_Power_0,'k:','LineWidth',1);
            end
            if(NP.figVplusVmoins==1&&NP.fig2inst==1)
                plot(tab_t,tab_Power_0_plus,'c:','LineWidth',1);
                plot(tab_t,(tab_Power_0-tab_Power_0_plus),'m:','LineWidth',1);
            end
            if(NP.fig2inst==1)
                plot(tab_t,tab_Power_L,'r:','LineWidth',1);
            end
            if(NP.figVplusVmoins==1&&NP.fig2inst==1)
                plot(tab_t,tab_Power_L_plus,'b:','LineWidth',1);
                plot(tab_t,(tab_Power_L-tab_Power_L_plus),'g:','LineWidth',1);
            end
            xlim( [ 0 NP.FinalTime ] );
            if(NP.figVplusVmoins==1)
                title('Energy flux at inlet and outlet');
                legend('P_0','P_0^+','P_0^-','P_L','P_L^+','P_L^-');
                %legend('P_0^+','P_0^-','P_L^+','P_L^-');
            else
                title('Net power at inlet (black) and at outlet (red)  ');
                legend('P_0','P_L');
            end
            hold off;
            pause(NP.tpause);
        end

        %%     figure 5 : energy in domain
        if (NP.fig5==1 )
            figure(22);
            plot(tab_t,tab_TotalEnergy,'r:','LineWidth',1);
            hold on;
            plot(tab_t,tab_TotalEnergy_Av,'r','LineWidth',2);
            title('Total Energy in domain');
            xlim( [ 0 NP.FinalTime ] );
            hold off;
        end

        %% Figure 3
        if (NP.fig3==1 )
            figure(3);hold off;
            if strcmp(NP.problemclass,'pipe') % post traitement pour pipe
                maxtfig3 = max(t,NP.Tscalefig3);
                mintfig3 = maxtfig3-NP.Tscalefig3;
                subplot(2,2,1);
                plot(tab_t,tab_V0,'b');
                title('at x=0 : q''(0,t) (blue)');
                xlim([ mintfig3 maxtfig3]);
                subplot(2,2,3);
                plot(tab_t,tab_F0,'g');
                title('at x=0 : p''(0,t) (green) ');
                xlim([ mintfig3 maxtfig3]);
                subplot(2,2,2);
                plot(tab_t,tab_VL,'b');
                title('at x=L : q''(L,t) (blue)');
                xlim([ mintfig3 maxtfig3]);
                subplot(2,2,4);
                plot(tab_t,tab_FL,'g');
                title('at x=L : p''(L,t) (green) ');
                xlim([ mintfig3 maxtfig3]);  
            else
                subplot(2,1,1);
            plot(tab_t,tab_Y0,'r',tab_t,tab_V0,'b',tab_t,tab_F0,'g');
            hold on;
            if(NP.figVplusVmoins)
                plot(tab_t,tab_V0plus,'c',tab_t,tab_V0moins,'m')
            end
            maxtfig3 = max(t,NP.Tscalefig3);
            mintfig3 = maxtfig3-NP.Tscalefig3;
            xlim([ mintfig3 maxtfig3]);
            if(NP.figVplusVmoins==0)
                title('Conditions at x=0 : Y(0,t) (red) ; V(0,t) (blue), F(0,t) (green) ');
            else
                title('Conditions at x=0 : Y(0,t) (red) ; V(0,t) (blue), F(0,t) (green), Vplus (cyan), Vmoins (magenta) ');
            end
            hold off;
            subplot(2,1,2);
            plot(tab_t,tab_YL,'r',tab_t,tab_VL,'b',tab_t,tab_FL,'g')
            hold on;
            if(NP.figVplusVmoins)
                plot(tab_t,tab_VLplus,'c',tab_t,tab_VLmoins,'m')
            end
            xlim([ mintfig3 maxtfig3 ]);
            if(NP.figVplusVmoins==0)
                title('Conditions at x=L : Y(L,t) (red) ; V(L,t) (blue), F(L,t) (green) ');
            else
                title('Conditions at x=L : Y(L,t) (red) ; V(L,t) (blue), F(L,t) (green), Vplus (cyan), Vmoins (magenta) ');
            end
            hold off;
          end
        end
    end
end
end
%%
function plotgrid()
global L X1 X2;

hold on;
if(~isempty(X1))
    plot( [X1 X1], [-1e12 1e12], 'k--');
end
if(~isempty(X2))
    plot([X2 X2], [-1e12 1e12], 'k--');
end
hold off;
end

%%
function plotCZ(problemtype,L)

%x = 0:L/500:L;
[x,c,Z,S,rho] = cZ(problemtype,500);

hFig = figure(4);
%set(hFig,'WindowKeyPressFcn',@keyPressCallback);
numsubplots=2;
if ~isempty(S)
    numsubplots=numsubplots+1;
end
if ~isempty(rho)
    numsubplots=numsubplots+1;
end
subplot(numsubplots,1,1);
plot(x,c);
title('wave velocity');xlabel('x');ylabel('c(x)');ylim([0 1.1*max(c)]);
nextsubplot =2;
if ~isempty(S)
    subplot(numsubplots,1,nextsubplot);
    plot(x,S);
    title('Cross-section area (for pipe)');xlabel('x');ylabel('S(x)');ylim([0 1.1*max(S)]);
    nextsubplot = nextsubplot+1;
end
if ~isempty(rho)
    subplot(numsubplots,1,nextsubplot);
    plot(x,rho);
    title('density (for acoustic medium)');xlabel('x');ylabel('\rho(x)');ylim([0 1.1*max(rho)]);
    nextsubplot = nextsubplot+1;
end
subplot(numsubplots,1,nextsubplot);
plot(x,Z);
title('specific impedance');xlabel('x');ylabel('Z(x)');ylim([0 1.1*max(Z)]);

end


%%
function [x,c,Z,S,rho] = cZ(problemtype,Nx)
global L X1 X2 Rout

% This function sets the physical properties of the 1D medium :
% x     mesh
% c     wave velocity (array with same dimension as x)
% Z     impedance of medium (array with same dimension as x)
% S     surface of the pipe/canal (not relevant for a string)
% rho   density of acoustic medium (not relevant for a string)
S = []; rho = [];

switch(problemtype)
    case('string')
        % case of a string with constant velocity
        T = 1;
        mu = 1;
        L = 1;
        x = linspace(0,L,Nx);
        c = sqrt(T/mu)*ones(size(x));
        Z = T./c;

    case('string1disc')
        % case of a string with a discontinuity;
        T = 1;
        c1 = 1;
        c2 = .2;
        X1 = .5;
        L = 1;
        x = linspace(0,L,Nx);
        c = c1*(x<X1)+c2*(x>=X1);
        Z = T./c;

    case('string2disc')
        % case of a string with a discontinuity;
        T = 1;
        c1 = 1;
        c2 = 4;
        c3 = 1;
        X1 = .5;
        X2 = .75;
        L = 1;
        x = linspace(0,L,Nx);
        c = c1*(x<X1)+c2*(x>=X1&x<X2)+c3*(x>=X2);
        Z = T/c;


    case('pipe')
        % cylindrical pipe, dimensional (a flute, L=60cm,r=1cm)
        c0 = 0.34;      % wave velocity in [m/ms]
        R0 = 0.06;      % radius
        S0 = pi*R0^2; % section [m^2]
        rho0 = 1.225;   % density [kg/m^3]
        Rout = R0; 
        L = 0.6+.85*Rout;        % Length  [m] including correction length
        x = linspace(0,L,Nx);
        c = c0*ones(size(x));
        Z = rho0*c0/S0*ones(size(x));
        S = S0*ones(size(x));
          

    case('pipeConCyl')
        c0 = 1;      % warning : this case is nondimensional
        rho0 = 1;   % density 
        Lcone = .2;     
        Lcyl = 1;
        Rin = 0.1;
        Rout = 0.5;
        L = Lcone+Lcyl+.61*Rout;        % Length including correction length
        x = linspace(0,L,Nx);
        RR = (x<Lcone).*(Rin+(Rout-Rin)/Lcone*x)+(x>=Lcone)*Rout;
        c = c0*ones(size(x));
        S = pi*RR.^2;
        Z = rho0*c./S;
       

    case('pipeadim')
        % cylindrical pipe, nondimensional
        c0 = 1;
        rho0 = 1;
        L = 1;
        R0 = 0.1; %  nondimensional radius
        S0 = pi*R0^2;
        x = linspace(0,L,Nx);
        c = c0*ones(size(x));
        Z = rho0*c0/S0*ones(size(x));
        S = S0*ones(size(x));
        Rout = R0;


    case('pipe1disc')
        % 2 connected cylindrical pipes (here nondimensional)
        L = 1;
        X1 = .5; % location of discontinuity
        c0 = 1;
        S1 = 1;
        S2 = 10;
        rho0 = 1;
        x = linspace(0,L,Nx);
        S = (x<X1)*S1+(x>=X1)*S2;
        c = c0*ones(size(x));
        Z = rho0*c0./S;
        Rout = sqrt(S(end)/pi);


    case('pipe2disc')
        % 3 connected cylindrical pipes (here nondimensional)
        L = 3;
        X1 = 1; % location of 1st discontinuity
        X2 = 2; % location of 2nd discontinuity
        c0 = 1;
        S1 = 1e-4;
        S2 = 10e-4;
        S3 = 1e-4;
        rho0 = 1;

        x = linspace(0,L,Nx);
        S = (x<X1)*S1+(x>=X1&x<X2)*S2+(x>=X2)*S3;
        c = c0*ones(size(x));
        Z = rho0*c0./S;
        Rout = sqrt(S(end)/pi);

    case('acousticuniform')
        % acoustic medium with uniform properties
        c0 = 1;
        S0 = 1;
        rho0 = 1;
        L = 1;
        x = linspace(0,L,Nx);
        rho = rho0*ones(size(x));
        c = c0*ones(size(x));
        Z = rho.*c/S0;

    case('acousticdisc')
        % acoustic medium with uniform properties
        L = 1;
        S0 = 1;
        rho1 = 1.225;%air
        c1 = 0.34;%air
        X1 = 0.75;
        rho2 = 10;%water
        c2 = 1.5;%water
        %rho2 = 1.77;%CO2
        %c2 = 0.26,;%CO2

        x = linspace(0,L,Nx);
        rho = rho1*(x<X1)+rho2*(x>=X1);
        c = c1*(x<X1)+c2*(x>=X1);
        Z = rho.*c/S0;


    case('canal1disc')
        % 2 connected canals
        % NB for canals the length/time/mass unities are kilometer, minute
        % and petagram (1 Petagram = 10^15 g = 10^12 kg)
        L = 1;   % 1km
        X1 = .5; % 500 meters
        X2 = .5; % 700 meters
        g = 9.81*60^2/1000; % 9.81 m^2/s converted in km^2/min
        rho = 1; % rho in Pg/km^3
        W = 1;
        H1 = 0.2; % 200m
        H2 = 0.01; % 10m
        x = linspace(0,L,Nx);
        H = (x<X1)*H1+(x>=X1)*H2;
        c = sqrt(g*H);
        Z = 1./(rho*W*sqrt(g^3*H));
        S = W*H;


    case('canalprog')
        % canal with progressive variation of height
        % NB for canals the length/time/mass unities are kilometer, minute
        % and petagram (1 Petagram = 10^15 g = 10^12 kg)
        X1 = .3; % 300 meters
        X2 = .7; % 700 meters
        g = 9.81*60^2/1000; % 9.81 m^2/s converted in km^2/min
        rho = 1; % rho in Pg/km^3
        W = 1;
        H1 = 0.05; % 50m
        H2 = 0.01; % 10m
        L = 1;
        x = linspace(0,L,Nx);
        H = (x<X1)*H1+(x>=X1&x<X2).*(H1+(H2-H1)*(x-X1)/(X2-X1))+(x>=X2)*H2;
        c = sqrt(g*H);
        Z = 1./(rho*W*sqrt(g^3*H));
        S = W*H;


        %case('custom')
        %  add your own case(s) here !

    otherwise
        error('Error : problemtype not recognized')
end
end


%function keyPressCallback(source,eventdata)
%        global stopsignal
% determine the key that was pressed
%        keyPressed = eventdata.Key;
%        if strcmpi(keyPressed,'s')
%            stopsignal=1;
%            error('program stopped by user')
%        end
%end

