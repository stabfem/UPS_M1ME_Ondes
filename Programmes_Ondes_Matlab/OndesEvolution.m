% from OLIVIER THUAL,
% http://thual.perso.enseeiht.fr/otapm/ond-surfli/index.htm
% modifi� en mars 2012 par FY moulin puis en 2017-2020 par D Fabre.
% permet le calcul de la propagation des ondes pour une condition initiale
% donn�e, par d�composition dans l'espace spectrale, et recomposition
% ult�rieure ? tout instant t par somme des ondes planes monochromatiques
% (dans cette version, on donne directement la condition initiale dans
% l'espace spectral)


function OndesEvolution
quarterfig = 0;
% il est possible de choisir diff?rentes ondes avec ce param?tre :
    % flagondes=1 : ondes de surface avec tension de surface (relation de dispersion compl?te)
    % flagondes=2 : ondes longues de gravit? (non dispersives donc !!!)
% PARAMETRES PHYSIQUES ET NUMERIQUE!c
% Il est possible de modifier ces ?l?ments physiques :
    L=20;           % longueur du domaine de calcul (en m) 
    Lvisu = 20; % longueur du domaine represente (en m) (on doit avoir L>Lvisu)
    profondeur=0.20; % profondeur d'eau H en metres
    amplitude = 1; % amplitude de la perturbation (arbitraire car le pb est lineaire)
% il est possible de choisir diff?rentes ondes avec ce param?tre :
    flagondes=1;     
    % flagondes=1 : ondes de surface avec tension de surface (relation de dispersion compl?te)
    % (DF : ne pas changer ce parametre)
% ce sont ici des param?tres purement num?riques :
    N=2^16;     % Nombre de points spatiaux pour le calcul (puissance de 2 pour acc?ler le calcul des FFT)
    TMAX = 20;
    M=200;       % nombre d'iterations temporelles
	dt=TMAX/M;     % pas de temps du calcul (en s) entre deux it?rations et sorties
    um=1.2*amplitude;    % echelle verticale pour affichage
% Ces param?tres peuvent ?tre chang?s seulement si on change de plan?te ou de liquide !!! :
    gravite=9.81;   % acceleration de la pesanteur en ms-2
    lam=0.0000001;   % facteur d'amortissement des ondes (exp(-lam*k^2*t)
	gamma=75.64e-3;    % tension de surface pour eau/air (Nm-1)
    rhoeau=1000.0;    % masse volumique de l'eau (kg/m3)
    % -----------------------------------
    
%clear all
%close all
%nomfilm='essai.avi';
%aviobj = avifile(nomfilm);
% flag=1 impulsion  2 paquet 
if (flagondes==1)
    fprintf('Simulation pour des ondes de surface gravit?-capillaires\n');
elseif (flagondes==2)
        fprintf('Simulation pour des ondes de gravit? longues\n');
elseif (flagondes==3)
        fprintf('Simulation pour ondes sur une corde \n');
else
        fprintf('la relation de dispersion est mal choisie\n');
end

while 1 
    fprintf('Veuillez choisir une condition initiale pour le probl?me\n');
%    fprintf('1: impulsion vers la droite,  2: paquet d ondes vers la droite,\n'); 
%    fprintf('3: impulsion (caillou dans l eau), 4: marche gaussienne,\n');
%    fprintf('5: onde stationnaire, 6: ondes progressive\n');
%	flag=input('7: marche droite (ajustement), 9: quitter le programme\n');
    fprintf('1: paquet d ondes vers la droite,\n');
    fprintf('2: Onde modul�e (superposition de deux ondes progressives) vers la droite,\n'); 
    fprintf('3: Impulsion ,\n');
    fprintf('4: Surelevation gaussienne,\n');
    fprintf('5: lacher de barrage (surelevation rectangulaire),\n'); 
%    fprintf('5: onde stationnaire,\n');
%    fprintf('6: onde progressive,\n');
%    fprintf('7: impulsion vers la droite,\n');
%    fprintf('8: rampe (fonction d erreur) \n');
	flag=input('0: quitter le programme\n');
%    close all
    if isempty(flag)
		flag=1
	end
	if flag==9
		return
    end
    

   
    
	% CHOIX DU TYPE DE CONDITION INITIALES
 
   % PAQUET D'ONDE VERS LA DROITE
    if flag==1
        profondeur = myinput('Profondeur en m ?',10);
        longueurdonde=myinput('Longueur d'' onde de la porteuse en m ?',.5);
        longueurpaquet=myinput('Largeur du paquet en m ?',5*longueurdonde);
        Lvisu=myinput('Taille du domaine repr?sent? en m ?',3*longueurpaquet);
        cgref=CG(2*pi/longueurdonde,gravite,profondeur,gamma,rhoeau);
        TMAX = myinput('Dur?e de la simulation ? ',Lvisu/cgref);
        L = Lvisu;
        [ko,KA,KB,K,dx,XA,XB]=Init(L,N); 
        k0=(L/longueurdonde)*ko; 
		dk0=(L/longueurpaquet)*ko;
		FZERO=2*sqrt(pi)*amplitude/(dk0*dx)*exp(-(K-k0).^2/dk0^2); % spectre gaussien centr?e en k0 pour les ondes vers la droite
    end
    
      % ONDE MODULEE VERS LA DROITE
	if flag==2
        profondeur = myinput('Profondeur en m ?',10);
        longueurdonde=myinput('Longueur d'' onde de la porteuse en m ?',.5);
        longueurpaquet=myinput('Longueur de la modulation en m ?',5*longueurdonde);
        Lvisu=myinput('Taille du domaine repr?sent? en m ?',3*longueurpaquet);
        cgref=CG(2*pi/longueurdonde,gravite,profondeur,gamma,rhoeau);
        TMAX = myinput('Dur?e de la simulation ? ',Lvisu/cgref);
        L = Lvisu;
        [ko,KA,KB,K,dx,XA,XB]=Init(L,N); 
        k0=(L/longueurdonde)*ko; 
		dk0=(L/longueurpaquet)*ko;
        [~,I1] = min(abs(K-(k0+dk0/2)));
        [~,I2] = min(abs(K-(k0-dk0/2)));
		FZERO=0*K;
        FZERO(I1)=1;FZERO(I2)=1;
    end
    
   % IMPULSION CENTRALE (CAILLOU DANS L'EAU) 
    if flag==3
        L = 20;
        [ko,KA,KB,K,dx,XA,XB]=Init(L,N); 
		FZERO=1/sqrt(dx)*amplitude*[0*KA-1 0 0*KB-1]; % spectre blanc "plat" pour toutes les ondes (gauche et droite)
    end
    
   % IMPULSION CENTRALE GAUSSIENNE  
    if flag==4
        profondeur = myinput('Profondeur en m ?',0.2);
        longueurpaquet=myinput('Largeur de la surelevation gaussienne en m ?',0.03);
        Lvisu=myinput('Taille du domaine repr?sent? en m ?',min(3000*longueurpaquet,30));
        cgref=max(CG([1e-30 2*pi/longueurpaquet*[.1 1 10]],gravite,profondeur,gamma,rhoeau));
        TMAX = myinput('Dur?e de la simulation ? ',Lvisu/cgref);
        L = Lvisu;
        [ko,KA,KB,K,dx,XA,XB]=Init(L,N); 
		fprintf('la taille du domaine est de %8.2f m \n',L); 
        dk0=(L/longueurpaquet)*ko;
		FZERO=2*sqrt(pi)*amplitude/(dk0*dx)*exp(-(K).^2/dk0^2); % spectre gaussien centr?e en 0 pour toutes les ondes
    end
    
      % IMPULSION VERS LA DROITE
    if flag==7
        L = 20;
        [ko,KA,KB,K,dx,XA,XB]=Init(L,N); 
		FZERO=1/sqrt(dx)*amplitude*[0*KA+1 0 0*KB];  % spectre blanc "plat" pour les ondes vers la droite
    end
    
  % ONDE STATIONNAIRE   
	if flag==8
        L = 20;
        [ko,KA,KB,K,dx,XA,XB]=Init(L,N); 
		nnoeuds=4;
        FZERO=[0*KA 0 0*KB];
        FZERO(nnoeuds+1)=amplitude/dx;
        FZERO(N-nnoeuds+1)=amplitude/dx; % deux fr?quences non nulles pour une onde vers la droite et vers la gauche
	end
  % ONDE PROGRESSIVE VERS LA DROITE   
	if flag==6
        L = 20;
        [ko,KA,KB,K,dx,XA,XB]=Init(L,N); 
		nnoeuds=4;
        FZERO=[0*KA 0 0*KB];
        FZERO(nnoeuds+1)=amplitude/dx; % 1 seule frequence non nulle, pour une onde vers la droite
    end
    
  

    
  % MARCHE D'EAU CENTRALE (AJUSTEMENT BARRAGE)  
	if flag==5
        fprintf('la taille du domaine est de %8.2f m \n',L); 
        longueurpaquet=myinput('quelle est la largeur de la surelevation en m ? \n',1);
        [ko,KA,KB,K,dx,XA,XB]=Init(L,N); 
        dk0=fix((L/longueurpaquet/pi))*ko;
		FZERO=2*amplitude/(dk0*dx)*sincAMOI(K/dk0); % spectre en sinc(x)=sin(x)/x pour la fonction heavyside en r?el (marche)
        FZERO(1) = 2*amplitude/(dk0*dx);
    end
    
     % MARCHE D'EAU CENTRALE (AJUSTEMENT BARRAGE)  
	if flag==8
        fprintf('la taille du domaine est de %8.2f m \n',L); 
        longueurpaquet=input('quelle est la largeur de la rampe en m ? \n');
        dk0=fix((L/longueurpaquet/pi))*ko;
		FZERO=amplitude*1i*2/(dk0*dx)*exp(-(K).^2/dk0^2)./K; % 
       % FZERO(1)=100.0/dk0;
    end
    
    
    
	% appel de la relation de dispersion water waves
	OMA=disper(KA,gravite,profondeur,flagondes,gamma,rhoeau);
	OMB=disper(KB,gravite,profondeur,flagondes,gamma,rhoeau);
	KD=[KB KA];
	OMEGA=[OMA 0 OMB];
	OMEG=[OMB OMA];

    
	%boucle sur le temps
    dt = TMAX/M;
	Mmax=M*dt;
	Mmaxh=Mmax/2;
	nla=0;
	nlamax=21;
	for t=[1.1*Mmax 0:dt:Mmax]
		U=FZERO.*exp(-i*OMEGA*t).*exp(-lam*K.^2*t);
        
% signification : u = int_k F(k) exp i (k x - omega(k) t)  
        
		U(N/2+1)=0;
		V=ifft(U,N);
		RV=real(V);
		YA=RV(1:N/2+1);
		YB=RV(N/2+1:N);
		X=[XB XA];
		Y=[YB YA];
		if (t>Mmax)
           % determination de l'echelle de la figure
           um = 2*max(abs(Y));
        elseif t==0 
          %  figure('Position',[50 50 800 500]); 
          figure(1);
          if(quarterfig)
              subplot(2,2,1);
          end
            plot(X,Y);
            axis([-Lvisu/2 Lvisu/2 -um um])
            xlabel('\it{x} (m)','FontSize',14)
			ylabel('\eta(x,0)','FontSize',14)
            title('condition initiale','FontSize',14)
            pause
%            hold on;
        else
          %  figure('Position',[50 50 800 500]);
            figure(1); pause(.15);
          if(quarterfig)
              subplot(2,2,1);
          end
            plot(X,Y);
            axis([-Lvisu/2 Lvisu/2 -um um])
            xlabel('\it{x} (m)','FontSize',14)
			ylabel('\eta(x,t)','FontSize',14)
            title(['t = ',num2str(t), 's'],'FontSize',14)
           nla=nla+1;
        end
	end
end
%
% <<  RELATIONS DE DISPERSION >>

% CHOIX DE LA RELATION DE DISPERSION
function om=disper(K,gravite,profondeur,flagondes,gamma,rhoeau)
if (flagondes==1) 
    om=disper_ondessurfacelibreavectension(K,gravite,profondeur,gamma,rhoeau);
end
if (flagondes==2) 
    om=disper_ondeslongues(K,gravite,profondeur);
end
if (flagondes==3) 
    om=disper_corde(K,1);
end

% RELATION DE DISPERSION DES ONDES DE SURFACE (THEORIE POTENTIELLE)
function om=disper_ondessurfacelibreavectension(K,gravite,profondeur,gamma,rhoeau)
om=sqrt((gravite*abs(K)+gamma/rhoeau*abs(K).^3).*tanh(abs(K)*profondeur));

function cg=CG(K,gravite,profondeur,gamma,rhoeau)
omega=disper_ondessurfacelibreavectension(K,gravite,profondeur,gamma,rhoeau);
H = profondeur;g=gravite;rho=rhoeau;k = abs(K);
cg = ((tanh(k*H).*(g+3*gamma/rho*k.^2)+H*(1-tanh(k*H).^2).*(k*g+gamma/rho*k.^3)))./(2*omega);


% RELATION DE DISPERSION DES ONDES LONGUES DE GRAVITE
function om=disper_ondeslongues(K,gravite,profondeur)
om=sqrt(gravite*abs(K).*abs(K)*profondeur);


    function om=disper_corde(K,C)
        om = K*C;

% << filew >>
%
% writing image on  files 
function filew(gcf,n)
[a,map]=capture(gcf);
filename=sprintf('image%2.2d.jpg',n)
imwrite(a,map,filename,'jpg')

function sortie=sincAMOI(x)
sortie = (x~=0)*sin(x)./x + 1*(x==0);

function answer = myinput(question, default)
answer = input([question, ' [default : ', num2str(default), ' ] : ']);
if isempty(answer)
    answer = default;
end

function [ko,KA,KB,K,dx,XA,XB]=Init(L,N)
    ko=2*pi/L;
	dx=L/N;
	XA=0:dx:N/2*dx;
	XB=-N/2*dx:dx:-dx;
	KA=0:ko:(N/2-1)*ko;
	KB=-(N/2-1)*ko:ko:-ko;
	K=[KA 0 KB];
        