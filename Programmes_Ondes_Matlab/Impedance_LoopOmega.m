% Calcul de |Zin| a l'aide du programme WaveSimulator.m en faisant une
% boucle sur la fréquence omega

omegatab = [0.05 :0.05: 10];
Ztab = zeros(size(omegatab));

for i = 41:60
    omega = omegatab(i);
    [ZZ,T,R] = WaveSimulator_ZTR(omega);
    Ztab(i) = ZZ;
end

figure(12);
semilogy(omegatab,Ztab);

csvwrite('Zin_Cylinder_WS.csv',[omegatab',Ztab']);



