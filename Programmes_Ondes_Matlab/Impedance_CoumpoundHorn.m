
%% Impedance d'un tuyau acoustique conique tronqué 
%
% comparaison entre plusieurs modelisations de l'impedance de sortie
close all
global rho c
rho = 1;  % masse volumique (kg/m^3)
c = 1;     % vitesse du son (m / ms)
Lcone = 2;     % distance pointe du cone - section de sortie
Lcyl =  1;   % longueur du tuyau
Rin = .2; % rayon du tuyau en entrée
Rpipe = .5; % rayon du tuyau en sortie

omega = linspace(0,10,300); 
ktab = omega/c;
Delta = 0.85*Rpipe; % correction de longueur
Zout0 = 0*ktab; % defini comme un tableau de meme dimension que k
Zout1 = Zout0.*( -1i*Delta.*ktab );
Zout2 = Zout0.*( -1i*Delta.*ktab + (ktab*Rpipe).^2/2 );
Z0in = rho*c/(pi*Rpipe^2);

%% Compound

omegatab = linspace(0,5,1000); % gamme de omega 
ktab = omegatab/c;
Zout0 = 0*ktab; % defini comme un tableau de meme dimension que k
Zout1 = rho*c/(pi*Rpipe^2)*( -1i*Delta.*ktab );
Zout2 = rho*c/(pi*Rpipe^2)*( -1i*Delta.*ktab + (ktab*Rpipe).^2/2 );
Zincyl = Zin_cyl(ktab,Lcyl,Rpipe,Zout2);
Zin = Zin_cone_theo(ktab,Rin,Rpipe,Lcone,Zincyl);

figure(1);hold off;
semilogy(omegatab,abs(Zin),'r');hold on;

%%
%emilogy(omega/(2*pi)*1000,abs(Zin_compound(ktab,L1,r1,r2,alpha,Zout1)/Z0in),'b');hold on;
%semilogy(omega/(2*pi)*1000,abs(Zin_compound(ktab,L1,r1,r2,alpha,Zout2)/Z0in),'g');hold on;
%Zinc = Zin_cone_theo(ktab,r1,r2,alpha,Zout2);
%semilogy(omega/(2*pi)*1000,abs(Zin_cyl_theo(ktab,L1,a,Zinc)/Z0in),'k:');hold on;

 ZZ = importdata('Data_ConeCyl_FF.txt');
 omegaFF = ZZ(:,2);ZaFF = abs(ZZ(:,4)+1i*ZZ(:,5));
 figure(4);hold off;
 semilogy(omegatab,abs(Zin),'r',omegaFF',ZaFF','b');
 hold on;
 [Z,A,B,C,D] = Zin_compound(ktab,Rin,Rpipe,Lcone,Lcyl,Zout2);
 semilogy(omegatab,abs(Z),'k:');
 ylim([1e-1,1e2]);legend('Theory','FreeFem');xlabel('\omega');ylabel('|Z|');


%%


if 0
xlabel('f [Hz]','fontsize',18); ylabel('|Z_{in}| / Z_1','fontsize',18);%ylim([1e-2 1e2])
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
legend('Ideal open','End correction','End correction + radiation','location','SE','fontsize',16)


%%Figure trace
omega = 2;
k = omega/c;
Zout2 = Zout0.*( -1i*Delta.*ktab + (k*r2*tan(alpha)).^2/4 )
[Z,A,B,C,D] = Zin_compound(k,L1,r1,r2,alpha,Zout2);
L2 = (r2-r1);
xL1 = linspace(0,L1,201);
rL2 = r1+linspace(0,L2,201);
pC = A*exp(1i*k*xL1)+B*exp(-1i*k*xL1);
pS = (C*exp(1i*k*rL2)+D*exp(-1i*k*rL2))./rL2;

qC = (A*exp(1i*k*xL1)-B*exp(-1i*k*xL1));
qS = (C*exp(1i*k*rL2).*(1-1./(1i*k*rL2))-D*exp(-1i*k*rL2).*(1+1./(1i*k*rL2)))./(rL2);

figure(4);hold off;
plot(xL1,real(pC),'r',rL2-r1+L1,real(pS),'r',xL1,imag(pC),'r:',rL2-r1+L1,imag(pS),'r:'  );
hold on;
plot(xL1,real(qC),'b',rL2-r1+L1,real(qS),'b',xL1,imag(qC),'b:',rL2-r1+L1,imag(qS),'b:'  );
end


function Z = Zin_cone(ktab,r1,r2,alpha,Zout)
global rho c
% calcul de l'impédance en inversant un système 2x2 pour les amplitudes C,D.
% 
% Condition en entrée, débit Qf imposé : 
% Q(0,t) = Qf exp (- i omega t) soit 
%       ( C (1+1/ikr1) e^(ikr1) + B (-1-1/ikr1 e^(-ikr1) )/r1  / Z0in = Qf 
%
% Condition en sortie : 
% P(L,t)/Q(L,t) = Zout 
%   = Z0out [C exp(ikr2)+D exp(-ikr2)] / [C(1-1/ikr2)e^(ikr2) +D(-1-1/ikr2)e^(-ikr2)]
%
% Mise sous forme matricielle  M * [C;D] = Y
% 
% Pour utiliser cette fonction de manière vectorielle il faut faire ce
% calul pour chaque valeur de kL en faisant une boucle.
Z1 = rho*c/(pi*r1^2*tan(alpha)^2);
Z2 = rho*c/(pi*r2^2*tan(alpha)^2);
Qf = 1;
  for j=1:length(ktab)
    k = ktab(j);
    M = [ ...
      [ (1-1/(1i*k*r1))*exp(1i*k*r1) , (-1-1/(1i*k*r1))*exp(-1i*k*r1) ]; ...
      [ (Z2-Zout(j)*(1-1/(1i*k*r2)))*exp(1i*k*r2) , (Z2+Zout(j)*(1+1/(1i*k*r2)))*exp(-1i*k*r2) ] ... 
        ];
    Y = [Qf*Z1*r1 ; 0];
    X = M\Y;
    C = X(1);D=X(2);
    Z(j) = (C*exp(1i*k*r1)+D*exp(-1i*k*r1))/(r1*Qf);
  end
end



function Z = Zin_cone_theo(ktab,Rin,Rout,Lcone,Zout)
global rho c
% calcul de l'impédance d'entrée en utilisant la solution theorique
% Référence : Fletcher & Rossing, page 210 ; Olson, 1957.
% Fonction vectorielle : marche si k et Zout sont des tableaux de valeur
alpha = atan((Rout-Rin)/Lcone);
x1 = Rin/tan(alpha)
x2 = Rout/tan(alpha)
Z1 = rho*c/(pi*x1^2*tan(alpha)^2);
Z2 = rho*c/(pi*x2^2*tan(alpha)^2);
kL = ktab*(x2-x1);
theta1 = atan(ktab*x1);
theta2 = atan(ktab*x2);
Z = Z1*(- 1i*Zout.*sin(kL-theta2)./sin(theta2) + Z2*sin(kL) ) ...
  ./ ( Zout    .*( sin(kL+theta1-theta2)./(sin(theta1).*sin(theta2) ) ) ...
     +1i*Z2 * sin(kL+theta1)./sin(theta1) ); 

end

%%
% Pour memoire : fonctions pour un tuyau cylindrique (non utilisées ici)

function Z = Zin_cyl(ktab,L,a,Zout)
% calcul de l'impédance en inversant un système 2x2 pour les amplitudes A,B.
% 
% Condition en entrée, débit Qf imposé : 
% Q(0,t) = Qf exp (- i omega t) soit 
%       ( A - B ) / Z0 = Qf 
%
% Condition en sortie : 
% P(L,t)/Q(L,t) = Z0[A exp(ikL)+B exp(-ikL)]/[A exp(ikL) -B exp(-ikL)] = Zout 
%       A exp(ikL) [ Z0 -Zout ] + B exp(-ikl) [ Z0 + Zout] = 0
%
% Mise sous forme matricielle  M * [A;B] = Y
% 
% Pour utiliser cette fonction de manière vectorielle il faut faire ce
% calcul pour chaque valeur de ktab en faisant une boucle.
  global rho c
  Z0 = rho*c/(pi*a^2);
  for j=1:length(ktab)
    kL = ktab(j)*L;
    M = [ [1, -1] ; [exp(1i*kL)*(Z0-Zout(j)),exp(-1i*kL)*(Z0+Zout(j)) ]];
    Qf = 1;
    Y = [Qf*Z0 ; 0];
    X = M\Y;
    A = X(1);B=X(2);
    Z(j) = (A+B)/Qf;
  end
end

function Z = Zin_cyl_theo(ktab,L,a,Zout)
% calcul de l'impédance d'entrée avec la formule du cours (calcul analytique)
% Fonction vectorielle : marche si ktab et Zout sont des tableaux de valeur
  global rho c
  Z0 = rho*c/(pi*a^2);
  Z = Z0*(Zout - 1i*Z0*tan(ktab*L))./(-1i*Zout.*tan(ktab*L) + Z0);
end


function [Z,A,B,C,D] = Zin_compound(ktab,Rin,Rpipe,Lcone,Lcyl,Zout)
global rho c
% calcul de l'impédance en inversant un système 4x4 pour les amplitudes A,B,C,D.
% 
% Condition en entrée, débit Qf imposé : 
% Q(0,t) = Qf exp (- i omega t) soit 
%       ( A (1+1/ikr1) e^(ikr1) + A (-1-1/ikr1 e^(-ikr1) )/r1  / Z0in = Qf 
%
% Conditions de raccord :
%   ( A e^(ikr2) + B e^(-ikr2) ) = C+D
%
% Condition en sortie : 
% P(L,t)/Q(L,t) = Zout 
%   = Z0out [C exp(ikL)+D exp(-ikL)] / [Ce^(ikr2) -De^(-ikr2)]
%
% Mise sous forme matricielle  M * [A;B;C;D] = Y
% 
% Pour utiliser cette fonction de manière vectorielle il faut faire ce
% calcul pour chaque valeur de kL en faisant une boucle.
alpha = atan((Rpipe-Rin)/Lcone);
r1 = Rin/tan(alpha);
r2 = Rpipe/tan(alpha);
Z1 = rho*c/(pi*Rin^2);
Z2 = rho*c/(pi*Rpipe^2);
Qf = 1;
  for j=1:length(ktab)
    k = ktab(j);
    M = [ 
          [ (1-1/(1i*k*r1))*exp(1i*k*r1)/r1/Z1,(-1-1/(1i*k*r1))*exp(-1i*k*r1)/r1/Z1, 0, 0]; ...
          [ -exp(1i*k*r2)/r2,-exp(-1i*k*r2)/r2,1,1 ]; ...
          [ -(1-1/(1i*k*r2))*exp(1i*k*r2)/r2,-(-1-1/(1i*k*r2))*exp(-1i*k*r2)/r2,1,-1  ]; ...
          [ 0,0,(Z2-Zout(j))*exp(1i*k*Lcyl) , (Z2+Zout(j))*exp(-1i*k*Lcyl) ] ... 
        ];
    Y = [Qf ; 0; 0 ; 0];
    X = M\Y;
    A = X(1); B = X(2); C = X(3); D = X(4);
    Z(j) = ((A*exp(1i*k*r1)+B*exp(-1i*k*r1))/r1)/Qf;


  end
end
