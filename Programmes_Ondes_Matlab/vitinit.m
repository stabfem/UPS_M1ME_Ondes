
function y= vitinit(x) 
global L

%% pour la corde pincée :
y = 0*x;

%% Pour la corde frappée :

%d = 0.15*L;
%ell = 0.05*L;
%a = d-ell/2;
%b = d+ell/2;
%W = 1;
%N = 8;
%y = 0*(x<=a)+W*(x>a&x<b)+0*(x>=b);
%
% Explanation : this syntax is equivalent to
%if (x<=a)
%    y = 0;
%elseif (x<b)
%    y = W;
%else
%    y = 0;
%end
% except that this simplest syntax is not vectorial ! 
% (works if x is a single value but not if it is an array of values)


end
