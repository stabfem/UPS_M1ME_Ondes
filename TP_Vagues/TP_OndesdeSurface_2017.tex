\documentclass[12pt,a4paper]{article}
\usepackage[top=1.7cm,left=1.4cm,right=1.4cm,bottom=1.5cm]{geometry}

\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}


\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{enumerate}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand{\Ro}{\ensuremath{{\mathcal{R}_0}}}
\newcommand{\RM}{\ensuremath{{\mathcal{R}_M}}}
\newcommand{\ex}{\ensuremath{\vec{e}_{x}}}
\newcommand{\ey}{\ensuremath{\vec{e}_{y}}}
\newcommand{\ez}{\ensuremath{\vec{e}_{z}}}
\newcommand{\er}{\ensuremath{\vec{e}_{\rho}}}
\newcommand{\et}{\ensuremath{\vec{e}_{\varphi}}}
\newcommand{\vp}{\ensuremath{\varphi}}
\newcommand{\e}  {\mathrm{e}}   
\newcommand{\dd}  {\textrm d}      
\newcommand{\im}  {\mathrm{i}} %\newcommand{\im}  {\textrm i}
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}
\newcommand{\ddp}[2]  {\frac {\partial #1} {\partial #2}}   
  \newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {{\textrm d}^{2} #1} {{\textrm d} #2 ^{2}}}

\begin{document}
\noindent
{\sc{Universit\'e Paul Sabatier}}\hfill{\sc{Ann\'ee universitaire 2016--2017}}\\
{\sc{M1 M\'ecanique, \'Energ\'etique}}\hfill{\sc{Ondes et Turbulence}}\\[.15cm]

\vspace{2cm}

\HRule
%{\large Nom~:\hfill Section~:\hspace{4cm}~\\[0.5cm]
 %Pr\'enom~:\hfill Groupe~:\hspace{3.85cm}~\vspace{-.0cm}}\\
%\HRule\\[-.25cm]
\begin{center}
{\Large \bfseries Enonc\'e de TP num\'erique sur Matlab \\  "Propagation 1D d'ondes de surface" }
\end{center}
\vspace{-.25cm}
\HRule
\begin{center}
{\it Dur\'ee~: 2 heures \\
}
\end{center}
\vspace{2cm}
\noindent 
L'objectif de ce TP est de r\'ealiser des simulations num\'eriques pour \'etudier le mouvement d'ondes de surface libre, en pr\'esence de gravité et/ou de tension de surface. On se place dans une configuration 1D horizontale infinie selon une direction de l'espace not\'ee $x$.  On consid\`ere une couche de fluide initialement au repos, d'\'epaisseur H, sur laquelle on vient rajouter une perturbation initiale de surface libre not\'ee $\eta(x,t=0)$. A l'instant initial ($t=0$), le profil de surface libre est donc $h(x,t=0)=H+\eta(x,t=0)$.

\vspace{0.25cm}
\noindent Pour résoudre ce problème, on utilise des versions "simplifi\'ees" du syst\`eme d'\'equations complet. On trouve les simplifications suivantes :
\begin{itemize}
\item
Pour les \'ecoulements potentiels instationnaires, lorsque la d\'eviation de la surface libre $\eta$ reste petite devant $H$, on peut lin\'eariser le syst\`eme d'\'equations et aboutir \`a une \'equation d'ondes dispersives. 
\item
Pour des \'ecoulements pour lesquels les \'echelles horizontales sont grandes devant les \'echelles verticales, on peut simplifier les \'equations en faisant l'hypoth\`ese d'hydrostasie, et en supposant que les composantes $u$ et $v$ sont ind\'ependantes de $z$. On aboutit alors aux \'equations de Saint-Venant ("shallow water" en anglais). Les \'equations obtenues restent non-lin\'eaires.
\end{itemize}

\vspace{0.25cm}
\noindent On fournit deux programmes matlab r\'esolvant les syst\`emes ci-dessus :
\begin{itemize}
\item
un solveur du probl\`eme aux valeurs initiales pour diff\'erentes familles d'onde (2 relations de dispersion possible : ondes de surface gravité-capillaires, ondes de gravité longues). Il repose sur une d\'ecomposition en spectral de la condition initiale (8 choix possibles de condition initiale) sur la base de fonctions orthogonales, et le calcul \`a tout instant t ult\'erieur. Le programme s'appelle {\bf "OndesEvolution.m"}.
\item
un solveur des \'equations de Saint-Venant compl\`etes, d\'evelopp\'e par Laurent Lacaze et appel\'e {\bf  "SaintVenant.m"} pour les ondes longues.
\end{itemize}
\clearpage

\section{Propagation lin\'eaire d'ondes de surface dispersives}

\subsection{Eléments de théorie}

On rappelle que l'hypothèse d'ondes linéaires consiste à supposer que l'élévation de la surface libre est de la forme $h(x,t) = H_0 + \eta(x,t)$ où $|\eta|$ est petit par rapport à $H$ et par rapport aux longueurs d'ondes.

Les ondes de surface étant en général {\em dispersives},  il n'est pas possible, même dans le cas linéaire, de ramener le problème à une unique équation aux dérivées partielles de type {\em equation de d'Alembert} gouvernant la seule inconnue $\eta(x,t)$. En revanche, en prenant la transformée de Fourier de $\eta(x,t)$ par rapport à l'espace, on aboutit à une équation linéaire très simple :
$$
\ddp{\hat{\eta}(k,t)}{t}  = -i \omega(k) \hat{\eta}(k,t)
$$ 
où $\omega(k)$ est donné par la relation de dispersion.
Cette équation a pour solution $\hat{\eta}(k,t)  = e^{-i \omega(k) t}  \hat{\eta}_0(k)$ où 
$\hat{\eta}_0(k)$ est la transformée de Fourier de la condition initiale $\eta(x,0)$.
En prenant la transformée de Fourier inverse de cette expression on arrive finalement à :
\begin{equation}
\eta(x,t) = \int_{- \infty}^{+ \infty} \hat{\eta}_0(k) e^{i( kx -  \omega(k) t)} d k, 
\quad \mbox { avec } \quad  \hat{\eta}_0(k) = \frac{1}{2 \pi} \int_{- \infty}^{+ \infty} \eta(x,0) e^{-i k x } d x 
\end{equation}
 

Ces deux équations sont à la base du programme fourni. En pratique les transformées de Fourier sont effectuées avec l'algorithme FFT inclus dans Matlab. 

\subsection{Travail à effectuer}

\noindent En utilisant la fonction "OndesEvolution.m" pour diff\'erents types d'onde et diff\'erentes conditions initiales (impulsion type caillou dans l'eau ou marche d'eau), interpr\'eter l'\'evolution obtenue en fonction du caract\`ere dispersif ou non des ondes \'etudi\'ees et du choix de l'échelle spatiale de la perturbation initiale. On pourra modifier la profondeur $H$ d'eau et la tension de surface $\gamma$ pour modifier la relation de dispersion. En particuliers, on pourra s'intéresser au cas où la profondeur d'eau est telle que la relation de dispersion est relativement plate, les effets de profondeur et de tension de surface se compensant .

\section{Propagation non lin\'eaire d'ondes de surface "longues"}
\subsection{Eléments de théorie}

\subsubsection{Equations de Saint-Venant}
Dans le cas des ondes "longues" (c'est à dire de longueur d'onde grande devant la profondeur), on peut simplifier le problème en supposant que $(i)$ le profil de vitesse est uniforme $\vec{u} = U(x,t) \vec{e_x}$ et $(ii)$ que la pression est hydrostatique.
Sous ces hypothèses, a partir de bilans de masse et de quantité de mouvement sur des volumes élémentaires, on peut ramener le problème à un système de deux équations différentielles appelées {\bf Equations de Saint-Venant} gouvernant l'évolution de $h(x,t)$ et $U(x,t)$ :

\begin{equation}
\ddp{h}{t} + U \ddp{h}{x} + h \ddp{U}{x} = 0, \label{eqSV1}
\end{equation}

\begin{equation}
\ddp{U}{t} + U \ddp{U}{x} + g \ddp{h}{x} = 0.  \label{eqSV2}
\end{equation}

Ces équations sont valables dans le cas non linéaire et ne nécessitent donc pas de faire l'hypothèse $h(x,t) = H_0 + \eta(x,t)$ avec $\eta  \ll H_0$.

\subsubsection{Equations caractéristiques}
Il n'est pas possible de ramener ces deux équations à une unique équation d'ordre 2 de type {\em équation de d'Alembert} ; en revanche on peut les combiner de manière à mettre le problème sous forme d'{\bf Equations caractéristiques}. Pour cela on fait l'opération $\pm \sqrt{g/h}$ (Eq. \ref{eqSV1}) + (Eq. \ref{eqSV2}) ce qui conduit à :

\begin{equation}
\left( \ddp{}{t} +  (U \pm \sqrt{gh}) \ddp{}{x} \right) \left( U \pm 2 \sqrt{gh} \right) = 0
\end{equation}

En notant que $c(x,t) = \sqrt{gh(x,t)}$ est la vitesse {\em locale} des ondes, on peut aussi mettre ce problème sous la forme suivante :

\begin{equation}
\left( \ddp{}{t} +  (U \pm c) \ddp{}{x} \right) J_{\pm} = 0, \quad \mbox{ avec } J_{\pm} = U \pm 2 c 
\end{equation}

La signification physique est la suivante : 
\begin{itemize}

\item L'invariant $J_+ = U + 2 c$ se conserve le long de courbes caractéristiques $C^+$ d'équation $\dto{x}{t} = U+c$.

\item 
L'invariant  $J_- = U - 2 c$ se conserve le long de courbes caractéristiques $C^-$ d'équation $\dto{x}{t} = U-c$.
 
 \end{itemize}
 
Ces éléments théoriques permettent d'interpréter les résultats, notamment le raidissement des fronts et la formation de ressauts (équivalent des ondes chocs en acoustique non linéaire). Ceux-ci apparaissent lorsque les courbes caractéristiques appartenant à une même famille ($C^+$ ou $C^-$) se croisent.


\subsubsection{Méthode de résolution numérique}

Le programme fourni ne résout pas le problème par la méthode des caractéristiques, mais par une méthode de type différences finies basée directement sur une discrétisation des équations de Saint-Venant (Eq. \ref{eqSV1}, \ref{eqSV2}).


\subsubsection{Travail à effectuer}
\noindent Au moyen du programme "SaintVenant.m", étudier l'évolution d'un lâcher d'eau (\og marche d'eau \fg) en fonction de la non linéarité initiale (rapport des hauteurs entre la hauteur d'eau au niveau de la \og marche \fg et à côté ). Comparer en particulier avec la simulation linéaire (au moyen de  "OndesEvolution.m"), et calculer le temps d'apparition des chocs (à comparer à la théorie basée sur la méthode des caractéristiques).


\clearpage

\vspace{2cm}
\clearpage

\section*{3.8bis \quad Sillage d'un bateau et d'une araignée d'eau (TD supplémentaire)}

\subsubsection*{3.8.1 \quad Sillage d'un bateau}

Un bateau navigue à la vitesse $V_0$ dans la direction ($-x$) sur un plan d'eau de profondeur supposée infinie. On néglige la tension de surface.

\begin{enumerate}

\item Préliminaire : Donnez la relation de dispersion $\omega(k)$, et justifiez que la vitesse de groupe des ondes vérifie $c_g = c/2$.


\item 
On considère une onde plane caractérisée par un nombre d'onde $k$ et se propageant dans une direction faisant un angle $\beta$ avec la direction $-x$ (on peut aussi introduire un vecteur d'onde $\vec{k} = k [- \cos \beta \vec{e_x} + \sin \beta \vec{e_y}])$ et de fréquence $\omega$. Montrez que cette onde est {\em stationnaire} dans le référentiel du bateau si la condition suivante est vérifiée :

\begin{equation}
c = \omega/k = V_0 \cos \beta
\end{equation}

\item On suppose que le bateau génère un sillage constitué d'ondes de toutes les orientations $\beta$ et vérifiant toutes la condition de stationnarité. Donnez la longueur d'onde 
$\lambda = 2 \pi /k$ de chacune de ces longueurs d'ondes en fonction de $\beta$, $V_0$ et $g$.

\item A l'instant initial le bateau est au point $A$, et à un instant $t$ ultérieur il est au point $B$.
étudiez la propagation d'un paquet d'ondes de direction $\beta$ émis lorsque le bateau était au point $A$. Montrez que celui-ci occupe la position (point $E$) de coordonnées 

\begin{equation}
x = V_0 t \left( 1 - \frac{1}{2} \cos^2 \beta \right) ; \qquad y = V_0 t \left( \frac{1}{2} \cos \beta \sin \beta \right) 
\end{equation}

\item En considérant toutes les valeurs possibles de $\beta$, montrez que les ondes émises lorsque le bateau était en $A$ se répartissent sur un cercle dont vous préciserez le rayon et le centre.

\item On note $\phi = atan(y/x)$. Justifiez que les ondes de direction $\beta$ émises à tous les instants précédent l'instant $t$ se rencontrent sur la droite de direction $\phi$.

\item Donnez la relation entre $\phi$ et $\beta$ et en déduire la structure du sillage.

\item On cherche maintenant à construire la forme des crête dans le plan $(x,y)$. Justifiez que ces courbes vérifient $dy/dx = \cot \beta$.

\item A partir des équations $(9)$, montrez que la géométrie des crêtes est donnée par 
\begin{equation}
x = X_1 \cos \beta \left( 1 - \frac{1}{2} \cos^2 \beta \right) ; \qquad y = X_1  \left( \frac{1}{2} \cos^2 \beta \sin \beta \right) 
\end{equation}



\end{enumerate}

\subsubsection*{3.8.2 \quad Sillage d'une araignée d'eau}

On reprend l'étude précédente dans le cas d'une araignée d'eau de $V_0$ dans la direction ($-x$) sur un plan d'eau de profondeur supposée infinie. On suppose maintenant que les ondes générées sont dominées par la capillarité et on néglige donc la gravité.

\begin{enumerate}

\item Préliminaire : Donnez la relation de dispersion $\omega(k)$, et justifiez que la vitesse de groupe des ondes vérifie $c_g = 3c/2$.

\item Reprendre l'étude précédente (questions 2 à 7) et construire la structure du sillage.

\item Reprendre le calcul de la géométrie des crêtes (questions 8 à 9) et montrez que celles-ci son maintenant données par :
\begin{equation}
x = X_1 \frac{1}{\cos^3 \beta} \left( 1 - \frac{3}{2} \cos^2 \beta \right) ; \qquad y = \frac{3}{2} X_1  \frac{ \sin \beta}{ \cos^2 \beta}  
\end{equation}
 


\end{enumerate}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
