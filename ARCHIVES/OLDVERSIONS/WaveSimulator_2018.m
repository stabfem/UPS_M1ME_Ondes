function main()
clear all
global L x Z c xstag Zstag cstag dt dx

%% Definition of the problem
L = 200; % Length 
problemtype = 'canal1disc'; 
% allowed values are 'string', 'string1disc',
% "string2disc','pipe','pipe1disc','horn','canal1disc','canalprog'
%
% see in function cZ at the end of this program
% to change the parameters, and add your own cases !

%% Definition of the boundary conditions
BC_0  = 'I0'; 
% This is the boundary condition at inlet. 
% Allowed values include :
%   'Y0'    : Imposed sinusoidal displacement 
%   'F0'    : Imposed sinusoidal force
%   'I0'    : Incident sinusoidal wave coming from the region x<0
%   'Pulse' : Gaussian pulse imposed at the inlet. 
% 'I0_Pulse': Gaussian pulse coming from the region x<0 

% parameters for sinusoidal forcing (Y0 or F0 or I0)
omega = 2*pi/(12*60); % frequency in rad/min
%omega = 4.2*pi/(2*L)
Amp = 1;
% parameters for 'pulse' and 'I0_Pulse' forcings 
t1 = .2;
ta = .05;
AmpPulse = 1;

BC_L = 'Fixed';
% This is the bounary condition at inlet. 
% Allowed values include :
%   'Fixed'         : fixed string (or closed pipe)
%   'Free'          : free string (or ideally open pipe)
%   'Transparent'   : Radiation condition
%   'Impedance'     : We specity an outlet impedance.

% parameter for 'Impedance'
%Zout = 4*Z(Nx);
Zout = 40;

%% Numerical parameters relevant to the time and space discretization

Nx = 401; % Number of spatial points
dx = L/(Nx-1);

dt = .05 ;
FinalTime = 60*12*4; % 4 days

x = 0:dx:L; % principal mesh 
[c,Z] = cZ(x,problemtype); % physical properties at mesh points

%% Numerical parameters defining how to plot the results 
NP.plotstep = 2000; % number of time steps between two plots
NP.wait = 0; % set to 1 to advance step-by-step ; to zero to advance continuously
NP.fig1 = 1; % to plot string shape on figure 1
NP.figY = 0;% to plot Y in figure 1  
NP.figV = 1; % to plot V as well as Y in figure 1
NP.figT = 1; % to plot F as well as Y in figure 1
NP.fig2 = 1; % to plot various statistics in figure 2
NP.fig2inst = 0; % to plot instantaneous quantities in figure 2
NP.figVplusVmoins=1; % to decompose Vplus/vmoins in figure 2
NP.Tscalefig2 = 5;
NP.fig3 = 0; % to plot inlet/outlet quantities on figure 3
NP.fig4 = 1; % to plot C and Z in figure 4

% Miscleanous parameters
NP.smoothingtime = 100*dt; % to soften the starting (useful with Y0)
NP.N_averaging = round(2*pi/omega/dt); % number of previous instants to compute time-averages
NP.tpause = 0.001; % pause after each plot to let time for figure refreshing
NP.FinalTime = FinalTime; % we also need to know this one in the post-processing routine

%%% END OF PARAMETERS ; THE REMAINDER SHOULD NOT BE MODIFIED

%% Staggered mesh 

xstag = [0 dx/2:dx:L-dx/2 L]; %staggered mesh  
[cstag,Zstag] = cZ(xstag,problemtype);% physical properties at these points

%% initial conditions
Y_current = 0*x; % here you can also use posinit(x) as in TP1 !
V_current = 0*x; % here you can also use vitinit(x) as in TP1 !
F_current(2:Nx) = -cstag(2:Nx).*Zstag(2:Nx).*diff(Y_current)/dx;
F_current(1) = c(1)*Z(1)*(3*Y_current(1)-4*Y_current(2)+Y_current(3))/(2*dx);
F_current(Nx+1) = -c(Nx)*Z(Nx)*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx);
Y_past = Y_current-dt*V_current;
Y_future = Y_current+dt*V_current;

t= 0;
PostProcessingAndPlot(t,Y_current,V_current,F_current,NP);
if (NP.fig4==1)
    plotCZ(problemtype,L)
end
pause

%% Defines a few object needed in the time loop
r = c*dt/dx;
rZ = c./Z*dt/dx;
rstag = cstag.*Zstag*dt/dx;
r0 = c(1)*dt/dx;  
rL = c(Nx)*dt/dx;
disp(['max(r) = ' num2str(max(abs(r)))])
if(max(abs(r))>1) 
    disp(['WARNING : max(r)  > 1 ! reduce dt to avoid intability']); 
else 
    if(max(abs(r))<0.5) 
    disp(['max(r)  < 1 ; stability criterion is largely satisfied ; you may increase the time step dt to speed up the code ']);
    end
end

%% Main Loop over time
for t =0:dt:FinalTime
   
% inner points : solve the wave equation
for j=2:Nx-1
Y_future(j) = rZ(j)*(rstag(j)*Y_current(j-1)+rstag(j+1)*Y_current(j+1)-(rstag(j)+rstag(j+1))*Y_current(j))...
            + 2*Y_current(j)-Y_past(j);
end

% boundary condition x=0:
switch(BC_0)
    case ('Fixed')
    Y_future(1) = 0;
    case('Free')
   Y_future(1) = 2*r0^2*Y_current(2)+2*(1-r0^2)*Y_current(1)-Y_past(1) ; 
    case('F0')
 Y_future(1) = 2*r0^2*Y_current(2)+2*(1-r0^2)*Y_current(1)-Y_past(1)+2*r0^2*dx*Amp*sin(omega*t); 
    case('Y0')
    Y_future(1)=Amp*sin(omega*(t+dt))*tanh((t+dt)/NP.smoothingtime); % the smoothing period is to make the start less abrupt
    case('I0') 
     Y_future(1) = (2*Y_current(1)+(r0-1)*Y_past(1)+2*r0^2*(Y_current(2)-Y_current(1)))/(1+r0)...
         -Amp/Z(1)*sin(omega*t)*dt*4*r0/(1+r0);
    case('Pulse')
    Y_future(1) = AmpPulse*exp(-(t-t1)^2/ta^2);
     case('I0_Pulse')
     Y_future(1) = (2*Y_current(1)+(r0-1)*Y_past(1)+2*r0^2*(Y_current(2)-Y_current(1)))/(1+r0)...
         -AmpPulse/Z(1)*exp(-(t-t1)^2/ta^2)*dt*4*c(1)*r0/(1+r0);
    otherwise
    disp('WARNING : left boundary condition not implemented !');
end%switch

% boundary condition x=L:
switch(BC_L)
    case('Fixed')
    Y_future(Nx) = 0; 
    case('Free')
    Y_future(Nx) = 2*rL^2*Y_current(Nx-1)+2*(1-rL^2)*Y_current(Nx)-Y_past(Nx) ; 
    case('Transparent')
     Y_future(Nx) = (2*Y_current(Nx)+(rL-1)*Y_past(Nx)+2*rL^2*(Y_current(Nx-1)-Y_current(Nx)))/(1+rL);
    case('Impedance')
        rL = c(Nx)*dt/dx;
        rZ = rL/Zout*Z(Nx);
         Y_future(Nx) = (2*rZ*Y_current(Nx)+(rL^2-rZ)*Y_past(Nx)+2*rL^2*rZ*(Y_current(Nx-1)-Y_current(Nx)))/(rZ+rL^2); %no reflexion
    otherwise
    disp('WARNING : right boundary condition not implemented !');
end%switch

% end of main integration scheme ; next is for figure displays

% deduce the velocity and force
V_current = (Y_future-Y_past)/(2*dt);
F_current(2:Nx) = -cstag(2:Nx).*Zstag(2:Nx).*diff(Y_current)/dx;
F_current(1) = c(1)*Z(1)*(3*Y_current(1)-4*Y_current(2)+Y_current(3))/(2*dx);
F_current(Nx+1) = -c(Nx)*Z(Nx)*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx);

% Call the postprocessing subroutine 
PostProcessingAndPlot(t,Y_current,V_current,F_current,NP);

if(NP.wait==1) 
       pause
end




%% Set  things  up  for  the  next  time  step
Y_past = Y_current ;
Y_current = Y_future ;

end

% Last call to 
PostProcessingAndPlot(-t,Y_current,V_current,F_current,NP);


end
%%
function [] = PostProcessingAndPlot(t,Y_current,V_current,F_current,NP)
%% This function handles the postprocessing

global x Z c xstag Zstag cstag dt dx

persistent tab_t tab_Y0 tab_F0 tab_V0 tab_V0plus tab_V0moins...
           tab_YL tab_FL tab_VL tab_VLplus tab_VLmoins ...
           tab_Power_0 tab_Power_0_plus tab_Power_L tab_Power_L_plus ...
           tab_Power_0_Av tab_Power_0_plus_Av tab_Power_L_Av tab_Power_L_plus_Av ...
           tab_TotalEnergy tab_TotalEnergy_Av ...
           minfig1Y maxfig1Y minfig1V maxfig1V ...
           minfig1F maxfig1F minfig2 maxfig2 minfig2b maxfig2b ...
           minfig3 maxfig3;



%%

if(t==0)
% Prepare the figures

if(NP.fig1)
    figure(1);
    Nsubfig1 = NP.figY+NP.figV+NP.figT;
    thesubfig = 1;
    if(NP.figY==1)
        subplot(Nsubfig1,1,thesubfig);
        plot(x,Y_current,'r');
        title('Initial displacement (red)');
        thesubfig = thesubfig+1;
    end
    if(NP.figY==1)
        subplot(Nsubfig1,1,thesubfig);
        plot(x,V_current,'b');
        title('Initial velocity (blue)');
        thesubfig = thesubfig+1;
    end
    if(NP.figT==1)
        subplot(Nsubfig1,1,thesubfig);
        plot(xstag,F_current,'g');
        title('Transverse force (green)');
    end
end

if (NP.fig2==1)
   figure(2);
   subplot(2,1,1);
    hold off;
    plot(0,0);
  %   xlim( [ 0 NP.FinalTime ] );
    title('Energy flux : Incident (red), reflected (green), transmitted (blue), net at inlet (black), net at outlet (cyan)  ');
    subplot(2,1,2);
    hold off;
    plot(0,0);
%    xlim( [ 0 NP.FinalTime ] );
    title('Total Energy in domain');
end

if (NP.fig3==1)
    figure(3);
    subplot(2,1,1);
    hold off;
    plot(0,0);
 %   xlim( [ 0 NP.FinalTime  ] );
    title('Conditions at x=0 : Y(0,t) (red) ; V(0,t) (blue) ; F(0,t) (green)   ');
    subplot(2,1,2);
    hold off;plot(0,0);
    xlim( [ 0 NP.FinalTime  ] );
    title('Conditions at x=L : Y(L,t) (red) ; V(L,t) (blue),  F(L,t) (green) ');
end

% Initialisations for figures
tab_t = []; tab_Y0 = []; tab_F0 = []; tab_V0 = []; 
tab_V0plus = []; tab_V0moins = [];
tab_YL = []; tab_FL = []; tab_VL = []; 
tab_VLplus = []; tab_VLmoins = [];
tab_Power_0 = []; tab_Power_0_plus = [];
tab_Power_L = []; tab_Power_L_plus = [];
tab_Power_0_Av = []; tab_Power_0_plus_Av = []; 
tab_Power_L_Av = []; tab_Power_L_plus_Av = [];
tab_TotalEnergy = []; tab_TotalEnergy_Av = [];
minfig1Y = 0; maxfig1Y = 0;minfig1V = 0; maxfig1V = 0;
minfig1F = 0; maxfig1F = 0;
minfig2 = -1; maxfig2 = 1;minfig2b=-0.1;maxfig2b=0.1; 
minfig3 = -1; maxfig3 = 1;


else
    
%% Post-processing actions at each time step
% First compute various statistics for figures 2 and 3

tab_t = [tab_t abs(t)];
tab_Y0 = [tab_Y0 Y_current(1)]; 
tab_V0 = [tab_V0 V_current(1)]; 
tab_F0 = [tab_F0 F_current(1)];

V0plus = (Z(1)*V_current(1)+F_current(1))/(2*Z(1));
V0moins = (Z(1)*V_current(1)-F_current(1))/(2*Z(1));
tab_V0plus =  [tab_V0plus V0plus]; 
tab_V0moins = [tab_V0moins V0moins]; 

tab_YL = [tab_YL Y_current(end)]; 
tab_VL = [tab_VL V_current(end)]; 
tab_FL = [tab_FL F_current(end)];

VLplus = (Z(end)*V_current(end)+F_current(end))/(2*Z(end));
VLmoins = (Z(end)*V_current(end)-F_current(end))/(2*Z(end));

tab_VLplus = [tab_VLplus VLplus]; 
tab_VLmoins = [tab_VLmoins VLmoins]; 


% Power at inlet and outlet (for figure 3)

tab_Power_0 = [tab_Power_0 F_current(1)*V_current(1)];
tab_Power_0_plus = [tab_Power_0_plus V0plus^2*Z(1)];
tab_Power_L = [tab_Power_L V_current(end)*F_current(end)];
tab_Power_L_plus = [tab_Power_L_plus VLplus^2*Z(end)];

Nt = round(abs(t)/dt);

Power_0_Av = mean(tab_Power_0(max([1,Nt-NP.N_averaging]):Nt));
tab_Power_0_Av = [tab_Power_0_Av Power_0_Av];

Power_0_plus_Av = mean(tab_Power_0_plus(max([1,Nt-NP.N_averaging]):Nt));
tab_Power_0_plus_Av = [tab_Power_0_plus_Av Power_0_plus_Av];

Power_L_Av = mean(tab_Power_L(max([1,Nt-NP.N_averaging]):Nt));
tab_Power_L_Av = [tab_Power_L_Av Power_L_Av];
Power_L_plus_Av = mean(tab_Power_L_plus(max([1,Nt-NP.N_averaging]):Nt));
tab_Power_L_plus_Av = [tab_Power_L_plus_Av Power_L_plus_Av];

TotalEnergy = dx/2*( .5*(V_current(1)^2*Z(1)/c(1)) ...
    + sum(V_current(2:end-1).^2.*Z(2:end-1)./c(2:end-1)) ...
    + .5*(V_current(end)^2*Z(end)/c(end)) ...
    + sum(F_current(2:end-1).^2./(Zstag(2:end-1).*cstag(2:end-1)))  );

tab_TotalEnergy = [tab_TotalEnergy TotalEnergy];
TotalEnergy_Av = mean(tab_TotalEnergy(max([1,Nt-NP.N_averaging]):Nt));
tab_TotalEnergy_Av = [tab_TotalEnergy_Av TotalEnergy_Av];

%% Figure 1 : Plot  the  graph  after  every  'NP.plotstep'  frame

if (t<0||mod( t/dt , NP.plotstep) == 0)   
    if(NP.fig1==1)
        Nsubfig1 = NP.figY+NP.figV+NP.figT;
        thesubfig = 1;
        figure(1);
        if(NP.figY==1)
            subplot(Nsubfig1,1,thesubfig);
            plot( x ,  Y_current,'r');
            plotgrid;
            title(['Y(x,t=' num2str(t), ')']);
            hold off;
            maxfig1Y = max([Y_current 1e-10 maxfig1Y]);
            minfig1Y = min([Y_current -1e-10 minfig1Y]);
            ylim([minfig1Y maxfig1Y]);
            thesubfig = thesubfig+1;
        end
        if(NP.figV==1)
            subplot(Nsubfig1,1,thesubfig);
            plot(x,V_current,'b' );
            plotgrid;
            title(['V(x,t=' num2str(t), ')']);
            hold off;
            maxfig1V = max([V_current 1e-10 maxfig1V]);
            minfig1V = min([V_current -1e-10 minfig1V]);
            ylim([minfig1V maxfig1V]);
            thesubfig = thesubfig+1;
        end
        if(NP.figT==1)
            subplot(Nsubfig1,1,thesubfig);
            plot(xstag,F_current,'g' );
            plotgrid;
            title(['F(x,t=' num2str(t), ')']);
            hold off;
            maxfig1F = max([F_current 1e-10 maxfig1F]);
            minfig1F = min([F_current -1e-10 minfig1F]);
            ylim([minfig1F maxfig1F]);
        end
        pause(NP.tpause)
    end

%% Figure 2 
    if (NP.fig2==1 )
        figure(2);
        subplot(2,1,1);
        plot(tab_t,tab_Power_0_Av,'k','LineWidth',4);
        hold on;
        if(NP.figVplusVmoins==1)
            plot(tab_t,tab_Power_0_plus_Av,'c','LineWidth',2);
            plot(tab_t,(tab_Power_0_Av-tab_Power_0_plus_Av),'m','LineWidth',2);
        end
        plot(tab_t,tab_Power_L_Av,'r','LineWidth',4);
        if(NP.figVplusVmoins==1)
            plot(tab_t,tab_Power_L_plus_Av,'b','LineWidth',2);
            plot(tab_t,(tab_Power_L_Av-tab_Power_L_plus_Av),'g','LineWidth',2);
        end
        if(NP.fig2inst==1)
            plot(tab_t,tab_Power_0,'k:','LineWidth',1);
        end
        if(NP.figVplusVmoins==1&&NP.fig2inst==1)
            plot(tab_t,tab_Power_0_plus,'c:','LineWidth',1);
            plot(tab_t,(tab_Power_0-tab_Power_0_plus),'m:','LineWidth',1);
        end
        if(NP.fig2inst==1)
            plot(tab_t,tab_Power_L,'r:','LineWidth',1);
        end 
        if(NP.figVplusVmoins==1&&NP.fig2inst==1)
            plot(tab_t,tab_Power_L_plus,'b:','LineWidth',1);
            plot(tab_t,(tab_Power_L-tab_Power_L_plus),'g:','LineWidth',1);
        end
       xlim( [ 0 NP.FinalTime ] );
        if(NP.figVplusVmoins==1)
            title('Energy flux at inlet and outlet');
            legend('P_0','P_0^+','P_0^-','P_L','P_L^+','P_L^-');
        else
            title('Net power at inlet (black) and at outlet (red)  ');
        end
        hold off;
        
        subplot(2,1,2);
        plot(tab_t,tab_TotalEnergy,'r:','LineWidth',1);
        hold on;
        plot(tab_t,tab_TotalEnergy_Av,'r','LineWidth',2);
        title('Total Energy in domain');
        xlim( [ 0 NP.FinalTime ] );      
        hold off;
    end

%% Figure 3
if (NP.fig3==1 )
        figure(3);
        subplot(2,1,1);
        plot(tab_t,tab_Y0,'r',tab_t,tab_V0,'b',tab_t,tab_F0,'g');
        hold on;
        if(NP.figVplusVmoins)
            plot(tab_t,tab_V0plus,'c',tab_t,tab_V0moins,'m')
        end
        maxtfig2 = max(t,NP.Tscalefig2);
        mintfig2 = maxtfig2-NP.Tscalefig2;
        xlim([ mintfig2 maxtfig2]);
        if(NP.figVplusVmoins==0)
            title('Conditions at x=0 : Y(0,t) (red) ; V(0,t) (blue), F(0,t) (green) ');
        else
            title('Conditions at x=0 : Y(0,t) (red) ; V(0,t) (blue), F(0,t) (green), Vplus (cyan), Vmoins (magenta) ');
        end
        hold off;  
        subplot(2,1,2);
        plot(tab_t,tab_YL,'r',tab_t,tab_VL,'b',tab_t,tab_FL,'g')
        hold on;
        if(NP.figVplusVmoins)
            plot(tab_t,tab_VLplus,'c',tab_t,tab_VLmoins,'m')
        end
        xlim([ mintfig2 maxtfig2 ]);
        if(NP.figVplusVmoins==0)
            title('Conditions at x=L : Y(L,t) (red) ; V(L,t) (blue), F(L,t) (green) ');
        else
            title('Conditions at x=L : Y(L,t) (red) ; V(L,t) (blue), F(L,t) (green), Vplus (cyan), Vmoins (magenta) ');
        end   
        hold off;
       end
     end
  end
end
%%
function plotgrid()
    global L X1 X2;
    
    hold on;
    if(~isempty(X1))
        plot( [X1 X1], [-1e12 1e12], 'k--');
    end
    if(~isempty(X2))
        plot([X2 X2], [-1e12 1e12], 'k--');
    end
    hold off;
end

%%
function plotCZ(problemtype,L)

x = 0:L/500:L;
[c,Z,S] = cZ(x,problemtype);

figure(4);
subplot(3,1,1);
plot(x,c);
title('wave velocity');xlabel('x');ylabel('c(x)');ylim([0 1.1*max(c)]);
subplot(3,1,2);
plot(x,Z);
title('specific impedance');xlabel('x');ylabel('Z(x)');ylim([0 1.1*max(Z)]);
if ~isempty(S)
    subplot(3,1,3);
    plot(x,S);
    title('Cross-section area (for pipe)');xlabel('x');ylabel('S(x)');ylim([0 1.1*max(S)]);
    
end
end


%%
function [c,Z,S] = cZ(x,problemtype)
global L X1 X2

% This function sets the physical properties of the 1D medium : 
% c  wave velocity (array with same dimension as x)
% Z  impedance of medium (array with same dimension as x)
% S  surface of the pipe/canal (not relevant for a string)

switch(problemtype)
    case('string')
% case of a string with constant velocity
T = 1;
mu = 1;
c = sqrt(T/mu)*ones(size(x));
Z = T./c;
S = [];

    case('string1disc')
% case of a string with a discontinuity;
T = 1;
c1 = 1;
c2 = .2;
X1 = .5;
c = c1*(x<X1)+c2*(x>=X1);
Z = T./c;
S = [];

    case('string2disc')
% case of a string with a discontinuity;
T = 1;
c1 = 1;
c2 = 4;
c3 = 1;
X1 = .5; 
X2 = .75;
c = c1*(x<X1)+c2*(x>=X1&x<X2)+c3*(x>=X2);
Z = T/c;
S = [];

    case('pipe')
% cylindrical pipe
c0 = 0.34;
S0 = 4e-4;
rho = 1.225; 
c = c0*ones(size(x));
Z = rho*c0/S0*ones(size(x));
S = S0*ones(size(x));

    case('pipe1disc')
% 2 connected cylindrical pipes
% NB for acoustic pipes the length/time unities are meter and millisecond
X1 = .5;
c0 = 0.34; % c=340 m/s = 0.34 m/(ms)
S1 = 1e-4;
S2 = 10e-4;
rho = 1.225; 
S = (x<X1)*S1+(x>=X1)*S2;
c = c0*ones(size(x));
Z = rho*c0./S;

   case('pipe2disc')
% 3 connected cylindrical pipes
% NB for acoustic pipes the length/time unities are meter and millisecond
X1 = .3;
X2 = .6;
c0 = 0.34; % c=340 m/s = 0.34 m/(ms)
S1 = 1e-4;
S2 = 25e-4;
S3 = 1e-4; 
rho = 1.225; 
S = (x<X1)*S1+(x>=X1&x<X2)*S2+(x>=X2)*S3;
c = c0*ones(size(x));
Z = rho*c0./S;

    case('horn')     
c0 = 0.34;
X1 = .5;
S0 = 4e-4;
m = 5; 
S = S0*(x<X1)+S0*(x>=X1).*cosh(m*(x-X1));
rho = 1.225;
c = c0*ones(size(x));
Z = rho*c./S;

case('canalprog')
% canal with progressive
% NB for canals the length/time/mass unities are kilometer, minute 
% and petagram (1 Petagram = 10^15 g = 10^12 kg)
X1 = .3; % 300 meters
X2 = .7; % 700 meters
g = 9.81*60^2/1000; % 9.81 m/s^2 converted in km/min^2
rho = 1; % rho in Pg/km^3
W = 1;
H1 = 0.05; % 50m
H2 = 0.01; % 10m 
H = (x<X1)*H1+(x>=X1&x<X2).*(H1+(H2-H1)*(x-X1)/(X2-X1))+(x>=X2)*H2;
c = sqrt(g*H);
Z = 1./(rho*W*sqrt(g^3*H));
S = W*H;


case('canal1disc')
% 2 connected canals
% NB for canals the length/time/mass unities are kilometer, minute 
% and petagram (1 Petagram = 10^15 g = 10^12 kg)
X1 = 100; % 100 km
g = 9.81*60^2/1000; % 9.81 m/s^2 converted in km/min^2
rho = 1; % rho in Pg/km^3
W1 = 10; % 10km
H1 = 0.2; % 200m
W2 = 0.1; % 100m
H2 = 0.01; % 10m 
H = (x<X1)*H1+(x>=X1)*H2;
W = (x<X1)*W1+(x>=X1)*W2;
c = sqrt(g*H);
Z = 1./(rho*g*W.*c);
S = W.*H;


    %case('custom')
        %  add your own case(s) here !

    otherwise
        error('Error : problemtype not recognized')
end
end

        


