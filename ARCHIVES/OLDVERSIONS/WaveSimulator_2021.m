
clear all;

Duration = 50;
dx = .01 ;
dt = .01 ;
L = 10;
plotstep = 20; % number of time steps between two plots
wait = 0; % set to 1 to advance step-by-step ; to zero to advance continuously


%For string with discontinuities 
X1 = 8; % position of first discontinuity
X2 = 15; % position of second discontinuity (set  X2>L if you want a single discontinuity)
c1 = 0.5;
c2 = 0.1;
c3 = 0.0002;

% For string loaded by a mass : set here the mass and position 
Mass = 0;
MassPosition = 4;
MassIndex=round(MassPosition/dx)+1;



BC_0  = 'I0'; 
BC_L = 'Transparent';
%%% boundary condidions : allowed options include :
%%%  'Fixed', 'Free', 'Pulse', 'Transparent', 'Y0', 'F0', 'I0', 'I0_Pulse'.

% parameters for sinusoidal forcing (Y0 or F0 or I0)
omega = pi/2;
Amp = 1;
smoothingtime = 100*dt; % to soften the starting (useful with Y0)

% parameters for 'pulse' and 'I0_Pulse' forcings 
t1 = 4;
ta = .5;

fig2 =0; % to plot various statistics in figure 2
figY =0; % to plot Y or not in figure 1
figV =1; % to plot V as well as Y in figure 1
figT =1; % to plot F as well as Y in figure 1
figVplusVmoins=0; % to decompose Vplus/vmoins in figure 2


%%% END OF PARAMETERS ; THE REMAINDER SHOULD NOT BE MODIFIED

stopTime = Duration;
% Time to  run  the  simulation

Nx = L/dx + 1;

x = 0:dx:L;
% Set  current  and  past  to  the  graph  of  a  plucked  s t r i n g
clear ('Y_current','Y_past','Y_future');

Y_current = 0*x;
V_current = 0*x;
Y_past = 0*x;
c = c1*(x<X1)+c2*(x>=X1&x<X2)+c3*(x>=X2);

r = c*dt/dx ;

figure(1);
plot(x,Y_current,'r',x,V_current,'b')
title('Initial shape (red) and velocity (blue)');
%if(BC_left==3)
    if (fig2==1)
    figure(2);
    subplot(3,1,1);
    hold off;
    plot(0,0);
    axis( [ 0 Duration -2 2 ] );
    title('Conditions at x=0 : Y(0,t) (red) ; dY/dt(0,t) (blue),  Vplus (cyan), Vmoins (magenta) ');
    subplot(3,1,2);
    hold off;plot(0,0);
    axis( [ 0 Duration -2 2 ] );
    title('Conditions at x=L : Y(0,t) (red) ; dY/dt(0,t) (blue),  Vplus (cyan), Vmoins (magenta) ');
    subplot(3,1,3);
    hold off;
    plot(0,0);
    axis( [ 0 Duration -2 2 ] );
     title('Energy flux : Incident (red), reflected (green), transmitted (blue), net at inlet (black), net at outlet (cyan)  ');

    end
    
pause

% Initialisations
tab_t = []; tab_Y0 = []; tab_F0 = []; tab_V0 = []; 
tab_V0plus = []; tab_V0moins = [];
tab_YL = []; tab_FL = []; tab_VL = []; 
tab_VLplus = []; tab_VLmoins = [];

tab_Power_0 = []; tab_Power_0_plus = [];
tab_Power_L = []; tab_Power_L_plus = [];
tab_Power_0_Av = []; tab_Power_0_plus_Av = []; 
tab_Power_L_Av = []; tab_Power_L_plus_Av = []; 


minfig1 = -1; maxfig1 = 1;minfig2 = -1; maxfig2 = 1;minfig2b=0.1;maxfig2b=0.1; minfig3 = -1; maxfig3 = 1;

% Loop over time
for t =0:dt:stopTime
   
% inner points : solve the wave equation
for j=2:Nx-1
Y_future(j) = r(j)^2*(Y_current(j-1)+Y_current(j+1))+2*(1-r(j)^2)*Y_current(j)-Y_past(j);
end

% if there is a mass along the string :
if(Mass~=0)
    ForceMass = (-3*Y_current(MassIndex)+4*Y_current(MassIndex+1)-Y_current(MassIndex+2))/dx ...
               - (3*Y_current(MassIndex)-4*Y_current(MassIndex-1)+Y_current(MassIndex-2))/dx;
    Y_future(MassIndex) = 2*Y_current(MassIndex)-Y_past(MassIndex)+ dt^2/Mass*ForceMass;
end


% boundary condition x=0:
switch(BC_0)
    case ('Fixed')
    Y_future(1) = 0;
    case('Free')
   Y_future(1) = 2*r(1)^2*Y_current(2)+2*(1-r(1)^2)*Y_current(1)-Y_past(1) ; 
    case('F0')
 Y_future(1) = 2*r(1)^2*Y_current(2)+2*(1-r(1)^2)*Y_current(1)-Y_past(1)+2*r(1)^2*dx*Amp*sin(omega*t); 
    case('Y0')
    Y_future(1)=Amp*sin(omega*(t+dt))*tanh((t+dt)/smoothingtime); % the smoothing period is to make the start less abrupt
    case('I0')
     Y_future(1) = (2*Y_current(1)+(r(1)-1)*Y_past(1)+2*r(1)^2*(Y_current(2)-Y_current(1)))/(1+r(1))...
         -Amp*sin(omega*t)*dt*4*c(1)*r(1)/(1+r(1));
    case('Pulse')
    Y_future(1) = Amp*exp(-(t-t1)^2/ta^2);
     case('I0_Pulse')
     Y_future(1) = (2*Y_current(1)+(r(1)-1)*Y_past(1)+2*r(1)^2*(Y_current(2)-Y_current(1)))/(1+r(1))...
         -Amp*exp(-(t-t1)^2/ta^2)*dt*4*c(1)*r(1)/(1+r(1));
    case default
    disp('WARNING : left boundary condition not implemented !');
end%switch

% boundary condition x=L:
switch(BC_L)
    case('Fixed')
    Y_future(Nx) = 0; 
    case('Free')
    Y_future(Nx) = 2*r(Nx)^2*Y_current(Nx-1)+2*(1-r(Nx)^2)*Y_current(Nx)-Y_past(Nx) ; 
    case('Transparent')
     Y_future(Nx) = (2*Y_current(Nx)+(r(Nx)-1)*Y_past(Nx)+2*r(Nx)^2*(Y_current(Nx-1)-Y_current(Nx)))/(1+r(Nx)); %no reflexion
    case default
    disp('WARNING : right boundary condition not implemented !');
end%switch


% end of main integration scheme ; next is for figure displays

% compute various statistics for figures 2 and 3

V_current = (Y_future-Y_past)/(2*dt);
tab_t = [tab_t t];
tab_Y0 = [tab_Y0 Y_current(1)]; 
tab_V0 = [tab_V0 V_current(1)]; 
tab_F0 = [tab_F0  (3*Y_current(1)-4*Y_current(2)+Y_current(3))/(2*dx)];

V0plus = (-(Y_future(1)-Y_past(1))/(2*dt*c(1))+(-3*Y_current(1)+4*Y_current(2)-Y_current(3))/(2*dx))/2; %% Amplitude incidente
V0moins = ((Y_future(1)-Y_past(1))/(2*dt*c(1))+(-3*Y_current(1)+4*Y_current(2)-Y_current(3))/(2*dx))/2; %% Amplitude reflechie
tab_V0plus =  [tab_V0plus V0plus]; 
tab_V0moins = [tab_V0moins V0moins]; 


tab_YL = [tab_YL Y_current(Nx)]; 
tab_VL = [tab_VL V_current(Nx)]; 
tab_FL = [tab_FL -(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx)];

VLplus = (-(Y_future(Nx)-Y_past(Nx))/(2*dt*c(Nx))-(-3*Y_current(Nx)+4*Y_current(Nx-1)-Y_current(Nx-2))/(2*dx))/2; %% Amplitude incidente
VLmoins = ((Y_future(Nx)-Y_past(Nx))/(2*dt*c(Nx))-(-3*Y_current(Nx)+4*Y_current(Nx-1)-Y_current(Nx-2))/(2*dx))/2; %% Amplitude reflechie
tab_VLplus = [tab_VLplus VLplus]; 
tab_VLmoins = [tab_VLmoins VLmoins]; 


% Power at inlet and outlet (for figure 3)

tab_Power_0 = [tab_Power_0 (3*Y_current(1)-4*Y_current(2)+Y_current(3))/(2*dx)*V_current(1)];
tab_Power_0_plus = [tab_Power_0_plus V0plus^2*c(1)];
tab_Power_L = [tab_Power_L -(V_current(Nx)*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx))];
tab_Power_L_plus = [tab_Power_L_plus VLplus^2*c(Nx)];

N_averaging = round(2*pi/omega/dt);
Nt = round(t/dt);

Power_0_Av = mean(tab_Power_0(max([1,Nt-N_averaging]):Nt));
tab_Power_0_Av = [tab_Power_0_Av Power_0_Av];

Power_0_plus_Av = mean(tab_Power_0_plus(max([1,Nt-N_averaging]):Nt));
tab_Power_0_plus_Av = [tab_Power_0_plus_Av Power_0_plus_Av];

Power_L_Av = mean(tab_Power_L(max([1,Nt-N_averaging]):Nt));
tab_Power_L_Av = [tab_Power_L_Av Power_L_Av];
Power_L_plus_Av = mean(tab_Power_L_plus(max([1,Nt-N_averaging]):Nt));
tab_Power_L_plus_Av = [tab_Power_L_plus_Av Power_L_plus_Av];

% Plot  the  graph  after  every  'plotstep'  frame
if mod( t/dt , plotstep) == 0   
    figure(1);
    if(figY==1)
        plot( x ,  Y_current,'r');
        title('String position Y (red)');
        hold on;
        maxfig1 = max([Y_current maxfig1]);
        minfig1 = min([Y_current minfig1]);
    end

    if(figV==1) 
        plot(x,V_current,'b' ), hold off;
        title('String position Y (red) and velocity V (blue)');
        maxfig1 = max([V_current maxfig1]);
        minfig1 = min([V_current minfig1]);
    end
     if(figT==1) 
        F_current(2:Nx-1) = -(Y_current(3:Nx)-Y_current(1:Nx-2))/(2*dx);
        F_current(1) = (3*Y_current(1)-4*Y_current(2)+Y_current(3))/(2*dx);
        F_current(Nx) = -(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx);
        hold on; plot(x,F_current,'g' ), hold off;
        %title('String position Y (red) , Velocity V (blue) and force F_T (green)');
        title('Velocity U'' (blue) and pressure P'' (green)');
        maxfig1 = max([F_current maxfig1]);
        minfig1 = min([F_current minfig1]);
    end
    
    
    
    hold on;
    plot( [X1 X1], [-1e12 1e12], 'k--', [X2 X2], [-1e12 1e12], 'k--');
    hold off;
    axis( [ 0 L minfig1 maxfig1 ] );
    
    pause(.1)
end

    if (mod( t/dt ,  20) == 0 & fig2==1 )
        figure(2);
        subplot(3,1,1);
        %plot(tab_t,tab_Y0,'r')
        %hold on; 
        plot(tab_t,tab_V0,'b',tab_t,tab_F0,'g')
        hold on;
        if(figVplusVmoins)
            plot(tab_t,tab_V0plus,'c',tab_t,tab_V0moins,'m')
        end
%       maxfig2 = max([tab_V0,tab_Y0,tab_F0 maxfig2]);
%       minfig2 = min([tab_V0,tab_Y0,tab_F0 minfig2]);
        Tscalefig2 = 20;
        maxtfig2 = max(t,Tscalefig2);
        mintfig2 = maxtfig2-Tscalefig2;
%       axis( [ mintfig2 maxtfig2 minfig2 maxfig2 ] );
        xlim([ mintfig2 maxtfig2]);
        if(figVplusVmoins==0)
            title('Conditions at x=0 : Y(0,t) (red) ; V(0,t) (blue), F(0,t) (green) ');
        else
            title('Conditions at x=0 : Y(0,t) (red) ; V(0,t) (blue), F(0,t) (green), Vplus (cyan), Vmoins (magenta) ');
        end
        hold off;
        
        subplot(3,1,2);
        plot(tab_t,tab_YL,'r',tab_t,tab_VL,'b',tab_t,tab_FL,'g')
        hold on;
        if(figVplusVmoins)
            plot(tab_t,tab_VLplus,'c',tab_t,tab_VLmoins,'m')
        end
 %       maxfig2b = max([tab_VL,tab_YL,tab_FL tab_VLplus tab_VLmoins maxfig2b]);
 %       minfig2b = min([tab_VL,tab_YL,tab_FL tab_VLplus tab_VLmoins minfig2b]);      
 %       axis( [ mintfig2 maxtfig2 minfig2b maxfig2b ] );
        xlim([ mintfig2 maxtfig2 ]);
        if(figVplusVmoins==0)
            title('Conditions at x=L : Y(L,t) (red) ; V(L,t) (blue), F(L,t) (green) ');
        else
            title('Conditions at x=L : Y(L,t) (red) ; V(L,t) (blue), F(L,t) (green), Vplus (cyan), Vmoins (magenta) ');
        end
        
        hold off;
        
        subplot(3,1,3);
        plot(tab_t,tab_Power_0,'k:','LineWidth',1)
        hold on;
        plot(tab_t,tab_Power_0_Av,'k','LineWidth',1);
        plot(tab_t,tab_Power_L_Av,'r','LineWidth',2);
        if(figVplusVmoins==1)
            plot(tab_t,tab_Power_0_plus_Av,'c','LineWidth',2);
            plot(tab_t,-(tab_Power_0_Av-tab_Power_0_plus_Av),'m','LineWidth',2);
            plot(tab_t,tab_Power_L_plus_Av,'b','LineWidth',2);
        end
       
%        plot(tab_t,tab_Power_L_Av-tab_Power_L_plus_Av,'c','LineWidth',2);
       
       
        
        hold off;
        maxfig3 = max([tab_Power_0_Av,tab_Power_0_plus_Av,tab_Power_L_Av, maxfig3]);
        minfig3 = 0;
        axis( [ 0 Duration minfig3 maxfig3 ] );
        if(figVplusVmoins==1)
            title('Energy flux : Incident (cyan), reflected (magenta), transmitted (blue), net at inlet (black), net at outlet (red)  ');
        else
            title('Net power at inlet (black) and at outlet (red)  ');
        end
        
        if(wait==1) 
            pause
        end
    
end

% Set  things  up  for  the  next  time  step
Y_past = Y_current ;
Y_current = Y_future ;

end






