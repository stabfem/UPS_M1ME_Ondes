\documentclass[11pt,a4paper]{article}
 
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{fancyhdr}
\usepackage{graphicx, amsmath, amssymb, latexsym}
%\usepackage{natbib}
 
 
\setlength{\textheight}{24.5cm} % 24
\setlength{\topmargin}{-1mm}
\setlength{\textwidth}{16cm}  % 15
%\setlength{\oddsidemargin}{20mm} % marge gauche (im)paire = 1in. + x1mm
%\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\oddsidemargin}{0mm} % marge gauche (im)paire = 1in. + x1mm
\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\fboxsep}{5mm}

\newcommand{\e}  {\mathrm{e}}   
\newcommand{\dd}  {\textrm d}      
\newcommand{\im}  {\mathrm{i}} %\newcommand{\im}  {\textrm i}
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddp}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {{\textrm d}^{2} #1} {{\textrm d} #2 ^{2}}}
%\newcommand{\tensor}[1]{\stackrel{\Rightarrow}{#1}}

\newcommand{\comment}[1]{}

%\graphicspath{{../Figures/}{../FiguresPierre/}}
\graphicspath{Figures/}

%\makeindex


\usepackage{listings}
\usepackage{color}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
 \usepackage{hyperref}

\newcommand{\lstsetFF}{
\lstset{frame=tb,
  language=c++,
  aboveskip=5mm,
  belowskip=5mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{blue},
  keywordstyle=\color{red},
  commentstyle=\color{mauve},
  stringstyle=\color{green},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3}
}

%------------------------------------------------------------------------------
\begin{document}

%\begin{titlepage}

\noindent
\textsc{Universit\'e Paul Sabatier \hfill Ann\'ee 2023/2024} \\
\textsc{M1 Mécanique \hfill Module Ondes}
%\vspace{1cm}

%\begin{center}


%\section*{TP numérique : ondes sur une corde} 
\begin{center}
{\Large \bf Mini-projet Ondes \\ 
Circuit acoustique dans un domaine axisymétrique ; \\ 
Notice d'utilisation des programmes FreeFem/StabFem}
\end{center}



\begin{figure}[h]
%$$
%\includegraphics[width= .7\linewidth]{Figures/reponse_forcage.eps}
$$
\begin{array}{cc}
\includegraphics[height = .4\linewidth]{../TDs_Ondes/Figures/FlangedPipe_SolutionFF.png}
& \hspace{-1.7cm}
\includegraphics[height = .4\linewidth]{../TDs_Ondes/Figures/ForcedFlow_CC.png}	
\\
(a) & (b)
\end{array}
$$
\caption{Illustration de la résolution numérique avec FreeFem/StabFem du problème acoustique en régime harmonique établi, pour ($a$) un tuyau cylindrique affleurant (c.a.d. débouchant dans un demi-espace), et $(b)$ un tuyau conique-cylindrique ouvert (c.a.d. débouchant dans un espace complet).
Tracé de la partie imaginaire de $\phi(r,z)$ dans la moitié gauche et de la partie réelle dans la moitié droite.}
\end{figure}

On souhaite résoudre à l'aide d'une méthode d'éléments finis l'équation des ondes en coordonnées
cylindriques ($r,\theta,z$), dans un domaine $\Omega$, de frontière $\Gamma$, représentant par exemple le tuyau d'un instrument à vent débouchant dans un domaine ouvert ou encore un pot d'échappement.

En toute généralité on décompose la frontière en 4 parties, en posant :
$\Gamma = \Gamma_w \cup \Gamma_i \cup \Gamma_{a} \cup \Gamma_o$.

\begin{itemize}
\item $\Gamma_w$ correspond aux murs rigides (walls); 
\item $\Gamma_a$ correspond à l'axe de symétrie en $r=0$;
\item $\Gamma_o$ correspond à la "sortie" du domaine (limite du domaine calculé).
\item $\Gamma_i$ correspond à l'"entrée" du domaine à la base de celui-ci (en $z=z_{in}$), sur laquelle on impose un forçage. Trois types de forçage peuvent être considérés :
\begin{description}
\item[$(i)$] "Forçage en débit" : on impose un débit (et donc une vitesse) à la base du circuit  (comme par exemple dans le cas du tuyau affleurant, figure $3(a)$). 
\item[$(ii)$] "Forçage en pression" : on impose une pression. 
\item[$(iii)$] "Forçage par une onde incidente" : on suppose que l'entrée du domaine numérique est raccordée à un tuyau de grande longueur, duquel provient une onde incidente, qui est en partie réfléchie (comme par exemple dans le cas du bidioptre, figure $3(b)$). 
\end{description}
\end{itemize}




On introduit le potentiel des vitesses $\Phi(r,z,t) = \phi(r,z) e^{-i \omega t}$
(on s'intéresse à des solutions axisymétriques ne dépendant pas de la coordonnée $\theta$, et selon la convention habituelle on ne retient que la partie réelle de cette expression complexe). 


\section{Formulation variationnelle du problème acoustique}


\subsection{Equation des ondes}


Rappelez l'équation des ondes et montrez que celle-ci se met sous la forme :

\begin{equation}
\Delta \phi + k^2 \phi = 0  \qquad \mbox{ pour } (r,z) \in \Omega
\label{eq:Ondes}
\end{equation}

où $k = \omega/c$ est le nombre d'onde acoustique.

Rappelez l'expression de l'opérateur Laplacien en coordonnées cylindriques $r,z$.

Montrez que les champs de vitesse et de pression se déduisent du potentiel des vitesses par
$$
{\bf u} = \nabla \Phi = [u_{r},u_z] =  [\partial \phi/\partial r_c,\partial \phi/\partial z] e^{-i \omega t};
$$
$$ 
p = { -} \rho \partial \Phi / \partial t = { + } i \omega \rho \phi e^{-i \omega t}.
$$




\subsection{Condition de sortie "non réfléchissantes"}



Etant donné que la "sortie" du domaine (frontière $\Gamma_o$) n'est pas une surface physique mais la frontière extérieure d'un domaine numérique nécessairement de taille finie, il faut s'assurer d'imposer sur cette sortie des conditions limites qui n'induisent pas de réflexion artificielle.




\paragraph{Sortie "1D"} 
\,

Considérons d'abord le cas où la sortie du domaine est supposée être un tube de grande longueur, 
tronqué à la position de sortie (en $z=L$) ; comme par exemple dans le cas du bidioptre (cf figure $(a)$).
Justifiez qu'au voisinage de la sortie du domaine, on peut supposer que la solution est de la forme suivante : 
$$
\phi(r,z) \approx C e^{i k z} + D e^{-i k z}
$$
Justifiez physiquement pourquoi $D$ est nécessairement nul.


Montrez que $D=0$  si et seulement si la condition suivante, dite {\em condition de Sommerfeld}, est satisfaite :

\begin{equation}
{\left( \partial \phi/ \partial z\right)}_{z=L} - i k \phi(r,L) = 0 
\label{eq:Sommerfeld}
\end{equation}

{\color{red} En pratique, ce cas est traité par les solveurs FreeFem fournis en affectant le label "31" à la frontière de sortie du domaine.}




\paragraph{Sortie "3D"} 
\,

Considérons maintenant le cas où la sortie du domaine est supposée être un espace libre tridimensionnel, et où la frontière 
$\Gamma_o$ est une sphère de rayon $R_e$ ; comme par exemple dans le cas du tube affleurant (cf figure $(a)$).
Dans ce cas il est pertinent d'utiliser les coordonnées sphériques  $(R;\theta,\varphi)$ où $R = \sqrt{r^2 + z^2}$.
A partir de l'équation des ondes en coordonnées sphériques, montrez que "loin de la sortie du tuyau" celle-ci admet une solution de la forme 

$$
\phi \approx \frac{ C e^{i k R} + D e^{-i k R}}{R}.
$$

Justifiez pourquoi $D$ est nécessairement nul, et montrez que $D=0$  si et seulement si la condition suivante  est satisfaite :
\begin{equation}
\left[ \partial \phi/ \partial R - (i k \phi - \phi / R ) \right]_{R = R_e} = 0 
\label{eq:Sommerfeld}
\end{equation}

{\color{red} En pratique, ce cas est traité par les solveurs FreeFem fournis en affectant le label "3" à la frontière de sortie du domaine.}

%Cette condition est valable lorsque $R \rightarrow \infty$. En pratique, on l'utilise comme condition limite à la frontière externe $\Gamma_o$ du domaine de calcul.




\subsection{Formulation faible}

On prend comme frontière extérieure une surface sphérique de rayon $R = R_{max}$. On considère tout d'abord le cas $(i)$ "forcé en débit", c.a.d. qu'à l'entrée a condition limite est $q' = Q_{in} e^{-i\omega t}$.

Justifiez que le problème à résoudre est le suivant :

\begin{eqnarray}
\Delta \phi + k^2 \phi = 0  \qquad &\mbox{ pour } & (r,z) \in \Omega
\label{eq:Ondes}
\\
\nabla \phi \cdot {\bf n} - (i k \phi - \delta_{3D} \, \frac{\phi}{\sqrt{r^2+z^2}}) =0 &\mbox{ sur }& \Gamma_o, \qquad \delta_{3D} = \left\{ \begin{array}{ll} 0 & \mbox{cas 1D} \\ 1 & \mbox{cas 3D}\end{array} \right. 
\\
\partial \phi/ \partial z  = Q/S_{in}  &\mbox{ sur }&  \Gamma_i,
\\
\partial \phi/ \partial r  = 0  &\mbox{ sur }&  \Gamma_a,
\\
\nabla \phi \cdot {\bf n} = 0  &\mbox{ sur }&  \Gamma_w,
\end{eqnarray}




Pour résoudre le problème à l'aide d'une méthode d'éléments finis, on introduit une fonction test 
$\phi^*$.

On part de la formulation faible suivante :

\begin{equation}
\forall \phi^*,  
\int\int\int \mbox{[Eq. Ondes (Eq. 4) ]} \times \phi^* dV -
\int\int_{\Gamma_o} \mbox{[Cond. Somm. (Eq. 5)] } \times \phi^* dS $$
$$
-
\int\int_{\Gamma_w \cup \Gamma_a} \nabla \phi \cdot {\bf n}  \times \phi^* dS-
\int\int_{\Gamma_i} (\nabla \phi \cdot {\bf n} - Q/S)  \times \phi^* dS = 0.
\end{equation}

Ici $dV = 2\pi r d r dz$ et $dS = 2\pi r d\ell$ sont les volumes et surface élémentaire et $d\ell$ l'élément de longueur élémentaire le long de $\Gamma$.  

A l'aide d'une intégration par parties, montrez que cette formulation est équivalente à la suivante :
\begin{equation}
\forall \phi^*,  
\int\int\int \left( - \nabla \phi^* \cdot \nabla \phi + k^2 \phi^* \phi \right)  dV +
 \int\int_{\Gamma_o}  \phi^* (i k \phi - \delta_{3D} \, \phi / R )  dS +
\int\int_{\Gamma_i}  \phi^* Q/S_{in}  dS = 0
\end{equation}


\subsection{Post-traitement des résultats}

Dans le post-traitement des résultats, on peut s'intéresser à des quantités globales ou "fonctions de transfert" qu'on souhaite maximiser ou minimiser. La définition peut varier selon le type de forçage en entrée :
\begin{description}
\item[$(i)$] Dans le cas "forcé en débit", la fonction de transfert est l'{\em impédance d'entrée} $Z_{in}$, déduite de la solution du problème par :
\begin{equation}
Z_{in}(\omega) = \frac{p'}{q'} =  \frac{ \frac{1}{S_{in}} \int_{\Gamma_i}  (i \rho \omega \phi ) dS}{\int_{\Gamma_i}  \partial \phi/\partial z dS }
\end{equation}
(on remarque qu'on prend ici la valeur moyenne sur la section de la pression).

Les fréquences de résonances correspondent aux maximums de $|Z_{in}|$. 
\item[$(ii)$] Dans le cas "forcé en pression", la fonction de transfert est l'{\em admittance d'entrée} $Y_{in} =  \frac{q'}{p'} = 1/Z_{in}$. Les fréquences de résonance correspondent aux maximums de $|Y_{in}|$ et donc aux minimums de $|Z_{in}|$.
 

\item[$(iii)$] Dans le cas "forcé par une onde incidente", la solution pour $z \leq z_{in}$ est supposée de la forme 
$\phi(r,z) \approx A e^{i k z'} + B e^{-i k z'}$, où $A$ est l'amplitude de l'onde incidente (imposée), $B$ celle de l'onde réfléchie (inconnue), et $z' = z-z_{in}$. La pression et le débit sont respectivement donnés par $p'(r,z) = -i \omega \rho ( A e^{i k z'} + B e^{-i k z'})$ et $q'(r,z) = i k S_{in} ( A e^{i k z'} - B e^{-i k z'})$. L'impédance d'entrée est donc $Z_{in} = Z_0 \frac{A+B}{A-B}$ avec $Z_0 =  \frac{\rho c}{S_{in}}$. Cette relation permet de calculer le coefficient de réflexion en amplitude $B/A$ : 
$$
\frac{B}{A} = \frac{Z_{in} - Z_0}{Z_{in}+ Z_0} 
$$
On peut finalement en déduire les coefficient de réflexion et de transmission en énergie :
$$ 
{\cal R}(\omega) = \left| \frac{B}{A} \right|^2 =  \left| \frac{Z_{in} - Z_0}{Z_{in}+ Z_0}  \right|^2 ; \qquad {\cal T}(\omega) = 1 - {\cal R}(\omega)
$$
 
\end{description}

En résumé, si l'on dispose d'un programme permettant de résoudre le problème dans le cas $(i)$ du forçage en débit et de calculer la fonction de transfert  $Z_{in}(\omega)$ associée, celui-ci permettra également de traiter les cas de forçage $(ii)$ et $(iii)$, les fonction de transfert caractérisant ces cas pouvant être déduites de $Z_{in}(\omega)$ au moment du post-traitement.

\section{Prise en main pour le premier cas : Tuyau cylindrique encastré}

On fournit un ensemble de programmes en FreeFem++ permettant de résoudre ce type de problème. On pourra également utiliser la suite StabFem (Interface en Matlab/Octave pour FreeFem++ développée à l'IMFT).  

\paragraph{\bf 2.1 Construction du maillage.} On a besoin pour cela d'un fichier de la forme \verb|Mesh_###.edp| qui construit un maillage et le sauvegarde dans un fichier \texttt{WORK/mesh.msh}. Par exemple, le fichier \texttt{Mesh\_FlangedPipe.edp} dont un extrait est donné ci-dessous :

\lstsetFF
\begin{lstlisting}
border axe1(t=Rext,0){x=0;y=t;label=6;}; // label 6 pour "axe de symetrie"
border axe2(t=0,-H){x=0;y=t;label=6;};
border fond(t=0,Rpipe){x=t;y=-H;label=1;};  // label 1 pour "entree"
border mur(t=-H,0){x=Rpipe;y=t;label=2;};  // label 2 pour "mur"
border plate(t=Rpipe,Rext){x=t;y=0;label=2;};
border sphere(t=0,pi/2){x=Rext*cos(t);y=Rext*sin(t);label=3;}; // label 3  "sortie"

mesh th = buildmesh(axe1(d*(Rext))+axe2(dpipe*(H))+fond(dpipe*Rpipe)+mur(dpipe*H)
                    +plate(d*(Rext-Rpipe))+sphere(pi/2*Rext*d/4) );

savemesh(th,ffdatadir+"mesh.msh"); // ffdatadir est par defaut "WORK/"
\end{lstlisting}

Vous pouvez vous inspirer de ce programme pour créer votre propre maillage; dans ce cas prenez soin d'utiliser la même convention pour les labels des différentes frontières.

Pour utiliser ce programme deux méthodes (pour un tuyau de longueur $L =60cm$ et de rayon $a = 6cm$) :
\begin{itemize}
\item Directement depuis la console shell, en mode ligne de commande : 

\verb| FreeFem++ Mesh_FlangedPipe.edp -L 0.6 -a 0.06 |

\item Depuis Matlab/Octave en utilisant l'interface StabFem : 

\verb| ffmesh = SF_Mesh('Mesh_FlangedPipe.edp','Options',{'L',0.6,'a',0.06);|
\end{itemize}
Notez que les paramètres $L$ et $a$ (en $m$) sont détectés par la commande \texttt{getARGV} dans le programme FreeFem++.
Notez aussi que si vous utilisez l'interface StabFem vous pouvez utiliser l'adaptation de maillage; voir les exemples fournis.

\paragraph{\bf 2.2 Résolution du problème avec forçage en débit. } Pour cela on fournit un programme
\verb|LinearForced_Acoustics.edp| dont un extrait est donné ci-dessous : 

\lstsetFF
\begin{lstlisting}
mesh th= readmesh(ffdatadir+"mesh.msh");

fespace VH(th,P2);
VH<complex> f,f1;

real omega = getARGV("-omega",1.);
solve HelmholtzSOMMERFELD(f,f1) =
         int2d(th)( (-(dx(f)*dx(f1)+dy(f)*dy(f1))+k^2*f*f1)*2*pi*x )
       - int1d(th,1)(f1*2*pi*x)  // condition d'entree
	   + int1d(th,3)((1i*k-1/sqrt(x^2+y^2))*f*f1*2*pi*x) // condition de sortie 3D
	   + int1d(th,31)((1i*k)*f*f1*2*pi*x); // condition de sortie 1D
	   
complex Zin = ( int1d(th,1)(1i*omega*rho*f*2*pi*x) / int1d(th,1)(2*pi*x) ) / ind1d(th,1)(dy(f)*2*pi*x);	   
\end{lstlisting}
	
Il n'est normalement pas nécessaire de modifier ce programme. Notez que celui-ci permet de traiter des conditions de sortie "1D" ou "3D", selon le label qui a été donné à la frontière de sortie (31 ou 3).

Pour utiliser ce programme deux méthodes (considérant le maillage construit à l'étape précédente, et avec les paramètres 
$\rho = 1.225 kg/m^3$, $c=0.34 m/ms$, $\omega = 2 rad/ms$) :
\begin{itemize}
\item Directement depuis la console shell : 

\verb| FreeFem++ LinearForced_Acoustics.edp -omega 2 -c 0.34 -rho 1.225|

Le programme affiche la partie réelle du champ de pression. Il génère également un fichier \verb| Data_Axe.txt | 
qui contient les valeurs de la pression et de la vitesse sur l'axe, que vous pouvez importer avec Matlab ou Python.

\item Depuis Matlab/Octave en utilisant l'interface StabFem : 

\verb| Forced = SF_Launch('LinearForced_Acoustics.edp','mesh',ffmesh, ... |

\verb|                    'Options',{'omega',2,'c',0.34,'rho',1.225}); |

%Pour générer les figures, voir l'exemple de script fourni.

\end{itemize}


\paragraph{\bf 2.3 Calcul des fonctions de transfert pour une gamme de fréquences $\omega$.} 
Pour cela on fournit un programme
\verb|LinearForcedLoop_Acoustics.edp| qui est basé sur le précédent et fait une boucle sur $\omega$.
Ce programme peut être utilisé :
\begin{itemize}
\item Directement depuis la console shell : 

\hspace{-2cm}{\small \verb| FreeFem++ LinearForcedLoop_Acoustics.edp -omegamin 0 -omegamax 10 -Nomega 101 -c 0.34 -rho 1.225|}

Le programme génère un fichier \verb| Data.txt | 
qui contient la fréquence (en colonne 2), ainsi que les parties réelle et imaginaires de l'impédance (colonnes 4 et 5).
%Pour importer ce fichier avec Matlab et tracer la norme de l'impédance en fonction de la fréquence, vous pouvez
%faire ainsi :

Pour tracer l'impédance contenue dans ce fichier et comparer avec la théorie 1D, 
vous pouvez (après l'avoir renommé sous le nome \verb|  Zin\_Cylinder\_FF.csv |) utiliser le programme python 
{\bf Impedance\_Cyl\_Comparaison.py}.

%\verb| M = importdata('WORK/Data.txt'); |
%
%\verb| figure ; semilogy(M(:,2)*1000/(2*pi),sqrt(M(:,5).^2+M(:,4).^2)); |
%
%\verb? xlabel(' f [Hz] ') ; ylabel(' |Z_{in}| '); ?
\item Depuis Matlab/Octave en utilisant l'interface StabFem : 

\verb| IMP = SF_Launch('LinearForcedLoop_Acoustics.edp','mesh',ffmesh, ... |

\verb|   'Options',{'c',0.34,'rho',1.225,'omegamin',0,'omegamax',10,'Nomega',201}); |
\end{itemize}

%Notez que dans la figure, la fréquence (ou "pulsation" ) $\omega$ (en $rad/ms$) a été convertie en fréquence $f$ (en $Hz$).

\section{Deuxième cas de prise en main : tuyau conique - cylindrique}

Pour étudier ce cas on pourra utiliser le programme {\bf Mesh\_ConCylFlanged.edp} fourni, qui crée un maillage pour la géométrie considérée. On utiliser par la suite les programmes 
 {\bf LinearForced\_Acoustics.edp} et {\bf  LinearForcedLoop\_Acoustics.edp} de la même manière que précédemment.


%\section{Exemple de script FreeFem/StabFem}

%On pourra s'inspirer de l'exemple suivant, disponible sur le site web du projet StabFem :

%\url{https://stabfem.gitlab.io/StabFem//ACOUSTICS_PIPES/SCRIPT_PIPE_60cm.html}





\end{document}
