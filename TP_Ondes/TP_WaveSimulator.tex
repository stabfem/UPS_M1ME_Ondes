\documentclass[11pt,a4paper]{article}
 
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{fancyhdr}
\usepackage{graphicx, amsmath, amssymb, latexsym}
%\usepackage{natbib}
 
 
\setlength{\textheight}{24.5cm} % 24
\setlength{\topmargin}{-5mm}
\setlength{\textwidth}{16cm}  % 15
%\setlength{\oddsidemargin}{20mm} % marge gauche (im)paire = 1in. + x1mm
%\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\oddsidemargin}{0mm} % marge gauche (im)paire = 1in. + x1mm
\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\fboxsep}{5mm}

\newcommand{\e}  {\mathrm{e}}   
\newcommand{\dd}  {\textrm d}      
\newcommand{\im}  {\mathrm{i}} %\newcommand{\im}  {\textrm i}
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddp}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {{\textrm d}^{2} #1} {{\textrm d} #2 ^{2}}}
%\newcommand{\tensor}[1]{\stackrel{\Rightarrow}{#1}}

\newcommand{\comment}[1]{}

%\graphicspath{{../Figures/}{../FiguresPierre/}}
\graphicspath{Figures/}

%\makeindex

%------------------------------------------------------------------------------
\begin{document}

%\begin{titlepage}

\noindent
\textsc{Universit\'e Paul Sabatier \hfill Ann\'ee 2022/2023} \\
\textsc{M1 Mécanique \hfill Module Ondes }
%\vspace{1cm}

%\begin{center}


%\section*{TP numérique : ondes sur une corde} 
\begin{center}
{\Large \bf Mini-projet Ondes \\ 
%Propagation d'ondes dans un milieu monodimensionnel ; \\ 
Approche monodimensionnelle, étude des régimes transitoires  \\
Notice d'utilisation du programme \texttt{WaveSimulator.m}
}
\end{center}

%\renewcommand{\thesubsection}{\arabic{subsection}}

\section{Théorie : équation des ondes dans un milieu 1D}

Dans ce TP, on va utiliser un programme qui simule la propagation d'ondes non-dispersives dans un milieu monodimensionel inhomogène, 
en partant d'une formulation unifiée qui englobe les cas d'une corde tendue, d'un tuyau acoustique et d'un canal en faible profondeur.


\subsection{Ondes sur une corde inhomogène}

Considérons tout d'abord le cas d'une corde tendue de tension $T$ (constante) et masse linéique $\mu(x)$. On peut formuler le problème à l'aide de deux variables couplées, la vitesse $V(x,t)$ et la force transverse $F_T(x,t)$ (définie comme la composante transverse de la force exercée par le tronçon de corde $[0,x]$ sur le tronçon $[x,L]$) . Celles-ci se déduisent de la variable primitive "déplacement transverse"  $Y(x,t)$ par les relations suivantes :
\begin{equation}
V = \ddp{Y}{t} ; \qquad F_T = - T \ddp{Y}{x}.
\end{equation}

Les variables $V$ et $F_T$ sont couplées par les deux équations aux dérivées partielles suivantes :

\begin{equation}
\mu \ddp{V}{t} = -  \ddp{F_T}{x} ; \qquad 
\ddp{F_T}{t} = -T \ddp{V}{x}.
\end{equation}

Qui peuvent se combiner pour donner l'équation des ondes pour la variable primitive $Y$: 

\begin{equation}
\ddp{^2Y}{t^2}   = c^2 \ddp{^2Y}{x^2}
\end{equation}

\subsection{Ondes dans un tuyau acoustique}

Considérons maintenant le problème des ondes acoustiques dans un tuyau de section variable $S(x)$. Supposons que les propriétés du fluide sont également inhomogènes (notées $\rho(x)$, $c(x)$).

Notons $q' = S u'$ le débit de volume et $p'$ la pression acoustique. Ces deux variables sont couplées par les deux équations différentielles suivantes :

\begin{equation}
\frac{S}{c^2} \ddp{p'}{t} = - \rho \ddp{q'}{x} ; \qquad 
\rho \ddp{U}{t} = - S \ddp{p'}{x}.
\label{eq:acoustiquecouplees}
\end{equation}

On peut aboutir à une équation similaire à celle du paragraphe précédent en introduisant la variable primitive ${\tilde X} = X S$ "déplacement de volume" (où $X$ est le déplacement lagrangien). Les deux variables $q'$ et $p'$ s'en déduisent par :

\begin{equation}
q' =  \ddp{\tilde{X}}{t} ; \qquad p' = - \frac{\rho c^2}{S} \ddp{\tilde{X}}{x}.
\end{equation}
Et les deux équations (\ref{eq:acoustiquecouplees}) peuvent être combinées pour obtenir l'équation suivante :

\begin{equation}
\ddp{^2\tilde{X}}{t^2}   = \frac{S}{\rho}  \ddp{}{x} \left( \frac{\rho c^2}{S} \ddp{\tilde{X}}{x} \right) 
\end{equation}

Cette équation est équivalente à celle vue en cours pour la variable $p'$ (Equation de Webster) mais est ici formulée en variable "primitive" $\tilde{X}$.


\subsection{Ondes dans un canal de faible profondeur}

Considérons maintenant un canal de profondeur $H(x)$ et largeur $W(x)$ variables. Dans le cas où la longueur d'onde est grande devant la profondeur, il est justifié de supposer que la vitesse $u'$ est uniforme dans chaque tranche. En notant $\eta$ l'élévation de la surface libre, les bilans de masse et de quantité de mouvement sur une volume élémentaire correspondant à une tranche d'épaisseur $dx$ conduisent aux deux équations couplés suivantes (cf. exercice 5.3.1 du fascicule de TD):
\begin{equation}
\rho W \ddp{\eta}{t} = -\rho \ddp{ (W H u') }{x} ; \qquad 
\rho H W  \ddp{u'}{t} = - g W H  \ddp{\eta}{x}.
\end{equation}

En introduisant $\tilde{Q} = \rho g H W u'$ le "débit-poids" , les relations prennent la forme

\begin{equation}
\rho W \ddp{\eta}{t} = -\frac{1}{g} \ddp{\tilde{Q}}{x} ; \qquad 
\frac{1}{g} \ddp{\tilde{Q}}{t} = - \rho g H W  \ddp{\eta}{x}.
\end{equation}



On peut de nouveau introduire la variable primitive ${\tilde X} = \rho g H W X$ "déplacement de poids" (où $X$ est le déplacement lagrangien). Les deux variables $\tilde{Q}$ et $\eta$ s'en déduisent par :

\begin{equation}
\tilde{Q} =  \ddp{\tilde{X}}{t} ; \qquad \eta = - \frac{1}{\rho g W} \ddp{\tilde{X}}{x}.
\end{equation}
Et ces deux équations peuvent être combinées pour obtenir l'équation suivante :

\begin{equation}
\ddp{^2\tilde{X}}{t^2}   = g H W  \ddp{}{x} \left( \frac{1}{W} \ddp{\tilde{X}}{x} \right).
\end{equation}





\subsection{Formulation générale}

On remarque que dans les trois problèmes précédents on a une variable "vitesse" ${\cal V}$ et une variable "force" ${\cal F}$ couplées par deux équations aux dérivées partielles qui s'écrivent :
\begin{equation}
\ddp{{\cal V}}{t} = - \frac{c}{Z} \ddp{\cal F}{x} ; \qquad 
\ddp{\cal F}{t} = -  c Z  \ddp{\cal V}{x}
\end{equation}
où les définitions de la vitesse $c$ et de l'impédance sont celles données dans le tableau "analogies" du fascicule de TD. 
On note ensuite qu'en introduisant la variable primitive ${\cal Y} = \int {\cal V} \,dt $ les deux équations se combinent sous la forme suivante :

\begin{equation}
\ddp{^2 \cal Y}{t^2}   = \frac{c}{Z}  \ddp{}{x} \left( c Z  \ddp{{\cal Y}}{x} \right) 
\label{eq:WaveUnified}
\end{equation}

\subsubsection{Energie et puissance}

Dans les trois cas considérés, on peut de plus remarquer que l'énergie mécanique linéique se met sous la forme 
\begin{equation} 
e_m =  \left( \frac{Z {\cal V}^2}{2 c} + \frac{ {\cal F}^2}{2 Z c} \right),
\end{equation}
et que la puissance échangée à la position $x$ s'écrit
\begin{equation}
{\cal P}(x,t) = {\cal V}(x,t) \cdot {\cal F}(x,t).
\end{equation}
En supposant que la solution peut être mise  {\em localement}  sous forme de d'Alembert, soit ${\cal V}(x,t) = {\cal V}^+(x-ct) +{\cal V}^-(x+ct)$ 
et  ${\cal F}(x,t) = {\cal F}^+(x-ct) - {\cal F}^-(x+ct)$  , cette puissance peut alors être décomposée sous la forme 
\begin{equation}
{\cal P} ={\cal P}^+ +  {\cal P}^- , \quad 
\mbox{ avec } {\cal P}^+ =  {\cal V}^+ \cdot {\cal F}^+ > 0 \quad 
\mbox{ et } {\cal P}^- =  {\cal V}^- \cdot {\cal F}^- < 0.
\end{equation}
On reconnait sous cette forme le flux d'énergie (positif) associé l'onde de vitesse $+c$ et celui (négatif) associé à l'onde de vitesse $-c$.


\section{Présentation du programme}

Pour ce TP, on fournit un programme  \verb| WaveSimulator.m | qui résout l'équation des ondes sous la forme unifiée (\ref{eq:WaveUnified}).
Ce programme est une version (très) améliorée du programme simple \verb| WaveEquation_FiniteDifferences.m |  utilisé dans le TP précédent. Voici quelques explications sur le fonctionnement du programme et la manière de l'adapter au cas qui vous intéresse :

\paragraph{Propriétés du milieu}

Dans le programme, les propriétés du milieu sont données par les lois $c(x)$ et $Z(x)$ (éventuellement discontinues). 
Différents cas sont déjà prédéfinis dans le programme selon la valeur de la variable \verb| problemtype|. Les cas prédéfinis sont :
\begin{itemize}
\item \verb| string | : corde homogène.
\item \verb| pipe | :  tuyau cylindrique.
\item \verb| pipe1disc | : 2 tuyaux cylindriques de section différente raccordés  (dioptre simple).
\item \verb| pipe2disc | : 3 tuyaux cylindriques de section différente raccordés entre eux (bidioptre)
\item \verb| canalprog | : canal avec profondeur variable de manière progressive.
\item (.....)
\end{itemize}

La définition de ces cas est donnée dans la fonction \verb|cZ|, à la fin du programme.  Dans cette fonction vous pourrez régler les paramètres de ces différents cas et éventuellement rajouter vos propres cas ; par exemple  \verb| problemtype = 'saxophone' |. 

\paragraph{Conditions limites}

Le programme permet  de choisir les conditions limites en $x=0$ et $x=L$ à l'aide des paramètres \verb| BC_0 | et \verb| BC_L | qui peuvent prendre les valeurs suivantes :
\begin{itemize}
\item \verb| Pulse | pour imposer à l'extrémité droite une déformation de type "impulsion gaussienne" sur la variable primitive : 
${\cal Y}(0,t) = A  \exp \left[ - \left(\frac{t-t_1}{t_a}\right)^2  \right]$.
\item \verb| P0_Pulse |  et \verb| V0_Pulse | pour une "impulsion gaussienne", respectivement sur la pression et sur le débit.
%${\cal Y}(0,t) = A  \exp \left[ - \left(\frac{t-t_1}{t_a}\right)^2  \right]$.
\item \verb| Y0 | pour imposer un forçage sinusoïdal de la  variable primitive : ${\cal Y} (0,t) = A \sin \omega t$.
\item \verb| P0 | pour imposer un forçage sinusoïdal de variable force (pression) : ${\cal F} = A \sin \omega t$.
\item \verb| V0 | pour imposer un forçage sinusoïdal de variable vitesse (débit acoustique) : ${\cal V} = A \sin \omega t$.
\item \verb| I0 | pour imposer une une onde incidente sinusoïdale de pulsation $\omega$ tout en laissant sortir une onde réfléchie  : 
${\cal F} = A \sin \omega (t - x/c) + G(t+x/c)$ où $G$ est arbitraire. 
\item \verb| I0_Pulse | pour imposer une une onde incidente de type "impulsion gaussienne" tout en laissant sortir une onde réfléchie  :  ${\cal F} = A \sin \omega (t - x/c) + G(t+x/c)$ où $G$ est arbitraire. 
\item \verb| Closed | : condition ${\cal Y} = 0$ (corde fixée ou tuyau fermé)
\item \verb| Open | : condition ${\cal F} = \ddp{\cal Y}{x} = 0$ (corde libre ou tuyau idéalement ouvert).
\item \verb| Transparent | pour une condition limite de "non-réflexion" en sortie modélisant un raccord avec un milieu semi-infini.
\item \verb| Radiation | pour une condition de rayonnement dans un espace tridimensionnel.
\item \verb| Radiation_Flanged | pour une condition de rayonnement dans un demi-espace tridimensionnel (tuyau encastré).
%our imposer une une condition "non-réflexion" en entrée modélisant un raccord avec un milieu semi-infini duquel provient une onde incidente de type "pulse" : ${\cal F} = A  \exp \left[ - \left(\frac{(t-x/c)-t_1}{t_a}\right)^2  \right] + G(t+x/c)$ où $G$ est arbitraire. 
\end{itemize}

\paragraph{Figures générées par le programme}

Le programme génère les figures suivantes :
\begin{itemize}
\item Figure 1(a,b,c) : tracé de $\cal Y$, $\cal V$ et/ou $\cal F$ (choix selon paramètres définis dans le code) au cours du temps.
\item Figure 2 : tracé de la puissance nette ${\cal P}(0,t)$ dans le plan d'entrée et de la puissance nette ${\cal P}(L,t)$ dans le plan de sortie, ainsi que (si le paramètre \verb|NP.figVplusVmoins=1|) de leurs composantes  ${\cal P}(0,t)^+$ (puissance incidente dans le domaine $x<0$), 
 ${\cal P}(0,t)^-$ (puissance réfléchie dans le domaine $x<0$) , ${\cal P}(L,t)^+$ (puissance arrivant à l'extrémité $L$ du domaine simulé) et   ${\cal P}(L,t)^-$ (puissance réfléchie dans le domaine simulé).
 
 Notez que le programme trace les valeurs les valeur moyennées en temps  (sur une période) $\bar{\cal P}(0,t),\bar{\cal P}^+(0,t),\bar{\cal P}^-(0,t),\bar{\cal P}(L,t),\bar{\cal P}^+(L,t)$ et $\bar{\cal P}^-(L,t)$ de ces puissances. Si la variable \verb| NP.fig2inst | est mise à 1, le programme trace de plus les valeurs instantanées.
%\item Figure 2(b) : tracé de l'énergie totale contenue dans le domaine, définie par $E(t) = \int_0^L e_m(x,t) dx$.

\item Figure 3 : Tracé des valeurs de diverses quantités dans les plans d'entrée et de sortie.

\item Figure 4 : Tracé de $c(x)$, $Z(x)$ et $S(x)$ (pour un tuyau ou un canal).

Notez que si celles-ci ne vous sont pas nécessaires vous pouvez désactiver les figures 2 3 et 4 en mettant à zéro les variables \verb|NP.fig2|  \verb|NP.fig3| et \verb|NP.fig4|.

 \end{itemize}
 

%(Etudiez le programme %ou tapez \verb| help WavesOnString_FiniteDifferences.m | pour plus de précisions).



\section{Prise en main du code}

On considère pour commencer un tuyau cylindrique de longueur $L = 60cm$ et rayon $a=6cm$ encastré dans une plaque et débouchant dans un demi-domaine ("flanged") : \verb| problemtype = 'pipe' |.

\subsection{Impulsion Gaussienne : étude des conditions limites }

On considère dans un premier temps une impulsion gaussienne en vitesse (\texttt{BC\_0 = 'v0\_pulse'}), c.a.d. que le débit acoustique en $x=0$ est de la forme $q'(x,t) =  A  \exp \left[ - \left(\frac{t-t_1}{t_a}\right)^2  \right]$. 

\begin{enumerate}
\item Essayez successivement les conditions aux limites \verb| BC_L = 'Closed' |, \verb| BC_L = 'Open' |, \verb| BC_L = 'Transparent' |, \verb| BC_L = 'Radiation_flanged' |. 
Observez la manière dont l'onde se réfléchit à chaque extrémité.

\item Activez maintenant la figure 3 (en réglant \verb|NP.fig3 = 1|). Observez, et comparez les cas 'Open' et 'Radiation\_Flanged'.

\item Activez maintenant la figure 2 (en réglant \verb|NP.fig2 = 1|). Comparez de nouveau les cas  'Open' et 'Radiation\_Flanged'.
\end{enumerate}


\subsection{Forçage harmonique}

On considère maintenant un tuyau ouvert réel. Pour avoir une condition limite en sortie, il faut d'une part régler  \verb| BC_L = 'Radiation_flanged' | (dans un demi-espace) ou  \verb| BC_L = 'Radiation' | (rayonnement dans un espace complet),
et d'autre part  rallonger le tuyau de la "longueur de correction" $\Delta = 0.85 a$ (demi-espace) ou $0.61a$ (espace complet).

On considèrera également désormais une condition limite de forcage harmonique en débit (\texttt{BC\_0 = 'v0'}).

\begin{enumerate}

\item Observez la solution du problème pour différentes valeurs de $\omega$.

\item La théorie prédit que les fréquences de résonances correspondent à $\omega = (n-1/2) \pi c/(L+\Delta)$, et les anti-résonances à $\omega =  n \pi c/(L+\Delta)$. Tester différentes valeurs, et expliquez ce qu'on observe dans le régime transitoire.

\item Réglez le paramètre \verb|NP.fig2mean = 1| pour observer les valeurs moyennées sur une période dans la figure 2. Observez. Puis réglez  \verb|NP.fig2inst = 0| pour ne garder désormais que celles-ci. 
 % to plot instantaneous quantities in figure 2

\item En se concentrant sur la troisième fréquence de résonance, augmentez la durée de la simulation temporelle  pour pouvoir estimer le temps caractéristique d'établissement du régime permanent. 


\item A partir des résultats du programme (figures 2 et/ou 3), estimez l'impédance d'entrée $Z_{IN}$ du tuyau,  et comparez aux prédictions théoriques.



\end{enumerate}

\end{document}



\comment{
\subsection{Propagation à travers une double discontinuité (facultatif)}

On considère maintenant une double discontinuité :

$$
c(x) = \left\{ \begin{array}{ll} c_1 \qquad & x<X_1 \\
c_2 & X_1<x<X_2 \\
c_3 & X_2 < x
\end{array}
\right.
$$


On considèrera toujours un domaine de longueur $L$ avec une condition limite de non-réflexion en sortie et d'onde incidente imposée en entrée.
 
On s'intéresse à la transmission d'une onde monochromatique de fréquence $\omega$ a travers cette double discontinuité.

On prendra les paramètres suivants :

$c_1 = 1$ ; $c_2 = 2$; $c_3 = 4$, $L = 20$, $X_1= 5$, $X_2=15$, 

\begin{enumerate}

\item On choisit tout d'abord $\omega = \pi$. A l'aide du programme, observez comment les réflexions multiples interagissent entre elles jusqu'à obtenir un régime établi.

 \item Cherchez à déterminer des valeurs de $\omega$ permettant d'éliminer complètement l'onde réfléchie. A quelle relation entre la longueur d'onde dans le milieu 2 et la distance entre les deux discontinuités correspond cette relation ?
 
  \item Mêmes questions pour $c_1 = 1$, $c_2 = 3$ et $c_3 = 9$. On pourra être amené à diminuer le pas de temps du schéma numérique.
 
 
 \item On considère maintenant le cas $c_2 = 4$, $c_3=1$. Quelles valeurs de $\omega$ correspondent-elles aux maximums et minimums du coefficient de transmission ?
 
 
 \end{enumerate}
}

\clearpage

\section{Travail à réaliser}

Après avoir pris en main le code et testé les différentes options sur le cas de la corde vibrante, on traitera l'un des deux cas suivants :

\subsection{Silencieux de voiture (groupe 1)} 



Ce cas correspond à la géométrie de l'exercice 2.3.2 du fascicule de TD. On traitera le cas d'un silencieux avec les dimensions suivantes :

$S_1 = 1 cm^2 ; \quad S_2 = 25 cm^2 ; S_3 = 1cm^2, L= 30 cm, X_1 = 30 cm, X_2 = 30cm$.

(il s'agit d'un tuyau de longueur totale 90cm, dont le second tiers est de section 25 fois plus large que les deux autres tiers).

On prendra une condition limite de sortie de type "transparent" qui correspond à un raccord avec un tuyau semi-infini en aval.


Le fluide est de l'air caractérisé par une vitesse du son $c = 0.34 m/ms$ .


La théorie (exercices 2.1.3 et 2.3.2)  prédit la valeur suivante pour le coefficient de transmission :

$$
T = \frac{4 Z_1 Z_3}{\left( Z_1 + Z_3 \right)^2 \cos^2 (k L_2 )+\left( Z_2 + \frac{Z_1  Z_3 }{Z_2 } \right)^2 \sin^2 (k L_2)}
$$

avec $k = \omega c$ et $L_2 = (X_2-X_1)$.

A l'aide du programme, étudiez le fonctionnement du silencieux pour différentes valeurs de $\omega$. Vous étudierez plus particulièrement deux cas correspondant à des valeurs maximale ou minimale du coefficient de transmission. Comparez avec la théorie et illustrez dans chaque cas la manière dont les réflexions successives interfèrent. 

{\em Remarque :  pour un tuyau acoustique, il est judicieux de choisir la milliseconde comme unité de temps, ce qui permet d'avoir des vitesses et des impedances d'ordre unité}.

\subsection{Résonance dans un estuaire} 

Ce cas correspond à l'exercice 5.3 du fascicule de TD.

On considère un premier canal de largeur $W_1 = 1km$, profondeur $H_1 = 200m$ et longueur $L_1 = 100 km$ raccordé à un second canal de  largeur 
$W_2 = 0.1km$, profondeur $H_2 = 10m$ et longueur $L_2 = 200 km$. Ce second canal est fermé en sortie (condition limite de sortie de type "fixed").

On considère une onde incidente d'amplitude $\eta = 0.1 m$ et de période $T = 12h$ en entrée du canal.

La théorie prédit un facteur d'amplification entre l'onde incidente et l'onde en fond d'estuaire donnée par la formule suivante :

$$
\frac{|\eta(L)|^2}{\eta(0)|^2} = \frac{Y_1^2}{ Y_2^2 \cos^2 (k_2 L_2)   + Y_1^2  \sin^2 (k_2 L_2) }
$$

où $k_2 = \omega c_2$ et $Y_1 = 1/Z_1$ et $Y_2 = 1/Z_2$ sont les admittances.

A l'aide du programme,  étudiez la propagation des ondes et mettrez en évidence un phénomène de réflexion.

Faites ensuite varier la longueur du canal pour mettre en évidence les cas de résonance et d'anti-résonance.

Comparez avec la théorie et illustrez dans chaque cas la manière dont les réflexions successives interfèrent. 

{\em Remarque :  pour un canal, il est judicieux de choisir la minute comme unité de temps, le kilomètre comme unité de longueur, et le téragramme comme unité de masse, ce qui permet d'avoir des vitesses et des impedances d'ordre unité}.







 
 \subsection{Rapport}
 
Pour le cas d'étude sélectionné, précisez les valeurs choisies pour les paramètres physiques, ($c_1, c_2, X_1$ , etc...), les paramètres numériques $(dx,dt,...)$ et les conditions d'entrée et de sortie.
 
Pour une (ou plusieurs) valeurs de $\omega$ choisies de manière judicieuse (égale ou proche d'une résonance ou d'une anti-résonance, condition de transmission maximale, etc...), illustrez le comportement de la corde à l'aide de résultats issus du programme.

Dans la description des résultats, on s'attachera a bien distinguer les aspects transitoires et le régime établi.

Décrivez au mieux le régime établi en faisant le lien avec les concepts théoriques vus en cours et en TD  (impédance d'entrée, coefficients de transmission/réflexion, flux d'énergie,...) 



\end{document}
