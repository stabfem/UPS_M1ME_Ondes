\documentclass[11pt,a4paper]{article}
 
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{fancyhdr}
\usepackage{graphicx, amsmath, amssymb, latexsym}
%\usepackage{natbib}
 
 
\setlength{\textheight}{24.5cm} % 24
\setlength{\topmargin}{-5mm}
\setlength{\textwidth}{16cm}  % 15
%\setlength{\oddsidemargin}{20mm} % marge gauche (im)paire = 1in. + x1mm
%\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\oddsidemargin}{0mm} % marge gauche (im)paire = 1in. + x1mm
\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\fboxsep}{5mm}

\newcommand{\e}  {\mathrm{e}}   
\newcommand{\dd}  {\textrm d}      
\newcommand{\im}  {\mathrm{i}} %\newcommand{\im}  {\textrm i}
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddp}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {{\textrm d}^{2} #1} {{\textrm d} #2 ^{2}}}
%\newcommand{\tensor}[1]{\stackrel{\Rightarrow}{#1}}

\newcommand{\comment}[1]{}

%\graphicspath{{../Figures/}{../FiguresPierre/}}
\graphicspath{Figures/}

%\makeindex


\usepackage{listings}
\usepackage{color}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
 \usepackage{hyperref}

\newcommand{\lstsetFF}{
\lstset{frame=tb,
  language=c++,
  aboveskip=5mm,
  belowskip=5mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{blue},
  keywordstyle=\color{red},
  commentstyle=\color{mauve},
  stringstyle=\color{green},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3}
}

%------------------------------------------------------------------------------
\begin{document}

%\begin{titlepage}

\noindent
\textsc{Universit\'e Paul Sabatier \hfill Ann\'ee 2024/2025} \\
\textsc{M1 Mécanique \hfill Module Ondes}
%\vspace{1cm}

%\begin{center}


%\section*{TP numérique : ondes sur une corde} 
\begin{center}
{\Large \bf Mini-projet Ondes \\ 
Principe du calcul de l'impédance d'entrée d'un tube acoustique \\ }
\end{center}

\section{Explications sur la théorie et exemples de programmes}

\subsection{Tube cylindrique affleurant}

Le cas d'un tube cylindrique a été vu en cours (paragraphe 4.4.2). Rappelons le principe. On part d'une description du champ acoustique sous la forme $p(x,t) = Re ( \underline{p}(x,\omega) e^{-i \omega t}) $ et $q(x,t) = Re ( \underline{q}(x,\omega) e^{-i \omega t}) $, où les fonctions (complexes) $ \underline{p}(x,\omega) $ et  $ \underline{q}(x,\omega)$  sont données par :


\begin{equation}
 \left\{
\begin{array}{rcl l}
 \underline{p}(x,\omega)  &=& A e^{+ i k x } + B e^{ -i kx }  & \quad 0<x<L ;\\
 \underline{q}(x,\omega)  &=& \displaystyle \frac{A e^{+ i k x } - B e^{- i k x}}{Z_0}  & \quad 0<x<L ;
\end{array}\right.
\label{eq:ondesABC}
\end{equation}
avec $L$ la longueur, $R_{pipe}$ le rayon, $k = \omega/c$ le nombre d'onde acoustique, $S = \pi R_{pipe}^2$ la section, et $Z_0 = \rho c/S$ l'impédance caractéristique du tuyau.

En entrée, on impose un débit acoustique $Q_f$ : $q(0,t) = Q_f e^{-i \omega t}$.
En sortie, on a une condition de rayonnement, qui s'écrit en introduisant une impédance de sortie :
$Z_{out}(\omega) = \underline{p}(L,\omega)/\underline{q}(L,\omega) = Z_0 \left[ -i \tan (k\Delta) + \beta  (k R_{pipe})^2 \right]$, 
avec $\Delta = 0.85 a$ et $\beta = 0.5$ dans le cas "affleurant".  

On peut résoudre le problème en écrivant deux équations pour les deux inconnues $A$ et $B$ :

\begin{equation}
 \left\{
\begin{array}{rcl l}
A-B &=& Q_f Z_0, \\ 
 Z_0 (A e^{ikx} +B e^{-ikx}  ) &=& Z_{out} (A e^{ikx} -B e^{-ikx}  ) 
\end{array}\right.
\label{eq:ondesABC}
\end{equation}

On fournit un programme python {\bf Impedance\_Cylinder.py} qui résout ce problème.

\subsection{Tube conico-cylindrique ouvert}

\begin{figure}[h]
$$\includegraphics[width=.7\linewidth]{../TDs_Ondes/Figures/GeometrieConicoCylindrique.png}$$
\caption{Géométrie simplifiée d'un instrument de musique à vent}
\end{figure}

On reprend l'étude dans le cas d'un tuyau conico-cylindrique selon la géométrie représentée ci-dessus.

Dans la partie conique, on travaille en coordonnées cylindriques en posant $r = (x+L_{p})/\cos \alpha$ où $L_{p} = R_{in}/\tan \alpha$ est la longueur de la portion tronquée du cone (voir figure). Les position d'entrée ($x=0$) et de sortie ($x= L_{cone}$) correspondent donc respectivement à $r=r_1 = R_{in}/\sin( \alpha) $  et $r=r_2 = R_{out}/\sin( \alpha) $.

En notant $S(r) = \pi (r \sin \alpha)^2$ la section locale, le champ acoustique est donné par :

\begin{equation}
 \left\{
\begin{array}{rcl l}
\underline{p}(r,\omega) &=& \displaystyle \frac{A e^{+ i k r } + B e^{ - i k r }  }{r}& \\
\underline{q}(r,\omega) &=& \displaystyle \frac{S(r)}{\rho c} \left( A \left[\frac{1}{r} - \frac{1}{i k r^2} \right]   e^{+ i k x } + B \left[-\frac{1}{r} - \frac{1}{i k r^2} \right] e^{ - i k x} ]\right) &  
\end{array}\right.
\label{eq:ondesABC}
\end{equation}

Dans la partie cylindrique, on a la description suivante, en posant $x'=x-L_{cone}$ :

\begin{equation}
 \left\{
\begin{array}{rcl l}
\underline{p}(x,\omega)&=& C e^{+ i k x' } + D e^{ -i kx' }  & \quad 0<x<L ;\\
\underline{q}(x,\omega)&=& \displaystyle \frac{C e^{+ i k x' } - D e^{ - i k x'}}{Z_0}  & \quad 0<x<L ;
\end{array}\right.
\label{eq:ondesABC}
\end{equation}

%Les instruments de musique de la famille des cuivres (trompette, trombone, cor, tuba, ...)
%peuvent être modélisés, en première approximation, par un premier tube cylindrique,
%de longueur $L_1$ et rayon $r_1$, raccordé à un second tube dont la forme est un cône tronqué 
%de longueur $L_2$ et angle au sommet $\alpha$.
%On note $r_1= a_1/\sin(\alpha)$ la distance entre l'entrée de la section conique et la pointe théorique du cône, $r_2 = L+r_1$ la %distance entre la sortie et la pointe théorique, et $a_2 = r_2 \tan \alpha$ le rayon en sortie de tube (cf. figure).

On suppose qu'en entrée on impose un forçage harmonique en débit d'amplitude $Q_f$, c.a.d. 
\begin{equation}
q(x=0,t) = Q_f e^{-i \omega t}.
\label{eq:Qf}
\end{equation}
On impose de nouveau un débit en entrée et une condition de rayonnement en sortie :
$q(r=r_1,t) = Q_f e^{-i \omega t}$ et $Z_{out}(\omega) = \underline{p}(x,\omega)/\underline{q}(x,\omega) = Z_0 \left[ -i \tan (k\Delta) + \beta  (k R_{pipe})^2 \right]$, 
avec $\Delta = 0.61 a$ et $\beta = 0.25$ dans le cas "ouvert".  
On écrit de plus deux conditions assurant la continuité de la pression et du débit acoustique :  $\underline{p}(r=r_2,\omega) = \underline{p}(x'=0,\omega)$ et $\underline{q}(r=r_2,\omega) = \underline{q}(x'=0,\omega)$.

On arrive finalement à un système de 4 équations à 4 inconnues, que l'on met sous la forme matricielle suivante :
%$
%M X= Y,
%$ avec $X = [A;B;C;D]$. 

\begin{equation*}
\left[ \begin{array}{cccc} 
 \left( 1-\frac{1}{ikr_1} \right) \frac{e^{ikr_1}}{r_1 Z_1} &\left( -1-\frac{1}{ikr_1} \right) \frac{e^{-ikr_1}}{r_1 Z_1} & 0 & 0 \\
   \frac{-e^{ikr_2}}{r_2} & {\color{red} \frac{-e^{ikr_2}}{r_2}} &  {\color{red} 1} &  {\color{red}1} \\
  \left( 1-\frac{1}{ikr_2} \right) \frac{e^{ikr_2}}{r_2 Z_2} & \left( -1-\frac{1}{ikr_2} \right) \frac{e^{-ikr_2}}{r_2 Z_2} &-1&1 \\
   0 & 0 & (Z_2-Z_{out}) e^{ikL_{cyl}} & (Z_2 +Z_{out})e^{-ikL_{cyl}}  
  \end{array}
  \right]
  \left[ \begin{array}{c} A\\B\\C \\D \end{array} \right] =   \left[ \begin{array}{c} 1\\0\\0 \\0 \end{array} \right] 
  \end{equation*}


On fournit un programme python {\bf Impedance\_ConeCyl.py}  qui résout ce problème.

\subsection{\color{red} Simple dioptre (pas à traiter en 2025)}

{\color{red}
Considérons maintenant le cas d'un dioptre simple : $S = S_1$ pour $x<0$ et $S = S_2$ pour $x>0$.
En considérant dans la portion $x<0$ une onde incidente d'amplitude $A$ et une onde réfléchie d'amplitude $B$, 
et dans la portion $x>0$ une onde incidente d'amplitude $C$, les conditions de raccord conduisent à $A+B=C$ et $(A-B)/Z1 = C/Z_2$ (cf cours). 

Ce problème peut se mettre sous la forme matricielle suivante :
$$
\left[ \begin{array}{cc} -1 & 1 \\ \frac{1}{Z_1} & \frac{1}{Z_2} \end{array} \right]  
\left[ \begin{array}{c} B \\ C  \end{array} \right] 
= 
\left[ \begin{array}{c} A \\ \frac{A}{Z_2}  \end{array} \right] .
$$

On fournit un programme  {\bf Transmission\_Dioptre.py} pour résoudre ce problème, dont on pourra s'inspirer pour traiter le cas du "bidioptre".}



\section{Travail demandé}

Adaptez l'approche proposée ci-dessus pour la géométrie considérée.

Ecrire un programme qui résout le problème, et trace l'impédance en fonction de la fréquence, ainsi que la structure du champ acoustique pour une fréquence donnée.

{\color{red} Pour les groupes II et IV (cor et saxtuba) vous pourrez suivre la démarche proposée (appendice A)  pour établir les équations à résoudre et construire le programme.}

{\color{red} Pour les groupes III et V (vuvuzela et bombarde) un programme est fourni (\texttt{Impedance\_ConeExp.py}). Expliquez la démarche permettant d'établir la forme matricielle du problème résolu.}


{\color{red} Pour le groupe I (smithophone) vous pourrez suivre la démarche proposée (appendice B)  pour établir les équations à résoudre et construire le programme.}




%\section{Solution pour le problème conico-cylindrique (section 1.2)}


% \left[\begin{array}{c} A\\B\\B\\D\end{array}\right] 
\appendix
\section{\color{red}  Aide à la résolution pour un tube cylindre+exponentiel (cor ou trombone)}

Supposons la loi de section suivante (ici on a pris l'origine du repère en $x=0$, l'entrée du tube cylindrique est en $x=-L_{cyl}$, la sortie du pavillon en $x = + L_{exp}$).


$$
R(x) = R_1 \quad \mbox{ pour } \quad -L_{cyl} < x < 0 , \qquad R(x) = R_1 e^{\alpha x/2} \quad \mbox{ pour } \quad 0 < x < L_{exp}.
$$ 
 
 La section s'en déduit avec $S(x) = \pi R(x)^2 =  \pi R_1^2 e^{\alpha x}$. Donc le taux d'expansion du pavillon exponentiel est $\alpha$.
 

\begin{enumerate}
\item Justifiez que dans la section cylindrique la loi de pression  peut être cherchée sous la forme suivante :
$$p(x,t) = A e^{i k x - i \omega t} + B e^{-i k x - i \omega t}$$ 

\item Donnez la loi de débit $q(x,t)$ dans la section cylindrique.

%$$q(x,t) =\frac{S_1}{\varrho c}  \left( A e^{i k x - i \omega t} - B e^{-i k x - i \omega t} \right) $$ 

\item Donnez l'équation vérifiée par la pression dans la section exponentielle (issue de l'équation de Webster dans le cas où $S(x) = S_1 e^{\alpha x}$  ; cf. cours).

\item Justifiez que dans la section exponentielle la loi de pression peut être cherchée sous la forme suivante :
$$
p(x,t) = C e^{i k^+ x - i \omega t} + D e^{i k^- x - i \omega t} \qquad \mbox{ avec } k^\pm = i \alpha /2 \pm \sqrt{\omega^2/c^2-\alpha^2/4}. 
$$ 

\item Donnez la loi de débit $q(x,t)$ correspondante dans la section exponentielle.

%$$
%q(x,t) = \frac{S_1 e^{\alpha x} }{\varrho}  \left( \frac{C k^+}{k} e^{i k^+ x - i \omega t} + \frac{D k^-}{k} e^{i k^- x - i \omega t} \right) 
%$$ 

\item Montrez que le problème peut se mettre sous la forme matricielle suivante :

\begin{equation*}
\left[ \begin{array}{cccc} 
 e^{-i k L_{cyl}} & - e^{i k L_{cyl}} & 0 & 0 \\
  -1 & -1 & 1 & 1  \\
 - 1 & 1  & k^+/k  & k^-/k \\
 0 & 0 & (Z_s -Z_{out}) e^{i k^+ L_{exp}} & (Z_s + Z_{out}) e^{i k^- L_{exp}} 
  \end{array}
  \right]
  \left[ \begin{array}{c} A\\B\\C \\D \end{array} \right] =   \left[ \begin{array}{c} Z_{c} Q_f \\0\\0 \\0 \end{array} \right] 
  \end{equation*}

Et que l'impédance d'entrée peut finalement être déduite par 

$$
Z_{in} = \frac{A e^{-i k L_{cyl}} + B e^{i k L_{cyl}}}{Q_f}
$$

\item Ecrire un programme qui résout ce problème. Vous pourrez vous inspirer des programmes fournis "cône+cylindre" et "cône+exponentiel".

\end{enumerate}


\section{Aide à la résolution pour un instrument composé de 3 sections cylindriques}

\begin{enumerate}

\item Justifiez qu'il est pertinent de rechercher la pression acoustique $p = P-P_0$ dans chacune des 3 sections par :

$$
\begin{array}{rcll}
p(x,t) &=&  A e^{i (-\omega t+ k x)} + B e^{i (-\omega t- k x)} & \qquad \mbox{ pour } 
0<x<L_1;
\\
p(x,t) &=&  C e^{i (-\omega t+ k x)} + D e^{i (-\omega t- k x)} & \qquad \mbox{ pour } 
L_1<x<L_2;
\\
p(x,t) &=&  E e^{i (-\omega t+ k x)} + F  e^{i (-\omega t+ k x)} &\qquad \mbox{ pour }  L_2<x<L_3. 
\end{array}
$$




\item Donnez l'expression du débit acoustique $q(x,t)$ dans chacune des 3 sections. 




%La transmission à travers le premier dioptre donne lieu à une onde transmise dans le milieu 2 notée $p_T$ et à une onde réfléchie dans le milieu 1 notée $p_r$. La transmission à travers le deuxième dioptre donne lieuà une onde réfléchie dans le milieu 2 notée $p_b$ et à une onde transmise dans le milieu 3 notée $p_t$. On se place dans le cas d’une onde harmonique de pulsation $\omega$.


\item Écrire les conditions de continuité de la vitesse et de la pression aux deux discontinuités ($x=L_1$ et $x=L_2$), ainsi que les conditions limites en entrée (débit imposée) et en sortie (condition d'impédance de sortie). 

\item 
En déduire un système de 6 équations pour les inconnues $A,B,C,D,E,F$.


\begin{eqnarray*} 
 Ae^{ikL_1} + Be^{-ikL_1} &=& Ce^{ikL_1}+De^{-ikL_1} 
\\
\frac{Ae^{ikL_1} - Be^{-ikL_1}}{Z_1} &=& \frac{Ce^{ikL_1}-De^{-ikL_1}}{Z_2} 
\\
 Ce^{ikL_2} + De^{-ikL_2} &=& Ee^{ikL_2}+Fe^{-ikL_2} 
\\
\frac{Ce^{ikL_2} - Be^{-ikL_2}}{Z_2} &=& \frac{Ee^{ikL_2}-Fe^{-ikL_2}}{Z_3} 
\\
\frac{A - B}{Z_1} &=& Q_f
\\
Ee^{ikL_3}+Fe^{-ikL_3}  &=& \frac{Z_{out}(\omega)}{Z_3} \left(Ee^{ikL_3}-Fe^{-ikL_3} \right) 
\end{eqnarray*}

\item Ecrire un programme qui résout ce problème et en déduit l'impédance d'entrée, et utilisez-le pour étudier l'impédance en fonction de la fréquence pour la géométrie proposée.

\end{enumerate}


\end{document}

On étudie le fonctionnement de l'instrument en régime harmonique, c.a.d. que toutes les quantités (pression et débit acoustique) sont 
proportionnelles à $e^{-i \omega t}$.

On note $Z_{out}(\omega) = p'(r=r_2,t)/q'(r=r_2,t)$ l'impédance de sortie, qui peut être approximée, pour un tuyau ouvert, par l'expression suivante, donnée en cours :

\begin{equation}
Z_{out}  = \frac{p'(r=r_2,t)}{q'(r=r_2,t)} \approx \frac{\rho c}{\pi a_2^2} \left( \frac{k^2 a_2^2}{4} + i k \Delta \right)  \mbox{ avec } k = \omega / c \mbox{ et } \Delta = 0.61 a_2.
\label{eq:Zoutcuivre}
\end{equation}


\begin{enumerate}

\item Dans la section cylindrique ($0<x<L$), on suppose que la solution est une onde d'amplitude $A$ allant vers la droite plus une onde d'amplitude $B$ allant vers la gauche.

Donnez l'expression de la pression $p'(x,t)$ et du débit acoustique $q'(x,t)$ correspondants.


\item Dans la section conique, il est pratique de travailler en coordonnées sphériques, en notant $r$ la coordonnée comptée par rapport à la pointe théorique (le cône tronqué est alors défini par l'intervalle $r_1<r<r_2$).
On suppose que la solution est une onde divergente $C$ plus une onde convergente d'amplitude $D$.

Donnez l'expression de la pression $p'(x,t)$, de la vitesse radiale $u'_r(r,t)$ et du débit acoustique $q'(r,t) = S(r) u'_r(r,t)$ correspondants (la section du tuyau à la position $r$ est $S(r) = 2\pi r^2 (1-\cos \alpha) \approx \pi r^2 \sin^2\alpha$).

\item A partir de la continuité de la pression ( $p'(x=L,t) = p'(r=r_1,t)$ ) et du débit ( $q'(x=L,t) = q'(r=r_1,t)$ ), et des équations \ref{eq:Qf} et \ref{eq:Zoutcuivre}, écrire un système de 4 équations à 4 inconnues pour $(A,B,C,D)$.

\item Ecrire un petit programme permettant de résoudre ce problème, et d'en déduire l'impédance d'entrée 
$Z_{in} = p'(x=0,t)/q'(x=0,t)$ en fonction de la fréquence $\omega$, des propriétés géométriques de l'instrument, et du modèle d'impédance de sortie $Z_{out}(\omega)$.

On pourra s'inspirer des programmes \texttt{Impedance\_CylindricalPipe.m} et 
\texttt{Impedance\_ConicalPipe.m}, disponibles sur le site web du cours.

\item A l'aide du programme, tracez $|Z_{in}(\omega)|$, et relevez les fréquences, la valeur de l'impédance et le facteur de qualité $Q$ correspondant aux résonances.

\item (Optionnel) Généralisez l'approche en ajoutant un pavillon exponentiel à la sortie du cône.



\end{enumerate}




\end{document}
