\documentclass[11pt,a4paper]{article}
 
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{fancyhdr}
\usepackage{graphicx, amsmath, amssymb, latexsym}
%\usepackage{natbib}
 
 
\setlength{\textheight}{24cm} % 24
\setlength{\topmargin}{-5mm}
\setlength{\textwidth}{16cm}  % 15
%\setlength{\oddsidemargin}{20mm} % marge gauche (im)paire = 1in. + x1mm
%\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\oddsidemargin}{0mm} % marge gauche (im)paire = 1in. + x1mm
\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\fboxsep}{5mm}

\newcommand{\e}  {\mathrm{e}}   
\newcommand{\dd}  {\textrm d}      
\newcommand{\im}  {\mathrm{i}} %\newcommand{\im}  {\textrm i}
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {{\textrm d}^{2} #1} {{\textrm d} #2 ^{2}}}
%\newcommand{\tensor}[1]{\stackrel{\Rightarrow}{#1}}

\newcommand{\comment}[1]{}

%\graphicspath{{../Figures/}{../FiguresPierre/}}
\graphicspath{Figures/}

%\makeindex

%------------------------------------------------------------------------------
\begin{document}

%\begin{titlepage}

\noindent
\textsc{Universit\'e Paul Sabatier \hfill Ann\'ee 2022/2023} \\
\textsc{M1 Mécanique \hfill Ondes dans les fluides}
%\vspace{1cm}

%\begin{center}


%\section*{TP numérique : ondes sur une corde} 
\begin{center}
{\Large \bf TP numérique 1 : ondes sur une corde}
\end{center}

%\renewcommand{\thesubsection}{\arabic{subsection}}

%\section{Un peu de musique...}

%\subsection{Le piano (groupe 1)}

%Le piano est un instrument à corde frappées à l'aide de marteaux qui communiquent une vitesse initiale approximativement uniforme a une portion de corde.
%Dans le piano l'effet de raideur en flexion due à l'épaisseur non nulle des cordes est particulièrement important, et conduit à un phénomène d'inharmonicité (les partiels supérieurs ne sont pas des multiples exacts du fondamental mais des fréquences légèrement plus aigües). Ce problème rend l'accordage d'un piano particulièrement difficile, et conduit à "dilater" les octaves en accordant par example le $do_5$ à un peu plus que 2 fois la fréquence du $do_4$.

%\subsection{ Le banjo (groupe 2)} 

%Le banjo est un instrument à cordes pincées pour lequel le chevalet est posé sur une membrane, caractérisée par une impédance plus faible que celle des tables d'harmonies d'autres instruments. Le facteur de qualité est donc relativement faible comparé à d'autres instruments (de l'ordre de 100), la transmission d'énergie est donc plus efficace. En conséquence le son de l'instrument est caractérisé par un volume important mais un temps d'amortissement assez court (la note de banjo "dure" beaucoup moins que celle d'une guitare).



\section{Présentation du TP}

\subsection{Présentation des programmes}

Dans ce TP on étudie de manière numérique la solution du problème de la propagation d'ondes transverses sur une corde de longueur $L$.

$$
\frac{\partial^2 Y}{\partial t^2} 
= c^2
\frac{\partial^2 Y }{\partial x^2}
$$
où $c^2 = T/\mu$ est la vitesse de l'onde, $T$ la tension et  $\mu$ la masse linéique.
%Dans cette première partie on supposera la corde homogène ($\mu$ = cte) et tenue à ses deux extrémités. 
On fournit trois programmes :

\begin{itemize}
\item Le programme \verb| WaveEquation_dAlembert.m | construit la solution du problème en utilisant la méthode de d'Alembert, qui consiste à écrire la solution sous la forme :

$$
Y(x,t) = F(x-ct) + G(x+ct)
$$

où les fonctions $F$ et $G$ dépendent des conditions initiales (cf. cours.) 
Ce programme fait appel à deux sous-programmes (fonctions) appelées \verb| dAlembert_F.m | et \verb| dAlembert_G.m | \,
définissant ces deux fonctions, qui font eux-même appel à deux sous-programmes (fonctions) 
\verb| posinit.m | et \verb| vitinit.m | définissant les conditions initiales $Y(x,0)$ et $V(x,0)$.

Cette méthode est en principe limitée à une corde de longueur infinie, mais peut être adaptée pour une corde de longueur finie $0<x<L$ en prolongeant les fonctions $F$ et $G$ en des fonctions périodiques en espace de période $2L$ (cf. TD, exercice 1.1.3).

\item Le programme \verb| WaveEquation_Fourier.m | construit la solution du problème par la méthode de Fourier,  qui consiste à développer la solution en une série de  la forme suivante :

$$
Y(x,t) = \sum_{n=1}^{N_{max}} \sin \left( \frac{ n\pi x}{L} \right) \left( A_n \sin \omega_n t +  B_n \cos \omega_n t \right)
$$

Ou les coefficients de Fourier $A_n$ et $B_n$ sont définis en fonction des conditions initiales $Y(x,0)$ et $V(x,0)$.
Ce programme fait également appel aux deux sous-programmes 
\verb| posinit.m | et \verb| vitinit.m | définissant les conditions initiales $Y(x,0)$ et $V(x,0)$.

\item Le programme  \verb| WaveEquation_FiniteDifferences_string.m | construit une solution numérique du problème à l'aide de la méthode des différences finies. Ce troisième programme est reproduit en annexe ; il fait également appel aux deux sous-programmes \verb| posinit.m | et \verb| vitinit.m | pour les initialisations.


\end{itemize}

\subsection{Travail demandé}

Après une prise en main des programmes sur le cas d'une corde idéale frappée ou pincée (faite en suivant les étapes proposées dans la section 2),
{\bf vous définirez un cas d'étude correspondant à une corde réelle d'instrument de musique}. 

Le but de votre travail est d'introduire dans la modélisation des effets physiques supplémentaires afin de se rapprocher d'une corde réelle, et d'analyser physiquement, numériquement, et musicalement les résultats obtenus.

Plusieurs pistes d'étude sont proposées dans la section 3 ; vous choisirez une ou deux pistes d'études parmi celles proposées (ou choisies librement).

Vous restituerez votre travail dans un rapport synthétique avec un maximum de 4 pages de texte et un maximum de 10 figures. Dans les résultats présentés vous utiliserez au moins 2 des trois méthodes numériques proposées.



 
 
 





\section{Prise en main des programmes}

Pour la prise en main des programmes on considèrera une corde de longueur $L=10$ et une vitesse de l'onde $c=1$. 

Vous considérerez soit le cas d'une corde pincée (groupe 1), soit le cas d'une corde frappée (groupe 2), ce qui devra être correctement défini dans les fonctions \verb|vitinit.m| et \verb|posinit.m| :

\begin{description}

\item[Corde pincée] (groupe 1):

La corde étant fixée à ses deux extrémités, on la pince initialement à une position $d$, 
en la déplaçant d'une amplitude $e$ : 

$$
Y(x,0)=\left\{ 
\begin{array}{l l}
\frac{e x}{d} & \qquad (0<x<d) \\
\frac{e (L-x)}{L-d} & \qquad (d<x<L) 
\end{array}
\right.; \qquad
V(x,0) = 0.
$$


\item[Corde frappée] (groupe 2):

La corde étant fixée à ses deux extrémités, on la frappe
à la position $x=d$ avec un marteau de largeur $\ell $, 
ce qui lui communique une vitesse $w$ sur le segment sur lequel s'est fait l'impact:


$$
V(x,0)=\left\{ 
\begin{array}{l l}
0 & \qquad (0<x<d-\ell/2) \\
w & \qquad   (d-\ell/2<x<d+\ell/2) \\
0& \qquad (d+\ell/2<x<L) 
\end{array}
\right.; \qquad 
Y(x,0) = 0.
$$

\end{description}

\subsection{Méthode de d'Alembert}

\begin{enumerate}

\item Etudiez le programme  \verb| dAlembert.m |. 

\item A l'aide de celui-ci, observez comment les composantes $F$ et $G$  interfèrent pour obtenir la solution du problème.
On prendra initialement les paramètres $L= 10$, $c=1$, $e=1$, $d=0.15 L$.

\item Observez également la forme du signal acoustique $U(t)$ (figure 2). 
Justifiez pourquoi il s'agit d'une fonction constante par morceaux.

\end{enumerate}

\subsection{Méthode de Fourier }


\begin{enumerate}
\setcounter{enumi}{3}

\item Etudiez maintenant le programme \verb| WaveEquation_Fourier.m |.

\item A l'aide de celui-ci, observez l'évolution temporelle de la forme de la corde et comparez au résultat obtenu avec le programme précédent. On prendra initialement $N_{max} = 50$. 

\item Que constate-t-on pour la vitesse (courbe bleue) ? Expliquez. 
Augmenter le nombre de termes dans la série $N_{max}$ améliore-t-il les résultats ?

\item Faire varier le paramètre $N_x$ améliore-t-il la précision des résultats ?

\item Observez maintenant les coefficients $A_n$ et $B_n$ (figure 20).  Etudiez l'effet du paramètre $d$ correspondant à la position
d'excitation de la corde (pincement ou frappement). 
Que se passe-t-il si $d$ est un sous-multiple de $L$  (c.a.d. $d=L/2$, $d=L/3$, ...)?

\item Observez finalement le signal acoustique reconstruit (figure 40) et son spectre de puissance (figure 41). Faire varier le paramètre $dt$ utilisé pour tracer la figure 40 améliore-t-il la précision des résultats ?


\end{enumerate}


%\subsection{Modélisation plus réaliste de la vitesse initiale}
%
%La modélisation du mouvement initial à l'aide d'une fonction discontinue étant peu réaliste, on considère maintenant une condition initiale donnée par 
%
%$$
%V(x,0)= W e^{-\left| \frac{x-(a+b)/2}{b-a} \right|^N}, \qquad Y(x,0) = 0.
%$$
%
%\begin{enumerate}
%%\item Commentez la forme de $V(x,0)$. Que devient cette formule quand $N\gg 1$ ? 
%\setcounter{enumi}{8}
%\item Modifiez la fonction \verb| vitinit | pour pouvoir traiter cette condition initiale. On prendra $N= 8$ et les valeurs de $a$ et $b$ précédentes.
%
%\item A l'aide du programme  \verb| WaveEquation_Fourier.m |, observez la forme de la corde au cours du temp et commentez. Retrouve-t-on le problème observé précédemment ?
%
%\item Qu'observe-t-on sur les coefficients $B_n$ ?
%
%% IDEE POUR 2021 : faire embrayer ici sur corde frappée.
%\end{enumerate}

\subsection{Méthode des différences finies}

\begin{enumerate}
\setcounter{enumi}{8}

\item Etudiez maintenant le programme \verb| WaveEquation_FiniteDifferences_string.m |.

\item Observez l'évolution temporelle, et comparez avec les deux programmes précédents.

\item Le programme calcule à l'issue de la simulation la quantité $E_T = max(|Y(x,T)-Y(x,0)|)$ avec $T = c/(2L)$ correspondant à une période d'oscillation. Quelle est la valeur théorique de $E_T$ ? Justifiez alors qu'on peut utiliser ce paramètre comme erreur numérique pour étudier la précision du programme.

\item Faites varier les paramètres $dx$ et $dt$ et observez l'effet sur les résultats, notamment sur l'erreur numérique $E_T$. Que peut-on en déduire sur la précision, la stabilité et l'ordre des schémas utilisés ? 

\item Déterminez, en expliquant les critères retenus, des valeurs de $dx$ et $dt$ permettant d'avoir un bon compromis entre la précision et le temps de calcul.


\end{enumerate}

\subsection{Synthèse}

Quels sont les avantages et inconvénients de chacune des trois méthodes 
(précision, simplicité, robustesse, généralité, limitations éventuelles) ?




\subsection{Adaptation au cas d'étude}

Choisissez un "cas de référence" de corde réelle, en précisant tous les paramètres physiques (longueur, tension, masse linéique) choisis. 

Reprenez maintenant toute la démarche de la section 2 pour votre "cas de référence".

Déterminez notamment un choix de paramètres numériques (pas de temps et d'espace, nombre de modes, etc...) permettant d'avoir des résultats précis avec les méthodes choisies pour la suite de votre travail.




\section{Pistes d'études}

On donne dans cette section plusieurs idées d'ingrédients supplémentaires que vous pourrez introduire 
dans la modélisation pour se rapprocher d'un instrument réel. Vous pourrez choisir d'introduire ces ingrédients dans 
l'une ou l'autre des méthodes numériques proposées.

Pour les validations des méthodes numériques utilisées il pourra être pertinent de représenter la solution à l'échelle d'une période temporelle. En revanche, pour une étude physique et musicale, il pourra être pertinent de considérer l'évolution à l'échelle de quelques secondes, ce qui corrrespond à quelques centaines de périodes temporelles d'oscillation.

\subsection{Condition initiale "réaliste"}

On peut tout d'abord améliorer la modélisation des conditions initiales en utilisant des fonctions régulières à la place des fonctions discontinues du cas idéal.

Par exemple, dans le cas d'une corde frappée, une idée est de remplacer la fonction "chapeau" par une fonction "hypergaussienne":
$$
V_0(x) = w \, \exp \left[ - \left( \frac{2(x-d)}{\ell}\right)^{N_i} \right] 
$$
où $N_i = 10$ par exemple. On peut de même modifier la condition initiale "pincée" en remplaçant la fonction linéaire par morceaux par une fonction plus régulière, quadratique par morceaux par exemple.



\subsection{Effet de la raideur en flexion}

On considère le cas d'une corde de rayon $a$ non négligeable. Dans ce cas le problème est gouverné par une équation des ondes modifiée qui s'écrit (cf. TD 1.2) :


$$
\mu \frac{\partial^2 Y}{\partial t^2} = 
T \frac{\partial^2 Y}{\partial x^2} - K \frac{\partial^4 Y}{\partial x^4}
$$

où $ K = \frac{\pi E a^4}{4}$ est le coefficient de raideur en flexion.

On peut également réécrire l'équation sous la forme suivante :

$$
\frac{\partial^2 Y}{\partial t^2} = 
c^2 \left( \frac{\partial^2 Y}{\partial x^2} - B L^2  \frac{\partial^4 Y}{\partial x^4} \right)
$$

où $B = \frac{K}{T L^2}  = \frac{\pi E a^4}{4 T L^2}$ est le coefficient d'inharmonicité. 

Modifiez l'un ou l'autre des programmes  \verb| WaveEquation_FiniteDifferences_string.m | et \verb| WaveEquation_Fourier.m |  
(ou les deux !) pour traiter ce cas.\footnote{Remarque : le problème étant d'ordre 4, on a besoin de plus de conditions limites. On pourra choisir soit des conditions limites "pivot" ($Y = 0$ et $\partial^2Y/\partial^2x  =0$ en $x=0$ et $x=L$) soit des conditions limite "encastrement" ($Y = 0$ et $\partial Y/\partial x  =0$ en $x=0$ et $x=L$).}

%A l'aide des résultats de l'exercice 1.1.4, donnez la solution théorique de ce problème par la méthode de Fourier.
%Modifiez le programme  \verb| WaveEquation_Fourier | pour résoudre ce problème.

Piste "étude numérique" : calculez la solution pour les paramètres de la question 1, en faisant varier la valeur du coefficient d'inharmonicité $B$ (on commencera par des valeurs très faibles, de l'ordre de $10^{-5}$). Comparez avec le cas idéal. 
Comment faut-il régler les paramètres numériques pour que la méthode converge ?
A partir de quelle valeur de $B$ observe-t-on des distorsions dès la première période d'oscillation ? 
Piste "étude physique et musicale" :  déterminez la valeur de $B$ pour une (ou plusieurs) corde(s) de votre instrument.
Représentez la forme de la corde et/ou le signal acoustique au bout de quelques secondes d'oscillation.


%Pourrait-on modifier le programme \verb| dAlembert.m | pour résoudre ce problème ? 

%\comment{

\subsection{ Prise en compte du mouvement du chevalet}

L'extrémité $x=0$ repose sur un chevalet qui n'est pas fixe mais susceptible de se déplacer verticalement (c'est d'ailleurs ce déplacement vertical du chevalet qui transmet l'énergie de vibration de la corde au reste de l'instrument et lui permet de rayonner le son).

On peut modéliser l'effet de ce chevalet en remplaçant la condition $Y(0,t)=0$ par une condition qui s'écrit :

$$
\frac{- F_T(0,t)}{V(0,t)} = \left( T \frac{\partial Y}{\partial x} \right)  \left/ \left( \frac{\partial Y}{\partial t} \right) \right.  = Z_{chev}
$$

où $Z_{chev}$ est l'impédance du chevalet.

On peut d'autre part définir le {\em Facteur de qualité } du raccordement corde/chevalet ainsi :

$$Q  = \frac{Z_{chev} c}{T} = \frac{Z_{chev}}{Z_{corde}} 
$$


On peut montrer que dans le cadre de la méthode de différences finies, cette impédance de chevalet peut être prise en compte 
en modifiant la condition limite de la manière suivante :
{\small
\begin{verbatim}
 Y_future(1) = (2*rs*Y_current(1)+(r^2-rs)*Y_past(1)+2*r^2*rs*(Y_current(2)-Y_current(1)))/(rs+r^2); %no reflexion
\end{verbatim} 
}
où $r_s = \frac{ T dt }{Z_{chev} dx}$.

%(la démonstration de cette formule est proche de l'exercice 1.3.3 du TD) 

Modifier le programme  \verb| WaveEquation_FiniteDifferences_string.m | pour traiter ce cas (on prendra pour commencer des valeurs du facteur de qualité de l'ordre de 10).

Observez la solution et expliquez les différences observées par rapport au cas de référence.

\subsection{Effet du frottement avec l'air}

Le frottement avec l'air est une des causes conduisant à l'amortissement du mouvement d'une corde (mais n'est en pratique pas la cause la plus importante).
La modélisation la plus simple de cet effet consiste à ajouter dans la modélisation une force de frottement de type "visqueux", c.a.d. proportionnelle à la vitesse de la corde. Cette hypothèse conduit à modifier l'équation des ondes sous la forme
$$
\mu \frac{\partial^2 Y}{\partial t^2} = T  \frac{\partial^2 Y}{\partial x^2} - 2 \pi \rho_a \nu_a \frac{\partial Y}{\partial t}
$$
Où $\rho_a$ est la masse volumique de l'air et $\nu_a$ la viscosité cinématique de l'air.

Cette modélisation est facile à introduire dans les programmes (Fourier et/ou différences finies) et conduit à un amortissement global avec des distorsions négligeables (montrez-le) !

Une modélisation plus avancée\footnote{ Fletcher \& Rossing, p.54; Chaigne \& Kergomard, p.194} conduit à un coefficient d'amortissement $\lambda_n$ pour chacun des modes de Fourier :

$$
Y(x,t) = \sum_{n=1}^{N_{max}} \sin \left( \frac{ n\pi x}{L} \right) \left( A_n \sin \omega_n t +  B_n \cos \omega_n t \right) e^{-\lambda_n t} 
$$
$$
\mbox{ Avec } \quad 
\lambda_n = \frac{\omega_n \rho_a }{\rho_s} \left(\frac{2 \sqrt{2} M + 1}{2 M^2} \right); \quad  \mbox{ où } M = \frac{a}{2} \sqrt{ \frac{\omega_n}{\nu_a}}
$$ 
où $a$ est le rayon de la corde et $\rho_s$ sa masse volumique.


\subsection{Dissipation interne dans la corde}

La dissipation d'énergie à l'intérieur de la corde est due au phénomène de viscoélasticité, et est en pratique plus importante que la dissipation visqueuse due au frottement avec l'air considérée dans le paragraphe précédent.
Pour introduire cet effet physique on pourra se référer à Chaigne \& Kergomard, p. 199, qui proposent le modèle simple suivant :

$$
\frac{1}{c^2} \frac{\partial^2 Y}{\partial t^2} = 
\frac{\partial^2 Y}{\partial x^2} + {\tau} \frac{\partial^3 Y}{\partial x^2\partial t}
$$

où $\tau$ est une constante de temps, correspondant à une relaxation viscoélastique, qui vérifie généralement $\tau \gg \omega_1^{-1}$. 

 %ou Fletcher \& Rossing, pp. 55.


\subsection{Effet des non linéarités}

Si l'amplitude du mouvement devient importante, on s'attend à observer une modification des résultats due à des effets non linéaires. 
Le principal effet non linéaire a prendre en compte est lié à l'allongement de la corde qui fait localement augmenter la tension de celle-ci. En pratique, dans la définition de la force transverse $F_T(x,t)$ donnée dans le cours, il faut remplacer $T$ par $T_0+ E s (d L- dx)/dx$, où $d L = \sqrt{(dx)^2 + (dY)^2}$. Ceci conduit à l'équation des ondes modifiée ainsi :

$$
\mu \ddpa{Y}{t} =  \left[ T_0 + \frac{3 E S}{2} \left( \dpa{Y}{x}\right)^2 \right]  \ddpa{Y}{x} 
$$

Vous pourrez modifier le programme de Différences finies pour prendre en compte cet effet. Observez alors l'effet de l'amplitude initiale (déplacement $e$ de la corde pincée ou vitesse $w$ de la corde frappée) sur la solution. A partir de quelle amplitude l'effet est-il sensible au bout d'une seule période d'oscillation ? Dans le cas de plus faibles amplitudes, qu'observe-t-on sur une durée plus longue ?






%\begin{enumerate}
%\item Modifiez vos fonctions  \verb| posinit.m | et \verb| vitinit.m | afin de pouvoir étudier ce cas.

%\item A l'aide des programmes  \verb| WaveEquation_Fourier.m | et/ou 

%\verb| WaveEquation_FiniteDifferences_string.m |, observez la déformation de la corde, et comparez au cas de la corde frappée. 

%\item  Expliquez physiquement la forme obtenue. Que peut-on dire de la courbure et de l'accélération de la corde pour les formes obtenues ?

%\item Si on pince initialement la corde au tiers de sa longueur ($b=L/3$) que constate-t-on sur les coefficients de Fourier $A_n$, $B_n$ ?

%\item (question facultative) Modifiez le programme \verb| dAlembert.m | afin de traiter ce cas. 

%\end{enumerate}


\comment{
\section{Consignes pour le rapport}


\subsection{Etude numérique}

Dans cette première partie du rapport, vous présenterez les méthodes utilisées en vous attachant aux aspects numériques.

Commencez par présenter le cas "idéal" (cf. section 4 avec $L=10,c=1)$  d'une corde frappée (groupe 1) ou pincée (groupe 2).

Illustrez avec des résultats numériques judicieusement choisis et commentés, obtenus avec au moins deux des trois méthodes disponibles, que vous comparerez entre elles. 
Commentez en terme de précision et stabilité.

Quels sont les avantages, limitations, et problèmes liés à la précision numérique de chacune des méthodes abordées ? 


\subsection{Etude musicale}

Dans cette seconde partie, vous vous attacherez à décrire plus précisément le cas de l'instrument choisi.
Vous sélectionnerez des valeurs des paramètres (longueur, masse linéique, tension, ...) caractéristiques de l'instrument, 
et introduirez les effets supplémentaires discutés dans la section 4. 

A l'aide de vos programmes, illustrez au mieux les particularités des instruments citées en introduction.




\vspace{.2cm}
}
%}
%{\em 

%Dans un instrument de musique, le son généré est directement lié à la force transverse exercée sur le chevalet, qui met en vibration la table d'harmonie, puis l'air ambiant, et enfin le tympan de l'oreille de l'auditeur.  

%C'est à dire : $\displaystyle p'(t) \approx F_T(0,t) = \left. \dpa{Y}{x}\right|_{x=0}$.


%A partir des résultats précédents, décrivez les propriétés du son produit par l'instrument
%(forme du signal temporel $p'(t)$, contenu spectral, fréquence fondamentale, etc...)

%}



\comment{

\section{Corde de masse linéique variable, de longueur semi-infinie}

\subsection{Présentation}

On considère maintenant une corde de masse linéique  $\rho(x)$ non uniforme (la vitesse des ondes $c(x)$ est donc elle aussi non uniforme). On considèrera en particulier les cas ou la masse linéique présente une ou deux discontinuités, ainsi que le cas extrême d'une masse $M$ localisée en un point.  

Pour cette partie on utilisera la méthode des différences finies, et on emploiera le programme 
\verb| WaveEquation_FiniteDifferences.m | qui est une version améliorée du programme simple utilisé dans la partie précédente.

Le programme permet en particulier de choisir les conditions limites en $x=0$ et $x=L$ à l'aide des paramètres \verb| BC_0 | et \verb| BC_L | qui peuvent prendre les valeurs suivantes :
\begin{itemize}
\item \verb| Fixed | pour une corde fixée à l'une et/ou l'autre extrémité (cas identique à celui traité dans la partie précédente).
\item \verb| Free | pour une condition de corde "libre" (force transversale nulle).
\item \verb| Transparent | pour une condition limite de "non-réflexion" en sortie modélisant un raccord avec un milieu semi-infini.
\item \verb| Pulse | pour imposer à l'extrémité droite une déformation localisée en temps : 
$Y(0,t) = \exp \left[ - \left(\frac{t-t_1}{t_a}\right)^2  \right]$.
\item \verb| Y0 | pour imposer un forçage sinusoïdal de la position : $Y(0,t) = \sin \omega t$.
\item \verb| F0 | pour imposer un forçage sinusoïdal de la force transverse : 
$(\partial Y/\partial x)_{x=0} = \sin \omega t$.
\item \verb| I0 | pour imposer une une condition "non-réflexion" en entrée modélisant un raccord avec un milieu semi-infini duquel provient une onde incidente de pulsation $\omega$.
\item \verb| I0_Pulse | pour imposer une une condition "non-réflexion" en entrée modélisant un raccord avec un milieu semi-infini duquel provient une onde incidente de type "pulse".
\end{itemize}

(Etudiez le programme %ou tapez \verb| help WavesOnString_FiniteDifferences.m | 
pour plus de précisions).


\comment{
\subsection{Etude des conditions limites}

Pour cette partie on considère le cas d'une corde homogène. 

\begin{enumerate}
\item On considère tout d'abord le cas d'une corde fixée à son extrémité droite et soumise à un "pulse" à son extrémité gauche ( \verb| BC_0 = Pulse | et  \verb| BC_L = Fixed |) .
A l'aide du programme, observez la manière dont l'onde se réfléchit à chaque extrémité.

\item Modifiez les paramètres du programme pour traiter successivement les cas d'une condition limite à droite "libre" et "transparente". Que constate-t-on sur les réflexions ?

\item (facultatif) On suppose maintenant que la corde est forçée de manière sinusoïdale en entrée 
(\verb| BC_0 = Y0 |). 
Testez les trois cas de conditions limites à droite. Interprétez physiquement. 
 
\end{enumerate}
}

\subsection{Simple discontinuité}

On suppose que la corde est inhomogène et présente une discontinuité de masse linéique localisée à la position $X1$. La vitesse de l'onde est donnée par 

$$
c(x) = \left\{ \begin{array}{ll} 
c_1 \qquad & x<X_1 \\
c_2 & X_1<x
\end{array} \right.
$$

Les valeurs de $X_1, c_1$ et $c_2$ correspondent aux variables 
\verb| X1  |, \verb| c1 | et \verb| c2 | qu'il faudra modifier dans le code.

On prendra $X_1 = 5$, $L=10$, $c_1 = 1$ et on fera varier la valeur de $c_2$.

On utilisera une condition limite transparente en $x=L$.

\begin{enumerate}

\item A l'aide du programme, simulez et observez la propagation d'une impulsion 
(\verb| BC_0='Pulse' |) sur la corde. Que constate-t-on quand l'onde atteint la discontinuité ? Testez différentes valeurs de $c_2$. Commentez à l'aide des résultats du cours. 

\item On impose maintenant en entrée une onde incidente sinusoïdale (\verb| BC_0='I0' |) de pulsation $\omega = 1$. Observez le comportement. Que se passe-t-il pour $t>2 X_1/c_1$ ?

\item Dans le régime établi, mesurez l'amplitude de l'onde transmise, et vérifiez que celle-ci est en accord avec la valeur théorique du coefficient de transmission en amplitude :
$C/A = 2c_1 /(c_1+c_2)$.

On pourra s'aider des résultats tracés sur la figure 2, qui représente (dans le plot du bas) le flux d'énergie incident (rouge), réfléchi (vert), transmis (bleu) et net en entrée (=incident - réfléchi). 
Le régime peut être considéré comme établi quand le flux net en entrée est égal au flux transmis en sortie.


\item  On considère maintenant un forçage sinusoïdal en amplitude (\verb| BC_0='Y0' |) .
Faites varier les valeurs de $\omega$ et de  $c_2$. Mettez en évidence un mécanisme de résonance. 

%\item Testez la condition de non-réflexion en entrée (\verb| BC_0 = I0 |) et observez que celle-ci permet bien de simuler le cas d'une corde infinie. 

\item A partir des résultats du programme, estimez l'impédance d'entrée $Z_{IN}$ de la corde, et comparez aux prédictions théoriques du TD.

(Rappel : $|Z_{IN}| =| F_{IN}|/ |V_{IN}| $ et $arg(Z_{IN}) = \varphi_{F_{IN}} - \varphi_{V_{IN}}$).





\end{enumerate}


\subsection{Propagation à travers une double discontinuité (facultatif)}

On considère maintenant une double discontinuité :

$$
c(x) = \left\{ \begin{array}{ll} c_1 \qquad & x<X_1 \\
c_2 & X_1<x<X_2 \\
c_3 & X_2 < x
\end{array}
\right.
$$


On considèrera toujours un domaine de longueur $L$ avec une condition limite de non-réflexion en sortie et d'onde incidente imposée en entrée.
 
On s'intéresse à la transmission d'une onde monochromatique de fréquence $\omega$ a travers cette double discontinuité.

On prendra les paramètres suivants :

$c_1 = 1$ ; $c_2 = 2$; $c_3 = 4$, $L = 20$, $X_1= 5$, $X_2=15$, 

\begin{enumerate}

\item On choisit tout d'abord $\omega = \pi$. A l'aide du programme, observez comment les réflexions multiples interagissent entre elles jusqu'à obtenir un régime établi.

 \item Cherchez à déterminer des valeurs de $\omega$ permettant d'éliminer complètement l'onde réfléchie. A quelle relation entre la longueur d'onde dans le milieu 2 et la distance entre les deux discontinuités correspond cette relation ?
 
  \item Mêmes questions pour $c_1 = 1$, $c_2 = 3$ et $c_3 = 9$. On pourra être amené à diminuer le pas de temps du schéma numérique.
 
 
 \item On considère maintenant le cas $c_2 = 4$, $c_3=1$. Quelles valeurs de $\omega$ correspondent-elles aux maximums et minimums du coefficient de transmission ?
 
 
 \end{enumerate}
 
 
\subsection{Corde chargée par une masse (facultatif)}

On revient à une corde homogène de célérité uniforme ($c_1= c_2 = 1$) mais on suppose que celle-ci est chargée à la position $x=X_1$ par une masse ponctuelle de masse $M$.

\begin{enumerate}

\item On étudie tout d'abord le régime harmonique établi correspondant à une onde incidente d'amplitude donnée en entrée.

La théorie (exercice 1.2.3) prédit que dans le régime harmonique établi, le coefficient de transmission est donné par :

$$
{\left| \frac{C}{A} \right|}^2 = \frac{ 1}{ 1+ \omega^2/ \omega_M^2}
$$

Où $\omega_M = 2 T/M c$ est la "fréquence de coupure" due à la masse.

A l'aide du programme, vérifiez cette loi en faisant varier les paramètres $\omega$ et $M$.
(on prendra pour simplifier $T=c=1$).

\item Observez maintenant l'effet d'un "pulse". Que se passe-t-il ? Essayez différentes valeurs de $M$ et commentez.

\end{enumerate}


 
 
 \subsection{Travail à réaliser}
 
Choisissez un cas d'étude (simple ou double discontinuité ou corde chargée par une masse),  et précisez les valeurs choisies pour les paramètres physiques, ($c_1, c_2, X_1$ , etc...), les paramètres numériques $(dx,dt,...)$ et les conditions d'entrée et de sortie.
 
Pour une (ou plusieurs) valeurs de $\omega$ choisies de manière judicieuse (égale ou proche d'une résonance ou d'une anti-résonance, condition de transmission maximale, etc...), illustrez le comportement de la corde à l'aide de résultats issus du programme.

Dans la description des résultats, on s'attachera a bien distinguer les aspects transitoires et le régime établi.

Décrivez au mieux le régime établi en faisant le lien avec les concepts théoriques vus en cours et en TD  (impédance d'entrée, coefficients de transmission/réflexion, flux d'énergie,...) 

 
 
 
 
 
 %\begin{description}
 
 
 
% \item[ Cas (a) : section de corde très lourde.]
 
% Afin de se rapprocher des conditions de l'exercice 1.2.3, on considère une section de corde très lourde. 
 
 %On prendra les données suivantes :
 
%$c_1 = 1$ ; $c_2 = 100$; $c_3 = 1$, $L_1 = 10$, $h= 1$.

%Observez le comportement dans les régime de haute et basse fréquence.

%(NB. Afin de satisfaire le critère de stabilité numérique on pourra être ammené à diminuer le pas de temps dt) 


%\item[Cas (b) : Adaptation d'impédance.]


%On prendra maintenant les données suivantes :
 


 
%Essayez plusieurs valeurs de $\omega$ et observez ce qui se passe. Observez en particulier l'établissement d'un régime établi à l'issue de réflexions successives entre les deux discontinuités.



%Peut-on trouver une valeur de $\omega$ permettant d'éliminer la réflexion de l'onde ?


%Faites varier $\omega$ et tentez de trouver empiriquement une relation entre $\omega$ et $L$ permettant d'éliminer la réflexion de l'onde.

%\end{description}

%\subsubsection{Propagation d'un pulse à travers un dioptre}

}
\clearpage
\subsection*{Annexe : programme WaveEquation\_FiniteDifference\_string.m}

\small
\begin{verbatim}
%%
%% Program WaveEquation_FiniteDifference_string.m
%% Computes the solution of the wave equation on a string of lenght L
%% with given initial conditions (requires two functions posinit and vitinit)
%%
clear all;
global L c;

% length and duration of integration
L = 10;  c=1; 

% time and space steps
stopTime = 2*L/c;
dx = .005*L ;
dt = .001*stopTime ;
Nx = L/dx + 1;
x = 0:dx:L;

% Set  current  and  past  to  the  graph  of  a  plucked  string
    Y_current = posinit(x);
    V_current = vitinit(x);
    Y_past = Y_current-V_current*dt;
    r = c*dt/dx ;

figure(1);
subplot(2,1,1);  plot(x,Y_current,'r') ;  title('Initial shape (red)');
subplot(2,1,2);  plot(x,V_current,'b') ; title('Initial velocity (red)');
pause

% Loop over time
for t =0:dt:stopTime
% inner points : solve the wave equation
for j=2:Nx-1
Y_future(j) = r^2*(Y_current(j-1)+Y_current(j+1))+2*(1-r^2)*Y_current(j)-Y_past(j);
end
% boundary condition x=0:
    Y_future(1) = 0;   Y_future(Nx) = 0; 

% Plot  the  graph  after  every  20 th  frame
if mod( t/dt ,  20) == 0   
    figure(1);
    plot( x ,  Y_current,'r');
    V_current = (Y_future-Y_past)/(2*dt);
    subplot(2,1,1);
    plot( x ,  Y_current,'r');  xlim( [ 0 L ]); title('String position Y(x) (red) ');
    subplot(2,1,2);
    plot( x ,  V_current,'b'); xlim( [ 0 L ]);  title('String velocity V(x) (blue) '); 
    pause(.001)
end
% Set  things  up  for  the  next  time  step
Y_past = Y_current ;
Y_current = Y_future ;
end

disp('max(|Y-Y0|) after one period :');  max(abs(Y_current-posinit(x)))
\end{verbatim}

\end{document}
