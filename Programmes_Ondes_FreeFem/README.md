
Ce répertoire contient les programmes utilisés a partir de 2023
dans le cours d'ONDES du M1 Energie, UPS.

Pour une utilisation en dehors de l'interface StabFem,
ces programmes s'utilisent ainsi :

> FreeFem++ Mesh_Bidioptre.edp - L2 10 ( autres parametres ... )
> FreeFem++ LinearForced_Acoustics.edp -omega 2 (autres parametres...)
> FreeFem++ LinearForcedLoop_Acoustics.edp -omegai 0
