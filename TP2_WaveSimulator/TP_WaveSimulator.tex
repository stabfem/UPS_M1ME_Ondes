\documentclass[11pt,a4paper]{article}
 
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{fancyhdr}
\usepackage{graphicx, amsmath, amssymb, latexsym}
%\usepackage{natbib}
 
 
\setlength{\textheight}{24.5cm} % 24
\setlength{\topmargin}{-5mm}
\setlength{\textwidth}{16cm}  % 15
%\setlength{\oddsidemargin}{20mm} % marge gauche (im)paire = 1in. + x1mm
%\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\oddsidemargin}{0mm} % marge gauche (im)paire = 1in. + x1mm
\setlength{\evensidemargin}{0mm} % book : marge gauche paire = 1in. + x2mm
\setlength{\fboxsep}{5mm}

\newcommand{\e}  {\mathrm{e}}   
\newcommand{\dd}  {\textrm d}      
\newcommand{\im}  {\mathrm{i}} %\newcommand{\im}  {\textrm i}
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddp}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {{\textrm d}^{2} #1} {{\textrm d} #2 ^{2}}}
%\newcommand{\tensor}[1]{\stackrel{\Rightarrow}{#1}}

\newcommand{\comment}[1]{}

%\graphicspath{{../Figures/}{../FiguresPierre/}}
\graphicspath{Figures/}

%\makeindex

%------------------------------------------------------------------------------
\begin{document}

%\begin{titlepage}

\noindent
\textsc{Universit\'e Paul Sabatier \hfill Ann\'ee 2018/2019} \\
\textsc{M1 Mécanique \hfill majeure Ondes et Turbulence / Ondes}
%\vspace{1cm}

%\begin{center}


%\section*{TP numérique : ondes sur une corde} 
\begin{center}
{\Large \bf TP numérique d'ondes II \\ Propagation d'ondes dans un milieu monodimensionnel ; \\ Etude des régimes transitoires et établis}
\end{center}

%\renewcommand{\thesubsection}{\arabic{subsection}}

\section{Théorie : équation des ondes dans un milieu 1D}

Dans ce TP, on va utiliser un programme qui simule la propagation d'ondes non-dispersives dans un milieu monodimensionel inhomogène, 
en partant d'une formulation unifiée qui englobe les cas d'une corde tendue, d'un tuyau acoustique et d'un canal en faible profondeur.


\subsection{Ondes sur une corde inhomogène}

Considérons tout d'abord le cas d'une corde tendue de tension $T$ (constante) et masse linéique $\mu(x)$. On peut formuler le problème à l'aide de deux variables couplées, la vitesse $V(x,t)$ et la force transverse $F_T(x,t)$ (définie comme la composante transverse de la force exercée par le tronçon de corde $[0,x]$ sur le tronçon $[x,L]$) . Celles-ci se déduisent de la variable primitive "déplacement transverse"  $Y(x,t)$ par les relations suivantes :
\begin{equation}
V = \ddp{Y}{t} ; \qquad F_T = - T \ddp{Y}{x}.
\end{equation}

Les variables $V$ et $F_T$ sont couplées par les deux équations aux dérivées partielles suivantes :

\begin{equation}
\mu \ddp{V}{t} = -  \ddp{F_T}{x} ; \qquad 
\ddp{F_T}{t} = -T \ddp{V}{x}.
\end{equation}

Qui peuvent se combiner pour donner l'équation des ondes pour la variable primitive $Y$: 

\begin{equation}
\ddp{^2Y}{t^2}   = c^2 \ddp{^2Y}{x^2}
\end{equation}

\subsection{Ondes dans un tuyau acoustique}

Considérons maintenant le problème des ondes acoustiques dans un tuyau de section variable $S(x)$. Supposons que les propriétés du fluide sont également inhomogènes (notées $\rho(x)$, $c(x)$).

Notons $U = S u'$ le débit de volume et $p'$ la pression acoustique. Ces deux variables sont couplées par les deux équations différentielles suivantes :

\begin{equation}
\frac{S}{c^2} \ddp{p'}{t} = - \rho \ddp{U}{x} ; \qquad 
\rho \ddp{U}{t} = - S \ddp{p'}{x}.
\label{eq:acoustiquecouplees}
\end{equation}

On peut aboutir à une équation similaire à celle du paragraphe précédent en introduisant la variable primitive ${\tilde X} = X S$ "déplacement de volume" (où $X$ est le déplacement lagrangien). Les deux variables $U$ et $p'$ s'en déduisent par :

\begin{equation}
U =  \ddp{\tilde{X}}{t} ; \qquad p' = - \frac{\rho c^2}{S} \ddp{\tilde{X}}{x}.
\end{equation}
Et les deux équations (\ref{eq:acoustiquecouplees}) peuvent être combinées pour obtenir l'équation suivante :

\begin{equation}
\ddp{^2\tilde{X}}{t^2}   = \frac{S}{\rho}  \ddp{}{x} \left( \frac{\rho c^2}{S} \ddp{\tilde{X}}{x} \right) 
\end{equation}

Cette équation est équivalente à celle vue en cours pour la variable $p'$ (Equation de Webster) mais est ici formulée en variable "primitive" $\tilde{X}$.


\subsection{Ondes dans un canal de faible profondeur}

Considérons maintenant un canal de profondeur $H(x)$ et largeur $W(x)$ variables. Dans le cas où la longueur d'onde est grande devant la profondeur, il est justifié de supposer que la vitesse $u'$ est uniforme dans chaque tranche. En notant $\eta$ l'élévation de la surface libre, les bilans de masse et de quantité de mouvement sur une volume élémentaire correspondant à une tranche d'épaisseur $dx$ conduisent aux deux équations couplés suivantes (cf. exercice 5.3.1 du fascicule de TD):
\begin{equation}
\rho W \ddp{\eta}{t} = -\rho \ddp{ (W H u') }{x} ; \qquad 
\rho H W  \ddp{u'}{t} = - g W H  \ddp{\eta}{x}.
\end{equation}

En introduisant $\tilde{Q} = \rho g H W u'$ le "débit-poids" , les relations prennent la forme

\begin{equation}
\rho W \ddp{\eta}{t} = -\frac{1}{g} \ddp{\tilde{Q}}{x} ; \qquad 
\frac{1}{g} \ddp{\tilde{Q}}{t} = - \rho g H W  \ddp{\eta}{x}.
\end{equation}



On peut de nouveau introduire la variable primitive ${\tilde X} = \rho g H W X$ "déplacement de poids" (où $X$ est le déplacement lagrangien). Les deux variables $\tilde{Q}$ et $\eta$ s'en déduisent par :

\begin{equation}
\tilde{Q} =  \ddp{\tilde{X}}{t} ; \qquad \eta = - \frac{1}{\rho g W} \ddp{\tilde{X}}{x}.
\end{equation}
Et ces deux équations peuvent être combinées pour obtenir l'équation suivante :

\begin{equation}
\ddp{^2\tilde{X}}{t^2}   = g H W  \ddp{}{x} \left( \frac{1}{W} \ddp{\tilde{X}}{x} \right).
\end{equation}





\subsection{Formulation générale}

On remarque que dans les trois problèmes précédents on a une variable "vitesse" ${\cal V}$ et une variable "force" ${\cal F}$ couplées par deux équations aux dérivées partielles qui s'écrivent :
\begin{equation}
\ddp{{\cal V}}{t} = - \frac{c}{Z} \ddp{\cal F}{x} ; \qquad 
\ddp{\cal F}{t} = -  c Z  \ddp{\cal V}{x}
\end{equation}
où les définitions de la vitesse $c$ et de l'impédance sont celles données dans le tableau "analogies" du fascicule de TD. 
On note ensuite qu'en introduisant la variable primitive ${\cal Y} = \int {\cal V} \,dt $ les deux équations se combinent sous la forme suivante :

\begin{equation}
\ddp{^2 \cal Y}{t^2}   = \frac{c}{Z}  \ddp{}{x} \left( c Z  \ddp{{\cal Y}}{x} \right) 
\label{eq:WaveUnified}
\end{equation}

\subsubsection{Energie et puissance}

Dans les trois cas considérés, on peut de plus remarquer que l'énergie mécanique linéique se met sous la forme 
\begin{equation} 
e_m =  \left( \frac{Z {\cal V}^2}{2 c} + \frac{ {\cal F}^2}{2 Z c} \right),
\end{equation}
et que la puissance échangée à la position $x$ s'écrit
\begin{equation}
{\cal P}(x,t) = {\cal V}(x,t) \cdot {\cal F}(x,t).
\end{equation}
En supposant que la solution peut être mise  {\em localement}  sous forme de d'Alembert, soit ${\cal V}(x,t) = {\cal V}^+(x-ct) +{\cal V}^-(x+ct)$ 
et  ${\cal F}(x,t) = {\cal F}^+(x-ct) - {\cal F}^-(x+ct)$  , cette puissance peut alors être décomposée sous la forme 
\begin{equation}
{\cal P} ={\cal P}^+ +  {\cal P}^- , \quad 
\mbox{ avec } {\cal P}^+ =  {\cal V}^+ \cdot {\cal F}^+ > 0 \quad 
\mbox{ et } {\cal P}^- =  {\cal V}^- \cdot {\cal F}^- < 0.
\end{equation}
On reconnait sous cette forme le flux d'énergie (positif) associé l'onde de vitesse $+c$ et celui (négatif) associé à l'onde de vitesse $-c$.


\section{Présentation du programme}

Pour ce TP, on fournit un programme  \verb| WaveSimulator.m | qui résout l'équation des ondes sous la forme unifiée (\ref{eq:WaveUnified}).
Ce programme est une version (très) améliorée du programme simple \verb| WaveEquation_FiniteDifferences.m |  utilisé dans le TP précédent. Voici quelques explications sur le fonctionnement du programme et la manière de l'adapter au cas qui vous intéresse :

\paragraph{Propriétés du milieu}

Dans le programme, les propriétés du milieu sont données par les lois $c(x)$ et $Z(x)$ (éventuellement discontinues). 
Différents cas sont déjà prédéfinis dans le programme selon la valeur de la variable \verb| problemtype|. Les cas prédéfinis sont :
\begin{itemize}
\item \verb| string | : corde homogène.
\item \verb| string1disc | : corde avec une discontinuité de masse linéique.
\item \verb| string2disc | : corde avec deux discontinuités de masse linéique.
\item \verb| pipe | :  tuyau cylindrique.
\item \verb| pipe1disc | : 2 tuyaux cylindriques de section différente raccordés entre eux. 
\item \verb| horn | : tuyau cylindrique raccordé à un pavillon exponentiel.
\item \verb| canalprog | : canal avec profondeur variable de manière progressive.
\end{itemize}

La définition de ces cas est donnée dans la fonction \verb|cZ|, à la fin du programme.  Dans cette fonction vous pourrez régler les paramètres de ces différents cas et éventuellement rajouter vos propres cas.
 

\paragraph{Conditions limites}

Le programme permet  de choisir les conditions limites en $x=0$ et $x=L$ à l'aide des paramètres \verb| BC_0 | et \verb| BC_L | qui peuvent prendre les valeurs suivantes :
\begin{itemize}
\item \verb| Fixed | : condition ${\cal Y} = 0$ (corde fixée ou tuyau fermé)
\item \verb| Free | : condition ${\cal F} = \ddp{\cal Y}{x} = 0$ (corde libre ou tuyau idéalement ouvert).
\item \verb| Transparent | pour une condition limite de "non-réflexion" en sortie modélisant un raccord avec un milieu semi-infini.
\item \verb| Pulse | pour imposer à l'extrémité droite une déformation de type "impulsion gaussienne" : 
${\cal Y}(0,t) = A  \exp \left[ - \left(\frac{t-t_1}{t_a}\right)^2  \right]$.
\item \verb| Y0 | pour imposer un forçage sinusoïdal de la  variable primitive : ${\cal Y} (0,t) = A \sin \omega t$.
\item \verb| F0 | pour imposer un forçage sinusoïdal de variable force : ${\cal F} = A \sin \omega t$.
\item \verb| I0 | pour imposer une une onde incidente sinusoïdale de pulsation $\omega$ tout en laissant sortir une onde réfléchie  : 
${\cal F} = A \sin \omega (t - x/c) + G(t+x/c)$ où $G$ est arbitraire. 
\item \verb| I0_Pulse | pour imposer une une onde incidente de type "impulsion gaussienne" tout en laissant sortir une onde réfléchie  :  ${\cal F} = A \sin \omega (t - x/c) + G(t+x/c)$ où $G$ est arbitraire. 

%our imposer une une condition "non-réflexion" en entrée modélisant un raccord avec un milieu semi-infini duquel provient une onde incidente de type "pulse" : ${\cal F} = A  \exp \left[ - \left(\frac{(t-x/c)-t_1}{t_a}\right)^2  \right] + G(t+x/c)$ où $G$ est arbitraire. 
\end{itemize}

\paragraph{Figures générées par le programme}

Le programme génère les figures suivantes :
\begin{itemize}
\item Figure 1(a,b,c) : tracé de $\cal Y$, $\cal V$ et $\cal F$ au cours du temps.
\item Figure 2(a) : tracé de la puissance nette ${\cal P}(0,t)$ dans le plan d'entrée, de ses composantes  ${\cal P}(0,t)^+$ (puissance incidente dans le domaine $x<0$), 
 ${\cal P}(0,t)^-$ (puissance réfléchie dans le domaine $x<0$) , ainsi que de la puissance nette ${\cal P}(L,t)$ dans le plan se sortie et de ses composantes  ${\cal P}(L,t)^+$ (puissance arrivant à l'extrémité $L$ du domaine simulé) et  
 ${\cal P}(L,t)^-$ (puissance réfléchie dans le domaine simulé).
 
 Notez que le programme trace les valeurs les valeur moyennées en temps  (sur une période) $\bar{\cal P}(0,t),\bar{\cal P}^+(0,t),\bar{\cal P}^-(0,t),\bar{\cal P}(L,t),\bar{\cal P}^+(L,t)$ et $\bar{\cal P}^-(L,t)$ de ces puissances. Si la variable \verb| fig2inst | est mise à 1, le programme trace de plus les valeurs instantanées.
\item Figure 2(b) : tracé de l'énergie totale contenue dans le domaine, définie par $E(t) = \int_0^L e_m(x,t) dx$.

\item Figure 3 : Tracé des valeurs de diverses quantités dans les plan d'entrée et de sortie.

\item Figure 4 : Tracé de $c(x)$, $Z(x)$ et $S(x)$ (pour un tuyau ou un canal).

Notez que si celles-ci ne vous sont pas nécessaires vous pouvez désactiver les figures 2 et 3 en mettant à zéro les variables \verb|fig2| et \verb|fig3|.

 \end{itemize}
 

%(Etudiez le programme %ou tapez \verb| help WavesOnString_FiniteDifferences.m | pour plus de précisions).



\section{Corde de masse linéique variable, de longueur semi-infinie}

On commencera par tester les différentes possibilités du programme dans le cas d'une corde.

\subsection{Etude des conditions limites}

Pour cette partie on considère le cas d'une corde homogène. 

\begin{enumerate}
\item On considère tout d'abord le cas d'une corde fixée à son extrémité droite et soumise à un "pulse" à son extrémité gauche ( \verb| BC_0 = Pulse | et  \verb| BC_L = Fixed |) .
A l'aide du programme, observez la manière dont l'onde se réfléchit à chaque extrémité.

\item Modifiez les paramètres du programme pour traiter successivement les cas d'une condition limite à droite "libre" et "transparente". Que constate-t-on sur les réflexions ?

\item (facultatif) On suppose maintenant que la corde est forçée de manière sinusoïdale en entrée 
(\verb| BC_0 = Y0 |). 
Testez les trois cas de conditions limites à droite. Interprétez physiquement. 
 
\end{enumerate}


\subsection{Simple discontinuité}

On suppose que la corde est inhomogène et présente une discontinuité de masse linéique localisée à la position $X1$. La vitesse de l'onde est donnée par 

$$
c(x) = \left\{ \begin{array}{ll} 
c_1 \qquad & x<X_1 \\
c_2 & X_1<x
\end{array} \right.
$$

Les valeurs de $X_1, c_1$ et $c_2$ correspondent aux variables 
\verb| X1  |, \verb| c1 | et \verb| c2 | qu'il faudra modifier dans le code.

On prendra $X_1 = 0.5$, $L=1$, $c_1 = 1$ et on fera varier la valeur de $c_2$.

On utilisera une condition limite transparente en $x=L$.

\begin{enumerate}

\item A l'aide du programme, simulez et observez la propagation d'une impulsion 
(\verb| BC_0='Pulse' |) sur la corde. Que constate-t-on quand l'onde atteint la discontinuité ? Testez différentes valeurs de $c_2$. Commentez à l'aide des résultats du cours. 

\item On impose maintenant en entrée une onde incidente sinusoïdale (\verb| BC_0='I0' |) de pulsation $\omega = 6\pi$. Observez le comportement. Que se passe-t-il pour $t>2 X_1/c_1$ ?

\item Dans le régime établi, mesurez l'amplitude de l'onde transmise, et vérifiez que celle-ci est en accord avec la valeur théorique du coefficient de transmission en amplitude :
$C/A = 2c_1 /(c_1+c_2)$.

On pourra s'aider des résultats tracés sur la figure 2, qui représente le flux d'énergie incident (cyan), réfléchi (magenta), transmis (bleu) et net en entrée (=incident - réfléchi, noir). Le régime peut être considéré comme établi quand le flux net en entrée est égal au flux transmis en sortie.


\item  On considère maintenant un forçage sinusoïdal en amplitude (\verb| BC_0='Y0' |) .
Faites varier les valeurs de $\omega$ et de  $c_2$. Mettez en évidence un mécanisme de résonance. 

%\item Testez la condition de non-réflexion en entrée (\verb| BC_0 = I0 |) et observez que celle-ci permet bien de simuler le cas d'une corde infinie. 

\item A partir des résultats du programme, estimez l'impédance d'entrée $Z_{IN}$ de la corde, et comparez aux prédictions théoriques du TD.

(Rappel : $|Z_{IN}| =| F_{IN}|/ |V_{IN}| $ et $arg(Z_{IN}) = \varphi_{F_{IN}} - \varphi_{V_{IN}}$).





\end{enumerate}

\comment{
\subsection{Propagation à travers une double discontinuité (facultatif)}

On considère maintenant une double discontinuité :

$$
c(x) = \left\{ \begin{array}{ll} c_1 \qquad & x<X_1 \\
c_2 & X_1<x<X_2 \\
c_3 & X_2 < x
\end{array}
\right.
$$


On considèrera toujours un domaine de longueur $L$ avec une condition limite de non-réflexion en sortie et d'onde incidente imposée en entrée.
 
On s'intéresse à la transmission d'une onde monochromatique de fréquence $\omega$ a travers cette double discontinuité.

On prendra les paramètres suivants :

$c_1 = 1$ ; $c_2 = 2$; $c_3 = 4$, $L = 20$, $X_1= 5$, $X_2=15$, 

\begin{enumerate}

\item On choisit tout d'abord $\omega = \pi$. A l'aide du programme, observez comment les réflexions multiples interagissent entre elles jusqu'à obtenir un régime établi.

 \item Cherchez à déterminer des valeurs de $\omega$ permettant d'éliminer complètement l'onde réfléchie. A quelle relation entre la longueur d'onde dans le milieu 2 et la distance entre les deux discontinuités correspond cette relation ?
 
  \item Mêmes questions pour $c_1 = 1$, $c_2 = 3$ et $c_3 = 9$. On pourra être amené à diminuer le pas de temps du schéma numérique.
 
 
 \item On considère maintenant le cas $c_2 = 4$, $c_3=1$. Quelles valeurs de $\omega$ correspondent-elles aux maximums et minimums du coefficient de transmission ?
 
 
 \end{enumerate}
}

\clearpage

\section{Travail à réaliser}

Après avoir pris en main le code et testé les différentes options sur le cas de la corde vibrante, on traitera l'un des deux cas suivants :

\subsection{Silencieux de voiture (groupe 1)} 



Ce cas correspond à la géométrie de l'exercice 2.3.2 du fascicule de TD. On traitera le cas d'un silencieux avec les dimensions suivantes :

$S_1 = 1 cm^2 ; \quad S_2 = 25 cm^2 ; S_3 = 1cm^2, L= 30 cm, X_1 = 30 cm, X_2 = 30cm$.

(il s'agit d'un tuyau de longueur totale 90cm, dont le second tiers est de section 25 fois plus large que les deux autres tiers).

On prendra une condition limite de sortie de type "transparent" qui correspond à un raccord avec un tuyau semi-infini en aval.


Le fluide est de l'air caractérisé par une vitesse du son $c = 0.34 m/ms$ .


La théorie (exercices 2.1.3 et 2.3.2)  prédit la valeur suivante pour le coefficient de transmission :

$$
T = \frac{4 Z_1 Z_3}{\left( Z_1 + Z_3 \right)^2 \cos^2 (k L_2 )+\left( Z_2 + \frac{Z_1  Z_3 }{Z_2 } \right)^2 \sin^2 (k L_2)}
$$

avec $k = \omega c$ et $L_2 = (X_2-X_1)$.

A l'aide du programme, étudiez le fonctionnement du silencieux pour différentes valeurs de $\omega$. Vous étudierez plus particulièrement deux cas correspondant à des valeurs maximale ou minimale du coefficient de transmission. Comparez avec la théorie et illustrez dans chaque cas la manière dont les réflexions successives interfèrent. 

{\em Remarque :  pour un tuyau acoustique, il est judicieux de choisir la milliseconde comme unité de temps, ce qui permet d'avoir des vitesses et des impedances d'ordre unité}.

\subsection{Résonance dans un estuaire} 

Ce cas correspond à l'exercice 5.3 du fascicule de TD.

On considère un premier canal de largeur $W_1 = 1km$, profondeur $H_1 = 200m$ et longueur $L_1 = 100 km$ raccordé à un second canal de  largeur 
$W_2 = 0.1km$, profondeur $H_2 = 10m$ et longueur $L_2 = 200 km$. Ce second canal est fermé en sortie (condition limite de sortie de type "fixed").

On considère une onde incidente d'amplitude $\eta = 0.1 m$ et de période $T = 12h$ en entrée du canal.

La théorie prédit un facteur d'amplification entre l'onde incidente et l'onde en fond d'estuaire donnée par la formule suivante :

$$
\frac{|\eta(L)|^2}{\eta(0)|^2} = \frac{Y_1^2}{ Y_2^2 \cos^2 (k_2 L_2)   + Y_1^2  \sin^2 (k_2 L_2) }
$$

où $k_2 = \omega c_2$ et $Y_1 = 1/Z_1$ et $Y_2 = 1/Z_2$ sont les admittances.

A l'aide du programme,  étudiez la propagation des ondes et mettrez en évidence un phénomène de réflexion.

Faites ensuite varier la longueur du canal pour mettre en évidence les cas de résonance et d'anti-résonance.

Comparez avec la théorie et illustrez dans chaque cas la manière dont les réflexions successives interfèrent. 

{\em Remarque :  pour un canal, il est judicieux de choisir la minute comme unité de temps, le kilomètre comme unité de longueur, et le téragramme comme unité de masse, ce qui permet d'avoir des vitesses et des impedances d'ordre unité}.







 
 \subsection{Rapport}
 
Pour le cas d'étude sélectionné, précisez les valeurs choisies pour les paramètres physiques, ($c_1, c_2, X_1$ , etc...), les paramètres numériques $(dx,dt,...)$ et les conditions d'entrée et de sortie.
 
Pour une (ou plusieurs) valeurs de $\omega$ choisies de manière judicieuse (égale ou proche d'une résonance ou d'une anti-résonance, condition de transmission maximale, etc...), illustrez le comportement de la corde à l'aide de résultats issus du programme.

Dans la description des résultats, on s'attachera a bien distinguer les aspects transitoires et le régime établi.

Décrivez au mieux le régime établi en faisant le lien avec les concepts théoriques vus en cours et en TD  (impédance d'entrée, coefficients de transmission/réflexion, flux d'énergie,...) 



\end{document}
