#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 15:04:56 2024

@author: fabred
"""

import numpy as np
import numpy.linalg as la
from matplotlib import pyplot as plt
import math
import csv
#plt.close("all")

# Lecture du fichier ecrit par programme python
filename = 'Zin_Cylinder_theo.csv'
data = np.loadtxt(filename, delimiter=",",skiprows=1,usecols=[0,1,2])
omegarange = data[:,0] 
Zin =np.zeros(len(omegarange),dtype=complex)
Zin.real = data[:,1] 
Zin.imag = data[:,2]

# Lecture du fichier ecrit par programme freefem
filename = 'Zin_Cylinder_FF.csv'
data = np.loadtxt(filename, delimiter=",",skiprows=1,usecols=[0,1,2,3,4,5])
omegarangeFF = data[:,1] 
ZinFF =np.zeros(len(omegarangeFF),dtype=complex)
ZinFF.real = data[:,3] # BUG RECTIFIE
ZinFF.imag = data[:,4] # BUG RECTIFIE


# Lecture du fichier ecrit par programme WaveSimulator
filename = 'Zin_Cylinder_WS.csv'
data = np.loadtxt(filename, delimiter=",",skiprows=1,usecols=[0,1])
omegarangeWS = data[:,0] 
absZinWS = data[:,1]

plt.figure(21)
plt.semilogy(omegarange/(2*np.pi)*1000,abs(Zin),'r',label='Théorie 1D')
plt.semilogy(omegarangeFF/(2*np.pi)*1000,abs(ZinFF),'b--',label='FreeFem')
plt.semilogy(omegarangeWS/(2*np.pi)*1000,absZinWS,'g-.',label='WaveSimulator')
plt.xlabel('f [Hz]') 
plt.ylabel('|Zin|')
plt.ylim(1e-1, 1e3)
plt.xlim(0, 1600)
plt.title("Impedance d'entrée d'un tube cylindrique")
plt.legend(loc="upper right")
plt.show
plt.savefig("Impedance_Pipe_Comparison3methods.png")

plt.figure(22)
plt.plot(omegarange,abs(Zin),'r',label='Théorie 1D')
plt.plot(omegarangeFF,abs(ZinFF),'b--',label='FreeFem')
plt.plot(omegarangeWS,absZinWS,'g-.',label='WaveSimulator')
plt.xlabel('omega [rad/ms]') 
plt.ylabel('|Zin|')
plt.ylim(0, 250)
plt.xlim(4.0, 4.25)
plt.title("Impedance d'entrée d'un tube cylindrique")
plt.legend(loc="upper right")
plt.show
plt.savefig("Impedance_Pipe_Comparison3methods_Zoom.png")
#%%
plt.figure(23)
plt.semilogy(omegarange,abs(Zin),'r',label='Théorie 1D')
plt.semilogy(omegarangeFF,abs(ZinFF),'b--',label='FreeFem')
plt.semilogy(omegarangeWS,absZinWS,'g-.',label='WaveSimulator')
plt.xlabel('omega (rad/ms)') 
plt.ylabel('|Zin|')
plt.ylim(1e-1, 1e3)
plt.title("Impedance d'entrée d'un tube cylindrique")
plt.legend(loc="upper right")
plt.show
plt.savefig("Impedance_Pipe_Comparison3methods.png")




#with open('Zin_Cylinder_theo.csv', newline='') as csvfile:
#    reader = csv.DictReader(csvfile)
#    data = csv.reader(csvfile, delimiter=' ', quotechar='|')
#    for row in reader:
#        print(row['first_name'], row['last_name'])
        
        