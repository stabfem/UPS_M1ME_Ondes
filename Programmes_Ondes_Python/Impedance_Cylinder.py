#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 12:15:27 2024

@author: fabred
"""

import numpy as np
import numpy.linalg as la
from matplotlib import pyplot as plt
import math
import csv

plt.close("all")

global rho,c
rho = 1.225 #kg/m^3 
c = 0.34 #(m/ms)


# Fonction Impedance d'un cylindre (par inversion d'un système linéaire) 
def Zin_cyl(k,L,a,Delta,beta):
  Z0 = rho*c/(np.pi*a**2)
  kL = k*L
  
  Zout = Z0*( -1j*Delta*k + beta*(k*a)**2 )
  
  M = np.matrix([ [1,                      -1] , 
                  [np.exp(1j*kL)*(Z0-Zout), np.exp(-1j*kL)*(Z0+Zout) ]])
  Qf = 1
  
  Y = np.array([Qf*Z0, 0])
  X = la.solve(M,Y)
  A = X[0] ; B = X[1]
  Z = (A+B)/Qf
  
  return Z,A,B

# Fonction Impedance d'entrée d'un cylindre (par formule analytique)
def Zin_cyl_theo(k,L,a,Delta,beta):
  Z0 = rho*c/(np.pi*a**2)
  kL = k*L
  Zout = Z0*( -1j*Delta*k + beta*(k*a)**2 )
  Z = Z0*(Zout - 1j*Z0*np.tan(kL))/(-1j*Zout*np.tan(kL) + Z0);
  return Z

# Calcul de l'impedance pour un tableau de valeurs de omega
Rpipe = 0.06
L = 0.6
Z0 = rho*c/(np.pi*Rpipe**2)
omegarange = np.arange(0,10,.01)
ktab = omegarange/c
Delta = 0.85*Rpipe # correction de longueur
beta = 0.5 # cas flanged
Zin =np.zeros(len(ktab),dtype=complex)
Zint =np.zeros(len(ktab),dtype=complex)
Zin0 =np.zeros(len(ktab),dtype=complex)
Zin1 =np.zeros(len(ktab),dtype=complex)

for i in range(len(ktab)):
    k =ktab[i]
    Zin[i],A,B = Zin_cyl(k,L,Rpipe,Delta,beta)
    Zint[i] = Zin_cyl_theo(k,L,Rpipe,Delta,beta)
    Zin0[i],A,B = Zin_cyl(k,L,Rpipe,0,0)
    Zin1[i],A,B = Zin_cyl(k,L,Rpipe,Delta,0)

# Figure
plt.figure(1)
plt.semilogy(omegarange/(2*np.pi)*1000,abs(Zin/Z0))
plt.semilogy(omegarange/(2*np.pi)*1000,abs(Zint/Z0))
plt.semilogy(omegarange/(2*np.pi)*1000,abs(Zin0/Z0),'--r')
plt.semilogy(omegarange/(2*np.pi)*1000,abs(Zin1/Z0),'--k')
plt.xlabel('f [Hz]') 
plt.ylabel('|Zin| / Z0')
plt.ylim(1e-2, 1e2)
plt.xlim(0, 1600)
plt.title("Impedance d'entrée d'un tube cylindrique")
#plt.legend({'Ideal open','End correction','End correction + radiation'},'location','SE','fontsize',16);
plt.show

# Ecriture d'un fichier csv pour comparaison avec FreeFem
data = np.zeros((len(omegarange),3))
data[:,0] = omegarange
data[:,1] = Zin.real
data[:,2] = Zin.imag
np.savetxt('Zin_Cylinder_theo.csv', data, delimiter=",")

     
# Calcul de la structure spatiale pour une valeur de omegaomega = 4.134; # valeur en rad/ms correspondant au 3e pic, c.a.d. 644 Hz
omega = 4.134
k = omega/c;
ZZ,A,B = Zin_cyl(k,L,Rpipe,Delta,beta)
xT = np.arange(0,L,L/500);
p = A*np.exp(1j*k*xT) + B*np.exp(-1j*k*xT);
u = 1/(rho*c)*(A*np.exp(1j*k*xT) - B*np.exp(-1j*k*xT));

# Figure
plt.figure(2)
#plt.plot(xT,p.real,'r',label='Re(p)')xT,p.imag,'r--',label='Im(p)',xT,u.real,'b',label='Re(q)',xT,u.imag,'b--',label='Im(q)');
plt.plot(xT,p.real,'r',label='Re(p)')
plt.plot(xT,p.imag,'r--',label='Im(p)')
plt.plot(xT,u.real,'b',label='Re(u)')
plt.plot(xT,u.imag,'b--',label='Im(u)');
plt.legend()
plt.show()


