#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 09:06:31 2024

@author: fabred
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 12:15:27 2024

@author: fabred
"""

import numpy as np
import numpy.linalg as la
from matplotlib import pyplot as plt
import math
import cmath # pour arithmetique complexe
plt.close("all")

global rho,c
rho = 1 
c = 1



# Fonction Impedance d'un tuyau cone+cylindre (par inversion d'un système linéaire) 
def Zin_ConeExp(k,Lcone,Lexp,R1,R2,alpha,Delta,beta):
   Z1 = rho*c/(np.pi*R1**2)
   Z2 = rho*c/(np.pi*R2**2)
   Rout = R2*np.exp(alpha*Lexp)
   Z3 = rho*c/(np.pi*Rout**2)
   alphaA = np.arctan((R2-R1)/Lcone) # angle du cone
   r1 = R1/np.tan(alphaA)
   r2 = R2/np.tan(alphaA)
   kplus  = 1j*alpha/2+cmath.sqrt(k**2-alpha**2/4)
   kmoins = 1j*alpha/2-cmath.sqrt(k**2-alpha**2/4)
   Qf = 1
   Zout = Z3*( -1j*Delta*k + beta*(k*Rout)**2 )
   M = np.matrix([ 
   [ (1-1/(1j*k*r1))*np.exp(1j*k*r1)/r1/Z1,(-1-1/(1j*k*r1))*np.exp(-1j*k*r1)/r1/Z1, 0, 0],
   [ -np.exp(1j*k*r2)/r2,-np.exp(-1j*k*r2)/r2,1,1 ],
   [ -(1-1/(1j*k*r2))*np.exp(1j*k*r2)/r2,-(-1-1/(1j*k*r2))*np.exp(-1j*k*r2)/r2, kplus/k, kmoins/k ],
   [ 0,0,(Z3-Zout)*np.exp(1j*kplus*Lexp) , (Z3+Zout)*np.exp(1j*kmoins*Lexp) ] 
   ]);
   Y = np.array([Qf, 0, 0, 0]);
   X = la.solve(M,Y)
   A = X[0]; B = X[1]; C = X[2]; D = X[3];
   Z = ((A*np.exp(1j*k*r1)+B*np.exp(-1j*k*r1))/r1)/Qf;
   return Z,A,B,C,D,kplus,kmoins


   


#%% Calcul de l'impedance pour un tableau de valeurs de omega
Lexp = .6 
Lcyl = .6   
R1 = .1
R2 = .2
alpha = 4 # taux d'expansion de la section (pas du rayon)
omegarange = np.arange(0.01,5,.01)
ktab = omegarange/c
Delta = 0.61*R2*np.exp(alpha*Lexp) # correction de longueur
beta = 0.25
Zin =np.zeros(len(ktab),dtype=complex)
Zint =np.zeros(len(ktab),dtype=complex)
for i in range(len(ktab)):
    k =ktab[i]
    Zin[i],A,B,C,D,kplus,kmoins = Zin_ConeExp(k,Lcyl,Lexp,R1,R2,alpha,Delta,beta)

# Figure
plt.figure(1)
Z0 = rho*c/(np.pi*R1**2)
plt.semilogy(omegarange,abs(Zin/Z0))
plt.xlabel(f'\omega') 
plt.ylabel('|Zin| / Z0')
#plt.ylim(1e-2, 1e2)
plt.xlim(0, 5)
plt.title("Impedance d'entrée d'un tube cone/exp")
#plt.legend({'Ideal open','End correction','End correction + radiation'},'location','SE','fontsize',16);
plt.show

# Ecriture d'un fichier csv pour comparaison avec FreeFem
data = np.zeros((len(omegarange),3))
data[:,0] = omegarange
data[:,1] = Zin.real
data[:,2] = Zin.imag
np.savetxt('Zin_CylExp_theo.csv', data, delimiter=",")


#%% Calcul de la structure spatiale pour une valeur de omega
Lexp = .6 
Lcone = .6   
R1 = .1
R2 = .2
alpha = 4
Delta = 0.61*R1*np.exp(alpha*Lexp) # correction de longueur
beta = 0.25
omega = 4.7
k = omega/c
ZZ,A,B,C,D,kplus,kmoins = Zin_ConeExp(k,Lcyl,Lexp,R1,R2,alpha,Delta,beta)

xtab = np.arange(-Lcyl,Lexp,.01)
Sin = np.pi*R1**2
Zin = rho*c/Sin

alphaA = np.arctan((R2-R1)/Lcone) 
r1 = R1/np.tan(alphaA)
rtabcone = r1+Lcone+xtab 
Stabcone = np.pi*(rtabcone*np.tan(alpha))**2

S = (xtab<0)*np.pi*(R1+(R2-R1)*(xtab+Lcone)/Lcone)**2+(xtab>=0)*np.pi*R2**2*np.exp(alpha*xtab)


p = (   (xtab<0) * ((A*np.exp(1j*k*rtabcone)+B*np.exp(-1j*k*rtabcone))/rtabcone )  
      + (xtab>=0)* (C*np.exp(1j*kplus*xtab) + D*np.exp(1j*kmoins*xtab) )
    )

q = (   (xtab<0) * ((A*np.exp(1j*k*rtabcone)*(1-1/(1j*k*rtabcone))-B*np.exp(-1j*k*rtabcone)*(1+1./(1j*k*rtabcone))))/rtabcone**2*S/rho/c
      + (xtab>=0)* (C*kplus/k*np.exp(1j*kplus*xtab) + D*kmoins/k*np.exp(1j*kmoins*xtab) )*S/rho/c
    )


plt.figure(4);plt.cla()
plt.plot(xtab,p.real,'r');plt.plot(xtab,p.imag,'r:');
plt.plot(xtab,q.real,'b');plt.plot(xtab,q.imag,'b:');
plt.title("Structure spatiale dans le tube cone/exp pour omega ="+str(omega))
plt.show()
 

