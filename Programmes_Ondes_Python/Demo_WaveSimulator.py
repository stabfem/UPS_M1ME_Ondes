#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  8 16:14:51 2023

@author: David Fabre, translated from MAtlab with help of Paul 
"""

import numpy as np
from matplotlib import pyplot as plt
import math
import Lib_WaveSimulator as LW
plt.close('all')


##%% Demo pour un tuyau ouvert, impulsion
R,T,absZin = LW.WaveSimulator(problemtype="pipe", 
                properties={"L":.6,"c0":0.34,"rho":1.225,"a":0.06},        
                BC_0 = "pulse",ta = .1,
                BC_L = "radiation",
                FinalTime=8,dt=0.005,Nx=201,
                fig1=50,
                fig2=0,
                fig3=0,
                fig3_plusminus=True,
                fig1_movie=False,
                fig4=0)
 
#%% Demo pour un tuyau ouvert, forcage en debit
R,T,absZin = LW.WaveSimulator(problemtype="pipe", 
                properties={"L":0.6+0.85*0.06,"c0":0.34,"rho":1.225,"a":0.06},        
                BC_0 = "v0", omega=4.1,
                BC_L = "radiation",
                FinalTime=10,
                fig1=0,
                fig2=20,
                fig3=0,
                Nx = 201,dt=0.005,
                fig3_mean=True,
                fig3_inst=True,
                fig3_plusminus=False,
                fig4=1
                )
 

#%%  Demo avec une boucle sur omega
omega_ = np.linspace(2,6,81);
R =np.zeros_like(omega_);
T =np.zeros_like(omega_);
Zin =np.zeros_like(omega_);
for j,omega in enumerate(omega_):
    Zin[j],R[j],T[j] =  LW.WaveSimulator(problemtype="pipe",
                    properties={"L":.6+.85*.06,"c0":0.34,"rho":1.225,"a":0.06},        
                    BC_0 = "v0",omega = omega,
                    BC_L = "radiation",beta=0.5,
                    FinalTime=50,
                    fig1=0,fig2=0,fig3=0)
    print(" omega = {omega}, Zin = {Zin[j]}")
 
    
# On trace la figure
plt.figure(40);
#plt.plot(omega_*1000/(2*np.pi),Zin)
plt.plot(omega_,Zin); plt.xlabel(f'\omega (rad/ms)'); plt.ylabel('|Z|')


# On ecrit dans un fichier CSV pour pouvoir utiliser le programme de comparaison
data = np.zeros((len(omega_),4))
data[:,0] = omega_
data[:,1] = Zin
data[:,2] = R
data[:,2] = T
np.savetxt('Zin_Cylinder_WS.csv', data, delimiter=",")


#%%  Demo pour une géométrie cone/cylindre
R1,T1,Zin1 =  LW.WaveSimulator(problemtype="pipeConCyl",
                properties={"c0":1,"rho":1,"Lcone":.2,"Lcyl":1,"Rin":0.1,"Rout":0.5},
                BC_0 = "v0",omega = 6,
                BC_L = "radiation",
                FinalTime=10,
                fig1=50,fig2=0,fig3=0)



   