#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 13:23:54 2024

Example de code pour caclculer le minimum de la vitesse de groupe

@author: fabred
"""
import numpy as np
import sympy as sp

k,g,gamma,rho = sp.symbols('k,g,gamma,rho')

omega = (k*(1+k**2))**(1/2)

print ('cg :')
cg = sp.diff(omega,k)
print(cg)

print ('dcg :')
dcg = sp.simplify(sp.diff(cg,k))
print(dcg)

kk = sp.solve(dcg,k)
print(kk)

kkk = np.sqrt(np.sqrt(4/3)-1)
print(kkk)

cgmin = cg.subs(k,kkk)
print(cgmin)