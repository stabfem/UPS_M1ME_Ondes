#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 14:49:42 2025

@author: fabred
"""
import numpy as np
from matplotlib import pyplot as plt
import math
from matplotlib.animation import ArtistAnimation


## Fonction principale : Wave Simulator

def WaveSimulator(
        # NB This function can be used with a large number of paramers. All have default values, as defined below.
                  # Physical parameters
                  problemtype="pipe", # Definition of problem, geometry, and properties of media
                  properties={"L":0.6,"c0":0.34,"rho":1.225,"a":0.06},
                                      # This is a dictionary defining the case (fields may vary depending on cases)
                  BC_0 = "pulse",     # Boundary condition at inlet
                  omega = 0,          # Frequency of forcing  (or inverse of time scale for impulsion)
                  ta = 0.1,           # Time scale for impulsion in case of "pulse"
                  Amp=1,AmpPulse=1,   # Some other parameters of the inlet condition
                  BC_L = "fixed",     # Boundary condition at outlet
                  beta = 0.25,        # parameter in radiation boundary condition     
                  Initial_Condition = 'zero', # If you want to specify an initial condition
                  InitGausD = 0.1,InitGausLoc = 0.5,InitGausAmp = 1,  #parameters for the initial condition  
                  FinalTime = 10,     # Duration of the simulation
                  # Numerical parameters 
                  dt = 0.005,         # Time step
                  Nx = 101,           # Number of grid points
                  fig1 = 1,           # to plot p(x,t) and q(x,t) in figure 1
                  fig2 = 0,           # to plot conditions at x=0 and x=L on figure 2
                  fig3 = 0,           # to plot Power at x=0 and x=L on figure 3
                  fig4 = 0,           # to plot Energy on figure 4
                  Tscalefig2 = 10,      # for fig 2 : will adjust x range to [t-Tscale,t]
                  fig1_movie=False,
                  fig3_mean=True,      # for fig 3 : averaged power
                  fig3_inst=True,     # for fig 3 : instantaneous power
                  fig3_plusminus=False, # for fig 3 : decompose +/- components
                 ):
    
    
    #problemtype = 'pipe'
    # Definition of the problem
    # Allowed values are 'string', 'string1disc', "string2disc',
    #                    'pipe','pipe1disc','pipe2disc'
    #                    'acousticuniform','acousticdisc'
    #                    'canal1disc','canalprog'
    #
    # see in function cZ in Lib_WaveSimulator.py
    # to change the parameters, and add your own cases !
    # Definition of the boundary conditions
    
    #BC_0 = 'pulse'
    # Allowed values include :
    #   'Y0'                        : Imposed sinusoidal displacement
    #   'V0'                        : Imposed sinusoidal velocity/flow rate
    #   'F0' | 'P0'                 : Imposed sinusoidal force/pressure
    #  'incident' | 'I0'           : Incident sinusoidal wave coming from the region x<0
    #   'pulse' | 'Y0_Pulse'        : Gaussian pulse imposed as displacement at the inlet.
    #   'F0_Pulse' | 'P0_Pulse'     : Gassian pulse imposed as pressure/force
    #   at the inlet++
    #   'Incident_Pulse'|'I0_Pulse' : Gaussian incident pulse coming from the region x<0
    #   'transparent' | 'noreflect' : Non-reflexion condition
    

    
    #BC_L = 'free'
    # This is the boundary condition at outlet (x=L).
    # Allowed values include :
    #   'Fixed'  | 'closed'         : fixed string (or closed pipe)
    #   'Free'   | 'open'           : free string (or ideally open pipe)
    #   'transparent' | 'noreflect' : Radiation condition
    #   'impedance'                 : We specity an outlet impedance.
    #      in this case one have to specify the impedance. For instance 
    #      Zout = 10
    #   'incident_pulse'|'I0_Pulse' : Gaussian incident pulse coming from the region x>L
    #   'radiation'                 : Radiation condition in a 3D domain (full)
    #   'radiation_flanged'         : Radiation condition in a 3D domain (flanged)
    
    
# We put the parameters related to boundart conditions in a dictionary   
    BC_param = {};
    BC_param["omega"] = omega
    BC_param["Amp"] = Amp
    BC_param["AmpPulse"] = AmpPulse
    BC_param["ta"] = ta
    BC_param["t1"] = 5*ta
    BC_param["beta"] = beta
    BC_param["Zout"] = 0 #??
    
    
# We put the parameters affecting the post-processing in a dictionary
    NP = {};
    NP["fig1"]=fig1 #Y_current, V_current et F_current par rapport a x
    NP["fig2"]=fig2  #Power et Power_Av
    NP["fig3"]=fig3  #V0,Y0,F0 et VL, YL, FL par rapport a t
    NP["fig4"]=fig4  #Total Energy
#    NP['plotstep'] = 100  # number of time steps between two plots
#    NP['wait'] = 0  # set to 1 to advance step-by-step ; to zero to advance continuously
    NP['Tscalefig2'] = Tscalefig2
    BC_param['smoothingtime'] = 100 * dt  # to soften the starting (useful with Y0)
    if not omega==0:
       NP['N_averaging'] = round(2 * np.pi / BC_param["omega"] / dt)  # number of previous instants to compute time-averages
    else:
       NP['N_averaging'] = 0
    NP['tpause'] = 0.001  # pause after each plot to let time for figure refreshing
    NP['omega'] = omega 
    NP['fig1_movie']=fig1_movie
    NP['fig3_mean']=fig3_mean
    NP['fig3_inst']=fig3_inst
    NP['fig3_plusminus']=fig3_plusminus
    
    ##############################################################
    #
    # END OF PARAMETERS ; THE REMAINDER SHOULD NOT BE MODIFIED
    #
    ##############################################################
    
    # Build the mesh and the properties of the medium
    # First build a full mesh whith twice the number of points
    xF,cF,ZF,SF,rho,L = cZ(problemtype, properties,2 * Nx - 1)
    # We deduce the mesh at principal points (x_i, i=1 to Nx)
    x = xF[::2]
    c = cF[::2]
    Z = ZF[::2]
    S = SF[::2]
    
    # And the staggered mesh (intermediate points x_{i+1/2} plus extremities)
    xstag = xF[[0] + list(range(1, 2 * Nx - 1, 2)) + [-1]]
    cstag = cF[[0] + list(range(1, 2 * Nx - 1, 2)) + [-1]]
    zstag = ZF[[0] + list(range(1, 2 * Nx - 1, 2)) + [-1]]
    # a few things
    dx = x[1] - x[0]   
    NP["dx"]=dx
    BC_param["Rout"] = np.sqrt(S[Nx-1]/np.pi)
    if "pulse" in BC_0 and not BC_param["ta"]==0:
        BC_param["omegarad"] = 5/BC_param["ta"] # pifometric
    else:
        BC_param["omegarad"] = omega
    #Initial Condition
    Params_CI={'InitGausD':InitGausD,'InitGausLoc':InitGausLoc,'InitGausAmp':InitGausAmp}
    Y_current,V_current=InitialCond(Initial_Condition,x,Params_CI)    
    Y_past = Y_current-dt*V_current;
    Y_future = Y_current+dt*V_current;
    
    
    ##Defines a few object needed in the time loop
    r = c*dt/dx;
    rZ = (c/Z)*(dt/dx);
    rZ[Nx-1]=rZ[Nx-2]
    rstag = cstag*zstag*dt/dx;
    BC_param["r0"] = c[0]*dt/dx;
    BC_param["rL"] = c[Nx-1]*dt/dx;
    nsteps = int(FinalTime/dt)
    BC_param["dt"] = dt
    BC_param["dx"] = dx
    
    print(' ');
    print('%%Program Wavesimulator.m ready to run');
    print(' ');
    print(['Selected case                : ',problemtype]);
    print(['Inlet boundary condition     : ',BC_0]);
    print(['Outlet boundary condition    : ',BC_L]);
    print(['omega                        : ',omega]);
    print(' ');
    print(['numerical stability criterion : max(r) = ' ,max(abs(r))])
    if(max(abs(r))>1):
        print('WARNING : max(r)  > 1 ! reduce dt to avoid intability');
    else:
        if(max(abs(r))<0.5):
            print('max(r) = '+str(max(r))+' < 1 ; stability criterion is largely satisfied ; you may increase the time step dt to speed up the code ');
   
    
    # Definition of the derived function "Force"
    F_current = np.zeros_like(x)
    F_current[1:Nx-2] = -cstag[1:Nx-2]*zstag[1:Nx-2]*np.diff(Y_current[0:Nx-2])/dx
    F_current[0] = c[0]*Z[0]*(3*Y_current[0]-4*Y_current[1]+Y_current[2])/(2*dx)
    F_current[Nx-1] = -c[Nx-1]*Z[Nx-1]*(3*Y_current[Nx-1]-4*Y_current[Nx-2]+Y_current[Nx-3])/(2*dx)
    N_tab = int(FinalTime/dt+1)
    
    ## Initialisation du post-processing
    PostProcessing = Initialise_PostProcessing(FinalTime,dt,NP,x,c,Z,S)
    
    ## Time loop
    t = 0
    while t<FinalTime+dt:
        nt = int(t/dt)
        # Avancée en temps avec schema differences finies pour la variable primitive      
        # Point interieurs : equation des ondes
        #Y_future[1:Nx-1] = <PREVIOUS FORMULA WAS WRONG ! WORKS ONLY FOR A UNIFORM MEDIUM>  2*Y_current[1:Nx-1] - Y_past[1:Nx-1] + r[1:Nx-1]**2*(Y_current[2:Nx] - 2*Y_current[1:Nx-1] + Y_current[:Nx-2])
        for j in range(1,Nx-1):
            Y_future[j] = ( rZ[j]*(rstag[j]*Y_current[j-1]+rstag[j+1]*Y_current[j+1]-(rstag[j]+rstag[j+1])*Y_current[j])  
                        + 2*Y_current[j]-Y_past[j] );
        # Condition limite x=0 et x= L
        Y_future[0] = CL_0(BC_0,t,BC_param,Y_current,Y_past,Z,c)
        Y_future[Nx-1] = CL_L(BC_L,t,BC_param,Y_current,Y_past,Z,c)
    
        # On en déduit les variables dérivées
        F_current[1:Nx-1] = -cstag[1:Nx-1]*zstag[1:Nx-1]*np.diff(Y_current[0:Nx-1])/dx
        F_current[0] = c[0]*Z[0]*(3*Y_current[0]-4*Y_current[1]+Y_current[2])/(2*dx)
        F_current[Nx-1] = -c[Nx-1]*Z[Nx-1]*(3*Y_current[Nx-1]-4*Y_current[Nx-2]+Y_current[Nx-3])/(2*dx)
        V_current[:] = (Y_future[:]-Y_past[:])/(2*dt);
    
        # Post-processing
        absZin,R,T = PostProcessing(t,nt,Y_current,V_current,F_current,x,Z,c,zstag,cstag)
         
        # Set for next time step 
        Y_past[:] = Y_current
        Y_current[:] = Y_future
        
        t=t+dt;
        
    # Return values    
    return absZin,R,T
   


## Fonction pour définir les propriétés du milieu 1D

def cZ(problemtype, properties,N):
# ... implementation of the cZ function for creating the mesh and properties of the medium
#
# This function sets the physical properties of the 1D medium :
# x     mesh
# c     wave velocity (array with same dimension as x)
# Z     impedance of medium (array with same dimension as x)
# S     surface of the pipe/canal (not relevant for a string)
# rho   density of acoustic medium (not relevant for a string)
    
    
        
    if problemtype=='pipe':
        #cylindrical pipe, dimensional (a flute, L=60cm,r=1cm)
        L = properties["L"];        # Length  ; should be defined in all cases
        c0 = properties["c0"];      # wave velocity in [m/ms]
        rho = properties["rho"];    # density [kg/m^3]
        a = properties["a"];        # radius  [m]
        S0 = np.pi*a*a; # section [m^2]
        dx=L/(N)
        x =np.arange( 0,L,dx)
        c = c0*np.ones(shape=N);
        S = S0*np.ones(shape=N);
        Z = rho*c/S;        
        
    elif problemtype=='pipe1disc':
        # 2 connected cylindrical pipes (here nondimensional)
        L = properties["L"];        # Length  ; should be defined in all cases
        X1 = properties["X1"]; # location of discontinuity
        c0 = properties["c0"];      # wave velocity in [m/ms]
        rho = properties["rho"];    # density [kg/m^3]
        S1 = properties["S1"];        # radius  [m]
        S2 = properties["S2"];        # radius  [m]
        dx=L/N
        x =np.arange( 0,L,dx)
        S = np.zeros(shape=N);
        for i in range(N):
             S[i] = (x[i]<X1)*S1+(x[i]>=X1)*S2;
        c = c0*np.ones(shape=N);
        Z = rho*c0/S;

    elif problemtype=='pipe2disc':
        # 3 connected cylindrical pipes (here nondimensional)
        L = properties["L"];        # Length  ; should be defined in all cases
        dx=L/N
        x =np.arange( 0,L,dx)
        X1 = properties["X1"]; # location of discontinuity
        X2 = properties["X2"]; # location of discontinuity
        c0 = properties["c0"];      # wave velocity in [m/ms]
        rho = properties["rho"];    # density [kg/m^3]
        S1 = properties["S1"];        # radius  [m]
        S2 = properties["S2"];        # radius  [m]
        S3 = properties["S3"];        # radius  [m]
        S = np.zeros(shape=N)
        for i in range(N):
            S[i] =(x[i]<X1)*S1+(x[i]>=X1 and x[i]<X2)*S2+(x[i]>=X2)*S3;
        c = c0*np.ones(shape=N);
        Z = rho*c0/S;
    
    elif problemtype=='pipeConCyl':
        c0 = properties["c0"];      # wave velocity in [m/ms]
        rho = properties["rho"];    # density [kg/m^3]
        Lcone = properties["Lcone"];     
        Lcyl = properties["Lcyl"];
        Rin = properties["Rin"];
        Rout = properties["Rout"];
        L = Lcone+Lcyl+.61*Rout;    # Length including correction length
        dx = L/N
        x = np.arange( 0,L,dx);
        RR = (x<Lcone)*(Rin+(Rout-Rin)/Lcone*x)+(x>=Lcone)*Rout;
        c = c0*np.ones(shape=N);
        S = np.pi*RR**2;
        Z = rho*c/S;
    
    elif problemtype=='string':
        # case of a string with constant velocity
        T = properties["T"];
        mu = properties["mu"];
        L = properties["L"];
        dx=L/(N)
        x =np.arange( 0,L,dx)
        c = np.sqrt(T/mu)*np.ones(shape=N);
        Z = T/c;
        rho=1;
        S=np.ones(shape=N);

    elif problemtype=='string1disc':
            # case TO BE CHECKED
            T = 1;
            c1 = 1;
            c2 = .2;
            X1 = .5;
            L = properties["L"];        # Length  ; should be defined in all cases
            dx=L/(N)
            x =np.arange( 0,L,dx)
            c = c0*np.ones(shape=N);
            for i in range(N):
                c = c1*(x[i]<X1)+c2*(x[i]>=X1);
            Z = T/c;

    elif problemtype=='string2disc':
            # case of a string with a discontinuity; TO BE CHECKED
            T = 1;
            c1 = 1;
            c2 = 4;
            c3 = 1;
            X1 = .5;
            X2 = .75;
            L = properties["L"];        # Length  ; should be defined in all cases
            dx=L/N
            x =np.arange( 0,L,dx)
            dx=L/(N)
            x =np.arange( 0,L,dx)
            c = c0*np.ones(shape=N);
            for i in range(N):
                c = c1*(x[i]<X1)+c2*(x[i]>=X1 and x[i]<X2)+c3*(x[i]>=X2);
            Z = T/c;
            
    elif problemtype=='acousticuniform':
            #acoustic medium with uniform properties TO BE CHECKED
            c0 = 1;
            S = 1;
            rho0 = 1;
            L = properties["L"];        # Length  ; should be defined in all cases
            dx=L/N
            x =np.arange( 0,L,dx)
            #x = linspace(0,L,Nx);
            rho = rho0*np.ones(shape=N);
            c = c0*np.ones(shape=N);
            Z = rho*c/S0;

    elif problemtype=='acousticdisc':
            # acoustic medium with uniform properties TO BE CHECKED
            L = properties["L"];        # Length  ; should be defined in all cases
            dx=L/N
            x =np.arange( 0,L,dx)
            S = 1;
            rho1 = 1.225;#air
            c1 = 0.34;#air
            X1 = 0.75;
            rho2 = 10;#water
            c2 = 1.5;#water
            c = c1*np.ones(shape=N);
            rho = rho1*np.ones(shape=N);
            
            for i in range(N):
                rho[i] = rho1*(x[i]<X1)+rho2*(x[i]>=X1);
                c[i] = c1*(x[i]<X1)+c2*(x[i]>=X1);
            Z = rho*c/S;
            Rout=np.nan


    elif problemtype=='canal1disc':
            # 2 connected canals TO BE CHECKED
            # NB for canals the length/time/mass unities are kilometer, minute
            # and petagram (1 Petagram = 10^15 g = 10^12 kg)
            L = properties["L"];        # Length  ; should be defined in all cases # 1km
            dx=L/N
            x =np.arange( 0,L,dx)
            X1 = .5; # 500 meters
            X2 = .5; # 700 meters
            g = 9.81*60*60/1000; # 9.81 m^2/s converted in km^2/min
            rho = 1; # rho in Pg/km^3
            W = 1;
            H1 = 0.2; # 200m
            H2 = 0.01; # 10m
            H=np.zeros(shape=N)
            for i in range(N):
                H[i] = (x[i]<X1)*H1+(x[i]>=X1)*H2;
            c =np.sqrt(g*H);
            c=c*np.ones(shape=N)
            Z = 1/(rho*W*np.sqrt(g*g*g*H));
            S = W*H;
            Rout=np.nan

    elif problemtype=='pipeConeExp':
        c0 = properties["c0"];      # wave velocity in [m/ms]
        rho = properties["rho"];    # density [kg/m^3]
        Lexp = properties["Lexp"];     
        Lcone = properties["Lcone"];
        R1 = properties["R1"];
        R2 = properties["R2"];
        alpha = properties["alpha"];
        L = Lcone+Lexp;    # Length 
        dx = L/N
        x = np.arange( 0,L,dx);
        RR = (x<Lcone)*(R1+(R2-R1)*x/Lcone)+(x>=Lcone)*R2*np.exp(alpha/2*(x-Lcone));
        c = c0*np.ones(shape=N);
        S = np.pi*RR**2;
        Z = rho*c/S;    

    elif problemtype=='canalprog':
            # canal with progressive variation of height TO BE CHECKED
            # NB for canals the length/time/mass unities are kilometer, minute
            # and petagram (1 Petagram = 10^15 g = 10^12 kg)
            X1 = .3; # 300 meters
            X2 = .7; # 700 meters
            g = 9.81*60*60/1000; # 9.81 m^2/s converted in km^2/min
            rho = 1; # rho in Pg/km^3
            W = 1;
            H1 = 0.05; # 50m
            H2 = 0.01; # 10m
            L = 1;
            dx=L/N
            x =np.arange( 0,L,dx)
            H=np.zeros(shape=N)
            Z=np.zeros(shape=N)
            for i in range(N):
            #x = linspace(0,L,Nx);
                H [i]= (x[i]<X1)*H1+(x[i]>=X1 and x[i]<X2)*(H1+(H2-H1)*(x[i]-X1)/(X2-X1))+(x[i]>=X2)*H2;
                Z[i] = 1/(rho*W*np.sqrt(g*g*g*H[i]));
            c = np.sqrt(g*H);
            c=c*np.ones(shape=N)
            S = W*H;
            Rout=np.nan
    else:
        raise RuntimeError('Error : problemtype not recognized')
    
    return x,c,Z,S,rho,L



## Function for initial condition
def InitialCond(Initial_Condition,x,Params_CI):
    InitGausAmp = Params_CI["InitGausAmp"]
    InitGausLoc = Params_CI["InitGausLoc"]
    InitGausD = Params_CI["InitGausLoc"]
    if Initial_Condition =='zero':
        Y_current = 0*x; # here you can also use posinit(x) as in TP1 !
        V_current = 0*x;
    elif Initial_Condition=='gaussiany':
            Y_current = InitGausAmp*np.exp(-(x-InitGausLoc)*(x-InitGausLoc)/InitGausD*InitGausD);
            V_current = 0*x;        
    elif Initial_Condition=='gaussian':
        Y_current=np.zeros(shape=len(x))
        for i in range(len(x)):
            Y_current[i] = -InitGausAmp*math.erf((x[i]-InitGausLoc)/InitGausD);
        V_current = 0*x;
    else :
        print('Initial condition type not recognized !')
    return Y_current,V_current



## Functions for boundary conditions
def CL_0(BC_0,t,BC_Param,Y_current,Y_past,Z,c):
     omega = BC_Param["omega"];
     AmpPulse = BC_Param["AmpPulse"];
     Amp = BC_Param["Amp"];
     dt = BC_Param["dt"];
     t1 = BC_Param["t1"];
     ta = BC_Param["ta"];
     r0 = BC_Param["r0"]; 
     dx = BC_Param["dx"];
     smoothingtime = BC_Param["smoothingtime"]
     BC_0 = BC_0.lower()
     if "pulse" in BC_0:
         if ta==0:
             raise RuntimeError(f"For a pulse you must provide parameter ta (duration of pulse)")
     if BC_0 in {'y0','p0','f0','i0','incident'}:
          if omega==0:
              raise RuntimeError(f"For a harmonic condition at inlet must provide a nonzero parameter omega (angular frequency)")
                     
     if BC_0=='pulse':
          Y0 = AmpPulse*np.exp(-((t+dt)-t1)*((t+dt)-t1)/(ta*ta))
     elif BC_0=='fixed' or BC_0=='closed':
          Y0 = 0;
     elif BC_0=='free' or BC_0=='open':
          Y0 = 2*r0*r0*Y_current[1]+2*(1-r0*r0)*Y_current[0]-Y_past[0] ;
     elif BC_0=='f0'or BC_0=='p0':
          Y0 =2*r0**2*Y_current[1]+2*(1-r0**2)*Y_current[0]-Y_past[0]+2*r0**2*dx*np.sin(omega*t)/(Z[0]*c[0]);
     elif BC_0== 'y0':
          Y0=Amp*np.sin(omega*(t+dt))*np.tanh((t+dt)/smoothingtime); # the smoothing period is to make the start less abrupt
     elif BC_0=='v0':
          Y0=Amp*(np.cos(omega*(t+dt))-1)*np.tanh((t+dt)/smoothingtime)/omega; 
     elif BC_0== 'i0' or BC_0=='incident':
          Y0 = (2*Y_current[0]+(r0-1)*Y_past[0]+2*r0*r0*(Y_current[1]-Y_current[0]))/(1+r0)-Amp/Z[0]*np.sin(omega*t)*dt*4*r0/(1+r0);
     elif BC_0=='v0_pulse':
          Y0 = AmpPulse*np.sqrt(np.pi)/2*(math.erf((t+dt-t1)/ta)+1)*ta;
     elif BC_0=='f0_pulse' or BC_0=='p0_pulse':
          Y0= 2*r0*r0*Y_current[1]+2*(1-r0*r0)*Y_current[0]-Y_past[0]+2*r0*r0*dx*AmpPulse*np.exp(-(t-t1)*(t-t1)/ta*ta)/(Z[0]*c[0]);
     elif BC_0=='i0_pulse'or BC_0=='incident_pulse':
          Y0 = (2*Y_current[0]+(r0-1)*Y_past[0]+2*r0*r0*(Y_current[1]-Y_current[0]))/(1+r0)+AmpPulse/(Z[0]*c[0])*np.exp(-(t-t1)*(t-t1)/ta*ta)*dt*4*c[0]*r0/(1+r0);
     elif BC_0=='transparent' or BC_0=='noreflect':
          Y0 = (2*Y_current[0]+(r0-1)*Y_past[0])+2*r0*r0*(Y_current[1]-Y_current[0])/(1+r0);
     else:
        print('WARNING : left boundary condition not implemented !');    
        Y0=0
     return Y0
 
     
def CL_L(BC_L,t,BC_Param,Y_current,Y_past,Z,c):
     dt = BC_Param["dt"];
     omega = BC_Param["omega"];
     AmpPulse = BC_Param["AmpPulse"];
     Amp = BC_Param["Amp"];
     rL = BC_Param["rL"];
     Zout = BC_Param["Zout"];
     Rout = BC_Param["Rout"];
     beta = BC_Param["beta"];
     omegarad = BC_Param["omegarad"];
     Nx = Y_current.size;
     if BC_L=='fixed' or BC_L=='closed':
          YL = 0;
         #print(YL)
         #print('fixedd')
     elif BC_L=='free'or BC_L=='open':
          YL = 2*rL*rL*Y_current[Nx-2]+2*(1-rL*rL)*Y_current[Nx-1]-Y_past[Nx-1] ;
     elif BC_L=='transparent' or BC_L=='noreflect':
          YL = (2*Y_current[Nx-1]+(rL-1)*Y_past[Nx-1]+2*rL*rL*(Y_current[Nx-2]-Y_current[Nx-1]))/(1+rL);
     elif BC_L=='impedance':
          #rL = c[Nx-1]*dt/dx;
          rZZ = rL/Zout*Z[Nx-1];
          YL = (2*rZZ*Y_current[Nx-1]+(rL*rL-rZZ)*Y_past[Nx-1]+2*rL*rL*rZZ*(Y_current[Nx-1]-Y_current[Nx-1]))/(rZZ+rL*rL);
     elif BC_L=='i0_pulse'or BC_L=='incident_pulse':
          YL = (2*Y_current[Nx-1]+(rL-1)*Y_past[Nx-1]+2*rL*rL*(Y_current[Nx-2]-Y_current[Nx-1]))/(1+rL)-AmpPulse/(Z[Nx-1]*c[Nx-1]*np.exp(-(t-t1)*(t-t1)/ta*ta)*dt*4*c[Nx-1]*rL/(1+rL));
     elif BC_L=='i0':
          YL = (2*Y_current[Nx-1]+(rL-1)*Y_past[Nx-1]+2*rL*rL*(Y_current[Nx-2]-Y_current[Nx-1]))/(1+rL)-Amp/Z(Nx)*np.sin(omega*t)*dt*4*rL/(1+rL);
     elif BC_L=='radiation':
          Zout = beta*Z[Nx-1]*(omegarad*Rout/c[Nx-1])**2;
          rZZ = rL/Zout*Z[Nx-1];
          YL = (2*rZZ*Y_current[Nx-1]+(rL*rL-rZZ)*Y_past[Nx-1]+2*rL*rL*rZZ*(Y_current[Nx-2]-Y_current[Nx-1]))/(rZZ+rL*rL);
     else :
         Yl = 0 
         print('WARNING : right boundary condition not implemented !');
     return YL
 
    
 
## Functions for postpocessing
def Initialise_PostProcessing(FinalTime,dt,NP,x,c,Z,S):
    # These are the "persistent parameters" which
    # are initialised when creating the PostProcessing function
    N_tab = round(FinalTime/dt+1)+1;
    Y0 = np.nan*np.ones(shape=N_tab)
    V0= np.nan*np.ones(shape=N_tab)
    F0= np.nan*np.ones(shape=N_tab)
    YL = np.nan*np.ones(shape=N_tab)
    VL= np.nan*np.ones(shape=N_tab)
    FL= np.nan*np.ones(shape=N_tab)
    Power_0= np.nan*np.ones(shape=N_tab)
    Power_0_plus=np.nan*np.ones(shape=N_tab)
    Power_L=np.nan*np.ones(shape=N_tab)
    Power_L_plus=np.nan*np.ones(shape=N_tab)
    TotalEnergy=np.nan*np.ones(shape=N_tab)
    TotalEnergy_Av=np.nan*np.ones(shape=N_tab)
    tab_Power_0_plus_Av=np.nan*np.ones(shape=N_tab)
    tab_Power_0_Av=np.nan*np.ones(shape=N_tab)
    tab_Power_L_Av=np.nan*np.ones(shape=N_tab)
    tab_Power_L_plus_Av=np.nan*np.ones(shape=N_tab)
    tableau = np.zeros(shape=N_tab)
    ScaleY = 0; ScaleV=0; ScaleF=0;
    Ntfinal = int(FinalTime/dt)
    images1 = [] 
    # initialise les figures
    if NP["fig1"]>0: 
       plt.figure(1)
       fig1,(fig1ax1,fig1ax2,fig1ax3) = plt.subplots(3, 1,num=1)
       if NP["fig1_movie"]:
           images1 = [] 
    if NP["fig2"]>0:
       plt.figure(2)
       #images2 = [] 
    if NP["fig3"]>0: 
       plt.figure(3);
       #images3 = [] 
    if NP["fig4"]>0:
       plt.figure(4)
       plt.subplot(3,1,1);plt.cla();plt.title('Physical properties of propagation domain')
       plt.plot(x,c,'r');plt.ylim([0, max(c)*1.2]);plt.ylabel('c');
       plt.subplot(3,1,2);plt.cla()
       plt.plot(x,Z,'b');plt.ylim([0, max(Z)*1.2]);plt.ylabel('Z0');
       plt.subplot(3,1,3);plt.cla()
       plt.plot(x,S,'k');plt.ylim([0, max(S)*1.2]);plt.ylabel('S');plt.xlabel('x')
      
    def PostProcessing(t,nt,Y_current,V_current,F_current,x,Z,c,zstag,cstag):
        nonlocal NP,Y0,V0,F0,YL,VL,YL,Power_0,Power_L,Power_L_plus,Ntfinal
        nonlocal tab_Power_0_Av,TotalEnergy,TotalEnergy_Av
        nonlocal tab_Power_0_plus_Av,tableau,tab_Power_0_plus_Av
        nonlocal tab_Power_L_Av,tab_Power_L_plus_Av,ScaleY,ScaleF,ScaleV
        # These parameters are "nonlocal" because defined and initialised in the outer function
        Nx = Y_current.size
        N_averaging = NP["N_averaging"]
        dx = NP["dx"]
        tableau[nt] =t
        absZin,coefR,coefT=0,0,0
        Y0[nt]=Y_current[0]
        V0[nt]=V_current[0]
        F0[nt]=F_current[0]
        YL[nt]=Y_current[Nx-1]
        VL[nt]=V_current[Nx-1]
        FL[nt]=F_current[Nx-1]
        ScaleY=max(ScaleY,max(abs(Y_current)))
        ScaleF=max(ScaleF,max(abs(F_current)))
        ScaleV=max(ScaleV,max(abs(V_current)))
    
        if NP["fig1"]>0 and (np.mod(nt,NP["fig1"])==0 or nt==Ntfinal):
           if not plt.fignum_exists(1):
               raise RuntimeError("Program stopped because user closed a figure")
           fig1ax1.cla();fig1ax1.set_axis_off();fig1ax1.set_axis_on()
           fig1ax1.set_title(f"Pressure p, acoustic rate q (and primitive variable X) for t = {t:.3f}")
           obj1,=fig1ax1.plot(x,F_current,'r');fig1ax1.set_ylabel('p(x,t)');fig1ax1.set_ylim([-ScaleF,ScaleF])
           fig1ax2.cla();fig1ax2.set_axis_off();fig1ax2.set_axis_on()
           obj2,=fig1ax2.plot(x,V_current,'b');fig1ax2.set_ylabel('q(x,t)');fig1ax2.set_ylim([-ScaleV,ScaleV])
           fig1ax3.cla();fig1ax3.set_axis_off();fig1ax3.set_axis_on()
           obj3,=fig1ax3.plot(x,Y_current,'g');fig1ax3.set_ylabel('X(x,t)');fig1ax3.set_ylim([-ScaleY,ScaleY])
           plt.pause(0.01)
           if NP["fig1_movie"]:
               images1.append([obj1,obj2,obj3])
           plt.figure(1)
           plt.show()
        
        V0plus = (Z[0]*V_current[0]+F_current[0])/(2*Z[0]);
        V0moins = (Z[0]*V_current[0]-F_current[0])/(2*Z[0]);
        VLplus = (Z[Nx-1]*V_current[Nx-1]+F_current[Nx-1])/(2*Z[Nx-1]);
        VLmoins = (Z[Nx-1]*V_current[Nx-1]-F_current[Nx-1])/(2*Z[Nx-1]);
        Power_0[nt] = F_current[0]*V_current[0];
        Power_0_plus[nt] = V0plus*V0plus*Z[0];
        Power_L[nt] = V_current[Nx-1]*F_current[Nx-1];
        Power_L_plus[nt] = VLplus*VLplus*Z[Nx-1];
        Power_0_Av = np.mean(Power_0[max(1, nt-N_averaging):nt])
        tab_Power_0_Av[nt] = Power_0_Av
        Power_0_plus_Av = np.mean(Power_0_plus[max(1, nt-N_averaging):nt])
        tab_Power_0_plus_Av[nt] = Power_0_plus_Av
        Power_L_Av = np.mean(Power_L[max(1, nt-N_averaging):nt])
        tab_Power_L_Av[nt] = Power_L_Av
        Power_L_plus_Av = np.mean(Power_L_plus[max(1, nt-N_averaging):nt])
        tab_Power_L_plus_Av[nt] = Power_L_plus_Av
       
        TotalEnergyt = dx/2 * (0.5 * (V_current[0]**2 * Z[0]/c[0]) + np.sum(V_current[1:Nx-1]**2 * Z[1:Nx-1] / c[1:Nx-1]) +0.5 * (V_current[Nx-1]**2 * Z[Nx-1] / c[Nx-1]) + np.sum(F_current[1:Nx-1]**2 / (zstag[1:Nx-1] * cstag[1:Nx-1])))
        TotalEnergy[nt] = TotalEnergyt
        TotalEnergy_Avt = np.mean(TotalEnergy[max(1, nt - N_averaging):nt])
        TotalEnergy_Av[nt] = TotalEnergy_Avt
  
        if NP["fig2"]>0 and (np.mod(nt,NP["fig2"])==0 or nt==Ntfinal):
           if not plt.fignum_exists(2):
               raise RuntimeError(f"Program stopped because user closed a figure")
           maxtfig2 = max(abs(t),NP["Tscalefig2"]);
           mintfig2 = maxtfig2-NP["Tscalefig2"];
           plt.figure(2)
           plt.subplot(2,2,3); plt.cla()
#           plt.plot(tableau,Y0,'r-')
           plt.plot(tableau,V0,'b-',label="q(0,t)")
           plt.xlim([mintfig2,maxtfig2]);plt.legend();plt.xlabel('x');plt.ylim([-ScaleV,ScaleV])
           plt.subplot(2,2,1); plt.cla()
           plt.plot(tableau,F0,'r-',label="p(0,t)")
           plt.xlim([mintfig2,maxtfig2]);plt.legend();plt.xlabel('x');plt.ylim([-ScaleF,ScaleF])
           plt.subplot(2,2,4); plt.cla()
#           plt.plot(tableau,YL,'r-')
           plt.plot(tableau,VL,'b-',label="q(L,t)")
           plt.xlim([mintfig2,maxtfig2]);plt.legend();plt.xlabel('x');plt.ylim([-ScaleV,ScaleV])
           plt.subplot(2,2,2); plt.cla()
           plt.plot(tableau,FL,'r-',label="p(L,t)")
           plt.xlim([mintfig2,maxtfig2]);plt.legend();plt.xlabel('x');plt.ylim([-ScaleF,ScaleF])
           plt.pause(0.0001)
           plt.show()
             
        if NP["fig3"]>0 and (np.mod(nt,NP["fig3"])==0 or nt==Ntfinal):
           if not plt.fignum_exists(3):
                raise RuntimeError(f"Program stopped because user closed a figure")
           plt.figure(3); plt.cla()
           if NP["fig3_inst"]:
               plt.plot(tableau,Power_0,'k:',label=r'${\cal P}(0,t)$')
               plt.plot(tableau,Power_L,'r:',label=r'${\cal P}(L,t)$')
               if NP["fig3_plusminus"]:
                   plt.plot(tableau,Power_0_plus,'c:',label=r'${\cal P}^+(0,t)$')
                   plt.plot(tableau,Power_0-Power_0_plus,'g:',label=r'${\cal P}^-(L,t)$')
                   plt.plot(tableau,Power_L_plus,'b:',label=r'${\cal P}^+(0,t)$')
                   plt.plot(tableau,Power_L-Power_L_plus,'m:',label=r'${\cal P}^-(0,t)$')
           if NP["fig3_mean"]:    
               plt.plot(tableau,tab_Power_0_Av,'k',label=r'$\bar{\cal P}(0,t)$')
               plt.plot(tableau,tab_Power_L_Av,'r',label=r'$\bar{\cal P}(L,t)$')
               if NP["fig3_plusminus"]:
                   plt.plot(tableau,tab_Power_0_plus_Av,'c',label=r'$\bar{\cal P}^+(0,t)$')
                   plt.plot(tableau,tab_Power_0_Av-tab_Power_0_plus_Av,'g',label=r'$\bar{\cal P}^-(0,t)$')
                   plt.plot(tableau,tab_Power_L_plus_Av,'b',label=r'$\bar{\cal P}^+(L,t)$')
                   plt.plot(tableau,tab_Power_L_Av-tab_Power_L_plus_Av,'m',label=r'$\bar{\cal P}^-(L,t)$')
           plt.legend()
           plt.show()
           plt.pause(0.0001) 
                     
        #  Eventually : estimate the input impedance, reflection and transmission coefs  
        if nt==Ntfinal: 
           if not NP["omega"]==0: 
              Nt = int((2*np.pi/NP["omega"])/dt)
              start = max(nt-2*Nt,0);        
              absZin = max(F0[start:nt+1])/max(V0[start:nt+1])
              coefR = -(tab_Power_0_Av[nt]-tab_Power_0_plus_Av[nt])/tab_Power_0_plus_Av[nt]
              coefT = tab_Power_L_plus_Av[nt]/tab_Power_0_plus_Av[nt]
              print(' At the issue of the simulation we estimate R, T and |Z| :' )
              print('     [Z| = '+str(absZin))
              print('      R  = '+str(coefR))
              print('      T  = '+str(coefT))
              print(' ')
           else:
              absZin,coefR,coefT=0,0,0
           
  
        if nt==Ntfinal and NP["fig1"]>0 and NP["fig1_movie"]:
            # At the end: generate the movie file   
            ani = ArtistAnimation(fig1, images1, interval=50, blit=True, repeat=False)
            ani.save('WaveSimulator_Animation_Figure1.gif', writer='pillow', fps=30)
            ani.save('WaveSimulator_Animation_Figure1.mp4', writer='ffmpeg', fps=30)
        
        return absZin,coefR,coefT 
                              
    return PostProcessing


#    elif problemtype=='pipeCylExp':
#        c0 = properties["c0"];      # wave velocity in [m/ms]
#        rho = properties["rho"];    # density [kg/m^3]
#        Lexp = properties["Lexp"];     
#        Lcyl = properties["Lcyl"];
#        Rin = properties["Rin"];
#        alpha = properties["alpha"];
#        L = Lcyl+Lexp;    # Length 
#        dx = L/N
#        x = np.arange( 0,L,dx);
#        RR = (x<Lcyl)*Rin+(x>=Lcyl)*Rin*np.exp(alpha/2*(x-Lcyl));
#        c = c0*np.ones(shape=N);
#        S = np.pi*RR**2;
#        Z = rho*c/S;
