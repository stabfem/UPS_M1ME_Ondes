#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 09:06:31 2024

@author: fabred
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 12:15:27 2024

@author: fabred
"""

import numpy as np
import numpy.linalg as la
from matplotlib import pyplot as plt
import math
plt.close("all")

global rho,c,ii
rho = 1 
c = 1
ii = complex(0.,1.)


# Fonction Impedance d'un tuyau cone+cylindre (par inversion d'un système linéaire) 
def Zin_ConeCyl(k,Lcone,Lcyl,Rin,Rout,Delta,beta):
   alphaA = np.arctan((Rout-Rin)/Lcone) # angle
   r1 = Rin/np.tan(alphaA)
   r2 = Rout/np.tan(alphaA)
   Z1 = rho*c/(np.pi*Rin**2)
   Z2 = rho*c/(np.pi*Rout**2)
   Qf = 1
   Zout = Z2*( -ii*Delta*k + beta*(k*Rout)**2 )
   M = np.matrix([ 
   [ (1-1/(ii*k*r1))*np.exp(ii*k*r1)/r1/Z1,(-1-1/(ii*k*r1))*np.exp(-ii*k*r1)/r1/Z1, 0, 0],
   [ -np.exp(ii*k*r2)/r2,-np.exp(-ii*k*r2)/r2,1,1 ],
   [ -(1-1/(ii*k*r2))*np.exp(ii*k*r2)/r2,-(-1-1/(ii*k*r2))*np.exp(-ii*k*r2)/r2,1,-1  ],
   [ 0,0,(Z2-Zout)*np.exp(ii*k*Lcyl) , (Z2+Zout)*np.exp(-ii*k*Lcyl) ] 
   ]);
   Y = np.array([Qf, 0, 0, 0]);
   X = la.solve(M,Y)
   A = X[0]; B = X[1]; C = X[2]; D = X[3];
   Z = ((A*np.exp(ii*k*r1)+B*np.exp(-ii*k*r1))/r1)/Qf;
   return Z,A,B,C,D


# Calcul de l'impedance pour un tableau de valeurs de omega
Lcone = 2 
Lcyl =  1   
Rin = .2
Rout = .5
Z0 = rho*c/(np.pi*Rout**2)
omegarange = np.arange(0.01,5,.01)
ktab = omegarange/c
Delta = 0.61*Rout # correction de longueur
beta = 0.25
Zin =np.zeros(len(ktab),dtype=complex)
Zint =np.zeros(len(ktab),dtype=complex)
for i in range(len(ktab)):
    k =ktab[i]
    Zin[i],A,B,C,D = Zin_ConeCyl(k,Lcone,Lcyl,Rin,Rout,Delta,beta)

# Figure
plt.figure(1)
plt.semilogy(omegarange,abs(Zin/Z0))
plt.xlabel('\omega') 
plt.ylabel('|Zin| / Z0')
#plt.ylim(1e-2, 1e2)
plt.xlim(0, 5)
plt.title("Impedance d'entrée d'un tube cone/cyl")
#plt.legend({'Ideal open','End correction','End correction + radiation'},'location','SE','fontsize',16);
plt.show

# Ecriture d'un fichier csv pour comparaison avec FreeFem
data = np.zeros((len(omegarange),3))
data[:,0] = omegarange
data[:,1] = Zin.real
data[:,2] = Zin.imag
np.savetxt('Zin_ConeCyl_theo.csv', data, delimiter=",")

     
# Calcul de la structure spatiale pour une valeur de omega
omega = 2
k = omega/c
ZZ,A,B,C,D = Zin_ConeCyl(k,Lcone,Lcyl,Rin,Rout,Delta,beta)
xtabcone = np.arange(0,Lcone,.01)
alpha = np.arctan((Rout-Rin)/Lcone)
r1 = Rin/np.tan(alpha)
rtabcone = r1+xtabcone
Stabcone = np.pi*(rtabcone*np.tan(alpha))**2
xtabcyl = np.arange(Lcone,Lcone+Lcyl,.01)

pCo = (A*np.exp(1j*k*rtabcone)+B*np.exp(-1j*k*rtabcone))/rtabcone;
pCy = C*np.exp(1j*k*(xtabcyl-Lcone))+D*np.exp(-1j*k*(xtabcyl-Lcone));

qCo = (A*np.exp(1j*k*rtabcone)*(1-1/(1j*k*rtabcone))-B*np.exp(-1j*k*rtabcone)*(1+1./(1j*k*rtabcone)))/rtabcone;
qCy = (C*np.exp(1j*k*(xtabcyl-Lcone))-D*np.exp(-1j*k*(xtabcyl-Lcone)));
uCo = qCo/Stabcone
uCy = qCy/(np.pi*Rout**2)


plt.figure(4)
plt.plot(xtabcone,pCo.real,'r',xtabcyl,pCy.real,'r',xtabcone,pCo.imag,'r:',xtabcyl,pCy.imag,'r:');
plt.plot(xtabcone,uCo.real,'b',xtabcyl,uCy.real,'b',xtabcone,uCo.imag,'b:',xtabcyl,uCy.imag,'b:');
plt.plot(xtabcone,qCo.real,'g',xtabcyl,qCy.real,'g',xtabcone,qCo.imag,'g:',xtabcyl,qCy.imag,'g:');
plt.ylim(-50,50)
plt.title("Structure spatiale dans le tube cone/cyl pour omega ="+str(omega))
plt.show()

# Figure
#plt.figure(2)
#plt.plot(xT,p.real,'r',xT,p.imag,'r--',xT,u.real,'b',xT,u.imag,'b--');
#plt.legend("Re(p)","Im(p')","Re(u')","Im(u')");
#plt.show()


