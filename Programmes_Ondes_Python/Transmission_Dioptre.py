#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 12:15:27 2024

@author: fabred
"""

import numpy as np
import numpy.linalg as la
from matplotlib import pyplot as plt
import math
import csv
plt.close("all")

global rho,c
rho = 1.22 #kg/m^3 
c = 0.334 #(m/ms)

# Fonction Transmission/Réflexion a travers un simple dioptre
def RT_dioptre(k,A,R1,R2):
  Z1 = rho*c/(np.pi*R1**2)
  Z2 = rho*c/(np.pi*R2**2)
  M = np.matrix([ [-1, 1] , [1/Z1,1/Z2]])
  Qf = 1
  Y = np.array([A, A/Z1])
  X = la.solve(M,Y)
  B = X[0] ; C = X[1]
  return B,C

# Calcul du coef de transmission pour un tableau de valeurs de omega
R1 = 0.1
R2 = 0.2
A = 1
omegarange = np.arange(0,10,.01)
ktab = omegarange/c
R_ =np.zeros(len(ktab))
T_ =np.zeros(len(ktab))
for i in range(len(ktab)):
    k =ktab[i]
    B,C = RT_dioptre(k,A,R1,R2)
    R_[i] = abs(B/A)
    T_[i] = 1-R_[i]

# Figure
plt.figure(1)
plt.plot(omegarange,T_,'r',label='T')
plt.plot(omegarange,R_,'b',label='R')
plt.xlabel('omega') 
plt.ylim(0,1)
plt.title("R et T pour un dioptre simple")
plt.legend()
plt.show()

