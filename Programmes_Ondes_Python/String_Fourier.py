#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  3 09:22:21 2023

@author: Paul Flaumenbaum
(translated from a Matlab program from D. Fabre)

* Remarque importante :
  Pour utiliser ce programme de manière optimale il faut modifier le mode graphique de Python.
  Pour cela (avec Spyder) :
  - Ouvrir le menu "Preferences / Ipython Console / Graphics"
  - Dans le menu "graphics backend" sélectionner "automatic"'
  
* Important remark :  
  For optimal operation of the program you should modify the graphical mode of Python.
  For this (using Spyder):
  - Open the menu "Preferences / Ipython Console / Graphics"
  - In the menu "graphics backend" select "automatic"
"""

import numpy as np
#from scipy.integrate import trapezoid
import matplotlib.pyplot as plt
import LibrOndes as LO
plt.close("all")

# physical parameters
(L,T,mu,c) = LO.variable()

# physical mesh
Nx = 500;
dx =L/Nx;
x =np.arange( 0,L,dx)


# initial conditions
Y0 = np.zeros(x.size)
V0 = np.zeros(x.size)
for i in range(x.size):
    Y0[i] = LO.posinit(x[i])
    V0[i] = LO.vitinit(x[i])


# computation of Fourier coefficients and frequencies
Nfourier = 50;
NModesFig = 0; # To plot the few first terms of the series
coef_fourier_A=np.zeros(shape=Nfourier)
coef_fourier_B=np.zeros(shape=Nfourier)
omega=np.zeros(shape=Nfourier)

for m in range (1,Nfourier):
  coef_fourier_A[m] = 2/(m*np.pi*c) * LO.trapezoid((V0*np.sin(m*np.pi*x/L)),x)
  coef_fourier_B[m] = 2.0/L*LO.trapezoid((Y0*np.sin(m*np.pi*x/L)) , x)  
  omega[m]=m*np.pi*c/L
     
  
# Figure 1 : Fourier coefficients  
C=max(max(abs(coef_fourier_B)),max(abs(coef_fourier_A)));    
max_vertical_range = 2*C
min_vertical_range = 1e-4*max_vertical_range;
min_horizontal_range = 0;
max_horizontal_range = L;

plt.figure(1)
plt.ylim(min_vertical_range,max_vertical_range), plt.xlim(0,15)
plt.ylabel('|A_n|, |B_n|')
plt.xlabel('f_n/f_1')
plt.title('Fourier Coefficients |A_n| (blue) and |B_n| (red) as function of frequencies');
plt.semilogy(omega/omega[1],abs(coef_fourier_A+1e-30),'bo')
plt.semilogy(omega/omega[1],abs(coef_fourier_B+1e-30),'ro')
plt.pause(0.1);




# Figure 2 : animated plot of displacement and velocity of string
Tmax = 2*L/c;
dt = 0.01*Tmax;
t=0;
while t<=Tmax:
    Yt = 0*x;
    Vt = 0*x;
    for m in range (Nfourier):
    #Calcul des positions et vitesses
        Yt = Yt + np.sin(m*np.pi*x/L)*(coef_fourier_A[m]*np.sin(omega[m]*t)+coef_fourier_B[m]*np.cos(omega[m]*t))
        Vt = Vt + np.sin(m*np.pi*x/L)*omega[m]*  (coef_fourier_A[m]*np.cos(omega[m]*t)-coef_fourier_B[m]*np.sin(omega[m]*t))
        
    plt.figure(2)
    plt.subplot(2,1,1);
    plt.cla()
    plt.xlim(0,L);
    plt.plot(x,Yt,'r')
    plt.plot(x,Y0,'r--');
    for m in range(1,NModesFig+1):
        plt.plot(x,np.sin(m*np.pi*x/L)*(coef_fourier_A[m]*np.sin(omega[m]*t)+coef_fourier_B[m]*np.cos(omega[m]*t)),'r:')
    plt.xlabel('x'); plt.ylabel('Y');
    plt.title('String position (red)'); 
    plt.subplot(2,1,2);
    plt.cla()
    plt.xlim(0,L);
    plt.plot(x,Vt,'b'),
    plt.plot(x,V0,'b--');
    for m in range(1,NModesFig+1):
        plt.plot(x,np.sin(m*np.pi*x/L)*omega[m]*  (coef_fourier_A[m]*np.cos(omega[m]*t)-coef_fourier_B[m]*np.sin(omega[m]*t))
,'b:')
    plt.xlabel('x'); plt.ylabel('V');
    plt.title('String velocity (blue)'); 
    plt.pause(0.1);
    t=t+dt;


# Figure 3 : Fourier coefficientss of acoustic signal
beta = 1;
coef_fourier_U = beta*m*np.pi/(L)*(complex(0,-1)*coef_fourier_A+coef_fourier_B);

max_vertical_range = 2*max(abs(coef_fourier_U));
min_vertical_range = 1e-4*max_vertical_range;
min_horizontal_range = 0;
max_horizontal_range = 20;

plt.figure(3)
plt.xlim( min_horizontal_range ,max_horizontal_range); plt.ylim(min_vertical_range ,max_vertical_range )
plt.ylabel('|U_n|^2')
plt.xlabel('f_n/f_1')
plt.title('Fourier Coefficients |U_n| as function of frequencies');
plt.semilogy(omega/omega[1],abs(coef_fourier_U),'ko') 


# Figure 4 : reconsctuction of acoustic signal
ttab = np.arange(0,Tmax,Tmax/500);
Ut = 0*ttab;
for n in range (len(ttab)):
  for m in range (Nfourier):
        Ut[n] = Ut[n] + (m*np.pi/L)* (coef_fourier_A[m]*np.sin(omega[m]*ttab[n])+coef_fourier_B[m]*np.cos(omega[m]*ttab[n]));

plt.figure(4)
plt.plot(ttab,Ut);
plt.ylabel('U(t)')
plt.xlabel('t')
plt.title('Reconstruction of acoustic signal')




