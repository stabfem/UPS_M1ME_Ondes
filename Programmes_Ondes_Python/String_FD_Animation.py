#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 10:16:14 2023

@author: Paul Flaumenbaum (Etudiant M1 Méca UPS)
(translated from a Matlab program from D. Fabre)

* Remarque importante :
  Pour utiliser ce programme de manière optimale il faut modifier le mode graphique de Python.
  Pour cela (avec Spyder) :
  - Ouvrir le menu "Preferences / Ipython Console / Graphics"
  - Dans le menu "graphics backend" sélectionner "automatic"'
  
* Important remark :  
  For optimal operation of the program you should modify the graphical mode of Python.
  For this (using Spyder):
  - Open the menu "Preferences / Ipython Console / Graphics"
  - In the menu "graphics backend" select "automatic"
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import ArtistAnimation
import LibrOndes as LO

# physical parameters -> see library LibrOndes
(L,T,mu,c) = LO.variable()

# time and space steps
dx = .01
dt = .005
r = c*dt/dx
Nx = round(L/dx)
x = np.arange(0,L,dx)
Y = np.zeros(shape=len(x)); V = np.zeros(shape=len(x))
Yfuture = np.zeros(shape=len(x)); Ypast = np.zeros(shape=len(x))
Y_0 = np.zeros(shape=len(x))

# Initial conditions
for i in range (len(x)):
   Y[i] = LO.posinit(x[i]);
   V[i] = LO.vitinit(x[i]);
   Ypast[i] = Y[i]-V[i]*dt;
   Y_0[i] = Y[i]; # for comparison at the end of simulation
   plt.figure(10);

# Initialization of figure   
fig, (ax1, ax2) = plt.subplots(2, 1)
images = []   
   
# Loop over time
stopTime = 2*L/c # corresponds to one period
for t in np.arange(dt,stopTime+dt,dt):   #MODIF
    #inner points : solve the wave equation
    for j in range (1,Nx-2):
        Yfuture[j] = r*r*(Y[j-1]+Y[j+1])+2*(1-r*r)*Y[j]-Ypast[j];
    # boundary conditions x=0 and x=L:
    Yfuture[0] = 0;
    Yfuture[Nx-1] = 0; 
    #Plot  the  graph  after  every  20 th  frame
    if  (round(t/dt)%50 == 0):
        ax1.cla();ax2.cla()
        line1, = ax1.plot(x, Y, 'r')
        ax1.set_xlim(0, L);ax1.set_xlabel('x');ax1.set_ylabel('Y');
        ax1.set_title('String position (red)')
        V = (Yfuture - Ypast) / (2 * dt)
        line2, = ax2.plot(x, V, 'b')
        ax2.set_xlim(0, L);ax2.set_xlabel('x');ax2.set_ylabel('V')
        ax2.set_title('String velocity V(x) (blue)')
        images.append([line1, line2])
        plt.pause(0.001)
        plt.show()
    # Set  things  up  for  the  next  time  step
    for i in range(0,Nx-1):
        Ypast[i] = Y[i] ;
        Y[i] = Yfuture[i] ;

# Compare the deformation after one period with initial condition        
E =  max(abs(Ypast-Y_0))   #MODIF
print('max(|Y-Y_0|) after one period : ')
print(E)

# Generate movie
ani = ArtistAnimation(fig, images, interval=50, blit=True, repeat=False)
#ani.save('mouvement_corde.mp4', writer='ffmpeg', fps=30)
ani.save('mouvement_corde.gif', writer='pillow', fps=30)
#plt.show()