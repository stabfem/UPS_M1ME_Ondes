#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This library contains functions defining the physical properties 
and the initial conditions for waves on a string.

It is used by the main programs :
    WaveFourrier.py
    WavedAlembert.py 
    WaveFiniteDifference.py

Created on Tue May  9 11:10:04 2023

@author: Paul Flaumenbaum (Etudiant M1 Méca UPS)
  (Adapted from Matlab programs by D. Fabre)
"""

import numpy as np
import math
#from scipy.integrate import trapezoid -> not available : function defined at the end

# Physical parameters
L = 10 
T = 1
mu = 1 
c=np.sqrt(T/mu);


#variables globales
def variable():
    global L
    global T
    global mu 
    global c
    return L,T,mu,c

    
# initial dispoacement of the string        
def posinit(x):
    d = L/3;
    e = 1;    
    if (x<d):
        y=e*x/d
    else:
        y=e/(L-d)*(L-x)             
    return y


# initial velocity of the string
def vitinit(x):
    v = 0
    return v


# The next functions are needed in d'Alembert's method to reconstruct the functions F and G.    
def intvitinit(x1): 
  xint = np.linspace(0,x1,int(x1/L*500))
  #np.range(xint)
  vint = np.zeros(xint.size)
  for i in range(xint.size):
      vint[i] = vitinit(xint[i])
  y=trapezoid(vint,xint) ## ERREUR CORRIGEE : a la place de y=trapezoid(xint,vint)
  return y

def dAlembert_F(x1):
  if(x1>=0 and x1<=L):
    y = posinit(x1)/2-1/c*intvitinit(x1)/2;
  elif(x1<=0 and x1>=-L):
  # prolongation par symetrie
    y = -posinit(-x1)/2-1/c*intvitinit(-x1)/2;
  else: 
   # prolongation par periodicite
    x1p = x1-2*L*round(x1/(2*L)-1e-10);
    y = dAlembert_F(x1p);
  return y



def dAlembert_G(x2):
  print(x2)
  if(x2>=0 and x2<=L):
    y = posinit(x2)/2+1/c*intvitinit(x2)/2;
  elif(x2<0 and x2>=-L):
    y = -posinit(-x2)/2+1/c*intvitinit(-x2)/2;
  else:
    x2p = x2-2*L*round(x2/(2*L)-1e-10);
    y = dAlembert_G(x2p);
  return y


# the next function is normally in the library scipy but in case it is not availalble..
def trapezoid(y,x):
    if len(x)>2:
        dx = x[1:]-x[0:-1]
        dxx = (dx[1:]+dx[0:-1])/2.0
        Y = sum(dxx*y[1:-1])+(dx[0]*y[0]+dx[-1]*y[-1])/2.
    elif len(x)==2:
        Y = sum(y)/2*(x[1]-x[0])
    else:                  
        Y = 0
    return Y


