#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  8 16:14:51 2023

@author: David Fabre, translated from MAtlab with help of Paul 
"""

import numpy as np
from matplotlib import pyplot as plt
import math
import Lib_WaveSimulator as LW
plt.close('all')


#%%  Demo pour une géométrie cone/cylindre. Essai pour une valeur de omega (pas forcément intéressante, et sans correction de longueur)
R1,T1,Zin1 =  LW.WaveSimulator(problemtype="pipeConCyl",
                properties={"c0":1,"rho":1,"Lcone":2,"Lcyl":1+.5*.61,"Rin":0.2,"Rout":0.5},
                BC_0 = "v0",
                omega = 1.38, # est ce que cette frequence est interessante  ???
                BC_L = "radiation",
                FinalTime=100, # est ce que ce temps est suffisant ???
                fig1=0,fig2=10000,fig3=500)


 

#%%  Demo avec une boucle sur omega
omega_ = np.linspace(.5,4,71); #est ce que cette gamme est interessante ?
R =np.zeros_like(omega_);
T =np.zeros_like(omega_);
Zin =np.zeros_like(omega_);
for j,omega in enumerate(omega_):
    Zin[j],R[j],T[j] =  LW.WaveSimulator(problemtype="pipeConCyl",
                    properties={"c0":1,"rho":1,"Lcone":2,"Lcyl":1+.5*.61,"Rin":0.2,"Rout":0.5},     
                    BC_0 = "v0",omega = omega,
                    BC_L = "radiation",beta=0.25,
                    FinalTime=50, # suffisant ?????
                    fig1=0,fig2=0,fig3=0)
    print(f" omega = {omega}, Zin = {Zin[j]}")
 
#%% On trace la figure
plt.figure(40);
plt.plot(omega_,Zin); plt.xlabel(f'\omega rad/(unite de temps)'); plt.ylabel('|Z|')
    
#%% On ecrit dans un fichier CSV pour pouvoir utiliser le programme de comparaison
data = np.zeros((len(omega_),4))
data[:,0] = omega_
data[:,1] = Zin
data[:,2] = R
data[:,2] = T
np.savetxt('Zin_ConeCyl_WS.csv', data, delimiter=",")






   