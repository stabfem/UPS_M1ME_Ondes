#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 10:16:14 2023

@author: Paul Flaumenbaum
(translated from a Matlab program from D. Fabre)
"""

import numpy as np
import matplotlib.pyplot as plt
import LibrOndes as LO

# physical parameters -> see library LibrOndes
(L,T,mu,c) = LO.variable()

# time and space steps
dx = .05
dt = .02
r = c*dt/dx
Nx = round(L/dx)
x = np.arange(0,L,dx)
Y_current = np.zeros(shape=len(x))
V_current = np.zeros(shape=len(x))
Y_future = np.zeros(shape=len(x))
Y_past = np.zeros(shape=len(x))
Y_0 = np.zeros(shape=len(x))

# Initial conditions
for i in range (len(x)):
   Y_current[i] = LO.posinit(x[i]);
   V_current[i] = LO.vitinit(x[i]);
   Y_past[i] = Y_current[i]-V_current[i]*dt;
   Y_0[i] = Y_current[i]; # for comparison at the end of simulation
   
# Loop over time
stopTime = 2*L/c # corresponds to one period
for t in np.arange(dt,stopTime,dt):
    #inner points : solve the wave equation
    for j in range (1,Nx-2):
        Y_future[j] = r*r*(Y_current[j-1]+Y_current[j+1])+2*(1-r*r)*Y_current[j]-Y_past[j];
    # boundary conditions x=0 and x=L:
    Y_future[0] = 0;
    Y_future[Nx-1] = 0; 
    #Plot  the  graph  after  every  20 th  frame
    if  (round(t/dt)%20 == 0):
        plt.subplot(2,1,1); plt.cla()
        plt.title('String position (red)');
        plt.plot( x ,  Y_current,'r');
        plt.xlabel('x'); plt.ylabel('Y'); 
        V_current = (Y_future-Y_past)/(2*dt);
        plt.subplot(2,1,2); plt.cla()
        plt.plot( x ,  V_current,'b');
        plt.title('String velocity V(x) (blue) ');
        plt.xlabel('x'); plt.ylabel('V'); 
        plt.pause(0.001)
        plt.show()
    # Set  things  up  for  the  next  time  step
    for i in range(0,Nx-1):
        Y_past[i] = Y_current[i] ;
        Y_current[i] = Y_future[i] ;

# Compare the deformation after one period with initial condition        
E =  max(abs(Y_current-Y_0))   
print('max(|Y-Y_0|) after one period : ')
print(E)