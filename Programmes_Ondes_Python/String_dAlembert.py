"""
Created on Wed May  3 09:22:21 2023

@author: Paul Flaumenbaum (Etudiant M1 Méca UPS)
(translated from a Matlab program from D. Fabre)

* Remarque importante :
  Pour utiliser ce programme de manière optimale il faut modifier le mode graphique de Python.
  Pour cela (avec Spyder) :
  - Ouvrir le menu "Preferences / Ipython Console / Graphics"
  - Dans le menu "graphics backend" sélectionner "automatic"'
  
* Important remark :  
  For optimal operation of the program you should modify the graphical mode of Python.
  For this (using Spyder):
  - Open the menu "Preferences / Ipython Console / Graphics"
  - In the menu "graphics backend" select "automatic"

"""
import numpy as np
import matplotlib.pyplot as plt
import LibrOndes as LO
plt.close("all")

#Physical parameters
(L,T,mu,c) = LO.variable()

Tmax = (2*L)/c ; # duree totale = 1 periode
dt = .025*Tmax; # delai entre chaque instant repr?sent?
dx = .01*L;
x =np.arange( -L,2*L,dx)
termeF=np.zeros(shape=len(x))
termeG=np.zeros(shape=len(x))
Y=np.empty(shape=len(x))

# Loop over time
for t in np.arange(0,Tmax+dt,dt):
  # construction of F,G and Y  
  for i in range (len(x)):
    x1 = x[i]-c*t;    
    x2 = x[i]+c*t;
    termeF[i] = LO.dAlembert_F(x1);
    termeG[i] = LO.dAlembert_G(x2);
    Y[i] = termeF[i]+termeG[i];

  # plot
  ymax1 =max(termeF[round(L/dx):round(2*L/dx+1)]);
  ymax2=max(termeG[round(L/dx):round(2*L/dx+1)]);
  ymax=2*max([ymax1, ymax2])
  plt.figure(1)
  plt.clf() 
  plt.xlim(-L,2*L);
  plt.ylim([-1.5 ,1.5]);
  plt.xlabel('x');plt.ylabel('Y');
  plt.plot([0 ,0],[-1e3, 1e3],'k:', [L, L],[-1e3 ,1e3],'k:');
  plt.plot( x , termeG,'g--');
  plt.plot(x,termeF,'b--')
  plt.plot(x,Y,'r--')
  plt.plot(x[round(L//dx):round(2*L//dx+1)], termeF[round(L//dx):round(2*L//dx+1)], 'b',x[round(L//dx):round(2*L//dx+1)], termeG[round(L//dx):round(2*L//dx+1)], 'g')
  plt.plot(x[round(L//dx):round(2*L//dx+1)], Y[round(L//dx):round(2*L//dx+1)],'r',2);
  plt.title('Y(x,t) (red) = F(x-ct) (blue) + G(x+ct) (green)')
  plt.pause(0.3)
  plt.show() # affiche la figure à l'écran
