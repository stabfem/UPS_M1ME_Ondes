#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  8 16:14:51 2023

@author: paul
"""

import numpy as np
from matplotlib import pyplot as plt
import math

def cZ(problemtype, N):
    # ... implementation of the cZ function for creating the mesh and properties of the medium
    if problemtype=='string':
        # case of a string with constant velocity
        T = 1;
        mu = 1;
        L = 1;
        dx=L/(N)
        x =np.arange( 0,L,dx)
        c = np.sqrt(T/mu)*np.ones(shape=N);
        Z = T/c;
        rho=1;
        S=np.ones(shape=N);
        Rout=np.nan
        
    elif problemtype=='pipe':
        #cylindrical pipe, dimensional (a flute, L=60cm,r=1cm)
        c0 = 0.34;      # wave velocity in [m/ms]
        rho = 1.225;   # density [kg/m^3]
        a = 0.06;       # radius  [m]
        L = 0.6;        # Length  [m]
        S0 = np.pi*a*a; # section [m^2]
        dx=L/(N)
        x =np.arange( 0,L,dx)
        c = c0*np.ones(shape=N);
        S = S0*np.ones(shape=N);
        Z = rho*c/S;        
        Rout = a;
        #Rout=np.nan
    # This function sets the physical properties of the 1D medium :
    # x     mesh
    # c     wave velocity (array with same dimension as x)
    # Z     impedance of medium (array with same dimension as x)
    # S     surface of the pipe/canal (not relevant for a string)
    # rho   density of acoustic medium (not relevant for a string)
    # = []; rho = [];

    elif problemtype=='string1disc':
            # case of a string with a discontinuity;
            T = 1;
            c1 = 1;
            c2 = .2;
            X1 = .5;
            L = 1;
            dx=L/(N)
            x =np.arange( 0,L,dx)
            c = c0*np.ones(shape=N);
            for i in range(N):
                c = c1*(x[i]<X1)+c2*(x[i]>=X1);
            Z = T/c;
            Rout=np.nan

    elif problemtype=='string2disc':
            # case of a string with a discontinuity;
            T = 1;
            c1 = 1;
            c2 = 4;
            c3 = 1;
            X1 = .5;
            X2 = .75;
            L = 1;
            dx=L/N
            x =np.arange( 0,L,dx)
            dx=L/(N)
            x =np.arange( 0,L,dx)
            c = c0*np.ones(shape=N);
            for i in range(N):
                c = c1*(x[i]<X1)+c2*(x[i]>=X1 and x[i]<X2)+c3*(x[i]>=X2);
            Z = T/c;
            Rout=np.nan
            
    elif problemtype=='pipeadim':
            #cylindrical pipe, nondimensional
            c0 = 1;
            rho = 1;
            L = 1;
            dx=L/N
            x =np.arange( 0,L,dx)
            R0 = 0.1; # nondimensional radius
            S0 = np.pi*R0*R0;
            c = c0*np.ones(shape=N);
            Z = rho*c0/S0*np.ones(shape=N);
            S = S0*np.ones(shape=N);
            Rout = R0;


    elif problemtype=='pipe1disc':
            # 2 connected cylindrical pipes (here nondimensional)
            L = 1;
            dx=L/N
            x =np.arange( 0,L,dx)
            X1 = .5; # location of discontinuity
            c0 = 1;
            S1 = 1;
            S2 = 10;
            rho = 1;
            S = S0*np.ones(shape=N);
            for i in range(N):
                 S[i] = (x[i]<X1)*S1+(x[i]>=X1)*S2;
            c = c0*np.ones(shape=N);
            Z = rho*c0/S;
            Rout = np.sqrt(S[N]/np.pi);


    elif problemtype=='pipe2disc':
            # 3 connected cylindrical pipes (here nondimensional)
            L = 30;
            dx=L/N
            x =np.arange( 0,L,dx)
            X1 = 10; # location of 1st discontinuity
            X2 = 20; # location of 2nd discontinuity
            c0 = 1;
            S1 = 1e-4;
            S2 = 10e-4;
            S3 = 1e-4;
            rho = 1.225;
            S=S1*np.ones(shape=N)
            for i in range(N):
                S[i] =(x[i]<X1)*S1+(x[i]>=X1 and x[i]<X2)*S2+(x[i]>=X2)*S3;
            
            c = c0*np.ones(shape=N);
            Z = rho*c0/S;
            Rout = np.sqrt(S[N-1]/np.pi);

    elif problemtype=='acousticuniform':
            #acoustic medium with uniform properties
            c0 = 1;
            S = 1;
            rho0 = 1;
            L = 1;
            dx=L/N
            x =np.arange( 0,L,dx)
            #x = linspace(0,L,Nx);
            rho = rho0*np.ones(shape=N);
            c = c0*np.ones(shape=N);
            Z = rho*c/S0;
            Rout=np.nan
            

    elif problemtype=='acousticdisc':
            # acoustic medium with uniform properties
            L = 1;
            dx=L/N
            x =np.arange( 0,L,dx)
            S = 1;
            rho1 = 1.225;#air
            c1 = 0.34;#air
            X1 = 0.75;
            rho2 = 10;#water
            c2 = 1.5;#water
            c = c1*np.ones(shape=N);
            rho = rho1*np.ones(shape=N);
            
            for i in range(N):
                rho[i] = rho1*(x[i]<X1)+rho2*(x[i]>=X1);
                c[i] = c1*(x[i]<X1)+c2*(x[i]>=X1);
            Z = rho*c/S;
            Rout=np.nan


    elif problemtype=='canal1disc':
            # 2 connected canals
            # NB for canals the length/time/mass unities are kilometer, minute
            # and petagram (1 Petagram = 10^15 g = 10^12 kg)
            L = 1;   # 1km
            dx=L/N
            x =np.arange( 0,L,dx)
            X1 = .5; # 500 meters
            X2 = .5; # 700 meters
            g = 9.81*60*60/1000; # 9.81 m^2/s converted in km^2/min
            rho = 1; # rho in Pg/km^3
            W = 1;
            H1 = 0.2; # 200m
            H2 = 0.01; # 10m
            H=np.zeros(shape=N)
            for i in range(N):
                H[i] = (x[i]<X1)*H1+(x[i]>=X1)*H2;
            c =np.sqrt(g*H);
            c=c*np.ones(shape=N)
            Z = 1/(rho*W*np.sqrt(g*g*g*H));
            S = W*H;
            Rout=np.nan

    elif problemtype=='canalprog':
            # canal with progressive variation of height
            # NB for canals the length/time/mass unities are kilometer, minute
            # and petagram (1 Petagram = 10^15 g = 10^12 kg)
            X1 = .3; # 300 meters
            X2 = .7; # 700 meters
            g = 9.81*60*60/1000; # 9.81 m^2/s converted in km^2/min
            rho = 1; # rho in Pg/km^3
            W = 1;
            H1 = 0.05; # 50m
            H2 = 0.01; # 10m
            L = 1;
            dx=L/N
            x =np.arange( 0,L,dx)
            H=np.zeros(shape=N)
            Z=np.zeros(shape=N)
            for i in range(N):
            #x = linspace(0,L,Nx);
                H [i]= (x[i]<X1)*H1+(x[i]>=X1 and x[i]<X2)*(H1+(H2-H1)*(x[i]-X1)/(X2-X1))+(x[i]>=X2)*H2;
                Z[i] = 1/(rho*W*np.sqrt(g*g*g*H[i]));
            c = np.sqrt(g*H);
            c=c*np.ones(shape=N)
            S = W*H;
            Rout=np.nan


            #case('custom')
            #  add your own case(s) here !
    else:
        print('Error : problemtype not recognized')
    return x,c,Z,S,rho,L,Rout




#def WaveSimulator():
    #global L, x, Z, c, xstag, Zstag, cstag, FinalTime, dt, dx, Rout
plt.close('all')
    
    # Definition of the problem
problemtype = 'pipe2disc'
    # allowed values are 'string', 'string1disc', "string2disc',
    #                    'pipe','pipe1disc','pipe2disc'
    #                    'acousticuniform','acousticdisc'
    #                    'canal1disc','canalprog'
    #
    # see in function cZ at the end of this program
    # to change the parameters, and add your own cases !
    
    # Definition of the boundary conditions
BC_0 = 'pulse'
# Allowed values include :
#   'Y0'                        : Imposed sinusoidal displacement
#   'V0'                        : Imposed sinusoidal velocity/flow rate
#   'F0' | 'P0'                 : Imposed sinusoidal force/pressure
#  'incident' | 'I0'           : Incident sinusoidal wave coming from the region x<0
#   'pulse' | 'Y0_Pulse'        : Gaussian pulse imposed as displacement at the inlet.
#   'F0_Pulse' | 'P0_Pulse'     : Gassian pulse imposed as pressure/force
#   at the inlet++
#   'Incident_Pulse'|'I0_Pulse' : Gaussian incident pulse coming from the region x<0
#   'transparent' | 'noreflect' : Non-reflexion condition

omega = np.pi
Amp = 1
# parameters for 'pulse'-type forcings
t1 = .5
ta = .2
AmpPulse = 1

BC_L = 'fixed'

# This is the boundary condition at outlet (x=L).
# Allowed values include :
#   'Fixed'  | 'closed'         : fixed string (or closed pipe)
#   'Free'   | 'open'           : free string (or ideally open pipe)
#   'transparent' | 'noreflect' : Radiation condition
#   'impedance'                 : We specity an outlet impedance.
#   'incident_pulse'|'I0_Pulse' : Gaussian incident pulse coming from the region x>L
#   'radiation'                 : Radiation condition in a 3D domain (full)
#   'radiation_flanged'         : Radiation condition in a 3D domain (flanged)

Zout = 10

# Definition of the initial condition
Initial_Condition = 'zero'

InitGausD = 0.1
InitGausLoc = 0.5
InitGausAmp = 1

# Numerical parameters relevant to the time and space discretization
FinalTime = 10  # total duration of the simulation
dt = .01  # time step
Nx = 41  # Number of spatial points ; spatial step is dx = L/(Nx-1);
fig1=1  #Y_current, V_current et F_current par rapport a x
fig2=0  #Power et Power_Av
fig3=0  #V0,Y0,F0 et VL, YL, FL par rapport a t
fig5=0  #Total Energy

# Numerical parameters defining how to plot the results
NP = {}
NP['plotstep'] = 100  # number of time steps between two plots
NP['wait'] = 0  # set to 1 to advance step-by-step ; to zero to advance continuously
NP['Tscalefig2'] = 10

# Miscleanous parameters
smoothingtime = 100 * dt  # to soften the starting (useful with Y0)
N_averaging = round(2 * np.pi / omega / dt)  # number of previous instants to compute time-averages
NP['tpause'] = 0.001  # pause after each plot to let time for figure refreshing

# END OF PARAMETERS ; THE REMAINDER SHOULD NOT BE MODIFIED

# Build the mesh and the properties of the medium
# First build a full mesh whith twice the number of points
xF, cF, ZF,S,rho,L,Rout= cZ(problemtype, 2 * Nx - 1)
# We deduce the mesh at principal points (x_i, i=1 to Nx)
x = xF[::2]
c = cF[::2]
Z = ZF[::2]
dx = x[1] - x[0]

#Initial Condition
if Initial_Condition =='zero':
    Y_current = 0*x; # here you can also use posinit(x) as in TP1 !
    V_current = 0*x;
elif Initial_Condition=='gaussiany':
        Y_current = InitGausAmp*np.exp(-(x-InitGausLoc)*(x-InitGausLoc)/InitGausD*InitGausD);
        V_current = 0*x;
        
elif Initial_Condition=='gaussian':
    Y_current=np.zeros(shape=len(x))
    for i in range(len(x)):
        Y_current[i] = -InitGausAmp*math.erf((x[i]-InitGausLoc)/InitGausD);
    
    V_current = 0*x;
    
        
else :
    print('Initial condition type not recognized !')
    
# And the staggered mesh (intermediate points x_{i+1/2} plus extremities)

xstag = xF[[0] + list(range(1, 2 * Nx - 1, 2)) + [-1]]
cstag = cF[[0] + list(range(1, 2 * Nx - 1, 2)) + [-1]]
zstag = ZF[[0] + list(range(1, 2 * Nx - 1, 2)) + [-1]]
t_final = 10.0    # Final time
Y_past = Y_current-dt*V_current;
Y_future = Y_current+dt*V_current;


##Defines a few object needed in the time loop
r = c*dt/dx;
rZ = (c/Z)*(dt/dx);
rZ[Nx-1]=rZ[Nx-2]
rstag = cstag*zstag*dt/dx;
r0 = c[0]*dt/dx;
rL = c[Nx-1]*dt/dx;
nsteps = int(t_final/dt)


print(' ');print('%%Program Wavesimulator.m ready to run');print(' ');
print(['Selected case                : ',problemtype]);
print(['Inlet boundary condition     : ',BC_0]);

print(['Outlet boundary condition    : ',BC_L]);
print(['Initial condition            : ',Initial_Condition]);print(' ');
print(['numerical stability criterion : max(r) = ' ,max(abs(r))])
if(max(abs(r))>1):
    print('WARNING : max(r)  > 1 ! reduce dt to avoid intability');
else:
    if(max(abs(r))<0.5):
        print('max(r)  < 1 ; stability criterion is largely satisfied ; you may increase the time step dt to speed up the code ');
    



F_current = np.zeros_like(x)
F_current[1:Nx-2] = -cstag[1:Nx-2]*zstag[1:Nx-2]*np.diff(Y_current[0:Nx-2])/dx
F_current[0] = c[0]*Z[0]*(3*Y_current[0]-4*Y_current[1]+Y_current[2])/(2*dx)
F_current[Nx-1] = -c[Nx-1]*Z[Nx-1]*(3*Y_current[Nx-1]-4*Y_current[Nx-2]+Y_current[Nx-3])/(2*dx)

Y_past = Y_current - dt*V_current
Y_future = Y_current + dt*V_current

N_tab = int(FinalTime/dt+1)


##Initialisation des Variables
N_tab = round(FinalTime/dt+1);
Y0 = np.nan*np.ones(shape=N_tab)
V0= np.nan*np.ones(shape=N_tab)
F0= np.nan*np.ones(shape=N_tab)
YL = np.nan*np.ones(shape=N_tab)
VL= np.nan*np.ones(shape=N_tab)
FL= np.nan*np.ones(shape=N_tab)
Power_0= np.nan*np.ones(shape=N_tab)
Power_0_plus=np.nan*np.ones(shape=N_tab)
Power_L=np.nan*np.ones(shape=N_tab)
Power_L_plus=np.nan*np.ones(shape=N_tab)
TotalEnergy=np.nan*np.ones(shape=N_tab)
TotalEnergy_Av=np.nan*np.ones(shape=N_tab)
tab_Power_0_Av=np.nan*np.ones(shape=N_tab)
tab_Power_0_plus_Av=np.nan*np.ones(shape=N_tab)
tab_Power_L_Av=np.nan*np.ones(shape=N_tab)
tab_Power_L_plus_Av=np.nan*np.ones(shape=N_tab)
tableau = np.zeros(shape=N_tab)
t = 0
NP = x[1:Nx]
while t<t_final:
   
        
    #Condition x=0
     Y_future[1:Nx-1] = 2*Y_current[1:Nx-1] - Y_past[1:Nx-1] + r[1:Nx-1]*(Y_current[2:Nx] - 2*Y_current[1:Nx-1] + Y_current[:Nx-2])
     if BC_0=='pulse':
        Y_future[0] = AmpPulse*np.exp(-((t+dt)-t1)*((t+dt)-t1)/(ta*ta))
     elif BC_0=='fixed' or BC_L=='closed':
       #elif BC_0=='fixed' or BC_0== 'closed':
         Y_future[0] = 0;
           #print('fixed')
     elif BC_0=='free' or BC_0=='open':
        Y_future[0] = 2*r0*r0*Y_current[1]+2*(1-r0*r0)*Y_current[0]-Y_past[0] ;
        Y_future[Nx-1] = 0;
     elif BC_0=='f0'or BC_0=='p0':
         Y_future[0] = 2*r0*r0*Y_current[1]+2*(1-r0*r0)*Y_current[0]-Y_past[0]+2*r0*r0*dx*Amp*np.sin(omega*(t))/(Z[0]*c[0]);
     elif BC_0== 'y0':
         Y_future[0]=Amp*np.sin(omega*(t+dt))*np.tanh((t+dt)/smoothingtime); # the smoothing period is to make the start less abrupt
     elif BC_0=='v0':
        Y_future[0]=Amp*(np.cos(omega*(t+dt))-1)*np.tanh((t+dt)/smoothingtime)/omega; 
     elif BC_0== 'i0' or BC_0=='incident':
         Y_future[0] = (2*Y_current[0]+(r0-1)*Y_past[0]+2*r0*r0*(Y_current[1]-Y_current[0]))/(1+r0)-Amp/Z[0]*np.sin(omega*t)*dt*4*r0/(1+r0);
     elif BC_0=='v0_pulse':
          Y_future[0] = AmpPulse*np.sqrt(np.pi)/2*(math.erf((t+dt-t1)/ta)+1)*ta;
     elif BC_0=='f0_pulse' or BC_0=='p0_pulse':
          Y_future[0]= 2*r0*r0*Y_current[1]+2*(1-r0*r0)*Y_current[0]-Y_past[0]+2*r0*r0*dx*AmpPulse*np.exp(-(t-t1)*(t-t1)/ta*ta)/(Z[0]*c[0]);
     elif BC_0=='i0_pulse'or BC_0=='incident_pulse':
          Y_future[0] = (2*Y_current[0]+(r0-1)*Y_past[0]+2*r0*r0*(Y_current[1]-Y_current[0]))/(1+r0)+AmpPulse/(Z[0]*c[0])*np.exp(-(t-t1)*(t-t1)/ta*ta)*dt*4*c[0]*r0/(1+r0);
     elif BC_0=='transparent' or BC_0=='noreflect':
          Y_future[0] = (2*Y_current[0]+(r0-1)*Y_past[0])+2*r0*r0*(Y_current[1]-Y_current[0])/(1+r0);
     else:
        print('WARNING : left boundary condition not implemented !');    
    #Condition x=L    
     if BC_L=='fixed' or BC_L=='closed':
         Y_future[Nx-1] = 0;
         #print(Y_future[Nx-1])
         #print('fixedd')
     elif BC_L=='free'or BC_L=='open':
         Y_future[Nx-1] = 2*rL*rL*Y_current[Nx-2]+2*(1-rL*rL)*Y_current[Nx-1]-Y_past[Nx-1] ;
     elif BC_L=='transparent' or BC_L=='noreflect':
         Y_future[Nx-1] = (2*Y_current[Nx-1]+(rL-1)*Y_past[Nx-1]+2*rL*rL*(Y_current[Nx-1]-Y_current[Nx-1]))/(1+rL);
     elif BC_L=='impedance':
         rL = c[Nx-1]*dt/dx;
         rZZ = rL/Zout*Z[Nx-1];
         Y_future[Nx-1] = (2*rZZ*Y_current[Nx-1]+(rL*rL-rZZ)*Y_past[Nx-1]+2*rL*rL*rZZ*(Y_current[Nx-1]-Y_current[Nx-1]))/(rZZ+rL*rL);
     elif BC_L=='i0_pulse'or BC_L=='incident_pulse':
         Y_future[Nx-1] = (2*Y_current[Nx-1]+(rL-1)*Y_past[Nx-1]+2*rL*rL*(Y_current[Nx-2]-Y_current[Nx-1]))/(1+rL)-AmpPulse/(Z[Nx-1]*c[Nx-1]*np.exp(-(t-t1)*(t-t1)/ta*ta)*dt*4*c[Nx-1]*rL/(1+rL));

     elif BC_L=='i0':
        Y_future[Nx-1] = (2*Y_current[Nx-1]+(rL-1)*Y_past[Nx-1]+2*rL*rL*(Y_current[Nx-2]-Y_current[Nx-1]))/(1+rL)-Amp/Z(Nx)*np.sin(omega*t)*dt*4*rL/(1+rL);
     elif BC_L=='radiation_flanged':
         rL = c[Nx-1]*dt/dx;
         Zout = Z[Nx-1]*(omega/(c[Nx-1]*c[Nx-1])*Rout*Rout)/2;
         rZZ = rL/Zout*Z[Nx-1];
         Y_future[Nx-1] = (2*rZZ*Y_current[Nx-1]+(rL*rL-rZZ)*Y_past[Nx-1]+2*rL*rL*rZZ*(Y_current[Nx-2]-Y_current[Nx-1]))/(rZZ+rL*rL);

     elif BC_L=='radiation':
         rL = c[Nx-1]*dt/dx;
         Zout = Z[Nx-1]*(omega/(c[Nx-1]*c[Nx-1])*Rout*Rout)/4;
         rZZ = rL/Zout*Z[Nx-1];
         Y_future[Nx-1]= (2*rZZ*Y_current[Nx-1]+(rL*rL-rZZ)*Y_past[Nx-1]+2*rL*rL*rZZ*(Y_current[Nx-2]-Y_current[Nx-1]))/(rZZ+rL*rL);
         # other idea which does not work yet...
         #     Z1I = 0; % positive -> delay but drift ; negative -> advance, no drift
         #     Z2R = 1;
         #     Bn = 1/(2*dt^3)*[5, -18, 24, -14, 3]*YNstack'; % bi-acceleration
         #     Y_future(Nx) = 2*Y_current(Nx)-Y_past(Nx) ...
         #       + dt^2*Z1I*(3*Y_current(Nx)-4*Y_current(Nx-1)+Y_current(Nx-2))/(2*dx) ...
         #       + dt^2*Z2R*Bn;
         #     Y_future(Nx)
         #    YNstack = [Y_future(Nx), YNstack(1:4)];
     else :
        print('WARNING : right boundary condition not implemented !');
   # F_current = np.zeros_like(x)
     F_current[1:Nx-2] = -cstag[1:Nx-2]*zstag[1:Nx-2]*np.diff(Y_current[0:Nx-2])/dx
     F_current[0] = c[0]*Z[0]*(3*Y_current[0]-4*Y_current[1]+Y_current[2])/(2*dx)
     F_current[Nx-1] = -c[Nx-1]*Z[Nx-1]*(3*Y_current[Nx-1]-4*Y_current[Nx-2]+Y_current[Nx-3])/(2*dx)
    # Compute the source term for the wave equation

    #PostProcessingAndPlot(t,Y_current,V_current,F_current,NP)
     Y_past[:] = Y_current
     Y_current[:] = Y_future
     V_current[:] = (Y_future[:]-Y_past[:])/(2*dt);
     t=t+dt;
     nt = round(abs(t)/dt)-1;
     tableau[nt] =t
     
     Y0[nt]=Y_current[0]
     V0[nt]=V_current[0]
     F0[nt]=F_current[0]
     YL[nt]=Y_current[Nx-1]
     VL[nt]=V_current[Nx-1]
     FL[nt]=F_current[Nx-1]

     if fig3==1:
        plt.figure(3);
        plt.subplot(2,1,1);
        plt.title('Y0(r), V0(b),F0(g)')
        plt.xlim( 0, FinalTime  );
        plt.plot(tableau,Y0,'r-')
        plt.plot(tableau,V0,'b-')
        plt.plot(tableau,F0,'g-')
        plt.subplot(2,1,2);
        plt.xlim( 0, FinalTime  );
        plt.plot(tableau,YL,'r-')
        plt.plot(tableau,VL,'b-')
        plt.plot(tableau,FL,'g-')
        plt.xlabel('YL(r), VL(b),FL(g)')
        plt.pause(0.0001)
        plt.show()
     if fig1==1:
        plt.figure(1)
        plt.subplot(3,1,1)
        plt.cla()
        plt.title('Y_current(r), V_current(b),F_current(g)')
        plt.plot(x,Y_current,'r')
        plt.subplot(3,1,2)
        plt.cla()
        plt.plot(x,V_current,'b')
        plt.subplot(3,1,3)
        plt.cla()
        plt.plot(x,F_current,'g')
        plt.pause(0.0001)
        plt.show()
     V0plus = (Z[0]*V_current[0]+F_current[0])/(2*Z[0]);
     V0moins = (Z[0]*V_current[0]-F_current[0])/(2*Z[0]);
     VLplus = (Z[Nx-1]*V_current[Nx-1]+F_current[Nx-1])/(2*Z[Nx-1]);
     VLmoins = (Z[Nx-1]*V_current[Nx-1]-F_current[Nx-1])/(2*Z[Nx-1]);

     Power_0[nt] = F_current[0]*V_current[0];
     Power_0_plus[nt] = V0plus*V0plus*Z[0];
     Power_L[nt] = V_current[Nx-1]*F_current[Nx-1];
     Power_L_plus[nt] = VLplus*VLplus*Z[Nx-1];
     
     
     Power_0_Av = np.mean(Power_0[max(1, nt-N_averaging):nt])
     tab_Power_0_Av[nt] = Power_0_Av

     Power_0_plus_Av = np.mean(Power_0_plus[max(1, nt-N_averaging):nt])
     tab_Power_0_plus_Av[nt] = Power_0_plus_Av

     Power_L_Av = np.mean(Power_L[max(1, nt-N_averaging):nt])
     tab_Power_L_Av[nt] = Power_L_Av

     Power_L_plus_Av = np.mean(Power_L_plus[max(1, nt-N_averaging):nt])
     tab_Power_L_plus_Av[nt] = Power_L_plus_Av
     if fig2==1:
        plt.figure(2)
        plt.xlim(0,FinalTime)
        plt.title('PowerL(k) Power0(r)')
        plt.plot(tableau,Power_L_plus,'b:')
        plt.plot(tableau,Power_0_plus,'c:')
        plt.plot(tableau,Power_L,'r:')
        plt.plot(tableau,Power_0,'k:')
        plt.plot(tableau,tab_Power_L_plus_Av,'b')
        plt.plot(tableau,tab_Power_0_plus_Av,'c')
        plt.plot(tableau,tab_Power_L_Av,'r')
        plt.plot(tableau,tab_Power_0_Av,'k')
        plt.pause(0.0001)
        plt.show()
    
     
    #TotalEnergy[nt] = (dx/2)*(0.5*(V_current[0]*V_current[0]*Z[0]/c[0] )+ sum(V_current[2:Nx-2]*V_current[2:Nx-2]*Z[2:Nx-2]/c[2:Nx-2]) + .5*(V_current[Nx-1]*V_current[Nx-1]*Z[Nx-1]/c[Nx-1]))+ sum(F_current[2:Nx-2]*F_current[2:Nx-2]/(zstag[2:Nx-2]*cstag[2:Nx-2]))
     TotalEnergyt = dx/2 * (0.5 * (V_current[0]**2 * Z[0]/c[0]) + np.sum(V_current[1:Nx-1]**2 * Z[1:Nx-1] / c[1:Nx-1]) +0.5 * (V_current[Nx-1]**2 * Z[Nx-1] / c[Nx-1]) + np.sum(F_current[1:Nx-1]**2 / (zstag[1:Nx-1] * cstag[1:Nx-1])))
     TotalEnergy[nt] = TotalEnergyt
     TotalEnergy_Avt = np.mean(TotalEnergy[max(1, nt - N_averaging):nt])
     TotalEnergy_Av[nt] = TotalEnergy_Avt
     if fig5==1:
        plt.figure(5)
        plt.xlim(0,FinalTime)
        plt.title('Total Energy in domain')
        plt.plot(tableau,TotalEnergy,'r:')
        plt.plot(tableau,TotalEnergy_Av,'r')
        plt.pause(0.001)
                               
        
        


    