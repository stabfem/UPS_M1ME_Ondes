

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from scipy.ndimage import convolve
from matplotlib.animation import FuncAnimation

global kappa

# Définition des fonctions utilisées dans le code

def myinput(question,default):
    answer = input(f"{question} [default:{default}] : " )
    if not answer:
        return default 
    else:
        return float(answer)
    
def Reflexion(thetaI, typeI):
    global kappa
    if typeI == "l": # Onde longitudinale
        thetal = thetaI
        thetat = np.arcsin(kappa * np.sin(thetaI))
        N = kappa**2 * np.sin(2 * thetal) * np.sin(2 * thetat) + np.cos(2 * thetat)**2
        Al = (kappa**2 * np.sin(2 * thetal) * np.sin(2 * thetat) - np.cos(2 * thetat)**2) / N
        At = 2 * kappa * np.sin(2 * thetal) * np.cos(2 * thetat) / N
        Rl = Al**2
        Rt = kappa * np.cos(thetat) / np.cos(thetal) * At**2
    else: # Onde transversale
        thetat = thetaI
        
        if 1 / kappa * np.sin(thetaI)  > 0:
            print("  Warning  : the longitudinal wave is evanescent")
            kx = kI*np.sin(thetaI)
            #xiy = np.sqrt(-kxT2) 
        else:
            print("  The longitudinal wave is propagative")
            thetal = np.arcsin(1 / kappa * np.sin(thetaI))
                
        N = kappa**2 * np.sin(2 * thetal) * np.sin(2 * thetat) + np.cos(2 * thetat)**2
        At = (-kappa**2 * np.sin(2 * thetal) * np.sin(2 * thetat) + np.cos(2 * thetat)**2) / N
        Al = 2 * kappa * np.sin(2 * thetat) * np.cos(2 * thetat) / N
        Rt = abs(At)**2
        Rl = 1 / kappa * np.cos(thetal) / np.cos(thetat) * abs(Al)**2

    return Al, At, Rl, Rt, thetal, thetat, #xiy

# Questions avant le code


print("### Visualisation of waves in solid media and reflexion on a surface ###")
print("Select one of the following choices : ")
print("  1 : longitudinal wave")
print("  2 : transverse wave")
print("  3 : reflexion of a longitudinal wave")
print("  4 : reflexion of a transverse wave")
print("  5 : Rayleigh wave")
print("  6 : plot of reflexion coefficients (incident longitudinal)")
print("  7 : plot of reflexion coefficients (incident transverse)")
print('  0 : Quit'); 

# L'utilisateur a fait son choix

choice = 0
while choice == 0:
    choice = myinput("Make your choice ?", 4) # Choix de l'utilisateur
    
    if choice == 1:
        thetadeg = myinput("Incidence angle theta_I (deg) ? ",45)
        lambdaI = myinput("Wavelength of incident wave ? ",1)
        thetaI = thetadeg*np.pi/180
        thetal = thetaI
        thetat = 0
        kl, kt, kI = 2*np.pi/lambdaI, 0, 0 
        cl, ct = 1, 0        
        omega = kl*cl
        AIl, AIt, Al, At = 0, 0, 1, 0
        
    elif choice == 2:
        thetadeg = myinput("Incidence angle theta_I (deg) ? ",45)
        lambdaI = myinput("Wavelength of incident wave ? ",1)
        thetaI = thetadeg*np.pi/180 
        thetat = thetaI 
        thetal = 0
        kt, kl, kI = 2*np.pi/lambdaI, 0, 0 
        ct, cl = 1, 0        
        omega = kt*ct
        AIl, AIt, At, Al = 0, 0, 1, 0
        
    elif choice == 3:
        kappa = myinput("Ratio of velocities of longitudinal/transverse waves ? kappa = ct / cl = ", 0.5)
        thetadeg = myinput("Incidence angle theta_I (deg) ? ", 45)
        lambdaI = myinput("Wavelength of incident wave ?", 1)
        
        thetaI = np.pi/180*thetadeg
        kI = 2*np.pi/lambdaI
        cl = 1
        ct = kappa*cl
        kl, kt = kI, kI/kappa
        omega = kI*cl
        Al, At, Rl, Rt, thetal, thetat = Reflexion(thetaI, "l")
        AIl, AIt = 1, 0
        
    elif choice == 4:
        kappa = myinput("Ratio of velocities of longitudinal/transverse waves ? kappa = ct / cl = ", 0.75)
        thetadeg = myinput("Incidence angle theta_I (deg) ? ", 30)
        lambdaI = myinput("Wavelength of incident wave ?", 1)
        
        thetaI = np.pi/180*thetadeg
        kI = 2*np.pi/lambdaI
        ct, cl, = 1, 1/kappa
        kt, kl = kI, kI/kappa
        omega = kI*cl
        Al, At, Rl, Rt, thetal, thetat = Reflexion(thetaI, "t")
        AIl, AIt = 0, 1
    
    elif choice == 5:
        print("not implemented yet")
        break
    
    elif choice == 6:
        kappa = myinput("Ratio of velocities of longitudinal/transverse waves ? kappa = ct / cl = ",0.5)
        thetaI = np.linspace(0,np.pi/2,200)
        Al, At, Rl, Rt, thetal, thetat = Reflexion(thetaI, "l")
        fig = plt.figure(21)
        plt.plot(thetaI * 180 / np.pi, np.abs(Al), 'b', label='|rll|')
        plt.plot(thetaI * 180 / np.pi, np.abs(At), 'g', label='|rlt|')
        plt.xlabel(r'$\theta_I$')
        plt.legend()
        plt.show()
        fig = plt.figure(22)
        plt.plot(thetaI * 180 / np.pi, Rl, 'b', label='Rll')
        plt.plot(thetaI * 180 / np.pi, Rt, 'g', label='Rlt')
        plt.xlabel(r'$\theta_I$')
        plt.legend()
        plt.show()
        break
        
    elif choice == 7:
         kappa = myinput("Ratio of velocities of longitudinal/transverse waves ? kappa = ct / cl = ",0.5)
         thetaI = np.linspace(0,np.pi/2,200)
         Al, At, Rl, Rt, thetal, thetat = Reflexion(thetaI, "t")
         fig = plt.figure(21)
         plt.plot(thetaI * 180 / np.pi, np.abs(Al), 'b', label='|rtl|')
         plt.plot(thetaI * 180 / np.pi, np.abs(At), 'g', label='|rtt|')
         plt.xlabel(r'$\theta_I$')
         plt.legend()
         plt.show()
         fig = plt.figure(22)
         plt.plot(thetaI * 180 / np.pi, Rl, 'b', label='Rtl')
         plt.plot(thetaI * 180 / np.pi, Rt, 'g', label='Rtt')
         plt.xlabel(r'$\theta_I$')
         plt.legend()
         plt.show()
         break

    Amp = myinput("Amplitude of incident wave ? (default: 0.05): ", 0.05)
    repmode = myinput("Representation: 1 [Color=shear, altitude=div] or 2 [Color=div, altitude=shear] (default: 1): ", 1)
    autoadvance = myinput("Generate a movie (1) or advance manually (0) ? (default: 0): ", 1)

     
# mesh

    ScaleFig = 5
    Npx = 82
    Npy = 82
    Xline = np.linspace(-ScaleFig, ScaleFig, Npx)
    Yline = np.linspace(-1.5 * ScaleFig, 0, Npy)
    X1, Y1 = np.meshgrid(Xline, Yline)
    
# Point de vue
    
    az = 0
    el = 90
    
    plt.close('all')
    for T in np.arange(-4, 8.25, 0.25):
        Xfrontl = cl * T
        Xfrontt = ct * T

  # Figure 
  
        matplotlib.pyplot.close()
        fig = plt.figure(figsize=(8, 6))
        plt.axis([-ScaleFig, ScaleFig, -ScaleFig, ScaleFig])
        ax = fig.add_subplot(111, projection='3d')
        
        u1 = (np.real(Al * np.exp(1j * kl * (X1 * np.sin(thetal) - Y1 * np.cos(thetal)) - 1j * omega * T) * np.sin(thetal))
               * (X1 * np.sin(thetal) - Y1 * np.cos(thetal) < Xfrontl)
               + np.real(At * np.exp(1j * kt * (X1 * np.sin(thetat) - Y1 * np.cos(thetat)) - 1j * omega * T) * np.cos(thetat))
               * (X1 * np.sin(thetat) - Y1 * np.cos(thetat) < Xfrontt)
               + AIl * np.real(np.exp(1j * kl * (X1 * np.sin(thetal) + Y1 * np.cos(thetal)) - 1j * omega * T) * np.sin(thetal))
               * (X1 * np.sin(thetal) + Y1 * np.cos(thetal) < Xfrontl)
               + AIt * np.real(np.exp(1j * kt * (X1 * np.sin(thetat) + Y1 * np.cos(thetat)) - 1j * omega * T) * (-np.cos(thetat)))
               * (X1 * np.sin(thetat) + Y1 * np.cos(thetat) < Xfrontt))
        
        v1 = (np.real(Al * np.exp(1j * kl * (X1 * np.sin(thetal) - Y1 * np.cos(thetal)) - 1j * omega * T) * (-np.cos(thetal)))
               * (X1 * np.sin(thetal) - Y1 * np.cos(thetal) < Xfrontl)
               + np.real(At * np.exp(1j * kt * (X1 * np.sin(thetat) - Y1 * np.cos(thetat)) - 1j * omega * T) * np.sin(thetat))
               * (X1 * np.sin(thetat) - Y1 * np.cos(thetat) < Xfrontt)
               + AIl * np.real(np.exp(1j * kl * (X1 * np.sin(thetal) + Y1 * np.cos(thetal)) - 1j * omega * T) * (+np.cos(thetal)))
               * (X1 * np.sin(thetal) + Y1 * np.cos(thetal) < Xfrontl)
               + AIt * np.real(np.exp(1j * kt * (X1 * np.sin(thetat) + Y1 * np.cos(thetat)) - 1j * omega * T) * np.sin(thetat))
               * (X1 * np.sin(thetat) + Y1 * np.cos(thetat) < Xfrontt))
        
        div = (np.real(Al * 1j * kl * np.exp(1j * kl * (X1 * np.sin(thetal) - Y1 * np.cos(thetal)) - 1j * omega * T))
               * (X1 * np.sin(thetal) - Y1 * np.cos(thetal) < Xfrontl)
               + np.real(AIl * 1j * kl * np.exp(1j * kl * (X1 * np.sin(thetal) + Y1 * np.cos(thetal)) - 1j * omega * T))
               *(X1 * np.sin(thetal) + Y1 * np.cos(thetal) < Xfrontl))
        
        rot = (np.real(At * np.exp(1j * kt * (X1 * np.sin(thetat) - Y1 * np.cos(thetat)) - 1j * omega * T))
               * (X1 * np.sin(thetat) - Y1 * np.cos(thetat) < Xfrontt)
               + np.real(AIt * np.exp(1j * kt * (X1 * np.sin(thetat) + Y1 * np.cos(thetat)) - 1j * omega * T))
           * (X1 * np.sin(thetat) + Y1 * np.cos(thetat) < Xfrontt))
        
        norm_rot = plt.Normalize(rot.min(),rot.max())
        colors_r = plt.cm.get_cmap()(norm_rot(rot))
        
        norm_div = plt.Normalize(div.min(),div.max())
        colors_d = plt.cm.get_cmap()(norm_div(div))
        
        if repmode == 1:
            X = X1 + Amp * u1
            Y = Y1 + Amp * v1
            Z = Amp * div
            surf = ax.plot_surface(X, Y, Z,facecolors=colors_r)
            ax.view_init(azim=az, elev=el)
            plt.colorbar(surf)
            plt.axis('equal')
            ax.dist = 8.5
            plt.show()
        else:
            X = X1 + Amp * u1
            Y = Y1 + Amp * v1
            Z = 5 * Amp * rot
            surf = ax.plot_surface(X, Y, Z,facecolors=colors_d)
            ax.view_init(azim=az, elev=el)
            plt.colorbar(surf)
            plt.axis('equal')
            ax.dist = 8.5
            plt.show()
        
        if autoadvance == 1:
            plt.pause(0.3)
        else:
            plt.pause(None)

        
       
            
  
    
