

% !TEX root = Cours_Ondes.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Ondes dans les solides élastiques}
\label{chap:elastiques}

%=======================================================================
\section{\'Equation de Navier}

%-----------------------------------------------------------------------
\subsection{Loi de Hooke}

On considère un point $M$ d'un solide élastique, localisé au point ${\bf x}$ dans une configuration de référence, et déplacé au point ${\bf x+u}$ à l'instant $t$. L'ensemble des vecteurs ${\bf u}({\bf x}, t)$ pour tous les points $M$ du solide définit le champ des déplacements. 

La différence des déplacements en deux points voisins ${\bf x}$ et ${\bf x} + \dd {\bf x}$ est, au premier ordre, $\dd {\bf u} = {\bf grad(u)} \dd {\bf x}$, de composantes $\dd u_i = u_{i,j} \dd x_j$ avec la notation $u_{i,j} = \partial u_i / \partial x_j$. Le tenseur de composantes 
\begin{equation*}
e_{ij} = \frac{1}{2} \left( \dpa{u_i}{x_j} + \dpa{u_j}{x_i} \right)
\end{equation*}
définit le tenseur des petites déformations, symétrique. Notons que $e_{ii} = {\rm div}{\bf u}$ représente la variation de volume par unité de volume.

Selon la loi de Hooke, les déformations sont des fonctions linéaires des contraintes : 
\begin{equation}
e_{ij} = \frac{1}{E} \left( (1+\nu) \sigma_{ij} - \nu \sigma_{kk}\delta_{ij} \right).
\end{equation}
où $E$ est le module d'Young et $\nu$ le coefficient de Poisson. \`A titre d'ordre de grandeur, $E = 200$ GPa et $\nu = 0,26$ pour l'acier, et $\nu$ est toujours compris entre $0$ et $1/2$. Inversement :
\begin{equation}
\sigma_{ij} =  2\mu e_{ij} + \lambda e_{kk} \delta_{ij}
\end{equation}
où les coefficients de Lamé $\lambda$ et $\mu$ sont définis par
\begin{equation}
\lambda = \frac{\nu E}{(1+\nu)(1-2\nu)}, \qquad 
\mu = \frac{E}{2(1+\nu)}.
\end{equation}

%tenseur des rotations $r_{ij}$, dilatation $\Delta = {\rm div} {\bf u}$, 

%-----------------------------------------------------------------------
\subsection{\'Equation de Navier}

Considérant dans tout ce chapitre que les forces de volume (telle que la pesanteur) sont négligeables, l'équation de la dynamique s'écrit
\begin{equation}
\rho \ddpa{u_i}{t} = \dpa{\sigma_{ij}}{x_j},
\end{equation}
et devient, compte tenu de la loi de Hooke :
\begin{equation}
\rho \ddpa{{\bf u}}{t} = (\lambda + \mu){\bf grad}({\rm div}{\bf u}) + \mu \nabla^2 {\bf u}.
\label{eq:navier}
\end{equation}
où $\rho$ représente la masse volumique du milieu non perturbé (l'erreur correspondant à cette approximation est en général négligeable).


%=======================================================================
\section{Ondes dans un milieu infini}

%-----------------------------------------------------------------------
\subsection{Ondes unidimensionnelles}

Cherchons, pour commencer, une solution où les déformations sont fonctions de la seule coordonnée d'espace $x$ et du temps : ${\bf u} = {\bf u}(x, t)$. L'équation de Navier donne, pour la composante $u_x$ :
\begin{equation}
\ddpa{u_x}{t}  - c_l^2 \ddpa{u_x}{x} = 0, \qquad 
c_l^2 = \frac{\lambda+2\mu}{\rho}. 
\label{eq:ondel1d}
\end{equation}
On retrouve l'équation de d'Alembert, dont les solutions sont des ondes de célérité $c_l$, où les déplacements sont dirigés dans la direction de propagation : il s'agit d'ondes longitudinales de compression et dilatation, analogues aux ondes acoustiques dans un fluide. Remarquons que ces ondes sont non dispersives, et irrotationnelles (${\rm \bf rot}\,{\bf u} = {\bf 0}$).

L'équation de la composante transverse $u_y$, 
\begin{equation}
\ddpa{u_y}{t}  - c_t^2 \ddpa{u_y}{x} = 0, \qquad 
c_t^2 = \frac{\mu}{\rho}, 
\label{eq:ondet1d}
\end{equation}
est encore une équation de d'Alembert, dont les solutions sont des ondes de célérité $c_t$. Les déplacements étant dirigés dans la direction $y$ normale à la propagation, il s'agit d'ondes transverses, ou ondes de cisaillement. La célérité de ces ondes est inférieure à celle des ondes longitudinales\footnote{Le module de Poisson étant compris entre $0$ et $1/2$, on a $c_l > \sqrt{c_t}$.}. Remarquons que ces ondes sont incompressibles (${\rm div}\,{\bf u} = 0$). L'équation pour $u_z$ est identique à celle pour $u_y$. Ainsi, une onde élastique est constituée en général de deux ondes, l'une longitudinale et l'autre transversale, progressant indépendamment l'une de l'autre. 


%-----------------------------------------------------------------------
\subsection{Ondes tridimensionnelles}

Revenons à l'équation de Navier (\ref{eq:navier}) que nous écrivons sous la forme
\begin{equation}
\ddpa{{\bf u}}{t} = (c_l^2 - c_t^2){\bf grad}({\rm div}\,{\bf u}) + c_t^2 \Delta {\bf u}.
\label{eq:navier2}
\end{equation}
Décomposons le déplacement ${\bf u}$ sous la forme
\begin{equation*}
{\bf u} = {\bf u}_l + {\bf u}_t
\end{equation*}
où ${\bf u}_l$ et ${\bf u}_t$ satisfont à 
\begin{equation}
{\rm \bf rot}\,{\bf u}_l = 0, \qquad {\rm div}\,{\bf u}_t = 0.
\label{eq:decomp}
\end{equation}
% https://fr.wikipedia.org/wiki/Théorème_de_Helmholtz-Hodge
On sait en effet de l'analyse vectorielle (théorème de Helmholtz-Hodge) qu'une telle décomposition, par la somme du gradient d'un scalaire (${\bf u}_l = {\rm \bf grad}\,\phi$) et du rotationnel d'un vecteur (${\bf u}_t = {\rm \bf rot}\,{\bf A}$, solénoïdal), est toujours possible. 

Substituons cette décomposition dans (\ref{eq:navier2}) et appliquons l'opérateur divergence aux deux membres de l'équation. Il vient :
\begin{equation*}
{\rm div} \left(\ddpa{{\bf u}_l}{t} - c_l^2 \Delta {\bf u}_l\right) = 0.
\end{equation*}
Par ailleurs, le rotationnel de l'expression entre parenthèses est nul en vertu de (\ref{eq:decomp}). Or, si le rotationnel et la divergence d'un vecteur sont nuls dans tout l'espace, ce vecteur est identiquement nul. Ainsi donc,
\begin{equation}
\ddpa{{\bf u}_l}{t} - c_l^2 \Delta {\bf u}_l = 0.
\label{eq:navier_l}
\end{equation}
Appliquant de même l'opérateur rotationnel à (\ref{eq:navier2}), il vient, compte tenu de ${\rm \bf rot}\,{\bf u}_l = 0$,
\begin{equation*}
{\rm \bf rot} \left(\ddpa{{\bf u}_t}{t} - c_t^2 \Delta {\bf u}_t\right) = 0.
\end{equation*}
Par ailleurs, la divergence de l'expression entre parenthèses est nulle en vertu de (\ref{eq:decomp}). Or, à nouveau, si le rotationnel et la divergence d'un vecteur sont nuls dans tout l'espace, ce vecteur est identiquement nul. Ainsi :
\begin{equation}
\ddpa{{\bf u}_t}{t} - c_t^2 \Delta {\bf u}_t = 0.
\label{eq:navier_t}
\end{equation}
Les équations (\ref{eq:navier_l}) et (\ref{eq:navier_t}) généralisent à trois dimensions les équations (\ref{eq:ondel1d}) et (\ref{eq:ondet1d}). La première est associée à des compressions et dilatations volumiques (irrotationnelles) tandis que l'autre est associée à des cisaillements (incompressibles).

%Prenant la divergence de l'équation de Navier, il vient l'équation des ondes de dilatation :
%\begin{equation}
%\ddpa{{\rm div}{\bf u}}{t} - c_1^2 \nabla^2 {\rm div}{\bf u}, \qquad c_1^2 = \frac{\lambda +2\mu}{\rho}
%\end{equation}
%Prenant le rotationnel de l'équation de Navier, il vient l'équation des ondes de rotation :
%\begin{equation}
%\ddpa{\bf{\rm rot} u}{t} - c_2^2 \nabla^2 {\bf r}, \qquad c_2^2 = \frac{\mu}{\rho}
%\end{equation}


%=======================================================================
\section{Ondes dans un milieu semi-infini}

%-----------------------------------------------------------------------
\subsection{Ondes de surface (Rayleigh)}

Les ondes de Rayleigh sont des ondes se propageant au voisinage de la surface d'un milieu élastique, sans pénêtrer à l'intérieur. Cette surface sera ici le plan $(x,y)$, le milieu occupant le demi-espace $z<0$. 

%.......................................................................
\paragraph{Solution générale.} 

Les déformations satisfont l'équation 
\begin{equation}
\ddpa{u}{t}  - c^2 \Delta u = 0.
\label{eq:u_rayleigh1}
\end{equation}
$u$ réprésentant l'une quelconque des composantes des vecteurs ${\bf u}_l$ ou ${\bf u}_t$, et $c$ correspondant à $c_l$ ou $c_t$.

Considérons une onde ``plane'' se propageant dans la direction $x$, invariante dans la direction $y$, avec $u$ de la forme
\begin{equation}
u = f(z)\,\e^{\im (kx-\omega t)}.
\label{eq:u_rayleigh2}
\end{equation}
Reportant cette expression dans (\ref{eq:u_rayleigh1}), la fonction $f(z)$ est solution de
\begin{equation*}
\ddto{f}{z} = \left( k^2 - \frac{\omega^2}{c^2} \right) f.
\end{equation*}
Pour $k^2 - \omega^2/c^2 < 0$, la fonction $f(z)$ est périodique et l'onde occupe alors tout le demi-espace, ce qui n'est pas ce que nous recherchons. Nous considérons donc $k^2 - \omega^2/c^2 > 0$, la solution bornée s'écrivant alors 
\begin{equation}
u \propto \e^{\kappa z} \e^{\im (kx-\omega t)}, \qquad 
\kappa = \sqrt{k^2 - \frac{\omega^2}{c^2}}. 
\label{eq:u_rayleigh3}
\end{equation}
Il s'agit d'une onde se propageant dans la direction $x$, évanescente (exponentiellement décroissante) dans la direction $z$.

%.......................................................................
\paragraph{Conditions à la surface libre.}

Il reste à satisfaire la condition (vectorielle) de contrainte nulle à la surface $z=0$, qui va imposer certaines relations sur les composantes des parties longitudinale et transversale de l'onde, ${\bf u}_l$ et ${\bf u}_t$. La condition de contrainte nulle en $z=0$ s'écrit
\begin{equation*}
\sigma_{xz} = 0, \qquad 
\sigma_{yz} = 0, \qquad 
\sigma_{zz} = 0,
\end{equation*}
soit, compte tenu de la loi de Hooke :
\begin{equation}
e_{xz} = 0, \qquad 
e_{yz} = 0, \qquad 
\nu (e_{xx} + e_{yy}) + (1-\nu) e_{zz} = 0.
\label{eq:cl_rayleigh}
\end{equation}
Le problème étant invariant dans la direction $y$, la deuxième condition s'écrit $\partial u_y/\partial z = 0$. Avec (\ref{eq:u_rayleigh3}), on en déduit $u_y = 0$, d'où, pour chacune des deux ondes longitudinale et transversale
\begin{equation*}
u_{ly} =  u_{ty} = 0. 
\end{equation*}
Ainsi, le vecteur déformation est dans le plan $(x,z)$. 

La partie transversale ${\bf u}_t$ de l'onde doit satisfaire la condition d'incompressibilité ${\rm div}\,{\bf u}_t = 0$, soit
\begin{equation*}
\dpa{u_{tx}}{x} + \dpa{u_{tz}}{z} = 0. 
\end{equation*}
Compte tenu de (\ref{eq:u_rayleigh3}), cette condition s'écrit
\begin{equation*}
\im k u_{tx} + \kappa_t u_{tz}  = 0, \qquad 
\kappa_t = \sqrt{k^2 - \omega^2/c_t^2},
\end{equation*}
de sorte que la déformation transversale s'écrit 
\begin{equation*}
u_{tx} = a \kappa_t \, \e^{\im (kx-\omega t) + \kappa_t z}, \qquad
u_{tz} = - \im a k  \, \e^{\im (kx-\omega t) + \kappa_t z},
\end{equation*}
où $a$ est une constante. 

La partie longitudinale ${\bf u}_l$ de l'onde doit satisfaire la condition ${\rm \bf rot}\,{\bf u}_l = 0$, soit
\begin{equation*}
\dpa{u_{lx}}{z} - \dpa{u_{lz}}{x} = 0, 
\end{equation*}
d'où on déduit, comme ci-dessus, que la déformation longitudinale est de la forme 
\begin{equation*}
u_{lx} = b k             \, \e^{\im (kx-\omega t) + \kappa_l z}, \qquad
u_{lz} = -\im b \kappa_l \, \e^{\im (kx-\omega t) + \kappa_l z},
\end{equation*}
où $b$ est une constante et $\kappa_l = \sqrt{k^2 - \omega^2/c_l^2}$. 

Il reste à satisfaire la première et la troisième des conditions (\ref{eq:cl_rayleigh}) à la surface. Introduisant les célérités $c_l$ et $c_t$, ces conditions s'écrivent 
\begin{eqnarray*}
\dpa{u_x}{z} + \dpa{u_z}{x} &=& 0 \\
c_l^2 \dpa{u_z}{z} + (c_l^2 - 2 c_t^2) \dpa{u_x}{x} &=& 0.
\end{eqnarray*}
Substituant $u_x = u_{lx} + u_{tx}$ et $u_z = u_{lz} + u_{tz}$, ces équations se réécrivent
\begin{eqnarray*}
a (k^2 + \kappa_t^2) + 2 b k \kappa_l &=& 0 \\
2 a c_t^2 k \kappa_t + b[c_l^2 (\kappa_l^2 - k^2) +2 c_t^2 k^2]  &=& 0.
\end{eqnarray*}
Notant que $\kappa_l^2 - k^2 = \omega^2/c_l^2 = - (k^2 - \kappa_t^2)(c_t^2/c_l^2)$, la deuxième équation se réécrit 
\begin{equation*}
2 a k \kappa_t + b (k^2 + \kappa_t^2)  = 0.
\end{equation*}
Le système de ces deux équations homogènes n'admet de solution non nulle pour $a$ et $b$ que si son déterminant est nul, soit
\begin{equation*}
(k^2 + \kappa_t^2)^2 = 4 k^2 \kappa_t \kappa_l.
\end{equation*}
Substituant les expressions (\ref{eq:u_rayleigh3}) de $\kappa_t$ et $\kappa_l$, cette équation apparaît comme la relation de dispersion ${\cal D}(k, \omega) = 0$ des ondes de Rayleigh. \'Elevant au carré et introduisant la célérité $c = \omega/k$, cette relation s'écrit
\begin{equation}
\left(2 - \frac{c^2}{c_t^2} \right)^4 = 
16 \left(1 - \frac{c^2}{c_t^2} \right) 
   \left(1 - \frac{c^2}{c_l^2} \right), 
\end{equation}
laquelle se réécrit, en introduisant la célérité normalisée $\xi = c/c_t$ :
\begin{equation*}
\xi^6 - 8 \xi^4 + 8 \xi^2 (3 - 2 c_t^2/c_l^2) - 16 (1 - c_t^2/c_l^2) = 0. 
\end{equation*}
Cette relation montre que la célérité normalisée ne dépend que du rapport $c_t/c_l$, lequel ne dépend que du coefficient de Poisson : 
\begin{equation*}
\frac{c_t}{c_l} = \sqrt{\frac{1-2\nu}{2(1-\nu)}}. 
\end{equation*}
$\xi = c/c_t$ doit être réel et positif, et inférieur à l'unité pour que $\kappa_t$ soit réel. Une seule racine satisfait à ces conditions, représentée sur la Figure \ref{fig:OndeRayleigh}. Ainsi, la célérité des ondes de Rayleigh, non dispersives, varie entre $0,874\,c_t$ et $0,955\,c_t$ pour $\nu$ variant de 0 à 0,5 ; elle est inférieure à $c_l$ et à $c_t$. 

Le rapport des amplitudes des ondes longitudinale et transversale est alors donné par :
\begin{equation*}
\frac{a}{b} = - \frac{2-\xi^2}{2\sqrt{1-\xi^2}}.
\end{equation*}

\begin{figure}[htbp]
\begin{center}
%\includegraphics[width=50mm]{../TD_Ondes/Figures/Rayleigh.png}
\caption{Célérité normalisée $c/c_t = \xi$ de l'onde de Rayleigh en fonction du coefficient de Poisson. }
\label{fig:OndeRayleigh}
\end{center}
\end{figure}

%-----------------------------------------------------------------------
\subsection{Réflexion d'une onde plane sur une surface libre}

Une onde élastique arrivant sur une surface libre se réfléchit en donnant naissance à deux ondes, l'une longitudinale et l'autre transversale, se propageant dans des directions différentes. On considère ici le cas particulier d'une onde longitudinale ${\bf u}_i$ arrivant sur une surface libre en $z=0$. L'onde réfléchie comprend {\em a priori} une composante longitudinale ${\bf u}_l$ et une composante transversale ${\bf u}_t$ (Figure). Le champ résultant s'écrit donc
\begin{equation*}
{\bf u} = {\bf u}_i + {\bf u}_l + {\bf u}_t
\end{equation*}
avec 
\begin{eqnarray*}
{\bf u}_i &=& a_i (\sin \theta_i,  \cos \theta_i) \e^{\im \alpha_i}, 
  \qquad \alpha_i = k_i (x\sin\theta_i + z\cos\theta_i - c_l t) \\
{\bf u}_l &=& a_l (\sin \theta_l, -\cos \theta_l) \e^{\im \alpha_l},
  \qquad \alpha_l = k_l (x\sin\theta_l - z\cos\theta_l - c_l t) \\
{\bf u}_t &=& a_t (\cos \theta_t, \sin \theta_t) \e^{\im \alpha_t},
  \qquad \alpha_t = k_t (x\sin\theta_t - z\cos\theta_t - c_t t).  
\end{eqnarray*}

Les directions et amplitudes des deux ondes réfléchies sont déterminées par les conditions aux limites de contrainte nulle sur la surface libre en $z=0$. Les facteurs exponentiels des contraintes, en particulier, doivent y être identiques, et les phases doivent donc être égales :
\begin{equation*}
\alpha_i = \alpha_l = \alpha_t \qquad {\rm en} \quad z=0, 
\end{equation*}
soit :
\begin{equation*}
k_i c_i \left( \frac{x\sin\theta_i}{c_l} - t \right) = 
k_l c_l \left( \frac{x\sin\theta_l}{c_l} - t \right) = 
k_t c_t \left( \frac{x\sin\theta_t}{c_t} - t \right),
\end{equation*}
qui doivent être vérifiées pour tout $x$ et tout $t$. On en déduit, avec $x=0$ :
\begin{equation}
k_i c_l = k_l c_l = k_t c_t = \omega
\end{equation}
qui donne les nombres d'onde des deux ondes réfléchies et montre que leur fréquence est identique à celle de l'onde incidente. On en déduit alors, avec $t=0$ :
\begin{equation*}
\frac{\sin\theta_i}{c_l} = \frac{\sin\theta_l}{c_l} = \frac{\sin\theta_t}{c_t}
\end{equation*}
(relations analogues aux conditions de Snell-Descartes des rayons optiques), soit 
\begin{equation}
\theta_l = \theta_i, \qquad \sin\theta_t = \frac{c_t}{c_l}\sin\theta_i.
\end{equation}
Ainsi, l'angle de réflexion de l'onde longitudinale est égal à l'angle d'incidence, mais celui de l'onde transversale est plus petit ($c_t<c_l$). 

Il reste à déterminer les amplitudes $a_l$ et $a_t$. Des lois de Hooke, on déduit que les contraintes, en $z=0$, sont données par 
\begin{eqnarray*}
\sigma_{xz} &=& ... \\
\sigma_{zz} &=& ... 
\end{eqnarray*}
De l'annulation de ces contraintes, on déduit le système d'équations déterminant les amplitudes $a_l$ et $a_t$ :
\begin{eqnarray*}
a_t ... + a_r ...  &=& a_i ... \\
a_t ... + a_r ...  &=& a_i ... 
\end{eqnarray*}

On pourrait, par le même type de calculs, déterminer les ondes réfléchies et transmises par une interface plane entre deux milieux élastiques, voir Landau \& Lifchitz (1967, \S22).

%-----------------------------------------------------------------------
%\subsection{Réflexion et réfraction d'une onde sur une interface (pas fait)}

%Une onde élastique arrivant sur une interface entre deux milieux élastiques est en partie transmise et en partie réfléchie. Une onde incidente purement longitudinale ou transversale est en général transformée en onde mixte. 

%Les relations déterminant les directions des ondes réfléchie et réfractée se déduisent de la constance de la fréquence et de celle des composantes du vecteur d'onde dans le plan tangent à l'interface. Soient $\theta$ et $\theta'$ les angles d'incidence et de réflexion (ou de réfraction), et $c$ et $c'$ leurs célérités. Alors,
%\begin{equation}
%(\omega/c) \sin \theta = (\omega/c') \sin \theta'.
%\label{eq:snell}
%\end{equation}
%Supposons par exemple l'onde incidente transversale, de célérité $c = c_{t1}$. Comme on a aussi $c' = c_{t1}$ pour l'onde transversale réfléchie, (\ref{eq:snell}) donne 
%\begin{equation*}
%\theta = \theta',
%\end{equation*}
%c'est-à-dire que l'angle de réflexion est égal à l'angle d'incidence. L'angle de réflexion de l'onde longitudinale réfléchie, de célérité $c' = c_{l1}$, est différent, donné par
%\begin{equation*}
%\frac{\sin \theta'}{\sin \theta} = \frac{c_{l1}}{c_{t1}} > 1.
%\end{equation*}
%On en déduit de même les angles des deux ondes réfractées, en fonction de leurs célérités $c_{l2}$ et $c_{t2}$. 

%Les amplitudes des ondes réfléchies et réfractées se déduisent de la continuité des contraintes à l'interface. Voir Landau \& Lifchitz (1967, \S22) pour l'étude de quelques cas particuliers. %Pour une onde longitudinale atteignant la surface d'un corps élastique placé dans le vide, 

%=======================================================================
\section{Vibration des barres et des plaques}

Les ondes dans les barres et les plaques se distinguent des ondes en milieu infini par le fait que l'une des dimensions du milieu -- diamètre d'une barre ou épaisseur d'une plaque -- est petite devant la longueur des ondes. 

%-----------------------------------------------------------------------
\subsection{Ondes longitudinales}

%.......................................................................
\paragraph{Ondes longitudinales dans les barres.} Les ondes longitudinales dans les barres correspondent à des déplacements ${\bf u} = u_z(z,t) {\bf e}_z$. L'équation du mouvement 
\begin{equation*}
\rho \ddpa{u_z}{t} = \dpa{\sigma_{zx}}{x} + \dpa{\sigma_{zy}}{y} + \dpa{\sigma_{zz}}{z}.
\end{equation*}
Du fait de l'absence de contraintes sur la surface latérale, seule la contrainte $\sigma_{zz}$ est non nulle, et donnée par :
\begin{equation*}
\sigma_{zz} = E e_{zz} = E \dpa{u_z}{z}.
\end{equation*}
On en déduit l'équation de propagation
\begin{equation*}
\ddpa{u_z}{t} - c^2 \ddpa{u_z}{z} = 0, \qquad c = \sqrt{\frac{E}{\rho}}.
\end{equation*}
Les ondes sont non dispersives. Leur célérité est inférieure à celle des ondes dans un milieu infini, car du fait de l'absence de contraintes transverses, la ``raideur'' du milieu est plus faible.

%.......................................................................
\paragraph{Ondes longitudinales dans les plaques minces.} Soit une plaque définissant un plan $(x,y)$, d'épaisseur $h$. On considère une onde ``plane'' se propageant dans la direction $x$. Les équations de la dynamique, jointes à la loi de Hooke, permettent d'établir que la composante $u_x$ du déplacement satisfait l'équation 
\begin{equation*}
\ddpa{u_x}{t} - c^2 \ddpa{u_x}{x} = 0, \qquad c = \sqrt{\frac{E}{\rho(1-\nu^2)}}.
\end{equation*}
La composante $u_y$, correspondant à des vibrations transverses à la direction de propagation mais dans le plan de la plaque, satisfait l'équation une équation semblable : 
\begin{equation*}
\ddpa{u_y}{t} - c^2 \ddpa{u_y}{x} = 0, \qquad c = \sqrt{\frac{E}{2\rho(1+\nu)}}.
\end{equation*}
La célérité est ici égale à celle $c_t$ des ondes transversales dans un milieu infini.

%-----------------------------------------------------------------------
\subsection{Ondes de flexion}

%.......................................................................
\paragraph{Ondes de flexion dans les plaques minces.} Soit $\zeta$ le  déplacement dans la direction $z$ normale à la plaque d'un élément de surface unité. L'équation de la dynamique dans la direction $z$ s'écrit
\begin{equation}
\rho h \ddpa{\zeta}{t} + \frac{h^3 E}{12(1-\nu^2)} \Delta^2 \zeta = 0
\end{equation}
où $\Delta = \partial_x^2 + \partial_y^2$ est l'opérateur Laplacien à deux dimensions. 

Cherchons une solution correspondant à une onde monochromatique de la forme
\begin{equation*}
\zeta \propto \e^{\im({\bf k.r} - \omega t)}. 
\end{equation*}
On en déduit la relation de dispersion
\begin{equation}
\frac{\omega}{k} = kh \sqrt{\frac{E}{12 \rho (1-\nu^2)}}.
\end{equation}
Ces ondes font intervenir le facteur $kh$, lié à l'échelle de longueur $h$ du problème. De ce fait, ces ondes sont {\em dispersives}, contrairement à toutes celles rencontrées jusqu'ici dans ce chapitre. Dans la limite $kh \rightarrow 0$ considérée ici (épaisseur faible), la célérité est très inférieure à celle des ondes précédentes. La relation ci-dessus n'est pas valable dans l'autre limite $kh \rightarrow \infty$, qui correspond non plus à une plaque mais à un milieu infini. 

%.......................................................................
\paragraph{Ondes de flexion dans les barres minces.} Soient $X(z,t)$ et $Y(z,t)$ les déplacements dans deux directions normales à la direction $z$ de la barre. Les équations de la dynamique s'écrivent, pour l'unité de longueur de la barre 
\begin{equation*}
\rho S \ddpa{X}{t} = EI_y \frac{\partial^4 X}{\partial z^4}, \qquad
\rho S \ddpa{Y}{t} = EI_x \frac{\partial^4 Y}{\partial z^4}
\end{equation*}
où $S$ est l'aire de la section de la barre et $I_x$ et $I_y$ sont ses moments d'inertie. \textcolor{blue}{donner la forme plus générale avec épaisseur variable : lames de xylophone = marimba.}

Cherchant des solutions de la forme $\e^{\im(kz - \omega t)}$, on obtient les relations de dispersion
\begin{equation}
\frac{\omega}{k} = k \sqrt{\frac{E I_y}{\rho S}}, \qquad 
\frac{\omega}{k} = k \sqrt{\frac{E I_x}{\rho S}}
\end{equation}
pour les vibrations dans les directions $x$ et $y$, respectivement. Ces ondes sont dispersives, comme pour les plaques.

%-----------------------------------------------------------------------
\subsection{Ondes de torsion dans les barres}

Les ondes de torsion s'étudient de la même façon que les ondes de flexion. On trouve que ces ondes ne sont pas dispersives.


