
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Cordes vibrantes}   \label{chap:cordes}

\nocite{Lig98} \nocite{BilKin00} \nocite{LanLif67}
%\begin{exercice} \label{exo:0201}
%\end{exercice}

%- $exp \pm kx$
%- raideur de la corde => terme d'ordre 4 => inharmonicité
%- barre inhomogène => onde piégée (wkb)
%- onde = milieu infini, mode = conditions aux limites
%- barre de marimba creusée au centre => raideur variable => partiels = harmoniques

%=======================================================================
\section{Equations du mouvement}

%.......................................................................
\paragraph{Equation fondamentale.} 


On considère une corde élastique  tendue entre deux points en $x=0$ et $x=L$ avec une tension $T>0$. \`A l'équilibre, la corde est rectiligne, sa masse par unité de longueur $\rho$ et sa tension $T$ sont uniformes. Déformons la corde dans une direction $y$ normale à $x$, et cherchons l'équation gouvernant la déformée $Y(x,t)$. 
Un élément de corde compris entre $x$ et $x+\delta x$ est soumis aux forces de tension $(-T{\bf e}_s)(x+\delta x)$ et $(-T{\bf e}_s)(x)$, où ${\bf e}_s$ est le vecteur tangent à la corde. La somme de ces deux forces, projetée sur la direction $y$, est
\begin{equation*}
T(x+\delta x) \sin(\alpha+\delta \alpha) - T(x) \sin(\alpha). 
\end{equation*}
Pour de petits angles $\alpha$, le terme dominant de cette force nette est $T(x) \delta \alpha$, qui au premier ordre peut aussi s'écrire, avec $\alpha = \partial Y / \partial x$,
\begin{equation*}
T \ddpa{Y}{x} \delta x.
\end{equation*}
Cette force engendre une accélération $\partial^2 Y / \partial t^2$ de l'élément de corde, dont la masse $\rho \delta s$ se réduit au premier ordre à $\rho \delta x$. Le principe fondamental de la dynamique s'écrit donc
\begin{equation}
\ddpa{Y}{t} - c^2 \ddpa{Y}{x} = 0, \qquad c^2 = \frac{T}{\rho}.
\label{eq:eqcorde}
\end{equation}
Cette équation, linéaire à coefficients constants, est l'équation fondamentale des cordes vibrantes. Elle fait intervenir un paramètre $c= \sqrt{T/\rho}$, homogène à une vitesse. Notons que les variations de tension et de masse volumique dans la corde n'interviennent pas, elles sont négligeables. \`A titre d'exemple, pour la corde {\em ré} en acier d'un violon, de masse volumique 7000 kg\,m$^{-3}$ et de diamètre 0,5 mm, une tension de 55 N donne une vitesse $c \approx 200$ m\,s$^{-1}$. De nombreux phénomènes vibratoires sont régis par la même équation. Le déplacement $Y$ s'effectuant dans une direction normale à la direction $x$ de propagation, l'onde est dite {\em transverse}.

%.......................................................................
%\paragraph{\'Energie cinétique et énergie potentielle.}

%=======================================================================
\section{Modes propres d'une corde de longueur finie}

%-----------------------------------------------------------------------
%\subsection{...} 

%.......................................................................
\paragraph{Modes propres de vibration.}

Pour une corde de longueur finie $L$, l'échelle de longueur $L$ est indépendante de l'échelle de temps $L/c$. Cette indépendance des échelles autorise à chercher solution sous la forme à variables séparées
\begin{equation}
Y(x, t) = X(x) \Theta(t)
\end{equation}
avec les conditions aux limites
\begin{equation}
Y(0, t) = Y(L, t) = 0 \quad {\rm pour} \quad t \ge 0.
\end{equation}
Substituant la forme ci-dessus dans l'équation (\ref{eq:eqcorde}), il vient
\begin{equation*}
\frac{X''}{X} = \frac{1}{c^2} \frac{\Theta''}{\Theta} = - \kappa^2
\end{equation*}
où la dernière égalité correspond au fait que si une fonction de $x$ est égale à une fonction de $t$, l'égalité ne peut correspondre qu'à une constante. On peut vérifier que si cette constante est positive, $X$ est la somme de deux exponentielles et les conditions aux limites ne peuvent être satisfaites, d'où le signe ``$-$''. La solution pour $X$ est donc de la forme
\begin{equation*}
X = A \sin \kappa x  + B \cos \kappa x.
\end{equation*}
Les conditions $X(0) = 0$ et $X(L) = 0$ imposent respectivement $B=0$ et $A \sin \kappa L = 0$, d'où
\begin{equation}
\kappa = \kappa_n := \frac{n \pi}{L}, \qquad n=1, 2, 3, ...
\end{equation}
(Il suffit de considérer les valeurs positives de $n$, les valeurs négatives correspondant à la constante changée de signe.)

La fonction $\Theta$ est alors une combinaison linéaire de $\sin \kappa c t$ et $\cos \kappa c t$, et les solutions pour $Y(x,t)$ sont finalement de la forme
\begin{equation}
Y_n(x,t) = \sin \kappa_n x \left( a_n \sin \kappa_n c t + b_n \cos \kappa_n c t \right), \qquad \kappa_n = \frac{n \pi}{L}.
\end{equation}
Chacune de ces solutions, appelées {\em modes propres}, correspond à une onde stationnaire d'enveloppe $\sin \kappa_n x$. \`A chaque mode est associé une nombre d'onde $\kappa_n$ et une pulsation propre $\omega_n = \kappa_n c$. Les points d'amplitude nulle sont les n{\oe}uds de vibration, tandis que les points d'amplitude maximale sont les ventres. $Y_1$ est le mode fondamental, de longueur d'onde $2\pi/\kappa_1 = 2L$. $Y_2$ est le premier harmonique, de longueur d'onde $2\pi/\kappa_2 = L$, et ainsi de suite. 

%.......................................................................
\paragraph{Solution générale comme somme de modes propres.}

La solution générale est une superposition de modes propres, de la forme
\begin{equation}
Y(x,t) = \sum_{n=1}^{\infty} \sin \kappa_n x \left( a_n \sin \kappa_n c t + b_n \cos \kappa_n c t \right).
\label{eq:solgencorde}
\end{equation}

%.......................................................................
\paragraph{Détermination des constantes $a_n$ et $b_n$.} Les constantes d'intégration $a_n$ et $b_n$ sont déterminées par les conditions initiales
\begin{equation}
Y(x, 0) = Y_0(x), \qquad \dpa{Y}{t}(x, 0) = v_0(x).
\end{equation}
Le problème différentiel étant du 2e ordre en temps, on doit en effet introduire deux conditions initiales. Substituant la solution générale (\ref{eq:solgencorde}) dans ces conditions initiales, il vient
\begin{equation}
Y_0(x) = \sum_{n=1}^{\infty}            b_n \sin \kappa_n x, \qquad 
v_0(x) = \sum_{n=1}^{\infty} \kappa_n c a_n \cos \kappa_n x
\label{eq:cinitcorde}
\end{equation}
qui sont les développements en série de Fourier de $Y_0$ et $v_0$. Les coefficients $a_n$ et $b_n$ peuvent être déterminés en exploitant l'orthogonalité des fonctions de base :
\begin{equation*}
\int_0^L \sin \kappa_n x \sin \kappa_m x = \frac{1}{2} L \delta_{nm}.
\end{equation*}
On obtient ainsi
\begin{equation}
a_n = \frac{2}{\kappa_ncL} \int_0^L v_0(x) \sin \kappa_n x \dd x, \qquad 
b_n = \frac{2}{L}          \int_0^L Y_0(x) \sin \kappa_n x \dd x.
\label{eq:cinitcorde}
\end{equation}

%.......................................................................
\paragraph{Exemple : corde pincée lâchée sans vitesse initiale.} On considère la condition initiale
\begin{eqnarray*}
Y_0(x) &=& (a/\ell) x  \quad {\rm pour} \quad 0 \le x \le \ell \\
Y_0(x) &=& a(L-x)/(L-\ell) \quad {\rm pour} \quad \ell \le x \le L
\end{eqnarray*}
et $v_0(x) = 0$, qui représente une corde pincée lâchée sans vitesse initiale. On en déduit immédiatement $a_n = 0$, et, après intégration par parties, 
\begin{equation*}
b_n = \frac{2a}{\ell (L-\ell) \kappa_n^2} \sin \kappa_n \ell 
    = \frac{2a}{\pi^2 n^2} \frac{L^2}{\ell (L-\ell)} \sin \kappa_n \ell.
\end{equation*}
On remarque que l'amplitude des harmoniques décroît avec leur rang comme $1/n^2$. Si $\ell = L/2$, la condition initiale est symétrique et seuls interviennent dans la solution les harmoniques impairs ($n = 1, 3, 5, ...$), eux-mêmes symétriques. 

%.......................................................................
%\paragraph{\'Equipartition des énergies cinétique et potentielle.} \`A rédiger.


%=======================================================================
\section{Solution de d'Alembert}

%.......................................................................
\paragraph{Solution de d'Alembert.} On considère une corde de grande longueur, où la région perturbée n'occupe qu'une partie de la corde, hors de laquelle $Y(x,t)$ est très petit ou nul. Introduisons le changement de variable 
\begin{equation*}
\xi = x + ct, \qquad \eta = x - ct.
\end{equation*}
Alors 
\begin{eqnarray*}
\ddpa{Y}{x} &=& \ddpa{Y}{\xi} + 2 \frac{\partial^2 Y}{\partial \xi \partial \eta} + \ddpa{Y}{\eta}, \\
\ddpa{Y}{t} &=& c^2 \left( \ddpa{Y}{\xi} - 2 \frac{\partial^2 Y}{\partial \xi \partial \eta} + \ddpa{Y}{\eta} \right).
\end{eqnarray*}
L'équation (\ref{eq:eqcorde}) s'écrit alors
\begin{equation*}
\frac{\partial^2 Y}{\partial \xi \partial \eta} = 0.
\end{equation*}
Une première intégration donne
\begin{equation*}
\dpa{Y}{\eta} = f(\eta), 
\end{equation*}
et la deuxième intégration, 
\begin{equation*}
Y = F(\eta) + G(\xi), \qquad {\rm avec } \quad F(\eta) = \int^\eta f(\eta) \dd \eta.
\end{equation*}
Ainsi, $Y(x,t)$ apparaît comme la somme de deux fonctions arbitraires d'une seule variable :
\begin{equation}
Y(x,t) = F(x-ct) + G(x+ct).
\end{equation}

Que représentent les fonctions $F$ et $G$ ? Considérons tout d'abord le cas $G=0$, pour lequel $Y = F(x-ct)$. \`A $t=0$, $Y = Y_0 = F(x)$, et $F(x)$ représente donc la valeur initiale de $Y$. \`A un instant $t$ ultérieur, on retrouve le même profil d'onde translaté vers la droite d'une longueur $ct$ : en effet, à l'abscisse $x+ct$ à l'instant $t$, on a 
\begin{equation*}
Y(x+ct, t) = F[(x+ct) - ct] = F(x) = Y(x,0).
\end{equation*}
La fonction $F(x-ct)$ s'interprète donc comme une onde se propageant vers la droite ($x$ croissant), sans se déformer, à la célérité $c$. De même, $G(x+ct)$ correspond à une onde se propageant vers la gauche, sans se déformer, à la célérité $c$. 

%.......................................................................
\paragraph{Conditions initiales.} La prise en compte des conditions initiales permet de déterminer complètement les fonctions $F$ et $G$. On a tout d'abord
\begin{equation}
Y(x,0) = F(x) + G(x) = Y_0(x).
\label{eq:cinit1corde}
\end{equation}
Ensuite, du fait que
\begin{equation*}
\dpa{Y}{t} = -c F'(x-ct) + c G'(x+ct), 
\end{equation*}
on a
\begin{equation*}
\dpa{Y}{t}(x,0) = -c F'(x) + c G'(x) = v_0(x),
\end{equation*}
qui s'intègre pour donner
\begin{equation}
-F(x) + G(x) = \frac{1}{c} \int_a^x v_0(s) \dd s
\label{eq:cinit2corde}
\end{equation}
où $a$ est une constante arbitraire. Des équations (\ref{eq:cinit1corde}) et (\ref{eq:cinit2corde}) on déduit
\begin{eqnarray*}
F(x) &=& \frac{1}{2}Y_0(x) - \frac{1}{2c} \int_a^x v_0(s) \dd s, \\
G(x) &=& \frac{1}{2}Y_0(x) + \frac{1}{2c} \int_a^x v_0(s) \dd s.
\end{eqnarray*}
d'où la solution finale, dite {\em solution de d'Alembert} : 
\begin{equation}
Y(x,t) = \frac{1}{2} \left( Y_0(x-ct) + Y_0(x+ct) \right) 
+ \frac{1}{2c} \int_{x-ct}^{x+ct} v_0(s) \dd s.
\label{eq:dAlembertcorde}
\end{equation}

%.......................................................................
\paragraph{Exemple 1.} Considérons la condition initiale sinusoïdale
\begin{equation}
Y_0(x) = a \sin kx, \qquad v_0(x) = 0.
\end{equation}
Substituant ces conditions initiales dans l'expression (\ref{eq:dAlembertcorde}), la solution s'écrit
\begin{eqnarray*}
Y(x,t) &=& \frac{a}{2} \left( \sin k(x+ct) + \sin k(x-ct) \right) \\
       &=& a \sin kx \cos kct.
\end{eqnarray*}
La solution est donc une onde stationnaire, résultat de la superposition de deux ondes de même amplitude se propageant en sens inverse. 

%.......................................................................
\paragraph{Exemple 2.} Considérons la condition initiale correspondant à la ``fonction chapeau'' :
\begin{equation}
Y_0(x) = H(x+a) - H(x-a), \qquad v_0(x) = 0,
\end{equation}
où $H(x)$ est la fonction de Heaviside. La discontinuité de $Y$ en $x=\pm a$ n'est pas physiquement satisfaisante, mais la solution du problème n'en est pas moins bien définie et instructive. Substituant ces conditions initiales dans l'expression (\ref{eq:dAlembertcorde}), la solution s'écrit
\begin{equation*}
Y(x,t) = \frac{1}{2} \left( H(x+ct+a) - H(x+ct-a) \right) 
       + \frac{1}{2} \left( H(x-ct+a) - H(x-ct-a) \right).
\end{equation*}
Le premier terme correspond à une fonction chapeau se propageant vers la gauche, et le second terme, à la même fonction chapeau se propageant vers la droite. Les deux ``chapeaux'' se superposent pour $0<t<a/c$, et sont disjoints pour $t>a/c$.

%.......................................................................
\paragraph{Exemple 3 : corde pincée lâchée sans vitesse initiale.} Cet exemple, déjà résolu par superposition de modes propres, sera traité en TD avec la méthode de d'Alembert. 


%=======================================================================
\section{Réflexion et transmission par une discontinuité}

%-----------------------------------------------------------------------
\subsection{Conditions de passage} 

On considère une corde de densité $\rho_1$ pour $x<0$ et $\rho_2$ pour $x>0$. De part et d'autre de la discontinuité en $x=0$, la vitesse de l'onde transverse est $c_1 = \sqrt{T/\rho_1}$ et $c_2 = \sqrt{T/\rho_2}$. Une onde incidente $F_i(x-c_1 t)$ arrivant de la gauche $(x<0)$est partiellement transmise à travers la discontinuité, et partiellement réfléchie : 
\begin{eqnarray*}
Y(x,t) &=& F_i(x-c_1 t) + F_r(x+c_1 t) \quad {\rm pour} \quad x<0 \\
Y(x,t) &=& F_t(x-c_2 t)                \quad {\rm pour} \quad x>0.
\end{eqnarray*}
Les ondes réfléchie et transmise, $F_r$ et $F_t$, sont déterminées par deux conditions de continuité en $x=0$. D'une part, le déplacement transverse est continu :
\begin{equation}
F_i(-c_1 t) + F_r(c_1 t) = F_t(-c_2 t).
\label{eq:contincorde1}
\end{equation}
D'autre part, la composante transverse de la tension, $T \partial Y / \partial x$, doit être continue (actions réciproques). Compte tenu de la continuité de $T$, cette condition impose la continuité de la pente $\partial Y / \partial x$, soit :
\begin{equation*}
F_i'(-c_1 t) + F_r'(c_1 t) = F_t'(-c_2 t).
\end{equation*}
L'intégration de cette équation par rapport au temps donne
\begin{equation}
- \frac{1}{c_1} F_i(-c_1 t) + \frac{1}{c_1} F_r(c_1 t) = 
- \frac{1}{c_2} F_t'(-c_2 t).
\label{eq:contincorde2}
\end{equation}
Des conditions de continuité (\ref{eq:contincorde1}) et  (\ref{eq:contincorde2}), on déduit
\begin{equation*}
F_t(-c_2 t) = \frac{2c_2}{c_1+c_2} F_i(-c_1 t)
\end{equation*}
d'où, après la substitution $t \rightarrow t-x/c_2$, l'onde transmise :
\begin{equation}
F_t(x-c_2 t) = \frac{2 c_2}{c_1+c_2} F_i\left(\frac{c_1}{c_2}(x-c_2t)\right)
\label{eq:ft_corde}
\end{equation}
et, de la même façon, l'onde réfléchie :
\begin{equation}
F_r(x+c_1 t) = - \frac{c_1-c_2}{c_1+c_2} F_i\left(-(x+c_1t)\right).
\label{eq:fr_corde}
\end{equation}

Trois cas particuliers sont à noter :
\begin{itemize}
\item $\rho_1 = \rho_2$, soit $c_2 = c_1$ : l'onde est intégralement transmise sans altération, il n'y a pas d'onde réfléchie.
\item $\rho_2 \gg \rho_1$, soit $c_2 \ll c_1$ : l'onde incidente est essentiellement réfléchie, avec changement de signe, et l'onde transmise est très faible. La partie droite de la corde ($x>0$) est immobile, elle se comporte comme un mur réfléchissant.
\item $\rho_2 \ll \rho_1$, soit $c_2 \gg c_1$ : l'onde réfléchie est de même signe et de même amplitude que l'onde incidente, tandis que l'amplitude de l'onde transmise est deux fois celle de l'onde incidente.  L'énergie transmise est faible, mais du fait de la légèreté de la corde, cette faible énergie suffit à exciter une grande amplitude.
\end{itemize}

%.......................................................................
\paragraph{Condition non réfléchissante à l'extrémité d'une corde.} 

La simulation numérique de la propagation d'une onde ne peut être réalisée que dans un domaine de taille finie. Or, on peut souhaiter simuler un domaine infini, ou ouvert, et se pose alors la question de la condition à imposer sur la frontière, disons en $x=L$, du domaine fini. Cette condition doit garantir qu'une onde incidente est intégralement transmise vers l'extérieur, sans réflexion parasite. En d'autres termes, il s'agit d'imposer une {\em condition non réfléchissante}, ou {\em condition de radiation} (Sommerfeld). Cette condition peut être déterminée de la façon suivante. Soit une onde $Y(x,t)$ correspondant à la superposition d'une onde incidente et d'une onde réfléchie :
\begin{equation*}
Y = F(x-ct) + G(x+ct).
\end{equation*}
La condition de non-réflexion est $G=0$, mais on ne peut en général pas imposer directement cette condition, car on ne sait en général pas distinguer, dans la solution $Y(x,t)$, les contributions $F$ et $G$. Cherchons une condition sur $Y$ qui réalise $G=0$. Il suffit en fait de chercher une condition qui réalise $G=G_0$ constante, c'est-à-dire $G'=0$, car toute constante $G_0$ peut être incorporée dans $F$ par la redéfinition $F + G_0 \rightarrow F$. Dérivons donc $Y$ par rapport à $x$ et $t$ :
\begin{eqnarray*}
\dpa{Y}{x} &=& F' + G' \\
\dpa{Y}{t} &=& -cF' + cG'
\end{eqnarray*}
d'où on déduit
\begin{eqnarray*}
F' &=& \frac{1}{2c} \left( c \dpa{Y}{x} - \dpa{Y}{t} \right) \\
G' &=& \frac{1}{2c} \left( c \dpa{Y}{x} + \dpa{Y}{t} \right).
\end{eqnarray*}
La condition de radiation $G=0$ impose en particulier $G'=0$ au bord du domaine, disons en $x=L$, laquelle condition s'écrit
\begin{equation*}
c \dpa{Y}{x} + \dpa{Y}{t} = 0 \qquad {\rm en } \quad x=L.
\end{equation*}
Cette expression ne fait intervenir que les dérivées de $Y(x,t)$ en $x=L$, lesquelles sont en général bien définies. 

%-----------------------------------------------------------------------
\subsection{Flux d'énergie transmise et réfléchie} 

Les résultats ci-dessous peuvent être éclairés par la détermination des flux d'énergie à la discontinuité. Le flux d'énergie incident est la puissance de la tension $(-T{\bf e}_s)$ exercée par la partie gauche de la corde sur la partie droite, soit :
\begin{equation*}
{\cal P}_i = -T ({\bf e}_s.{\bf e}_y) v_i(0, t) = -T \dpa{Y_i}{x} \dpa{Y_i}{t} = c_1 T F_i'^2. 
\end{equation*}
(La composante de la tension dans la direction $x$ ne travaille pas.) Le flux d'énergie transmis s'écrit de même
\begin{equation*}
{\cal P}_t = -T ({\bf e}_s.{\bf e}_y) v_t(0, t) = - T \dpa{Y_t}{x} \dpa{Y_t}{t} = c_2 T F_t'^2, 
\end{equation*}
et peut s'exprimer en fonction du flux incident en substituant $F_t'$ par son expression en fonction de $F_i'$ obtenue à partir de (\ref{eq:ft_corde}) :
\begin{equation}
{\cal P}_t = \frac{4 c_1 c_2}{(c_1+c_2)^2} {\cal P}_i. 
\label{eq:Pt_corde}
\end{equation}
Ce résultat montre en particulier que si $c_2 = c_1$, la puissance incidente est intégralement transmise, et que si $c_2 \ll c_1$ ($\rho_2 \gg \rho_1$)  la puissance transmise, ${\cal P}_t \sim 4 (c_2/c_1) {\cal P}_i$, est très faible. 

De même, pour la puissance réfléchie (où la force à considérer est $+ T {\bf e}_s$) :
\begin{equation*}
{\cal P}_r = + T ({\bf e}_s.{\bf e}_y) v_r(0, t) = T \dpa{Y_r}{x} \dpa{Y_r}{t} = c_1 T F_r'^2, 
\end{equation*}
soit
\begin{equation}
{\cal P}_r = \frac{(c_2-c_1)^2}{(c_1+c_2)^2} {\cal P}_i. 
\label{eq:Pr_corde}
\end{equation}
Cette puissance est bien nulle si $c_2=c_1$, et égale à ${\cal P}_i$ si $c_2/c_1$ est très grand ou très petit.

Ces résultats vérifient en particulier que la puissance incidente se retrouve dans la somme des puissances transmise et réfléchie :
\begin{equation}
{\cal P}_i = {\cal P}_t + {\cal P}_r. 
\end{equation}

%-----------------------------------------------------------------------
\subsection{Admittance et impédance} 

L'admittance d'un milieu, ou son inverse l'impédance, est une notion très générale en physique, qui traduit l'aptitude du milieu à répondre à une sollicitation, {\em i.e.} transmettre un ``flux'' lorsqu'il est soumis à une ``force'' (au sens de la thermodynamique). Par exemple, pour un fil électrique soumis à une différence de potentiel $U$ et parcouru par un courant $I$, l'impédance (ici une simple résistance) est $Z=U/I$. Pour une corde vibrante, l'impédance $Z$ est définie comme le rapport de la force transverse et de la vitesse transverse, soit :
\begin{equation}
Z = \frac{- T \dpa{Y}{x}}{\dpa{Y}{t}}.
\end{equation}
On a alors, pour une onde se propageant vers la droite ($Y = F(x-ct)$) :
\begin{equation}
Z = \frac{T}{c} = \sqrt{\rho T} = \rho c.
\end{equation}
L'impédance est liée au flux d'énergie mécanique ${\cal P}$ (puissance de la force exercée par la partie gauche sur la partie droite de la corde) par la relation 
\begin{equation*}
{\cal P} = -T \dpa{Y_i}{x} \dpa{Y_i}{t} = Z \left(\dpa{Y}{t}\right)^2.
\end{equation*}

Exprimons, à partir de (\ref{eq:Pt_corde}), le flux ${\cal P}_t$ d'énergie transmise en fonction du flux incident ${\cal P}_i$, en substituant aux célérités $c$ les impédances $Z = T/c$. Il vient :
\begin{equation}
{\cal P}_t = 
\frac{4 Z_1/Z_2}{(1+Z_1/Z_2)^2} {\cal P}_i =
\frac{4 Z_2/Z_1}{(1+Z_2/Z_1)^2} {\cal P}_i.
\label{eq:Pt_Pi_corde} 
\end{equation}
Notons l'invariance de cette expression dans la substitution $Z_1/Z_2 \rightarrow Z_2/Z_1$. Le tracé du rapport des puissances ${\cal P}_t/{\cal P}_i$ en fonction du rapport des impédances $Z_2/Z_1$ montre que la puissance transmise est très faible si les impédances sont très différentes, et égale à la puissance incidente lorsque les impédances sont égales. Dans ce dernier cas, les impédances sont dites {\em accordées}). La relation (\ref{eq:Pt_Pi_corde}) est tout à fait générale.


%-----------------------------------------------------------------------
\textcolor{blue}{
\subsection{Deux discontinuités, condition de transmission complète}
} 

\textcolor{blue}{
Peut-on, en interposant entre deux milieux semi-infinis un troisième milieu (densité $\rho_2$, célérité $c_2$, épaisseur $L$), assurer une transmission complète de l'onde incidente ?
L'écriture des $2\times2$ conditions aux limites montre que c'est possible, pourvu que soit satisfaite la condition
\begin{equation}
\cos\left(\frac{2\omega L}{c_2}\right) = 
\left(\frac{c_3-c_2}{c_3+c_2}\right) 
\left(\frac{c_1+c_2}{c_1-c_2}\right)
\end{equation}
Cette condition admet une solution particulière pour 
\begin{equation*}
c_2 = \sqrt{c_1 c_3},
\end{equation*}
soit $\rho_2 = \sqrt{\rho_1 \rho_3}$ puisque la tension $T$ est partout la même. Le membre de droite de l'équation ci-dessus vaut alors $-1$, et l'équation est satisfaite pour la longueur 
\begin{equation*}
L = \frac{\pi}{2} \frac{c_2}{\omega} = \frac{\lambda_2}{4}. 
\end{equation*}
Le résultat est particulièrement simple : l'épaisseur du milieu intermédiaire doit être égal à un quart de longueur d'onde. On retrouve pour d'autres types d'ondes cette possibilité de transmission intégrale à la traversée d'une discontinuité : en acoustique, électromagnétisme, optique. Applications nombreuses : revêtements anti-réflexion radar sur les sous-marins, couches minces sur les lentilles des objectifs photo, jonctions électriques, gels pour l'échographie. Concept sous-jacent : accord des impédances à une jonction, discuté plus loin. 
}

